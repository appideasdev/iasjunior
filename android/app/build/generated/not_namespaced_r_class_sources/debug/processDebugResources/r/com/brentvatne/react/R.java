/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * gradle plugin from the resource data it found. It
 * should not be modified by hand.
 */
package com.brentvatne.react;

public final class R {
    private R() {}

    public static final class attr {
        private attr() {}

        public static final int ad_marker_color = 0x7f020025;
        public static final int ad_marker_width = 0x7f020026;
        public static final int auto_show = 0x7f020036;
        public static final int bar_height = 0x7f02003e;
        public static final int buffered_color = 0x7f020040;
        public static final int controller_layout_id = 0x7f02006a;
        public static final int default_artwork = 0x7f02006e;
        public static final int fastforward_increment = 0x7f020083;
        public static final int hide_during_ads = 0x7f020094;
        public static final int hide_on_touch = 0x7f020095;
        public static final int played_ad_marker_color = 0x7f0200cc;
        public static final int played_color = 0x7f0200cd;
        public static final int player_layout_id = 0x7f0200ce;
        public static final int repeat_toggle_modes = 0x7f0200df;
        public static final int resize_mode = 0x7f0200e0;
        public static final int rewind_increment = 0x7f0200e3;
        public static final int scrubber_color = 0x7f0200f7;
        public static final int scrubber_disabled_size = 0x7f0200f8;
        public static final int scrubber_dragged_size = 0x7f0200f9;
        public static final int scrubber_drawable = 0x7f0200fa;
        public static final int scrubber_enabled_size = 0x7f0200fb;
        public static final int show_shuffle_button = 0x7f020106;
        public static final int show_timeout = 0x7f020107;
        public static final int shutter_background_color = 0x7f020108;
        public static final int surface_type = 0x7f020118;
        public static final int touch_target_height = 0x7f020141;
        public static final int unplayed_color = 0x7f020146;
        public static final int use_artwork = 0x7f020147;
        public static final int use_controller = 0x7f020148;
    }
    public static final class color {
        private color() {}

        public static final int exo_edit_mode_background_color = 0x7f040038;
    }
    public static final class dimen {
        private dimen() {}

        public static final int exo_media_button_height = 0x7f050054;
        public static final int exo_media_button_width = 0x7f050055;
    }
    public static final class drawable {
        private drawable() {}

        public static final int exo_controls_fastforward = 0x7f06006b;
        public static final int exo_controls_fullscreen_enter = 0x7f06006c;
        public static final int exo_controls_fullscreen_exit = 0x7f06006d;
        public static final int exo_controls_next = 0x7f06006e;
        public static final int exo_controls_pause = 0x7f06006f;
        public static final int exo_controls_play = 0x7f060070;
        public static final int exo_controls_previous = 0x7f060071;
        public static final int exo_controls_repeat_all = 0x7f060072;
        public static final int exo_controls_repeat_off = 0x7f060073;
        public static final int exo_controls_repeat_one = 0x7f060074;
        public static final int exo_controls_rewind = 0x7f060075;
        public static final int exo_controls_shuffle = 0x7f060076;
        public static final int exo_edit_mode_logo = 0x7f060077;
    }
    public static final class id {
        private id() {}

        public static final int exo_artwork = 0x7f07003f;
        public static final int exo_content_frame = 0x7f070040;
        public static final int exo_controller = 0x7f070041;
        public static final int exo_controller_placeholder = 0x7f070042;
        public static final int exo_duration = 0x7f070043;
        public static final int exo_ffwd = 0x7f070044;
        public static final int exo_next = 0x7f070045;
        public static final int exo_overlay = 0x7f070046;
        public static final int exo_pause = 0x7f070047;
        public static final int exo_play = 0x7f070048;
        public static final int exo_position = 0x7f070049;
        public static final int exo_prev = 0x7f07004a;
        public static final int exo_progress = 0x7f07004b;
        public static final int exo_repeat_toggle = 0x7f07004c;
        public static final int exo_rew = 0x7f07004d;
        public static final int exo_shuffle = 0x7f07004e;
        public static final int exo_shutter = 0x7f07004f;
        public static final int exo_subtitles = 0x7f070050;
        public static final int fill = 0x7f070053;
        public static final int fit = 0x7f070056;
        public static final int fixed_height = 0x7f07005c;
        public static final int fixed_width = 0x7f07005d;
        public static final int none = 0x7f070076;
        public static final int surface_view = 0x7f0700ac;
        public static final int texture_view = 0x7f0700b5;
        public static final int zoom = 0x7f0700c4;
    }
    public static final class layout {
        private layout() {}

        public static final int exo_playback_control_view = 0x7f09001d;
        public static final int exo_player_control_view = 0x7f09001e;
        public static final int exo_player_view = 0x7f09001f;
        public static final int exo_simple_player_view = 0x7f090020;
    }
    public static final class string {
        private string() {}

        public static final int error_instantiating_decoder = 0x7f0b0053;
        public static final int error_no_decoder = 0x7f0b0054;
        public static final int error_no_secure_decoder = 0x7f0b0055;
        public static final int error_querying_decoders = 0x7f0b0056;
        public static final int exo_controls_fastforward_description = 0x7f0b0057;
        public static final int exo_controls_next_description = 0x7f0b0058;
        public static final int exo_controls_pause_description = 0x7f0b0059;
        public static final int exo_controls_play_description = 0x7f0b005a;
        public static final int exo_controls_previous_description = 0x7f0b005b;
        public static final int exo_controls_repeat_all_description = 0x7f0b005c;
        public static final int exo_controls_repeat_off_description = 0x7f0b005d;
        public static final int exo_controls_repeat_one_description = 0x7f0b005e;
        public static final int exo_controls_rewind_description = 0x7f0b005f;
        public static final int exo_controls_shuffle_description = 0x7f0b0060;
        public static final int exo_controls_stop_description = 0x7f0b0061;
        public static final int unrecognized_media_format = 0x7f0b006a;
    }
    public static final class style {
        private style() {}

        public static final int ExoMediaButton = 0x7f0c00a9;
        public static final int ExoMediaButton_FastForward = 0x7f0c00aa;
        public static final int ExoMediaButton_Next = 0x7f0c00ab;
        public static final int ExoMediaButton_Pause = 0x7f0c00ac;
        public static final int ExoMediaButton_Play = 0x7f0c00ad;
        public static final int ExoMediaButton_Previous = 0x7f0c00ae;
        public static final int ExoMediaButton_Rewind = 0x7f0c00af;
        public static final int ExoMediaButton_Shuffle = 0x7f0c00b0;
    }
    public static final class styleable {
        private styleable() {}

        public static final int[] AspectRatioFrameLayout = { 0x7f0200e0 };
        public static final int AspectRatioFrameLayout_resize_mode = 0;
        public static final int[] DefaultTimeBar = { 0x7f020025, 0x7f020026, 0x7f02003e, 0x7f020040, 0x7f0200cc, 0x7f0200cd, 0x7f0200f7, 0x7f0200f8, 0x7f0200f9, 0x7f0200fa, 0x7f0200fb, 0x7f020141, 0x7f020146 };
        public static final int DefaultTimeBar_ad_marker_color = 0;
        public static final int DefaultTimeBar_ad_marker_width = 1;
        public static final int DefaultTimeBar_bar_height = 2;
        public static final int DefaultTimeBar_buffered_color = 3;
        public static final int DefaultTimeBar_played_ad_marker_color = 4;
        public static final int DefaultTimeBar_played_color = 5;
        public static final int DefaultTimeBar_scrubber_color = 6;
        public static final int DefaultTimeBar_scrubber_disabled_size = 7;
        public static final int DefaultTimeBar_scrubber_dragged_size = 8;
        public static final int DefaultTimeBar_scrubber_drawable = 9;
        public static final int DefaultTimeBar_scrubber_enabled_size = 10;
        public static final int DefaultTimeBar_touch_target_height = 11;
        public static final int DefaultTimeBar_unplayed_color = 12;
        public static final int[] PlayerControlView = { 0x7f02006a, 0x7f020083, 0x7f0200df, 0x7f0200e3, 0x7f020106, 0x7f020107 };
        public static final int PlayerControlView_controller_layout_id = 0;
        public static final int PlayerControlView_fastforward_increment = 1;
        public static final int PlayerControlView_repeat_toggle_modes = 2;
        public static final int PlayerControlView_rewind_increment = 3;
        public static final int PlayerControlView_show_shuffle_button = 4;
        public static final int PlayerControlView_show_timeout = 5;
        public static final int[] PlayerView = { 0x7f020036, 0x7f02006a, 0x7f02006e, 0x7f020083, 0x7f020094, 0x7f020095, 0x7f0200ce, 0x7f0200df, 0x7f0200e0, 0x7f0200e3, 0x7f020106, 0x7f020107, 0x7f020108, 0x7f020118, 0x7f020147, 0x7f020148 };
        public static final int PlayerView_auto_show = 0;
        public static final int PlayerView_controller_layout_id = 1;
        public static final int PlayerView_default_artwork = 2;
        public static final int PlayerView_fastforward_increment = 3;
        public static final int PlayerView_hide_during_ads = 4;
        public static final int PlayerView_hide_on_touch = 5;
        public static final int PlayerView_player_layout_id = 6;
        public static final int PlayerView_repeat_toggle_modes = 7;
        public static final int PlayerView_resize_mode = 8;
        public static final int PlayerView_rewind_increment = 9;
        public static final int PlayerView_show_shuffle_button = 10;
        public static final int PlayerView_show_timeout = 11;
        public static final int PlayerView_shutter_background_color = 12;
        public static final int PlayerView_surface_type = 13;
        public static final int PlayerView_use_artwork = 14;
        public static final int PlayerView_use_controller = 15;
    }
}
