package com.iasjunior;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

public class SplashActivity extends AppCompatActivity {

    private final int SPLASH_DISPLAY_LENGTH = 4000;
    SharedPreferences settings;
    boolean firstRun;
    //SessionManager session;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);

        StartAnimations();

    }

    /**
     * *********** *   Splash Time Loading  Method   ***************
     */

    private void StartAnimations() {
        new android.os.Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent_mainpage = new Intent(SplashActivity.this, MainActivity.class);
                intent_mainpage.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent_mainpage);
                finish();
            }
        }, SPLASH_DISPLAY_LENGTH);
    }


}
