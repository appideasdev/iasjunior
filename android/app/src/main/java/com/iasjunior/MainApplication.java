package com.iasjunior;

import android.app.Application;

import com.facebook.react.ReactApplication;
import com.reactnativedocumentpicker.ReactNativeDocumentPicker;
import com.dieam.reactnativepushnotification.ReactNativePushNotificationPackage;
import com.christopherdro.htmltopdf.RNHTMLtoPDFPackage;
import com.RNFetchBlob.RNFetchBlobPackage;
import com.inprogress.reactnativeyoutube.ReactNativeYouTube;
import com.brentvatne.react.ReactVideoPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import org.wonday.pdf.RCTPdfView;
import com.imagepicker.ImagePickerPackage;
import com.horcrux.svg.SvgPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;

import java.util.Arrays;
import java.util.List;


public class MainApplication extends Application implements ReactApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
          new MainReactPackage(),
            new ReactNativeDocumentPicker(),
            new ReactNativePushNotificationPackage(),
            new RNHTMLtoPDFPackage(),
            new RNFetchBlobPackage(),
            new ReactNativeYouTube(),
            new ReactVideoPackage(),
            new VectorIconsPackage(),
            new RCTPdfView(),
            new ImagePickerPackage(),
            new RNHTMLtoPDFPackage(),
            new RNHTMLtoPDFPackage(),
            new RNFetchBlobPackage(),
            new ReactNativeYouTube(),
            new VectorIconsPackage(),
            new RCTPdfView(),
            new ImagePickerPackage(),
            new RNFetchBlobPackage(),
            new ReactNativeYouTube(),
            new VectorIconsPackage(),
            new RCTPdfView(),
            new ImagePickerPackage(),
            new ReactNativeYouTube(),
            new RNFetchBlobPackage(),
            new VectorIconsPackage(),
            new RCTPdfView(),
            new ImagePickerPackage(),
            new RCTPdfView(),
            new RNFetchBlobPackage(),
            new VectorIconsPackage(),
            new ImagePickerPackage(),
            //new ReactNativeYouTube(),
            new SvgPackage(),
            new ImagePickerPackage(),
            new VectorIconsPackage()
      );
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    SoLoader.init(this, /* native exopackage */ false);
  }

}
