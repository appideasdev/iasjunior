 /**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
  Platform, 
  StyleSheet, 
  Text, 
  View,
  StatusBar,
  ImageBackground,
  ScrollView,
  Image,
  TextInput,
  TouchableOpacity,
  Alert,
  AsyncStorage,
  PushNotificationIOS,
  ActivityIndicator,
} from 'react-native';

import styles from './src/components/Styles/GlobalStyle.js';
import GlobalVariables from './src/components/GlobalVariables/GlobalVariables.js';
import {Actions, ActionConst} from 'react-native-router-flux';
import { CheckBox } from 'react-native-elements'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import RF from "react-native-responsive-fontsize";
import renderIf from './renderIf'


var PushNotification = require('react-native-push-notification');
var RNPushNotificationRegistrationService = require('react-native-push-notification');

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
export default class Login extends Component<Props> {

  static navigationOptions = {
          
        header: null
    }

    constructor(props){
        
        super(props);

        this.state = {
            email               : null,
            Password            : null,
            isloading           : false, 
            checked             : false,
            showverification    : false,
            DeviceOS            : null,
            Devicetoken         : null,
        }

        PushNotificationIOS.addEventListener('register', (token) => {

          this.setState({
            deviceToken: token
          })
          

        });

    }

    componentDidMount = async () => {

        AsyncStorage.multiGet(["email", "password","checked"]).then(response => {
            
            var email                 = response[0][1];
            var password              = response[1][1];
            var checked               = response[2][1];

            console.log(email+password);

            this.setState({ email: email })  
            this.setState({ Password: password })  
            this.setState({ checked: checked }) 
            //this.setState({showverification : false })    
        });

        PushNotification.configure({


 
              // (optional) Called when Token is generated (iOS and Android)
              onRegister:  function(token) {
                  console.log( 'STUDENTTOKEN:', token );
                
              },
           
              // (required) Called when a remote or local notification is opened or received
              onNotification: function(notification) {
                  console.log( 'NOTIFICATION:', notification );
                  // process the notification
           
                  // required on iOS only (see fetchCompletionHandler docs: https://facebook.github.io/react-native/docs/pushnotificationios.html)
                  notification.finish(PushNotificationIOS.FetchResult.NoData);
              },
           
              // ANDROID ONLY: GCM or FCM Sender ID (product_number) (optional - not required for local notifications, but is need to receive remote push notifications)
              senderID: "1073434687873",
           
              // IOS ONLY (optional): default: all - Permissions to register.
              permissions: {
                  alert: true,
                  badge: true,
                  sound: true
              },
           
              // Should the initial notification be popped automatically
              // default: true
              popInitialNotification: true,
           
              /**
                * (optional) default: true
                * - Specified if permissions (ios) and token (android and ios) will requested or not,
                * - if not, you must call PushNotificationsHandler.requestPermissions() later
                */
              requestPermissions: true,

              //largeIcon : 'http://mentorship.iasjunior.com/admin/adminassets/images/15389899325bbb1f6c3223d_thumb.png',
              //smallIcon : 'http://mentorship.iasjunior.com/admin/adminassets/images/15389899325bbb1f6c3223d_thumb.png',

        });

    }

    

    verfication(){

      var email = this.state.email;
      this.setState({isloading : true });
    
      if( email ){

          var GVar = GlobalVariables.getApiUrl();

          let formdata = new FormData();

          formdata.append('student_email',        email );
         
          fetch( GVar.BaseUrl + GVar.Verification,{
                      method: 'POST',
                      body: formdata,        
                      }).then((response) => response.text())
                     .then((responseJson) => {

                      this.setState({isloading : false });
                      this.setState({showverification : false })

                      console.log(responseJson,"responseJsonresponseJson")
                     
                      
                      var x = JSON.parse(responseJson.replace("-", ""));
                       
                      alert(x.message);  

                     
                     }).catch((error)=>{ Alert.alert(error)});

      }
    
    }  

   
    Login(){



        this.setState({isloading : true });

        var email         = this.state.email;
        var Password      = this.state.Password;
        var DeviceToken   = this.state.deviceToken;
        var DeviceOS      = Platform.OS;

       

        if(email == null ){
          Alert.alert("Please Enter Email");
          this.setState({isloading : false });
        }else if( Password == null ){
          Alert.alert("Please Enter Password");
          this.setState({isloading : false });
        }else{


        

          var GVar = GlobalVariables.getApiUrl();

          let formdata = new FormData();
          
          formdata.append('email',        email );
          formdata.append('password',  Password );
          formdata.append('DeviceOS',  DeviceOS );
          formdata.append('Devicetoken',  DeviceToken );


          console.log("login formdata",formdata);


          fetch( GVar.BaseUrl + GVar.Login,{
                      method: 'POST',
                      body: formdata,        
                      }).then((response) => response.text())
                     .then((responseJson) => {

                      this.setState({isloading : false });
                     
                      
                      var x = JSON.parse(responseJson.replace("-", ""));
                      this.setState({isLoading : false });
                     
                      if( x.status == 1 ){

                        
                          AsyncStorage.setItem('TOKEN', x.token);
                          
                          if( x.isverfied == 0 ){

                            AsyncStorage.setItem('verifymessage', x.message );
                            AsyncStorage.setItem('isverfied', '0' );
                            alert(x.message)

                          }else{
                            AsyncStorage.setItem('isverfied', '1' );
                          }
                          
                          this.props.navigation.navigate('Tabmenu');
                                                   
                      }else{
                        alert(x.message)
                        
                        if( x.isverfied == 0 ){
                          this.setState({showverification : true })
                        }
                      }
                          
                     }).catch((error)=>{ Alert.alert(error)});
        }

       
    }

    Register(){
        Actions.Register();
    }

    

    _remember(){

      
      //if( this.state.checked ){

        var check = !this.state.checked;
        this.setState({checked: !this.state.checked});

        var email = this.state.email;
        var Password = this.state.Password;

        if( email && Password){

            checked = '';

            if( check ){
              checked = 'true';

              AsyncStorage.setItem('email', email);
              AsyncStorage.setItem('password', Password);
              AsyncStorage.setItem('checked', checked);
            }else{
              AsyncStorage.removeItem('email');
              AsyncStorage.removeItem('password');
              AsyncStorage.removeItem('checked');
            }
        }else{

          Alert.alert("Please enter both email and password");
        }
      
      
    }



  render() {
    return (
      <View style={styles.container}>
      <StatusBar
           backgroundColor="#232A40"
           barStyle="light-content"/>

            <ImageBackground   source={require('./src/assets/images/global/background.png')}
                    style={{flex:1,}}
              >
              <ScrollView>
                  
                  <View style={{flex: 1}} pointerEvents={this.state.isloading ? 'none' : 'auto'}>
                    <View style={{flex:2}} >
                         <Image style={{height:RF(12), width:RF(32),alignSelf:'center', marginTop:hp('10%')}} source={require('./src/assets/images/global/logo.png')} />
                    </View>

                    <View style={{flex:3,flexDirection: 'column',justifyContent: 'space-between',marginTop:hp('12%')}} >
                          <View style={{width: wp('100%'),height: hp('8%'),  alignSelf:'center'}} >
                              <Text style={{ fontSize: RF(3.5), color:'#FFFFFF',alignSelf:'center'}}>LOGIN</Text>
                          </View>

                          <View style={{width: wp('65%'), height: hp('6%'), backgroundColor: '#FFFFFF', alignSelf:'center', borderRadius:wp('2%'), justifyContent:'center'}} >
                                <TextInput style={{
                                          //textAlign:'center',
                                          TextColor: '#000000',
                                          fontSize: RF(2.5),
                                          marginLeft : wp('2%')
                                      }}
                                    onChangeText={(email) => this.setState({email})}
                                    value={this.state.email}
                                    keyboardType='email-address'
                                    placeholder={'Email Address'}
                                    underlineColorAndroid="transparent"
                                    placeholderTextColor= '#2a29c9'
                                    />   
                          </View>
                          
                          <View style={{width: wp('65%'), height: hp('6%'), backgroundColor: '#FFFFFF', alignSelf:'center', marginTop:hp('2%'),borderRadius:wp('2%'),justifyContent:'center'}} >
                                <TextInput style={{
                                        TextColor: '#000000',
                                        fontSize: RF(2.5),
                                        marginLeft : wp('2%')
                                    }}
                                    onChangeText={(Password) => this.setState({Password})}
                                    value={this.state.Password}
                                    placeholder={'Password'}
                                    underlineColorAndroid="transparent"
                                    secureTextEntry={true}
                                    placeholderTextColor= '#2a29c9'
                                    />
                          </View>

                    </View>

                    <View style={{flex:2,flexDirection: 'column',justifyContent: 'space-between',}} >
                          
                              <View style={{width: wp('80%'), flexDirection: 'row',height: hp('6%'),}} >
                                    <View style={{width: wp('50%'), height: hp('10%')}} >
                                        <CheckBox 
                                              center
                                              checkedColor = '#FFFFFF'
                                              size = {hp('2.5%')}
                                              containerStyle={{backgroundColor:'transparent', borderColor:'transparent'}}
                                              textStyle ={{fontSize: RF(2.5),fontWeight: 'bold',color:'#FFFFFF'}}
                                              checked={this.state.checked} 
                                              title='Remember me' 
                                              onPress={this._remember.bind(this) }/>
                                    </View>

                                 
                                    <View style={{width : wp('50%'), height: hp('10%')}} >
                                        <TouchableOpacity onPress={() => this.props.navigation.navigate('ForgetPassword')}>
                                            <Text style={{fontSize: RF(2.5),fontWeight: 'bold',alignSelf:'center',marginTop:18, color:'#FFFFFF'}}>Forgot Password ?</Text>
                                        </TouchableOpacity>
                                    </View>
                              </View>

                              <View style={{width: wp('65%'), height: hp('6%'), alignSelf:'center', marginTop:hp('2%'),borderRadius:wp('2%')}} >
                                    <TouchableOpacity onPress={this.Login.bind(this)}>
                                    <Image style={{width: wp('35%'), height: hp('7%'),alignSelf:'center', marginTop: 10, borderRadius:wp('2%')}} source={require('./src/assets/images/B2.png')} />
                                    
                                    </TouchableOpacity>
                              </View>
                    </View>

                    <View style={{flex:2, flexDirection: 'column',justifyContent: 'space-between',marginTop:hp('5%')}} >
                          <View style={{width: wp('80%'), height: hp('20%'), alignSelf:'center'}} >

                                    { renderIf(this.state.showverification)(
                                    <View style={{width: wp('100%'), height: hp('10%'), alignSelf:'center'}} >
                                        <TouchableOpacity onPress={this.verfication.bind(this)}>
                                                <Text style={{fontSize: RF(2.5),fontWeight: 'bold',alignSelf:'center',color:'red'}}>Click here to resend the verfication link again</Text>
                                        </TouchableOpacity>
                                    </View>
                                    )
                                  }

                                    <View style={{width: wp('100%'), height: hp('10%'), alignSelf:'center'}} >
                                        <Text style={{fontSize: RF(2.5),fontWeight: 'bold',alignSelf:'center',color:'#FFFFFF'}}>No Account Yet ?</Text>
                                    </View>

                                 
                                    <View style={{width : wp('100%'), height: hp('10%'),alignSelf:'center', marginTop:hp('-5%')}} >
                                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Register')}>
                                            <Text style={{fontSize: RF(2.5),fontWeight: 'bold',alignSelf:'center', color:'#FFFFFF'}}>Signup</Text>
                                        </TouchableOpacity>
                                    </View>

                                    <View style={{width : wp('100%'), height: hp('10%'),alignSelf:'center', marginTop:hp('0%')}} >
                                        <TouchableOpacity onPress={() => this.props.navigation.navigate('IASjuniorMain',{ param: 'AuthStack' })}>
                                            <Text style={{fontSize: RF(3),fontWeight: 'bold',alignSelf:'center', color:'#1ab394'}}>www.iasjunior.com</Text>
                                        </TouchableOpacity>
                                    </View>
                              </View>
                           
                    </View>


                  </View>

              </ScrollView>   
              { this.state.isloading ? <ActivityIndicator
                                      animating     ={true}
                                      transparent   ={true}
                                      visible       ={false}
                                      style         ={styles.indicator}
                                      size          ="large"
                                    /> : <View/>} 
              </ImageBackground>

      </View>
    );
  }
}

