/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
  Platform, 
  StyleSheet, 
  Text, 
  View,
  StatusBar,
  ImageBackground,
  ScrollView,
  Image,
  TextInput,
  TouchableOpacity,
  Alert,
  AsyncStorage,
  ActivityIndicator
} from 'react-native';

import styles from './src/components/Styles/GlobalStyle.js';
import GlobalVariables from './src/components/GlobalVariables/GlobalVariables.js';
import {Actions, ActionConst} from 'react-native-router-flux';
import { CheckBox } from 'react-native-elements'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import RF from "react-native-responsive-fontsize";


const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
export default class ForgetPassword extends Component<Props> {

  static navigationOptions = {
          
        header: null
    }

    constructor(props){
        
        super(props);

        this.state = {
            email               : null,
            Password            : null,
            isloading           : false, 
            checked             : false,
        }

    }
    navigateToScreen = (route) => () => {
      const navigateAction = NavigationActions.navigate({
        routeName: route
      });
      this.props.navigation.dispatch(navigateAction);
    }

    componentDidMount = async () => {

        
    }


    ForgetPassword(){

        this.setState({isloading : true });

        var email = this.state.email;
        

        if(email == null ){
          Alert.alert("Please Enter Email");
          this.setState({isloading : false });
        }else{

          this.setState({isloading : true });
          var GVar = GlobalVariables.getApiUrl();


          let formdata = new FormData();

          formdata.append('student_email',        email );

          fetch( GVar.BaseUrl + GVar.ForgotPassword,{
                      method: 'POST',
                      body: formdata,        
                      }).then((response) => response.text())
                     .then((responseJson) => {

                      this.setState({isloading : false });
                     
                      
                      var x = JSON.parse(responseJson.replace("-", ""));
                      console.log("fogor password",x);
                      this.setState({isLoading : false });
                      alert(x.message)
                      this.props.navigation.navigate('Login')

                     }).catch((error)=>{ Alert.alert(error)});

        }       
    }

    Register(){
        Actions.Register();
    }

    _remember(){

      var check = !this.state.checked;
      this.setState({checked: !this.state.checked});

      var email = this.state.email;
      var Password = this.state.Password;

      checked = '';

      if( check ){
        checked = 'true';

        AsyncStorage.setItem('email', email);
        AsyncStorage.setItem('password', Password);
        AsyncStorage.setItem('checked', checked);
      }else{
        AsyncStorage.removeItem('email');
        AsyncStorage.removeItem('password');
        AsyncStorage.removeItem('checked');
      }


      

     
    }

  render() {
    return (
      <View style={styles.container}>
      <StatusBar
           backgroundColor="#232A40"
           barStyle="light-content"/>

            <ImageBackground   source={require('./src/assets/images/global/background.png')}
                    style={{flex:1,}}
              >
              <ScrollView>
                  
                  <View style={{flex: 1}}  pointerEvents={this.state.isloading ? 'none' : 'auto'}>
                    <View style={{flex:2}} >
                             <Image style={{height:RF(12), width:RF(32),alignSelf:'center', marginTop:hp('10%')}} source={require('./src/assets/images/global/logo.png')} />
                        </View>

                    <View style={{flex:3,flexDirection: 'column',justifyContent: 'space-between',marginTop:hp('12%')}} >
                          <View style={{width: wp('100%'),height: hp('8%'),  alignSelf:'center'}} >
                              <Text style={{ fontSize: RF(3.5), color:'#FFFFFF',alignSelf:'center'}}>FORGOT PASSWORD ?</Text>
                          </View>

                          <View style={{width: wp('65%'), height: hp('6%'), backgroundColor: '#FFFFFF', alignSelf:'center', borderRadius:wp('2%')}} >
                                <TextInput style={{
                                          //textAlign:'center',
                                          TextColor: '#000000',
                                          fontSize: RF(2.5),
                                          marginLeft : wp('2%')
                                      }}
                                    onChangeText={(email) => this.setState({email})}
                                    value={this.state.email}
                                    keyboardType='email-address'
                                    placeholder={'Email Address'}
                                    underlineColorAndroid="transparent"
                                    placeholderTextColor= '#2a29c9'
                                    />   
                          </View>
                          
                    </View>

                    

                    <View style={{flex:2,flexDirection: 'column',justifyContent: 'space-between',}} >

                              <View style={{width: wp('65%'), height: hp('10%'), alignSelf:'center', marginTop:hp('2%'),borderRadius:wp('2%')}} >
                                    <  TouchableOpacity 
                                    onPress={
                                      this.ForgetPassword.bind(this)
                                    }>
                                    <Image style={{width: wp('35%'), height: hp('7%'),alignSelf:'center', marginTop: 10, borderRadius:wp('2%')}} source={require('./src/assets/images/B2.png')} />
                                    
                                    { this.state.isloading ? <ActivityIndicator size="small" color="#00ff00" /> : <View/>}
                                    </TouchableOpacity>
                              </View>
                    </View>

                   <View style={{flex:2,flexDirection: 'column',justifyContent: 'space-between',}} >
                          
                              <View style={{width: wp('80%'), flexDirection: 'row',height: hp('6%'),}} >
                                    <View style={{width: wp('50%'), height: hp('10%')}} >
                                       <TouchableOpacity onPress={() => this.props.navigation.navigate('Login')}>
                                            <Text style={{fontSize: RF(2.5),fontWeight: 'bold',alignSelf:'center',marginTop:18, color:'#FFFFFF'}}>Login Now</Text>
                                        </TouchableOpacity>
                                    </View>

                                 
                                    <View style={{width : wp('50%'), height: hp('10%')}} >
                                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Register')}>
                                            <Text style={{fontSize: RF(2.5),fontWeight: 'bold',alignSelf:'center',marginTop:18, color:'#FFFFFF'}}>Signup Now</Text>
                                        </TouchableOpacity>
                                    </View>
                              </View>
                    </View>


                  </View>

              </ScrollView>    
              </ImageBackground>

      </View>
    );
  }
}

