/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
  Platform, 
  StyleSheet, 
  Text, 
  View,
  StatusBar,
  ImageBackground,
  ScrollView,
  Image,
  TextInput,
  TouchableOpacity,
  Alert,
  ActivityIndicator,
  AsyncStorage
} from 'react-native';

import styles from './src/components/Styles/GlobalStyle.js';
import GlobalVariables from './src/components/GlobalVariables/GlobalVariables.js';
import DatePicker from 'react-native-datepicker';
import { Dropdown } from 'react-native-material-dropdown';
import ImagePicker from 'react-native-image-picker';
import { StackNavigator, NavigationActions } from  'react-navigation';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import RF from "react-native-responsive-fontsize";
import Login from './Login.js'
import renderIf from './renderIf'

const App = StackNavigator({
    Login: { screen: Login},
})


type Props = {};
export default class Register extends Component<Props> {

    static navigationOptions = {
          
        header: null
    }

    navigateToScreen = (route) => () => {
      const navigateAction = NavigationActions.navigate({
        routeName: route
      });
      this.props.navigation.dispatch(navigateAction);
    }

    constructor(props){
        
        super(props);

        this.state = {
            email                     : null,
            Password                  : null,
            isLoading                 : false,
            isSuccessShowMessage       : false,
            isErrorShowMessage        : false,
        }

    }

   
    Register(){

       this.setState({isLoading : true });
       




        var name                = this.state.name;
        var email               = this.state.email;
        var Password            = this.state.Password;
        var cPassword           = this.state.cPassword;
        var phone               = this.state.phone;
        var student_image       = this.state.student_image;
        var time_given          = this.state.time_given;
        var Qualification       = this.state.qualification;
        var Others              = this.state.others;
        var exam_pattern        = this.state.exam_pattern;
        var plan_upsc           = this.state.plan_upsc;
        var scale               = this.state.scale;


        var error = 0;


        var error_message = '';

        

        if(name == null || name == undefined ){
          error = error + 1;
          error_message = error_message + '\nPlease enter name';
          //Alert.alert("Please Enter Name");
        } 

        if(email == null || email == undefined){
          error = error + 1;
          error_message = error_message + '\nPlease enter email id';
          //Alert.alert("Please Enter Email");
        }else{

          filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
          if ( !filter.test( email )) {
              error = error + 1;
              error_message = error_message + '\nPlease enter a valid email id';
          }
        } 

        if( Password == null || Password == undefined ){
          error = error + 1;
          error_message = error_message + '\nPlease enter password';
          //Alert.alert("Please Enter Password");
        }

        if( cPassword == null || cPassword == undefined ){
          error = error + 1;
          error_message = error_message + '\nPlease enter confirm password';
          //Alert.alert("Please Enter Confirm Password");
        }

        if( Password && cPassword ){
          if( Password != cPassword ){
            error = error + 1;
            error_message = error_message + '\nYour password and confirm password is not same';
            //Alert.alert("Please check same confirm password and password");
          }
        }

        if( phone == null || phone == undefined ){
          error = error + 1;
          error_message = error_message + '\nPlease enter phone no.';
          //Alert.alert("Please Enter Phone No");
        }

        
        if( student_image == null || student_image == undefined ){
          //error = error + 1;
          //error_message = error_message + '\nPlease upload student image';
          //Alert.alert("Please upload student image");

          student_image = [];
          student_image.uri = '';
        }

        if( time_given == null ){
          error = error + 1;
          error_message = error_message + '\nPlease enter how much time can you give for IAS junior mentorship Program ?';
          //Alert.alert("Please Enter Phone No");
        }

        if( Qualification == null || Qualification == undefined ){
          error = error + 1;
          error_message = error_message + '\nPlease enter qualification';
          //Alert.alert("Please Enter Phone No");
        }else{

          if( Qualification == 'Others'){

              if( Others == null || Others == undefined ){
                error = error + 1;
                error_message = error_message + '\nPlease enter others qualification';
              } 
          }

          console.log(Qualification,"Qualification");
          console.log(Others,"Others");
        }


        if( exam_pattern == null || exam_pattern == undefined ){
          error = error + 1;
          error_message = error_message + '\nPlease select Do you know about Exam Pattern?';
          //Alert.alert("Please Enter Phone No");
        }

        if( plan_upsc == null  || plan_upsc == undefined ){
          error = error + 1;
          error_message = error_message + '\nPlease select your plan for UPSC';
          //Alert.alert("Please Enter Phone No");
        }

        if( scale == null || scale == undefined ){
          error = error + 1;
          error_message = error_message + '\nPlease select scale';
          //Alert.alert("Please Enter Phone No");
        }


        console.log(error,"error");
       
        
        if( error == 0 ){

              var GVar = GlobalVariables.getApiUrl();

              let formdata = new FormData();

              formdata.append('student_name',  name );
              formdata.append('student_email',        email );
              formdata.append('password',  Password );
              formdata.append('confirm_password',  cPassword );
              formdata.append('phone',  phone );
              if( student_image.uri ){

                formdata.append('student_photo', {
                    uri: student_image.uri,
                    type: 'image/jpeg', // or photo.type
                    name: 'testPhotoName'
                  });
              }

              formdata.append('stuBackground',  Qualification );
              formdata.append('other_background',  Others );
              formdata.append('exam_pattern',  exam_pattern );
              formdata.append('time_for_mentorship_exam',  time_given );
              formdata.append('year_plan_upsc_exam',  plan_upsc );
              formdata.append('scale_yourself',  scale );

              console.log(formdata,"formdata");

              var GVAr  = GlobalVariables.getApiUrl();
                try {
                      
                        let response = fetch(GVar.BaseUrl + GVar.SignUp,{
                              method: 'POST',
                              body: formdata,        
                             }).then((response) => response.text())
                           .then((responseJson) => {

                                  this.setState({isLoading : false });
                                  
                           
                            
                                  var x = JSON.parse(responseJson.replace("-", ""));

                                  this.setState({status : x.status });
                                  this.setState({
                                    email         : null,
                                    name          : null,
                                    Password      : null,
                                    cPassword     : null,
                                    phone         : null,
                                    student_image : null,
                                    time_given    : null,
                                    Qualification : null,
                                    Others        : null,
                                    exam_pattern  : null,
                                    plan_upsc     : null,
                                    scale         : null,
                                  });

                                  console.log('responceapi',x);

                                  if( x.status == 1 ){

                                    this.setState({isSuccessShowMessage : true });
                                    this.setState({SuccessMessage : x.message });

                                      if( Platform.OS == 'ios'){
                                        Alert.alert(
                                          'Thank you for registering',
                                          'Please Login Now ',
                                          [
                                            {text: 'Login Now', onPress: () => {AsyncStorage.setItem('TOKEN', x.token);AsyncStorage.setItem('student_id', JSON.stringify(x.student_id));this.props.navigation.navigate('First')}},
                                          ],
                                          { cancelable: false }
                                          )
                                      }else{
                                        Alert.alert(
                                          'Thank you for registering',
                                          'Please complete the payment process to activate the modules',
                                          [
                                          {text: 'No, Pay Later!', onPress: () => {AsyncStorage.setItem('TOKEN', x.token);AsyncStorage.setItem('student_id', x.student_id);this.props.navigation.navigate('First')}},
                                          {text: 'Yes, Pay Now', onPress: () => {AsyncStorage.setItem('TOKEN', x.token);AsyncStorage.setItem('student_id', x.student_id);this.props.navigation.navigate('Payment');}},
                                          ],
                                          { cancelable: false }
                                          )
                                      }
                                    

                                  }else{

                                    this.setState({isErrorShowMessage : true });
                                    this.setState({ErrorMessage : x.message });
                                  }

                                  

                                  

                                  
                            
                             });

                  }catch (error) {

                          if( error.line == 18228 ){
                            
                            Alert.alert("Message","Please connect to internet");
                            
                          }

                  }

        }else{
          this.setState({isLoading : false });
          alert(error_message);
        }
        
    }

    setUserLogin(){

    }

     setCamera(){

    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        let source = { uri: response.uri };

        this.setState({student_image : source});

      }
    });
  }

  onChangeQualification( value ){



    if( value == 'Others' ){
      this.setState({showothers : true});
    }else{
      this.setState({showothers : false});
    }

    this.setState({qualification : value });
  }

  render() {
    let data = [{
      value: 'Married',
    }, {
      value: 'Unmarried',
    }];

    let optional_subject = [{
      value: 'Physics',
    }, {
      value: 'Maths',
    },{
      value: 'English',
    },{
      value: 'History',
    },{
      value: 'Hindi',
    }];

    let scaledata = [{
      value: '1',
    }, {
      value: '2',
    },{
      value: '3',
    },{
      value: '4',
    },{
      value: '5',
    },{
      value: '6',
    },{
      value: '7',
    },{
      value: '8',
    },{
      value: '9',
    },{
      value: '10',
    },
    ];

    let plan_upscdata = [ {
      value: '2019',
    },{
      value: '2020',
    },{
      value: '2021',
    },{
      value: '2022',
    },{
      value: '2022',
    },{
      value: '2023',
    },{
      value: '2024',
    },{
      value: '2025',
    },{
      value: '2026',
    },
    {
      value: '2027',
    },
    {
      value: '2028',
    }
    ];

    let exam_patterndata =  [{
      value: 'Yes',
    }, {
      value: 'No',
    }
    ];

    let qualificationdata =  [{
      value: 'High School Student',
    }, {
      value: 'Degree Student',
    },{
      value: 'Full Time Aspirant',
    },{
      value: 'Working Professional',
    },{
      value: 'Others',
    }
    ];

    return (
      <View style={styles.container}>
      <StatusBar
           backgroundColor="#232A40"
           barStyle="light-content"/>
           
            <ImageBackground   source={require('./src/assets/images/global/background.png')} style={{flex:1,}}>
               <ScrollView>
                  <View style={{flex: 1}} pointerEvents={this.state.isLoading ? 'none' : 'auto'}>
                       
                        <View style={{flex:2}} >
                             <Image style={{height:RF(12), width:RF(32),alignSelf:'center', marginTop:hp('10%')}} source={require('./src/assets/images/global/logo.png')} />
                        </View>

                        <View style={{flex: 4,flexDirection: 'column',justifyContent: 'space-between',marginTop:10,}} >


                          <View style={{flex:1,flexDirection: 'column',justifyContent: 'space-between',marginTop:hp('10%')}} >
                              <View style={{width: '100%',}} >

                                  <View style={{width: wp('100%'),height: hp('8%'),  alignSelf:'center'}} >
                                      <Text style={{ fontSize: RF(3.5), color:'#FFFFFF',alignSelf:'center'}}>CREATE ACCOUNT</Text>
                                  </View>

                                  <View style={{flex: 1, flexDirection: 'row'}}>
                                            <View style={{width:'50%',}} >

                                                  <View style={{flexDirection:'column', marginTop:hp('2%')}} >
                                                        
                                                          <View style={{flexDirection:'row'}}>
                                                              <Text style={{fontSize: RF(2.5),color:'#FFFFFF', marginLeft:wp('1%')}}>Enter Name</Text>
                                                              <Text style={{fontSize: RF(2.5),color:'#FF0000', marginLeft:wp('1%')}}>*</Text>
                                                          </View>

                                                          <View style={{width: '95%', height: hp('6%'), backgroundColor: '#FFFFFF', alignSelf:'center', borderRadius:wp('2%')}} >
                                                                <TextInput style={{
                                                                      //textAlign:'center',
                                                                      TextColor: '#000000',
                                                                      fontSize: RF(2.5),
                                                                      marginLeft : wp('2%')
                                                                  }}
                                                                onChangeText={(name) => this.setState({name})}
                                                                value={this.state.name}
                                                                placeholder={'Name'}
                                                                underlineColorAndroid="transparent"
                                                                placeholderTextColor= '#2a29c9'
                                                                />   
                                                          </View>

                                                        
                                                  </View>

                                                  <View style={{flexDirection:'column', marginTop:hp('2%')}} >
                                                        
                                                          <View style={{flexDirection:'row',}}>
                                                              <Text style={{fontSize: RF(2.5),color:'#FFFFFF', marginLeft:wp('1%')}}>Enter Password</Text>
                                                              <Text style={{fontSize: RF(2.5),color:'#FF0000', marginLeft:wp('1%')}}>*</Text>
                                                          </View>

                                                          <View style={{width: '95%', height: hp('6%'), backgroundColor: '#FFFFFF', alignSelf:'center', borderRadius:wp('2%')}} >
                                                                <TextInput style={{
                                                                      //textAlign:'center',
                                                                      TextColor: '#000000',
                                                                      fontSize: RF(2.5),
                                                                      marginLeft : wp('2%')
                                                                  }}
                                                                  onChangeText={(Password) => this.setState({Password})}
                                                                  value={this.state.Password}
                                                                  placeholder={'Password'}
                                                                  underlineColorAndroid="transparent"
                                                                  secureTextEntry={true}
                                                                placeholderTextColor= '#2a29c9'
                                                                /> 
                                                          </View>    
                                                  </View>

                                                  <View style={{flexDirection:'column', marginTop:hp('2%')}} >
                                                        
                                                          <View style={{flexDirection:'row',}}>
                                                              <Text style={{fontSize: RF(2.5),color:'#FFFFFF', marginLeft:wp('1%')}}>Enter Phone</Text>
                                                              <Text style={{fontSize: RF(2.5),color:'#FF0000', marginLeft:wp('1%')}}>*</Text>
                                                          </View>

                                                          <View style={{width: '95%', height: hp('6%'), backgroundColor: '#FFFFFF', alignSelf:'center', borderRadius:wp('2%')}} >
                                                                <TextInput style={{
                                                                      //textAlign:'center',
                                                                      TextColor: '#000000',
                                                                      fontSize: RF(2.5),
                                                                      marginLeft : wp('2%')
                                                                  }}
                                                                  onChangeText={(phone) => this.setState({phone})}
                                                                  value={this.state.phone}
                                                                  keyboardType='numeric'
                                                                  placeholder={'Phone'}
                                                                  underlineColorAndroid="transparent"
                                                                  placeholderTextColor= '#2a29c9'
                                                                  /> 
                                                           </View>   
                                                  </View>

                                                  
                                            </View>



                                            <View style={{width:'50%', height: 50,}}>
                                                  <View style={{flexDirection:'column', marginTop:hp('2%')}} >
                                                        
                                                          <View style={{flexDirection:'row',}}>
                                                              <Text style={{fontSize: RF(2.5),color:'#FFFFFF', marginLeft:wp('1%')}}>Enter Email</Text>
                                                              <Text style={{fontSize: RF(2.5),color:'#FF0000', marginLeft:wp('1%')}}>*</Text>
                                                          </View>

                                                          <View style={{width: '95%', height: hp('6%'), backgroundColor: '#FFFFFF', alignSelf:'center', borderRadius:wp('2%')}} >
                                                                <TextInput style={{
                                                                      //textAlign:'center',
                                                                      TextColor: '#000000',
                                                                      fontSize: RF(2.5),
                                                                      marginLeft : wp('2%')
                                                                  }}
                                                            onChangeText={(email) => this.setState({email})}
                                                            value={this.state.email}
                                                            keyboardType='email-address'
                                                            placeholder={'Email Address'}
                                                            underlineColorAndroid="transparent"
                                                            placeholderTextColor= '#2a29c9'
                                                            /> 
                                                          </View>    
                                                  </View>

                                                  <View style={{flexDirection:'column', marginTop:hp('2%')}} >
                                                        
                                                          <View style={{flexDirection:'row',}}>
                                                              <Text style={{fontSize: RF(2.5),color:'#FFFFFF', marginLeft:wp('1%')}}>Enter Confirm Password</Text>
                                                              <Text style={{fontSize: RF(2.5),color:'#FF0000', marginLeft:wp('1%')}}>*</Text>
                                                          </View>

                                                          <View style={{width: '95%', height: hp('6%'), backgroundColor: '#FFFFFF', alignSelf:'center', borderRadius:wp('2%')}} >
                                                                <TextInput style={{
                                                                      //textAlign:'center',
                                                                      TextColor: '#000000',
                                                                      fontSize: RF(2.5),
                                                                      marginLeft : wp('2%')
                                                                  }}
                                                                  onChangeText={(cPassword) => this.setState({cPassword})}
                                                                  value={this.state.cPassword}
                                                                  placeholder={'Confirm Password'}
                                                                  underlineColorAndroid="transparent"
                                                                  secureTextEntry={true}
                                                                  placeholderTextColor= '#2a29c9'
                                                                  />   
                                                                </View>  
                                                  </View>

                                                  <View style={{flexDirection:'column', marginTop:hp('2%')}} >
                                                        
                                                          <View style={{}}>
                                                              <Text style={{fontSize: RF(2.5),color:'#FFFFFF', marginLeft:wp('1%')}}>Profile photo</Text>
                                                          </View>

                                                          <View style={{width: '95%', height: hp('6%'), backgroundColor: '#FFFFFF', alignSelf:'center', borderRadius:wp('2%'), justifyContent:'center'}} >
                                                                <TouchableOpacity onPress={this.setCamera.bind(this)}>
                                                                  <Text style={{marginLeft : wp('4%'), fontSize: hp('2%'), color:'#2a29c9', alignSelf:'center'}}>Choose File</Text>
                                                              </TouchableOpacity>
                                                          </View>      
                                                  </View>
                                                  
                                            </View>

                                  </View>

                                  <View style={{flexDirection:'column', marginTop:hp('2%')}} >
                                                        
                                                          <View style={{flexDirection:'row',}}>
                                                              <Text style={{fontSize: RF(2.5),color:'#FFFFFF', marginLeft:wp('1%')}}>How much time can you give for IAS junior mentorship Program ?</Text>
                                                              <Text style={{fontSize: RF(2.5),color:'#FF0000', marginLeft:wp('1%')}}>*</Text>
                                                          </View>

                                                          <View style={{width: '98%', height: hp('6%'), backgroundColor: '#FFFFFF', alignSelf:'center', borderRadius:wp('2%')}} >
                                                                <TextInput style={{
                                                                      //textAlign:'center',
                                                                      TextColor: '#000000',
                                                                      fontSize: RF(2.5),
                                                                      marginLeft : wp('2%')
                                                                  }}
                                                                  onChangeText={(time_given) => this.setState({time_given})}
                                                                  value={this.state.time_given}
                                                                  placeholder={'(ex: 3 hours)'}
                                                                  underlineColorAndroid="transparent"
                                                                  placeholderTextColor= '#2a29c9'
                                                                  keyboardType='numeric'
                                                                  />   
                                                          </View>        
                                  </View>

                                  
                                  <View style={{flexDirection:'column', marginTop:hp('2%')}} >
                                                        
                                      <View style={{flexDirection:'row',}}>
                                          <Text style={{fontSize: RF(2.5),color:'#FFFFFF', marginLeft:wp('1%')}}>Qualification</Text>
                                          <Text style={{fontSize: RF(2.5),color:'#FF0000', marginLeft:wp('1%')}}>*</Text>
                                      </View>

                                      <View style={{width: '95%', height: hp('7%'), alignSelf:'center',marginTop:-25}} >
                                            <Dropdown
                                               baseColor= '#FFFFFF'
                                                //label='(1 lowest)'
                                                itemColor ='#000000'
                                                data={qualificationdata}
                                                fontSize = {RF(2.5)}
                                                disabledItemColor = '#FFFFFF'
                                              valueExtractor={({value})=> value}
                                              onChangeText={this.onChangeQualification.bind(this)}
                                            /> 
                                      </View>      
                                  </View>

                                  { renderIf(this.state.showothers)(
                                    <View style={{flexDirection:'column', marginTop:hp('5%')}} >
                                                        
                                          <View style={{flexDirection:'row',}}>
                                              <Text style={{fontSize: RF(2.5),color:'#FFFFFF', marginLeft:wp('1%')}}>Others</Text>
                                              <Text style={{fontSize: RF(2.5),color:'#FF0000', marginLeft:wp('1%')}}>*</Text>
                                          </View>

                                          <View style={{width: '98%', height: hp('6%'), backgroundColor: '#FFFFFF', alignSelf:'center', borderRadius:wp('2%')}} >
                                                <TextInput style={{
                                                      //textAlign:'center',
                                                      TextColor: '#000000',
                                                      fontSize: RF(2.5),
                                                      marginLeft : wp('2%')
                                                  }}
                                                    onChangeText={(others) => this.setState({others})}
                                                    value={this.state.others}
                                                    placeholder={'Others'}
                                                    underlineColorAndroid="transparent"
                                                    placeholderTextColor= '#2a29c9'
                                                    />   
                                          </View> 
                                    </View>
                                    )
                                  }


                                  <View style={{flexDirection:'column', marginTop:hp('5%')}} >
                                                        
                                      <View style={{flexDirection:'row',}}>
                                          <Text style={{fontSize: RF(2.5),color:'#FFFFFF', marginLeft:wp('1%')}}>Do you know about Exam Pattern?</Text>
                                          <Text style={{fontSize: RF(2.5),color:'#FF0000', marginLeft:wp('1%')}}>*</Text>
                                      </View>

                                      <View style={{width: '95%', height: hp('7%'), alignSelf:'center',marginTop:-25}} >
                                            <Dropdown
                                                baseColor= '#FFFFFF'
                                                //label='(1 lowest)'
                                                itemColor ='#000000'
                                                data={exam_patterndata}
                                                fontSize = {RF(2.5)}
                                                disabledItemColor = '#FFFFFF'
                                                valueExtractor={({value})=> value}
                                                onChangeText={(value)=>{ this.setState({exam_pattern : value })}}
                                              /> 
                                      </View>        
                                  </View>


                                  <View style={{flexDirection:'column', marginTop:hp('5%')}} >
                                                        
                                      <View style={{flexDirection:'row',}}>
                                          <Text style={{fontSize: RF(2.5),color:'#FFFFFF', marginLeft:wp('1%')}}>Which year you’re planning to write UPSC exam ?</Text>
                                          <Text style={{fontSize: RF(2.5),color:'#FF0000', marginLeft:wp('1%')}}>*</Text>
                                      </View>

                                      <View style={{width: '95%', height: hp('7%'), alignSelf:'center',marginTop:-25}} >
                                            <Dropdown
                                                baseColor= '#FFFFFF'
                                                //label='(1 lowest)'
                                                itemColor ='#000000'
                                                data={plan_upscdata}
                                                fontSize = {RF(2.5)}
                                                disabledItemColor = '#FFFFFF'
                                                valueExtractor={({value})=> value}
                                                onChangeText={(value)=>{ this.setState({plan_upsc : value })}}
                                              /> 
                                      </View>  
                                  </View>
                
                                  <View style={{flexDirection:'column', marginTop:hp('5%')}} >
                                                        
                                      <View style={{flexDirection:'row',}}>
                                          <Text style={{fontSize: RF(2.5),color:'#FFFFFF', marginLeft:wp('1%')}}>How do you scale yourself in your academics in the rate of 1 to 10?</Text>
                                          <Text style={{fontSize: RF(2.5),color:'#FF0000', marginLeft:wp('1%')}}>*</Text>
                                      </View>

                                      <View style={{width: '95%', height: hp('7%'), alignSelf:'center',marginTop:-25}} >
                                            <Dropdown
                                                baseColor= '#FFFFFF'
                                                //label='(1 lowest)'
                                                itemColor ='#000000'
                                                data={scaledata}
                                                fontSize = {RF(2.5)}
                                                disabledItemColor = '#FFFFFF'
                                                valueExtractor={({value})=> value}
                                                onChangeText={(value)=>{ this.setState({scale : value })}}
                                              /> 
                                      </View>        
                                  </View>

                                  


                              </View>
                          </View>     
                        </View>

                        <View style={{flex:2,flexDirection: 'column',justifyContent: 'space-between',}} >
                          
                              <View style={{width: wp('65%'), height: hp('6%'), alignSelf:'center', marginTop:hp('10%'),borderRadius:wp('2%')}} >
                                    <TouchableOpacity onPress={this.Register.bind(this)}>
                                    <Image style={{width: wp('35%'), height: hp('7%'),alignSelf:'center', marginTop: 10, borderRadius:wp('2%')}} source={require('./src/assets/images/B2.png')} />
                                    </TouchableOpacity>
                              </View>
                        </View>

                         <View style={{flex:2, flexDirection: 'column',justifyContent: 'space-between',marginTop:hp('5%')}} >
                          <View style={{width: wp('80%'),  alignSelf:'center'}} >
                                    <View style={{width: wp('100%'), height: hp('10%'), alignSelf:'center'}} >
                                        <Text style={{fontSize: RF(2.5),fontWeight: 'bold',alignSelf:'center',color:'#FFFFFF'}}>Already have an Account ?</Text>
                                    </View>

                                 
                                    <View style={{width : wp('100%'), height: hp('10%'),alignSelf:'center', marginTop:hp('0%')}} >
                                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Login')}>
                                            <Text style={{fontSize: RF(2.5),fontWeight: 'bold',alignSelf:'center', color:'#FFFFFF'}}>Sign In</Text>
                                        </TouchableOpacity>
                                    </View>

                                    { renderIf(this.state.isSuccessShowMessage)(

                                    <View style={{width : wp('96%'),alignSelf:'center', marginTop:hp('0%'), alignItems:'center', backgroundColor:'#dff0d8', borderRadius:wp('3%'), borderWidth:1}} >
                                          <Text style={{fontSize: RF(2.5),fontWeight: 'bold',alignSelf:'center', color:'#3c763d', marginLeft:wp('1%')}}>{this.state.SuccessMessage}</Text>
                                    </View>
                                     )
                                    }

                                    { renderIf(this.state.isErrorShowMessage)(

                                    <View style={{width : wp('96%'), alignSelf:'center', marginTop:hp('0%'), alignItems:'center', backgroundColor:'#f2dede', borderRadius:wp('3%'), borderWidth:1}} >
                                          <Text style={{fontSize: RF(2.5),fontWeight: 'bold',alignSelf:'center', color:'#a94442', marginLeft:wp('1%')}}>{this.state.ErrorMessage}</Text>
                                    </View>
                                     )
                                    }

                                    <View style={{width : wp('100%'), height: hp('10%'),alignSelf:'center', marginTop:hp('5%')}} >
                                        <TouchableOpacity onPress={() => this.props.navigation.navigate('IASjuniorMain')}>
                                            <Text style={{fontSize: RF(3),fontWeight: 'bold',alignSelf:'center', color:'#1ab394'}}>www.iasjunior.com</Text>
                                        </TouchableOpacity>
                                    </View>
                              </View>
                           
                    </View>

                        
                  </View>      
              </ScrollView> 
              { this.state.isLoading ? <ActivityIndicator
                                      animating     ={true}
                                      transparent   ={true}
                                      visible       ={false}
                                      style         ={styles.indicator}
                                      size          ="large"
                                    /> : <View/>} 

            </ImageBackground>
                  
      </View>
    );
  }
}

