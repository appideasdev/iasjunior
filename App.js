import React from 'react';
import {
  ActivityIndicator,
  AsyncStorage,
  Button,
  StatusBar,
  StyleSheet,
  View,
  Alert,
} from 'react-native';
import { StackNavigator, SwitchNavigator } from 'react-navigation'; // 1.3.0

import Login from './Login.js';
import Register from './Register.js';
import ForgetPassword from './ForgetPassword.js';
import Tabmenu from './src/components/Tabmenu.js';
import IASjuniorMain from './IASjuniorMain.js';
import Permissions from 'react-native-permissions'


class AuthLoadingScreen extends React.Component {
  constructor() {
    super();
    this.state={
      photoPermission:'undetermined',
      cameraPermission:'undetermined'
    }
    this._bootstrapAsync();
  }

  componentDidMount() {
    // Permissions.checkMultiple(['camera', 'photo']).then(response => {
    //   //response is an object mapping type to permission
    //   this.setState({
    //     cameraPermission: response.camera,
    //     photoPermission: response.photo,
    //   })
    // });

    // if(this.state.photoPermission == 'undetermined' || this.state.cameraPermission == 'undetermined' || this.state.photoPermission == 'denied' || this.state.cameraPermission == 'denied'|| this.state.photoPermission == 'restricted' || this.state.cameraPermission == 'restricted'){
    //   this._alertForPhotosPermission();
    // }
  }

  _requestPermission = () => {
    Permissions.request('photo').then(response => {
      // Returns once the user has chosen to 'allow' or to 'not allow' access
      // Response is one of: 'authorized', 'denied', 'restricted', or 'undetermined'
      this.setState({ photoPermission: response })
    })
  }
 
  // Check the status of multiple permissions
  _checkCameraAndPhotos = () => {
    Permissions.checkMultiple(['camera', 'photo']).then(response => {
      //response is an object mapping type to permission
      this.setState({
        cameraPermission: response.camera,
        photoPermission: response.photo,
      })
    })
  }
 


  _alertForPhotosPermission() {
    Alert.alert(
      'IAS junior need permission to access Camera',
      'We need access so you can set your profile pic',
      [
        {
          text: 'No',
          onPress: () => console.log('Permission denied'),
          style: 'cancel',
        },
        this.state.photoPermission == 'undetermined'
          ? { text: 'OK', onPress: this._requestPermission }
          : { text: 'Open Settings', onPress: Permissions.openSettings },
      ],
    )
  }


 

  // Fetch the token from storage then navigate to our appropriate place
  _bootstrapAsync = async () => {
    const userToken = await AsyncStorage.getItem('TOKEN');

    // This will switch to the App screen or Auth screen and this loading
    // screen will be unmounted and thrown away.
    this.props.navigation.navigate(userToken ? 'App' : 'Auth');
  };

  // Render any loading content that you like here
  render() {
    return (
      <View style={styles.container}>
        <StatusBar barStyle="default" />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

const AppStack = StackNavigator({
    Tabmenu: { screen: Tabmenu},
    
},
{
  headerMode: 'none',
  navigationOptions: {
    headerVisible: false,
  }
 });

const AuthStack = StackNavigator({ Login: Login , Register: Register, ForgetPassword: ForgetPassword, IASjuniorMain: IASjuniorMain});

export default SwitchNavigator(
  {
    AuthLoading: AuthLoadingScreen,
    App: AppStack,
    Auth: AuthStack,
  },
  {
    initialRouteName: 'AuthLoading',
  }
);

