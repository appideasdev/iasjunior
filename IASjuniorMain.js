import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  Button,
  WebView,
  ImageBackground,
  AsyncStorage
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import RF from "react-native-responsive-fontsize";

class IASjuniorMain extends Component {

  /* static navigationOptions = {
    title: 'IAS junior',
    headerStyle: { backgroundColor:'#36404a' },
    headerTitleStyle: { color: '#FFFFFF' },
    headerTintColor: 'white',
  }*/

  
  static navigationOptions = ({ navigation, screenProps }) => ({
    title: 'IAS junior',
    headerStyle: { backgroundColor:'#36404a' },
    headerTitleStyle: { color: '#FFFFFF' },
    headerTintColor: 'white',
    //headerLeft: <Icon name={'arrow-left'} size={18} style={{marginLeft:wp('5%')}} color="#FFFFFF"  onPress={ () => { navigation.navigate('Auth')}} />,
  
  });
     


  constructor(props) {
   
    super(props);
   
  }


  
  

  render () {
    return (
          <View style={{flex: 1,widthwp:wp('100%')}}>
                  <WebView
                    useWebKit={true}
                    ref="webview"
                    style={{width: '100%'}}
                    automaticallyAdjustContentInsets={false}
                    source={{uri: 'https://www.iasjunior.com/'}}
                    javaScriptEnabled={true}
                    domStorageEnabled={true}
                    decelerationRate="normal"
                    startInLoadingState={true}
                  />   
          </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center'
  }
});

export default IASjuniorMain;