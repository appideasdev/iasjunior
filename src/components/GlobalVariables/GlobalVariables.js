import React, { Component } from 'react';


export default class GlobalVariables extends Component<Props> {

    
    static Api = {

      BaseUrl                   :   'https://mentorship.iasjunior.com/student/',
      Login                     :   'api/login',
      Signup                    :   'api/signup',
      ForgotPassword            :   'api/forget-password',
      Verification              :   'api/re-verify-email',
      Profile                   :   'api/student-profile',
      Schedule                                    :   'api/schedule',
      Updates                   :   'api/updates',
      GetUpdatesById                              :   'api/updates/',
      UpdatesFile                           :   'https://mentorship.iasjunior.com/admin/assets/update/',
      Motivation                            :   'api/motivation',
      MotivationFile                       :   'https://mentorship.iasjunior.com/admin/assets/motivation/',
      CurrentAffairs                       :   'api/current-affairs',
      CurrentAffairsFile                 :   'https://mentorship.iasjunior.com/admin/assets/content/',
      PracticeModules                  :   'api/practice-modules',
      PracticeTest                       :   'api/practice-test/',
      AssignedModules                  :   'api/assigned-modules/',
      ModuleContent              :   'api/modules-content/',
      Content                    :   'api/content/',
      UPSCStudyMaterial          :   '/api/study-materials',     
      Results                    :   'api/result',
      Dashboard                  :   'api/dashboard',
      Quotes                     :   'api/quotes',
      ChangePassword             :   'api/change-password',
      Chatting                   :   'api/chatting',
      Payment                    :   'api/payment',

      //Details
      StudentProfile             :    'api/student-profile',
      EditStudentProfile         :    'api/student-edit-profile',
      UpdateStudentImage         :    'api/student-update-image',

      //FAQ
      Chatting                   :    'api/chatting',
      SendFaqApi                    :  'api/faq',

      //SignUp
      SignUp                    : 'api/signup',




      InsertPrelimQuestions      :   'api/insert-prelim-question',
      StartPrelimTest            :   'api/start-prelim-test',
      StartPrelimReviews         :   'api/start-prelim-review',
      UpdatePrelimAnswers        :   'api/update-prelim-answer',
      DownloadPrelimPdf          :   'https://mentorship.iasjunior.com/student/api/DownloadApi/prelimQuizPdf/',
     

      InsertPracticeQuestions      :   'api/insert-practice-question',
      StartPracticeTest            :   'api/start-practice-test',
      StartPracticeReviews         :   'api/start-practice-review',
      UpdatePracticeAnswers        :   'api/update-practice-answer',
      DownloadPracticePDF          :   'https://mentorship.iasjunior.com/student/api/practice-test-download/',

      UpdateReview               :   'api/update-review',

      InsertMainsQuestions         :   'api/insert-mains-question',
      StartMainsTest               :   'api/start-mains-test',
      StartMainsReviews            :   'api/start-mains-review',
      UpdateMainsAnswers           :   'api/update-mains-answer',
      UpdateMainsSingleFileAnswers :   'api/update-mains-single-file',
      UpdateMainsFileSpecificAnswers       :   'api/update-mains-file',
      DownloadMainsPdf              :  'https://mentorship.iasjunior.com/student/api/DownloadApi/moduleMainTestPdf/',
      UpdateMainsFile               :  'https://mentorship.iasjunior.com/student/api/update-mains-file',

      MockPrelims                    :   'api/mock-prelims',
      InsertPrelimFullQuestions      : 'api/insert-full-prelim-question',
      GetPrelimFullQuestions         : 'api/start-full-prelim-test',
      GetPrelimFullReviews           : 'api/start-full-prelim-review',
      MockPrelimPdf                  : 'https://mentorship.iasjunior.com/student/api/DownloadApi/mockPrelimPdf/',

      MockMain                      :  'api/mock-mains',
      InsertMockMainsFullQuestions      :'api/insert-full-mains-question',
      GetMockMainsFullQuestions         : 'api/start-full-mains-test',
      GetMockMainsFullReviews           : 'api/start-full-mains-review',
      MockMainPDF                       : 'https://mentorship.iasjunior.com/student/api/DownloadApi/mockMainTestPdf/',
     
    }

    static getApiUrl() {
      return this.Api;
    } 
}

