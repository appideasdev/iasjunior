import PropTypes from 'prop-types';
import React, {Component} from 'react';
import styles from './SideMenu.style';
import {NavigationActions} from 'react-navigation';
import {
  ScrollView, 
  Text, 
  View,
  TouchableOpacity,
  Alert,
  AsyncStorage,
  ImageBackground,
  Image
} from 'react-native';
import { StackNavigator } from 'react-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import RF from "react-native-responsive-fontsize";
import renderIf from './../../../renderIf.js';


class SideMenu extends Component {
  navigateToScreen = (route) => () => {
    const navigateAction = NavigationActions.navigate({
      routeName: route
    });
    this.props.navigation.dispatch(navigateAction);
  }

  constructor(props){
        
        super(props);

        this.state = {
            showIAS : false
        }

      
    }


  _signOutAsync(){

    //AsyncStorage.clear();
    let keys = ['TOKEN', 'student_id'];
    AsyncStorage.multiRemove(keys, (err) => {
     
      AsyncStorage.removeItem('TOKEN');
      AsyncStorage.removeItem('student_id');
      this.props.navigation.navigate('Auth');
    });
    
  };

  _changeIASstate(){

      this.setState({showIAS: !this.state.showIAS})
  }

  render () {
    return (
      <View style={styles.container}>
      

          <View style={{height:hp('10%'), justifyContent:'center', backgroundColor:'#1f2f46'}}>
           
              <View style={{flex:1, justifyContent:'center', }} >
                  <Image style={{height:RF(8), width:RF(20),alignSelf:'center',}} source={require('./../../assets/images/global/logo.png')}/>
              </View>
          </View>
       

        <View style={{backgroundColor:'#36404a', height:hp('86%')}}>

        <ScrollView>
          <TouchableOpacity onPress={() =>  {this.props.navigation.navigate('First', { isRefresh: 1 }); this.props.navigation.closeDrawer()}}>
          <View style={styles.innercontainer}>
            <Image style={{height:RF(4), width:RF(4),alignSelf:'center',color:'#FFFFFF'}} source={require('./../../assets/images/menuicons/dashboardnew.png')}/>
            <Text style={{marginLeft:10, color:'#FFFFFF', fontSize:RF(3)}}>
              Dashboard
            </Text>
          </View>
          </TouchableOpacity>

          <View  style={styles.line}/>

          <TouchableOpacity onPress={() =>  this.props.navigation.navigate('Schedule')}>
          <View style={styles.innercontainer}>
            <Image style={{height:RF(4), width:RF(4),alignSelf:'center',}} source={require('./../../assets/images/menuicons/schedulewhiteicon.png')}/>
            <Text style={{marginLeft:10, color:'#FFFFFF', fontSize:RF(3)}}>
              Schedule
            </Text>
          </View>
          </TouchableOpacity>

          <View  style={styles.line}/>

          <TouchableOpacity onPress={() =>  this.props.navigation.navigate('Updates')}>
          <View style={styles.innercontainer}> 
            <Image style={{height:RF(4), width:RF(4),alignSelf:'center',}} source={require('./../../assets/images/menuicons/updateicon.png')}/>
            <Text style={{marginLeft:10, color:'#FFFFFF', fontSize:RF(3)}}>
              Updates
            </Text>
          </View>
          </TouchableOpacity>

          <View  style={styles.line}/>

          <TouchableOpacity onPress={() =>  this.props.navigation.navigate('Motivation')}>
          <View style={styles.innercontainer}>
            <Image style={{height:RF(4), width:RF(4),alignSelf:'center',}} source={require('./../../assets/images/menuicons/motivation.png')}/>
            <Text style={{marginLeft:10, color:'#FFFFFF', fontSize:RF(3)}}>
              Motivation
            </Text>
          </View>
          </TouchableOpacity>

          <View  style={styles.line}/>

          <TouchableOpacity  onPress={() =>  this.props.navigation.navigate('UPSCSolutions')}>
          <View style={styles.innercontainer}>
            <Image style={{height:RF(4), width:RF(4),alignSelf:'center',}} source={require('./../../assets/images/menuicons/solutions.png')}/>
            <Text style={{marginLeft:10, color:'#FFFFFF', fontSize:RF(3)}}>
              UPSC QPs and Solutions
            </Text>
          </View>
          </TouchableOpacity>

          <View  style={styles.line}/>

          <TouchableOpacity onPress={() =>  this.props.navigation.navigate('CurrentAffairs')}>
          <View style={styles.innercontainer}>
            <Image style={{height:RF(4), width:RF(4),alignSelf:'center',}} source={require('./../../assets/images/menuicons/currentaffairicon.png')}/>
            <Text style={{marginLeft:10, color:'#FFFFFF', fontSize:RF(3)}}>
              Current Affairs
            </Text>
          </View>
          </TouchableOpacity>

          <View  style={styles.line}/>

          <TouchableOpacity onPress={() =>  this.props.navigation.navigate('PracticeModules')}>
          <View style={styles.innercontainer}>
            <Image style={{height:RF(4), width:RF(4),alignSelf:'center',}} source={require('./../../assets/images/menuicons/practiceicon.png')}/>
            <Text style={{marginLeft:10, color:'#FFFFFF', fontSize:RF(3)}}>
              Practice MCQ` s
            </Text>
          </View>
          </TouchableOpacity>

          <View  style={styles.line}/>

          <TouchableOpacity onPress={() =>  this.props.navigation.navigate('AssignedModules')}>
          <View style={styles.innercontainer}>
            <Image style={{height:RF(4), width:RF(4),alignSelf:'center',}} source={require('./../../assets/images/menuicons/modulewhite.png')}/>
            <Text style={{marginLeft:10, color:'#FFFFFF', fontSize:RF(3)}}>
              Assigned Modules
            </Text>
          </View>
          </TouchableOpacity>

          <View  style={styles.line}/>


          <TouchableOpacity onPress={() =>  this.props.navigation.navigate('MockprelimsSyllabus')}>
          <View style={styles.innercontainer}>
            <Image style={{height:RF(4), width:RF(4),alignSelf:'center',}} source={require('./../../assets/images/menuicons/syllabusd.png')}/>
            <Text style={{marginLeft:10, color:'#FFFFFF', fontSize:RF(3)}}>
              Full Syllabus Prelims Exam
            </Text>
          </View>
          </TouchableOpacity>

          <View  style={styles.line}/>

          <TouchableOpacity onPress={() =>  this.props.navigation.navigate('MockmainSyllabus')}>
          <View style={styles.innercontainer}>
            <Image style={{height:RF(4), width:RF(4),alignSelf:'center',}} source={require('./../../assets/images/menuicons/syllabusd.png')}/>
            <Text style={{marginLeft:10, color:'#FFFFFF', fontSize:RF(3)}}>
              Full Syllabus Mains Exam
            </Text>
          </View>
          </TouchableOpacity>

          <View  style={styles.line}/>

          
          <TouchableOpacity onPress={() =>  this.props.navigation.navigate('Result')}>
          <View style={styles.innercontainer}>
            <Image style={{height:RF(4), width:RF(4),alignSelf:'center',}} source={require('./../../assets/images/menuicons/resultswhite.png')}/>
            <Text style={{marginLeft:10, color:'#FFFFFF', fontSize:RF(3)}}>
              Result
            </Text>
          </View>
          </TouchableOpacity>

          <View  style={styles.line}/>


        

          <View style={{flexDirection:'column', marginLeft: 20,marginTop: hp('2%')}}>
              
              <TouchableOpacity onPress={this._changeIASstate.bind(this)}>

                  <View style={{flex:1, flexDirection:'row'}}>
                      <View>
                          <Image style={{height:RF(4), width:RF(4),alignSelf:'center',}} source={require('./../../assets/images/menuicons/currentaffairicon.png')}/>
                      </View>

                      <View>
                        <Text style={{marginLeft:10, color:'#FFFFFF', fontSize:RF(3)}}>
                          IAS Junior
                        </Text>
                      </View>

                      <Image style={{height:RF(4), width:RF(4),marginLeft:wp('40%'),}} source={require('./../../assets/images/menuicons/chevron_right_white.png')}/>
                    
                  </View>

              </TouchableOpacity>

              { renderIf(this.state.showIAS)(
                  <View>
                    
                    <TouchableOpacity onPress={() =>  this.props.navigation.navigate('IASjunior',{ param: 'First' })}>
                        <View style={styles.innercontainer}>
                          <Image style={{height:RF(4), width:RF(4),alignSelf:'center',}} source={require('./../../assets/images/menuicons/currentaffairicon.png')}/>
                          <Text style={{marginLeft:10, color:'#FFFFFF', fontSize:RF(3)}}>
                            IAS Junior
                          </Text>
                        </View>
                        </TouchableOpacity>

                    <View  style={styles.line}/>
                    
                    <TouchableOpacity onPress={() =>  this.props.navigation.navigate('OnlineMentorship')}>
                        <View style={styles.innercontainer}>
                          <Image style={{height:RF(4), width:RF(4),alignSelf:'center',}} source={require('./../../assets/images/menuicons/onlinementorship.png')}/>
                          <Text style={{marginLeft:10, color:'#FFFFFF', fontSize:RF(3)}}>
                            Online Mentorship
                          </Text>
                        </View>
                    </TouchableOpacity>

                    <View  style={styles.line}/>

                    <TouchableOpacity onPress={() =>  this.props.navigation.navigate('UPSC')}>
                        <View style={styles.innercontainer}>
                          <Image style={{height:RF(4), width:RF(4),alignSelf:'center',}} source={require('./../../assets/images/menuicons/upscwhite.png')}/>
                          <Text style={{marginLeft:10, color:'#FFFFFF', fontSize:RF(3)}}>
                            UPSC
                          </Text>
                        </View>
                    </TouchableOpacity>

                    <View  style={styles.line}/>

                    <TouchableOpacity onPress={() =>  this.props.navigation.navigate('StudyMaterial')}>
                    <View style={styles.innercontainer}>
                      <Image style={{height:RF(4), width:RF(4),alignSelf:'center',}} source={require('./../../assets/images/menuicons/studymaterials.png')}/>
                      <Text style={{marginLeft:10, color:'#FFFFFF', fontSize:RF(3)}}>
                        Study Materials
                      </Text>
                    </View>
                    </TouchableOpacity>

                  </View>  
              )}

                        
          </View>

          
          <View  style={styles.line}/>

          <TouchableOpacity onPress={() =>  this.props.navigation.navigate('ChangePassword')}>
          <View style={styles.innercontainer}>
            <Image style={{height:RF(4), width:RF(4),alignSelf:'center',}} source={require('./../../assets/images/menuicons/changepassword.png')}/>
            <Text style={{marginLeft:10, color:'#FFFFFF', fontSize:RF(3)}}>
              Change Password
            </Text>
          </View>
          </TouchableOpacity>

          <View  style={styles.line}/>

          <TouchableOpacity onPress={() =>  this.props.navigation.navigate('FAQ')}>
          <View>
          <View style={styles.innercontainer}>
            <Image style={{height:RF(4), width:RF(4),alignSelf:'center',}} source={require('./../../assets/images/menuicons/ask.png')}/>
            <Text style={{marginLeft:10, color:'#FFFFFF', fontSize:RF(3)}}>
              Ask Query
            </Text>
          </View>
          </View>
          </TouchableOpacity>

          <View  style={styles.line}/>

          <TouchableOpacity onPress={this._signOutAsync.bind(this)}>
            
              <View style={styles.innercontainer}>
                <Image style={{height:RF(4), width:RF(4),alignSelf:'center',}} source={require('./../../assets/images/menuicons/logoutwhite.png')}/>
                <Text style={{marginLeft:20, color:'#FFFFFF', fontSize:RF(3)}}>
                  Logout
                </Text>
              </View>
            
          </TouchableOpacity>

        <View  style={styles.line}/>

        </ScrollView>
        </View>
        
        
      </View>
    );
  }
}

SideMenu.propTypes = {
  navigation: PropTypes.object
};

export default SideMenu;