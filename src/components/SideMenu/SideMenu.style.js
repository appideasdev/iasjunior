import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import RF from "react-native-responsive-fontsize";



export default {
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#1f2f46',
    borderWidth :0
  },
  navItemStyle: {
    padding: 10
  },
  navSectionStyle: {
    backgroundColor: '#1f2f46'
  },
  sectionHeadingStyle: {
    paddingVertical: 10,
    paddingHorizontal: 5
  },
  footerContainer: {
    padding: 20,
    //backgroundColor: '#FFFFFF'
  },
  innercontainer: {
    flex: 1,
    flexDirection: 'row',
    marginLeft: 20,
    marginTop: hp('2%')
  },
  line : { flex:1, borderBottomColor: '#182430', borderBottomWidth: 1, marginTop: hp('1%') },
  
};