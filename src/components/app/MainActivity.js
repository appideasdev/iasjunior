import React, { Component } from 'react';
 
import { 
  StyleSheet, 
  View, 
  Text, 
  YellowBox, 
  TouchableOpacity, 
  AsyncStorage,
  Image,
  StatusBar,
  ScrollView,
  WebView,
  ActivityIndicator,
  FlatList,
  Dimensions,
  RefreshControl,
  Alert
 } from 'react-native';
 

import MarqueeText from 'react-native-marquee';

import { StackNavigator } from 'react-navigation';
import IOSIcon from "react-native-vector-icons/Ionicons";
import Icon from 'react-native-vector-icons/FontAwesome';
import { PieChart,ProgressCircle } from 'react-native-svg-charts'

import GlobalVariables from './../GlobalVariables/GlobalVariables.js';
import styles from './../Styles/GlobalStyle.js';
import Pdf from 'react-native-pdf';
import RNFetchBlob from 'rn-fetch-blob';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import RF from "react-native-responsive-fontsize";


import BottomNavigation, {
  FullTab
} from 'react-native-material-bottom-navigation'

import Video from 'react-native-video';
import MediaControls, { PLAYER_STATES } from 'react-native-media-controls';
import HTML from 'react-native-render-html';
import HTMLView from 'react-native-htmlview';


import Quotes from "../Quotes.js";
import renderIf from './../../../renderIf'

class MainActivity extends Component {


    constructor(props) {

     
      super(props);
     
      this.state = {
        TOKEN     : null,
        Profile   : null,
        isLoading : true,
        avatarSource : null,
        quotesSlug   : null,
        currentTime: 0,
        duration: 0,
        isFullScreen: false,
        isLoading: true,
        paused: true,
        playerState: 1,
        needToPay  : null,
      };

      AsyncStorage.getItem('TOKEN').then((data) => { 
          
         this.setState({TOKEN:data});

         this.props.navigation.navigate(data ? 'App' : 'Auth');

      });

      AsyncStorage.getItem('quotesSlug').then((data) => { 

          this.setState({quotesSlug : data });

      });
    }

    componentDidMount = async () => {

          var GVar = GlobalVariables.getApiUrl();

          AsyncStorage.getItem('TOKEN').then((data) => { 

              this.setState({TOKEN : data });
                
              fetch( GVar.BaseUrl + GVar.Profile,{
                  method: 'POST',
                  headers:{
                              'token' : data
                          },
                  }).then((response) => response.text())
                 .then((responseJson) => {

                  this.setState({isloading : false });
                  
                  var x = JSON.parse(responseJson.replace("-", ""));

                  if( x.status == 1 ){

                      var Profile = x.profile;
                      console.log(Profile,"ProfileProfile")

                      if( Profile.student_photo == null){
                            
                            this.setState({ student_photo : '15385388885bb43d888ade6_thumb.png'})
                      }else{
                        
                            this.setState({ student_photo :  Profile.student_photo })
                      }




                      AsyncStorage.multiSet([
                            ["student_id",            Profile.student_id],
                            ["student_name",          Profile.student_name],
                            ["student_email",         Profile.student_email],
                            //["student_image",         Profile.student_photo],
                        ]);

                      AsyncStorage.setItem('student_image', this.state.student_photo);

                      this.setState({student_id:Profile.student_id})
                      this.setState({student_email:Profile.student_email})
                      this.setState({student_name:Profile.student_name})

                      this.setState({needToPay: x.needToPay})

                      
                      
                      if( Profile.verified_email_address == 0 ){

                          AsyncStorage.getItem('verifymessage').then((data) => { 

                              this.setState({verifymessage : data });
                              this.setState({isverfied : false });
                              this.setState({showverification : true });
                          });
                      }else{

                        this.setState({isverfied : true });
                        this.setState({showverification : false });
                      }

                      let formdata = new FormData();

                      formdata.append('student_id', Profile.student_id ); 
                      
                      

                      fetch( GVar.BaseUrl + GVar.Dashboard,{
                          method: 'POST',
                          headers:{
                                      'token' : data
                                  },
                          body: formdata,          
                          }).then((response) => response.text())
                         .then((responseJson) => {

                          this.setState({isLoading : false });
                          
                          var x = JSON.parse(responseJson.replace("-", ""));

                          this.setState({status : x.status });

                          var dashboard = x.dashboard;
                          if( x.status == 1 ){

                            console.log("DASHBOARD",x.dashboard.marquee.marquee_text)

                            var moduleCompleted = dashboard.completed;
                            var moduleProgress  = dashboard.progress;
                            var modulePending   = dashboard.pending;

                            var allSubject      = dashboard.allSubject;
                            var assignedModulesScores = dashboard.assignedModules;

                            var leader = dashboard.leader;


                            this.setState({marquee_text : dashboard.marquee.marquee_text });
                            this.setState({moduleCompleted : dashboard.completed});
                            this.setState({moduleProgress : dashboard.progress});
                            this.setState({modulePending : dashboard.pending});
                            this.setState({allSubject : dashboard.allSubject});
                            this.setState({assignedModulesScores : dashboard.assignedModules});

                            str = dashboard.leader.leaderboard;

                            str = str.replace(/\\'/g, '\'');
                            str = str.replace(/\\"/g, '"');
                            str = str.replace(/\\0/g, '\0');
                            str = str.replace(/\\\\/g, '\\');

                            this.setState({leader : str});

                            

                          this.setState({data : data})
                        }

                          
                      }).catch((error)=>{ Alert.alert(error)});

               
                  }else{
                      this.props.navigation.navigate('Login');
                  }
              }).catch((error)=>{ Alert.alert(error)});
          });
    }

    componentWillReceiveProps(nextProps) {

      this.props = nextProps;
      console.log(nextProps,"nextProps");
      if (nextProps.navigation.state.params.isRefresh) {
        //this.props.retrieveCurrentUser(this.props.token);
          var GVar = GlobalVariables.getApiUrl();

          AsyncStorage.getItem('TOKEN').then((data) => { 

              this.setState({TOKEN : data });
                
              fetch( GVar.BaseUrl + GVar.Profile,{
                  method: 'POST',
                  headers:{
                              'token' : data
                          },
                  }).then((response) => response.text())
                 .then((responseJson) => {

                  this.setState({isloading : false });
                  
                  var x = JSON.parse(responseJson.replace("-", ""));

                  if( x.status == 1 ){

                      var Profile = x.profile;
                      console.log(Profile,"ProfileProfile")

                      if( Profile.student_photo == null){
                            
                            this.setState({ student_photo : '15385388885bb43d888ade6_thumb.png'})
                      }else{
                        
                            this.setState({ student_photo :  Profile.student_photo })
                      }




                      AsyncStorage.multiSet([
                            ["student_id",            Profile.student_id],
                            ["student_name",          Profile.student_name],
                            ["student_email",         Profile.student_email],
                            //["student_image",         Profile.student_photo],
                        ]);

                      this.setState({student_id:Profile.student_id})
                      this.setState({student_email:Profile.student_email})
                      this.setState({student_name:Profile.student_name})

                      this.setState({needToPay: x.needToPay})

                      
                      if( Profile.verified_email_address == 0 ){

                          AsyncStorage.getItem('verifymessage').then((data) => { 

                              this.setState({verifymessage : data });
                              this.setState({isverfied : false });
                              this.setState({showverification : true });
                          });
                      }else{

                        this.setState({isverfied : true });
                        this.setState({showverification : false });
                      }

                      let formdata = new FormData();

                      formdata.append('student_id', Profile.student_id ); 
                      
                      

                      fetch( GVar.BaseUrl + GVar.Dashboard,{
                          method: 'POST',
                          headers:{
                                      'token' : data
                                  },
                          body: formdata,          
                          }).then((response) => response.text())
                         .then((responseJson) => {

                          this.setState({isLoading : false });
                          
                          var x = JSON.parse(responseJson.replace("-", ""));

                          this.setState({status : x.status });

                          var dashboard = x.dashboard;
                          if( x.status == 1 ){

                            var moduleCompleted = dashboard.completed;
                            var moduleProgress  = dashboard.progress;
                            var modulePending   = dashboard.pending;

                            var allSubject      = dashboard.allSubject;
                            var assignedModulesScores = dashboard.assignedModules;

                            var leader = dashboard.leader;


                            this.setState({moduleCompleted : dashboard.completed});
                            this.setState({moduleProgress : dashboard.progress});
                            this.setState({modulePending : dashboard.pending});
                            this.setState({allSubject : dashboard.allSubject});
                            this.setState({assignedModulesScores : dashboard.assignedModules});

                            str = dashboard.leader.leaderboard;

                            str = str.replace(/\\'/g, '\'');
                            str = str.replace(/\\"/g, '"');
                            str = str.replace(/\\0/g, '\0');
                            str = str.replace(/\\\\/g, '\\');

                            this.setState({leader : str});

                            

                          this.setState({data : data})
                        }

                          
                      }).catch((error)=>{ Alert.alert(error)});

               
                  }else{
                      this.props.navigation.navigate('Login');
                  }
              }).catch((error)=>{ Alert.alert(error)});
          });
      }
    }

    /*static getDerivedStateFromProps(nextProps, prevState) {
      
      console.log(nextProps,"nextProps");
      console.log(prevState,"prevState");


    }*/


    verfication(){

      var student_email = this.state.student_email;
      this.setState({isloading : true });
    
      if( student_email ){

          var GVar = GlobalVariables.getApiUrl();

          let formdata = new FormData();

          formdata.append('student_email',        student_email );
         
          fetch( GVar.BaseUrl + GVar.Verification,{
                      method: 'POST',
                      body: formdata,        
                      }).then((response) => response.text())
                     .then((responseJson) => {

                      this.setState({isloading : false });
                      //this.setState({showverification : false })

                      console.log(responseJson,"responseJsonresponseJson")
                     
                      
                      var x = JSON.parse(responseJson.replace("-", ""));
                       
                      alert(x.message);  

                     
                     }).catch((error)=>{ Alert.alert(error)});

      }
    
    }  


    data(){

        var completed   = 0;
        var incomplete  = 0;
        var inprogress  = 0;

        var scompleted  = this.state.moduleCompleted; // yellow
        var spending    = this.state.modulePending;   // red
        var sprogress   = this.state.moduleProgress;  // green

        console.log(scompleted,"scompleted");
        console.log(spending,"spending");
        console.log(sprogress,"sprogress");
    
        if( scompleted == undefined || scompleted == 0){
          completed = 0
        }else{

          completed = 100 / scompleted;
        }

        if( spending == undefined || spending == 0 ){
          incomplete = 0
        }else{

           incomplete = 100 / spending;
        }

        if( sprogress == undefined || sprogress == 0 ){
          
          inprogress = 0
          
        }else{

          inprogress = 100 / sprogress ;
        }

      
        console.log(completed,"completed");
        console.log(incomplete,"incomplete");
        console.log(inprogress,"inprogress");


        return  data = [
                                  {
                                      key: 1,
                                      amount: completed,
                                      svg: { fill: '#FFFF00' },
                                  },
                                  {
                                      key: 2,
                                      amount: incomplete,
                                      svg: { fill: '#FF0000' }
                                  },
                                  {
                                      key: 3,
                                      amount: inprogress,
                                      svg: { fill: '#008000' }
                                  },
                              ]

    }
    

    onSeek = seek => {
      this.videoPlayer.seek(seek);
    };

    onPaused = playerState => {
      this.setState({
        paused: !this.state.paused,
        playerState,
      });
    };

    onReplay = () => {
      this.setState({ playerState: PLAYER_STATES.PLAYING });
      this.videoPlayer.seek(0);
    };

    onProgress = data => {
      const { isLoading, playerState } = this.state;
      // Video Player will continue progress even if the video already ended
      if (!isLoading && playerState !== PLAYER_STATES.ENDED) {
        this.setState({ currentTime: data.currentTime });
      }
    };

    onLoad = data => this.setState({ duration: data.duration, isLoading: false });

    onLoadStart = data => this.setState({ isLoading: true });

    onEnd = () => this.setState({ playerState: PLAYER_STATES.ENDED });

    onError = () => alert('Oh! ', error);

    exitFullScreen = () => {};

    enterFullScreen = () => {};

    onFullScreen = () => {};
    
    onSeeking = currentTime => this.setState({ currentTime });

 
    progressbar( percentage ){

      var temppercaentage = Math.ceil(percentage)

      var npercentage     = temppercaentage % 10;

      console.log(npercentage,"npercentage");
      
      return  <ProgressCircle
                  style={ { height: 200, marginBottom:5 } }
                  progress={ npercentage }
                  progressColor={ 'rgb(134, 65, 244)' }
                  { ...this.props }
              />;
    }


    _onRefresh = () => {

      console.log("yesitisworking");

      console.log("ALLSTATES",this.state)
      this.state = this.state


      this.setState({refreshing: true});
      
      var GVar = GlobalVariables.getApiUrl();

       AsyncStorage.getItem('TOKEN').then((data) => { 

                this.setState({TOKEN : data });
                  
                fetch( GVar.BaseUrl + GVar.Profile,{
                    method: 'POST',
                    headers:{
                                'token' : data
                            },
                    }).then((response) => response.text())
                   .then((responseJson) => {

                    this.setState({isloading : false });
                    
                    var x = JSON.parse(responseJson.replace("-", ""));

                    if( x.status == 1 ){

                        var Profile = x.profile;
                        console.log(Profile,"ProfileProfile")

                        if( Profile.student_photo == null){
                              
                              this.setState({ student_photo : '15385388885bb43d888ade6_thumb.png'})
                        }else{
                          
                              this.setState({ student_photo :  Profile.student_photo })
                        }




                        AsyncStorage.multiSet([
                              ["student_id",            Profile.student_id],
                              ["student_name",          Profile.student_name],
                              ["student_email",         Profile.student_email],
                              //["student_image",         Profile.student_photo],
                          ]);

                        this.setState({student_id:Profile.student_id})
                        this.setState({student_email:Profile.student_email})
                        this.setState({student_name:Profile.student_name})

                        this.setState({needToPay: x.needToPay})

                        
                        if( Profile.verified_email_address == 0 ){

                            AsyncStorage.getItem('verifymessage').then((data) => { 

                                this.setState({verifymessage : data });
                                this.setState({isverfied : false });
                                this.setState({showverification : true });
                            });
                        }else{

                          this.setState({isverfied : true });
                          this.setState({showverification : false });
                        }

                        let formdata = new FormData();

                        formdata.append('student_id', Profile.student_id ); 
                        
                        

                        fetch( GVar.BaseUrl + GVar.Dashboard,{
                            method: 'POST',
                            headers:{
                                        'token' : data
                                    },
                            body: formdata,          
                            }).then((response) => response.text())
                           .then((responseJson) => {

                            this.setState({isLoading : false });
                            
                            var x = JSON.parse(responseJson.replace("-", ""));

                            this.setState({status : x.status });

                            var dashboard = x.dashboard;
                            if( x.status == 1 ){

                              var moduleCompleted = dashboard.completed;
                              var moduleProgress  = dashboard.progress;
                              var modulePending   = dashboard.pending;

                              var allSubject      = dashboard.allSubject;
                              var assignedModulesScores = dashboard.assignedModules;

                              var leader = dashboard.leader;


                              this.setState({moduleCompleted : dashboard.completed});
                              this.setState({moduleProgress : dashboard.progress});
                              this.setState({modulePending : dashboard.pending});
                              this.setState({allSubject : dashboard.allSubject});
                              this.setState({assignedModulesScores : dashboard.assignedModules});

                              str = dashboard.leader.leaderboard;

                              str = str.replace(/\\'/g, '\'');
                              str = str.replace(/\\"/g, '"');
                              str = str.replace(/\\0/g, '\0');
                              str = str.replace(/\\\\/g, '\\');

                              this.setState({leader : str});

                              

                            this.setState({data : data})
                          }

                            
                        }).catch((error)=>{ Alert.alert(error)});

                 
                    }else{
                        this.props.navigation.navigate('Login');
                    }
                }).catch((error)=>{ Alert.alert(error)});
            });    

      this.setState({refreshing: false});

    }

   
    render() {

   
        
        const Labels = ({ slices, height, width }) => {
            return slices.map((slice, index) => {
                const { labelCentroid, pieCentroid, data } = slice;
                return (
                    <Text
                        key={index}
                        x={pieCentroid[ 0 ]}
                        y={pieCentroid[ 1 ]}
                        fill={'white'}
                        textAnchor={'middle'}
                        alignmentBaseline={'middle'}
                        fontSize={24}
                        stroke={'black'}
                        strokeWidth={0.2}
                    >
                        {data.amount}
                    </Text>
                )
            })
        }


        return (
          <View style={{flex: 1, width:wp('100%')}}>

                <ScrollView 
                  refreshControl={
                      <RefreshControl
                        refreshing={this.state.refreshing}
                        onRefresh={this._onRefresh}
                      />
                    }> 

                        <HTML html={'<iframe  height="250"  src="https://www.youtube.com/embed/dSvQxPPWVNI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'} />
                        
                        { this.state.status ? 

                            <View style={{ alignItems:'center', alignself:'center',}}>



                                    { renderIf(this.state.showverification)(
                                      <View style={{ alignItems:'center', alignself:'center',marginTop:hp('2%'),backgroundColor:'#FFFFFF', width:wp('95%')}}>
                                      
                                              <View style={{width: wp('90%'), alignSelf:'center', alignItems:'center'}} >
                                                  <TouchableOpacity onPress={this.verfication.bind(this)}>
                                                          <Text style={{fontSize: RF(2.5),fontWeight: 'bold',alignSelf:'center',color:'red',alignSelf:'center'}}>Your email address is not verified yet.</Text>
                                                          <Text style={{fontSize: RF(2.5),fontWeight: 'bold',alignSelf:'center',color:'#1ab394',alignSelf:'center'}}> Click  here to resend the verfication link again. </Text>
                                                          <Text style={{fontSize: RF(2.5),fontWeight: 'bold',alignSelf:'center',color:'red', alignSelf:'center'}}>You will not receive any important updates or score evaluation  notification if the email address is not verified..</Text>
                                                  </TouchableOpacity>
                                              </View>
                                      </View>        
                                      )
                                    }
                                      
                                    
                                    <View style={{ alignItems:'center', alignself:'center',marginTop:hp('2%'),backgroundColor:'#FFFFFF', width:wp('95%')}}>
                                        <View style={{flex:1, flexDirection:'column', width:wp('95%'), alignself:'center',marginBottom:hp('4%')}}>
                                          
                                          <View style={{flex:1,flexDirection:'row', backgroundColor:'#FFFFFF', width:wp('95%'), alignself:'center', marginTop:hp('5%'), alignItems:'center', justifyContent:'center'}}>
                                              <Text style={styles.Showheader}>Assigned Modules </Text>
                                              <View style={{backgroundColor:'#1ab394',fontSize:RF(3.5), color:'#FFFFFF', width:'10%', borderRadius:2,  alignItems:'center', marginTop:hp('1%')}}>
                                                  <Text style={{fontSize:RF(3.5), color:'#FFFFFF',}}>{ this.state.moduleProgress + this.state.moduleCompleted + this.state.modulePending}</Text>
                                              </View>
                                          </View>
                                          <View style={{flex:1,flexDirection:'row', backgroundColor:'#FFFFFF', width:wp('90%'), alignself:'center', marginTop:hp('0%'), justifyContent:'space-between'}}>
                                              <Text style={styles.Show}>In Progress</Text>
                                              <Text style={styles.Show}>{this.state.moduleProgress}</Text>
                                          </View>

                                          <View style={{flex:1,flexDirection:'row', backgroundColor:'#FFFFFF', width:wp('90%'), alignself:'center', marginTop:hp('0%'), justifyContent:'space-between'}}>
                                              <Text style={styles.Show}>Completed</Text>
                                              <Text style={styles.Show}>{this.state.moduleCompleted}</Text>
                                          </View>

                                          <View style={{flex:1,flexDirection:'row', backgroundColor:'#FFFFFF', width:wp('90%'), alignself:'center', marginTop:hp('0%'), justifyContent:'space-between'}}>
                                              <Text style={styles.Show}>Pending</Text>
                                              <Text style={styles.Show}>{this.state.modulePending}</Text>
                                          </View>
                                          <PieChart
                                              style={{ height: 200 }}
                                              valueAccessor={({ item }) => item.amount}
                                              data={this.data()}
                                              spacing={0}
                                              outerRadius={'95%'}
                                          >
                                              <Labels/>
                                          </PieChart>
                                        </View>
                                    </View>

                                    <View style={{ alignItems:'center', alignself:'center',marginTop:hp('2%'),backgroundColor:'#FFFFFF', width:wp('95%')}}>
                                        <View style={{flex:1, flexDirection:'column', width:wp('95%'), alignself:'center',marginBottom:hp('4%')}}>
                                          
                                          <View style={{flex:1,flexDirection:'row', backgroundColor:'#FFFFFF', width:wp('95%'), alignself:'center', marginTop:hp('5%'), alignItems:'center', justifyContent:'center'}}>
                                              <Text style={styles.Showheader}>YOUR SCORE</Text>
                                          </View>
                                          <View style={{flex:1,flexDirection:'row', backgroundColor:'#FFFFFF', width:wp('90%'), alignself:'center', marginTop:hp('5%'), justifyContent:'space-between'}}>
                                              <Text style={styles.Show}>MODULE</Text>
                                              <Text style={styles.Show}>SCORE (%)</Text>
                                          </View>

                                          <View style={{flex:1,flexDirection:'row', backgroundColor:'#FFFFFF', width:wp('90%'), alignself:'center', marginTop:hp('0%'), justifyContent:'space-between'}}>
                                              <FlatList
                                                extraData={this.state}
                                                data={this.state.assignedModulesScores}
                                                renderItem={({item}) => 
                                                  <View style={{flex:1,flexDirection:'row', backgroundColor:'#FFFFFF', width:wp('90%'), alignself:'center', marginTop:hp('1%'), justifyContent:'space-between'}}>
                                                      <Text style={styles.Show}>{item.moduleName}</Text>
                                                      <View style={{backgroundColor:'#1ab394',fontSize:RF(3.5), color:'#FFFFFF', width:'20%', borderRadius:2,  alignItems:'center'}}>
                                                          <Text style={{fontSize:RF(3.5), color:'#FFFFFF',}}>{Math.ceil(item.percentage)}</Text>
                                                      </View>
                                                      
                                                  </View>
                                              }/>
                                          </View>
                                          
                                        </View>
                                    </View>

                                    {this.state.allSubject && this.state.allSubject.length > 0 ? 

                                    <View style={{ alignItems:'center', alignself:'center',marginTop:hp('2%'),backgroundColor:'#FFFFFF', width:wp('95%'), alignItems:'center'}}>
                                        <View style={{flex:1, flexDirection:'column', width:wp('95%'), alignself:'center',marginBottom:hp('4%')}}>
                                          

                                          <View style={{flex:1,flexDirection:'row', backgroundColor:'#FFFFFF', width:wp('95%'), alignself:'center', marginTop:hp('5%'), alignItems:'center', justifyContent:'center'}}>
                                              <Text style={styles.Showheader}>All Subjects</Text>
                                          </View>

                                          <View style={{flex:1,flexDirection:'row', backgroundColor:'#FFFFFF', width:wp('90%'), alignself:'center', marginBottom:hp('2%'), justifyContent:'space-between', alignItems:'center', marginLeft:wp('2.5%')}}>
                                              <FlatList
                                                horizontal
                                                extraData={this.state}
                                                data={this.state.allSubject}
                                                renderItem={({item}) => 

                                                    
                                                    <View style={{flex:1,marginTop:hp('2%'), borderWidth:1,marginLeft:wp('2.5%')}}>
                                                        
                                                        <View style={{flex:1,flexDirection:'row', backgroundColor:'#FFFFFF', width:wp('80%'), alignself:'center', marginTop:hp('1%'), justifyContent:'space-between', }}>
                                                          <Text style={styles.Show}>{item.subjectName}</Text>
                                                          <Text style={styles.Show}>{Math.ceil(item.percentage)} %</Text>
                                                        </View>

                                                        {this.progressbar(item.percentage)}

                                                    </View>

                                                  
                                              }/>
                                            </View>

                                        </View>
                                    </View>
                                    :
                                    <View/>}

                                    

                            </View>
                         
                          :
                          <View style={{ alignItems:'center', alignself:'center',}}>
                            <View style={{flex:1,height:hp('100%'), flexDirection:'row', backgroundColor:'#FFFFFF', width:wp('95%'), alignself:'center', marginTop:hp('5%')}}>
                                <Text style={{fontSize:RF(3.5), color:'#FFFFFF', alignself:'center', marginLeft:wp('2%')}}>{this.state.message}</Text>
                            </View>
                          </View>
                        }

                      
                        {this.state.status ? 

                        <ScrollView>
                          <View style={{flex:1,alignItems:'center', alignself:'center',marginTop:hp('2%'),backgroundColor:'#FFFFFF', width:wp('95%'), justifyContent:'center', marginLeft:wp('2.5%')}}>
                            <View style={{flex:1,flexDirection:'row', backgroundColor:'#FFFFFF',  alignself:'center', marginTop:hp('2%'), alignItems:'center', justifyContent:'center'}}>
                                <Text style={styles.Showheader}>LEADER BOARD</Text>
                            </View>
                            <View style={{flex:1,flexDirection:'row', backgroundColor:'#FFFFFF',  alignself:'center', alignItems:'center', justifyContent:'center'}}>
                                <WebView scalesPageToFit={true} source={{uri: 'https://mentorship.iasjunior.com/student/api/leaderboard/1-'+this.state.student_id}} style={{flex:1, height:hp('100%',)}}/>
                            </View>
                          </View>  
                        </ScrollView>

                        : <View/>}

                              
                        
                                                     
                </ScrollView>
                {this.state.isLoading ? <ActivityIndicator
                    animating     ={true}
                    transparent   ={true}
                    visible       ={false}
                    style         ={styles.indicator}
                    size          ="large"
                  /> : <View/>  } 



                <Quotes/>
          </View>   
        )
    }
}

export default MainActivity;