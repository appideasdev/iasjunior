import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  AsyncStorage,
  Alert,
  TextInput,
  Image,
  StatusBar,
  ImageBackground,
  ScrollView,
  TouchableOpacity,
  ActivityIndicator
} from 'react-native';

import DatePicker from 'react-native-datepicker';
import { Dropdown } from 'react-native-material-dropdown';
import ImagePicker from 'react-native-image-picker';
import { StackNavigator, NavigationActions } from  'react-navigation';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import RF from "react-native-responsive-fontsize";
import Quotes from "../Quotes.js";
import Icon from 'react-native-vector-icons/FontAwesome';

import renderIf from './../../../renderIf'
import { HeaderBackButton } from 'react-navigation';
import GlobalVariables from '../GlobalVariables/GlobalVariables';
import styles from './../Styles/GlobalStyle.js';

class DetailScreen extends Component {

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Profile',
      headerStyle: { backgroundColor:'#36404a' },
      headerTitleStyle: { color: '#FFFFFF' },
      headerTintColor: 'white',
      headerLeft:(<HeaderBackButton tintColor='#FFFFFF' onPress={()=>{navigation.navigate('First');}}/>)
  
  
    };
  };

  constructor(props) {
   
    super(props);
   
    this.state = {
      TOKEN     : null,
      Profile   : null,
      isloading : true,
      avatarSource : null,
      quotesSlug   : null,
    };

    AsyncStorage.getItem('TOKEN').then((data) => { 
        
       this.setState({TOKEN:data});
    });

    AsyncStorage.getItem('student_id').then((data) => { 

        this.setState({student_id : data });

    });

  }


  componentDidMount = async () => {

    var GVar = GlobalVariables.getApiUrl();

      AsyncStorage.getItem('TOKEN').then((data) => { 

          this.setState({TOKEN : data });
        
          fetch( GVar.BaseUrl + GVar.StudentProfile,{
                      method: 'POST',
                      headers:{
                                  'token' : data
                              },
                      }).then((response) => response.text())
                     .then((responseJson) => {

                      this.setState({isloading : false });
                     
                      
                      var x = JSON.parse(responseJson.replace("-", ""));
                      this.setState({isLoading : false });

                      console.log(x,"profile")
                     
                      if( x.status == 1 ){

                          var Profile   = x.profile;

                          this.setState({ name  : Profile.student_name});
                          this.setState({ phone  : Profile.phone});
                          this.setState({ email  : Profile.student_email});
                          this.setState({ student_id  : Profile.student_id});
                       
                        
                          var date0 = new Date(new Date(Profile.subscriptionEndDate * 1000).getTime());
                          newonedays = date0.toGMTString();
                          date = newonedays.split(' ', 4).join(' ');

                          this.setState({ SubscrittionDate  : date});
                          

                         

                          var date7 = new Date(new Date(Profile.subscriptionEndDate * 1000).getTime() - (365*24*60*60*1000));
                          newtwelveMonths = date7.toGMTString();
                          dateprevius = newtwelveMonths.split(' ', 4).join(' ');
                          this.setState({ JoiningDate  : dateprevius});
                         
                          

                          if( Profile.student_photo == null){
                            
                            this.setState({ student_photo : '15385388885bb43d888ade6_thumb.png'})
                          }else{
                            
                            this.setState({ student_photo :  Profile.student_photo })
                          }


                      }else{
                        Alert.alert(x.message)
                      }
                          
                     }).catch((error)=>{ Alert.alert(error)});


      });
  }

    UPDATEPROFILE(){

      var GVar = GlobalVariables.getApiUrl();

        var name                = this.state.name;
        var phone               = this.state.phone;
       
        var token               = this.state.TOKEN;
        var student_id          = this.state.student_id;


        var error = 0;

        var error_message = '';


        if( name == '' ){
          error = error + 1;
          error_message = error_message + '\nPlease enter name.';
         
        }
        
        if( phone == '' ){
          error = error + 1;
          error_message = error_message + '\nPlease enter phone no.';
         
        }

        let formdata = new FormData();

        formdata.append('student_id',  student_id );
        formdata.append('student_name',  name );
        formdata.append('phone',  phone );

        
       
        if( error == 0 ){

        

            let formdata = new FormData();

            formdata.append('student_id',  student_id );
            formdata.append('student_name',  name );
            formdata.append('phone',  phone );
            


            console.log(formdata,"formdata");

            fetch( GVar.BaseUrl + GVar.EditStudentProfile,{
                          method: 'POST',
                          headers:{
                                      'token' : token
                                  },
                          body : formdata,        
                          }).then((response) => response.text())
                         .then((responseJson) => {

                          var x = JSON.parse(responseJson.replace("-", ""));
                          this.setState({isLoading : false });
                          Alert.alert(x.message);
                         
                          
            }).catch((error)=>{ Alert.alert(error)});
        
        }else{
          alert(error_message);
        }
        
    }

    setCamera(){

      var GVar = GlobalVariables.getApiUrl();

          const options = {
            quality: 1.0,
            maxWidth: 500,
            maxHeight: 500,
            storageOptions: {
              skipBackup: true
            }
          };

          ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
              console.log('User cancelled photo picker');
            }
            else if (response.error) {
              console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
              console.log('User tapped custom button: ', response.customButton);
            }
            else {
              let source = { uri: response.uri };

              this.setState({student_image : source});

              this.setState({
                avatarSource: source
              });


                let formdata = new FormData();

                formdata.append('student_id',  this.state.student_id );
                formdata.append('student_photo', {
                  uri: response.uri,
                  type: 'image/jpeg', // or photo.type
                  name: 'testPhotoName'
                });


                console.log(formdata,"formdata");
                console.log(this.state.TOKEN,"token");

                fetch( GVar.BaseUrl + GVar.UpdateStudentImage,{
                              method: 'POST',
                              headers:{
                                          'token' : this.state.TOKEN
                                      },
                              body : formdata,        
                              }).then((response) => response.text())
                             .then((responseJson) => {

                              
                              console.log(responseJson,'responseJson2');
                              var x = JSON.parse(responseJson.replace(/-\s?/g, ""));
                              Alert.alert(x.message);
                             
                              
                }).catch((error)=>{ Alert.alert(error)});
            }
          });
  }



  render () {



    var student = this.state.Profile;

    console.log(this.state.student_photo,"imageimage");

    var student_photo = '15385388885bb43d888ade6_thumb.png';

    if( student == null ){

      student_photo = '15385388885bb43d888ade6_thumb.png';
      
    }else{
      student_photo = student.student_photo;
    }

    //var student_photo = student.student_photo;

    
    return (
      <View style={styles.container}>
      <StatusBar
           backgroundColor="#232A40"
           barStyle="light-content"/>
           
            <View style={{flex:1, backgroundColor:'#FFFFFF'}} pointerEvents={this.state.isloading ? 'none' : 'auto'}>
              <ScrollView>
                  <View style={{flex: 1, flexDirection: 'column', justifyContent: 'space-between', alignItems:'center'}} pointerEvents={this.state.isloading ? 'none' : 'auto'}>
                    
                    <View style={{width:wp('100%'), height:hp('25%'),backgroundColor:'#1ab394', justifyContent:'center', alignItems:'center', marginTop:'0%'}} >

                       
                        <View style={{flexDirection:'column'}}>

                            <TouchableOpacity onPress={this.setCamera.bind(this)}>
                               { this.state.avatarSource === null ? <Image style={{height:75, width:75, alignSelf:'center', marginTop: 0,  borderRadius:37,}} source={{uri: 'http://mentorship.iasjunior.com/student/assets/student/'+this.state.student_photo}}/>
                                 :
                                  <Image style={{height:75, width:75, alignSelf:'center', marginTop: 10,  borderRadius:37,}} source={this.state.avatarSource} />
                                }
                                <Image style={{height:RF(4), width:RF(4),alignSelf:'center',marginTop:hp('-2%'), marginLeft:wp('10%')}} source={require('./../../assets/images/menuicons/camera.png')}/>
                            </TouchableOpacity> 
                            
                        </View>
                        <Text style={{fontSize:15,alignSelf:'center', color:'#FFFFFF'}}>{this.state.email}</Text>
                       
                    </View>


                    

                    <View style={{flex:1, }}>

                        <View style={{flexDirection:'row', marginTop:hp('2%'),justifyContent:'space-between'}} >
                                                          
                                <View style={{flexDirection:'row',}}>
                                    <Text style={{fontSize: RF(2.5),color:'#000000', marginLeft:wp('1%')}}>Joining Date</Text>
                                </View>

                                <View style={{ height: hp('7%'),  alignSelf:'center', }} >
                                      <Text style={{fontSize: RF(2.5),color:'#000000', marginLeft:wp('1%')}}>{this.state.JoiningDate}</Text> 
                                </View>  
                        </View>

                        <View style={{flexDirection:'row', marginTop:hp('2%'),justifyContent:'space-between'}} >
                                                          
                                <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                                    <Text style={{fontSize: RF(2.5),color:'#000000', marginLeft:wp('1%')}}>Subscription End Date</Text>
                                </View>

                                <View style={{ height: hp('7%'),  alignSelf:'center', }} >
                                      <Text style={{fontSize: RF(2.5),color:'#000000', marginLeft:wp('1%')}}>{this.state.SubscrittionDate}</Text> 
                                </View>  
                        </View>


                        <View style={{flexDirection:'column',}} >
                                                          
                                <View style={{flexDirection:'row',}}>
                                    <Text style={{fontSize: RF(2.5),color:'#000000', marginLeft:wp('1%')}}>Name</Text>
                                   
                                </View>

                                <View style={{width: wp('98%'), height: hp('7%'),  alignSelf:'center', borderRadius:wp('2%'), borderWidth:1}} >
                                      <TextInput style={{
                                            TextColor: '#000000',
                                            fontSize: RF(2.5),
                                            marginLeft : wp('2%')
                                        }}
                                        onChangeText={(name) => this.setState({name})}
                                        value={this.state.name}
                                        placeholder={'Name'}
                                        underlineColorAndroid="transparent"
                                        placeholderTextColor= '#2a29c9'
                                        />   
                                </View>  
                        </View>

                        <View style={{flexDirection:'column', marginTop:hp('2%')}} >
                                                          
                                <View style={{flexDirection:'row',}}>
                                    <Text style={{fontSize: RF(2.5),color:'#000000', marginLeft:wp('1%')}}>Phone No</Text>
                                    
                                </View>

                                <View style={{width: wp('98%'), height: hp('7%'),  alignSelf:'center', borderRadius:wp('2%'), borderWidth:1}} >
                                      <TextInput style={{
                                            TextColor: '#000000',
                                            fontSize: RF(2.5),
                                            marginLeft : wp('2%')
                                        }}
                                        onChangeText={(phone) => this.setState({phone})}
                                        value={this.state.phone}
                                        placeholder={'Phone No'}
                                        underlineColorAndroid="transparent"
                                        placeholderTextColor= '#2a29c9'
                                        />   
                                </View>  
                        </View>

                    </View>

                    <View style={{flex:2,flexDirection: 'column',justifyContent: 'space-between',marginTop:'10%', marginBottom:5}} >
                          
                    <View style={{flex:1,width: wp('65%'),justifyContent: 'center', height: hp('6%'), alignSelf:'stretch', marginBottom:'10%'}} >
                    <TouchableOpacity onPress={this.UPDATEPROFILE.bind(this)}>
                    <ImageBackground  imageStyle={{ borderRadius: 10 }} style={{width: wp('40%'),justifyContent: 'center', height: hp('7%'),alignSelf:'center',  borderRadius:wp('50%')}} source={require('./../../assets/images/btn_bg.png')} >
                    <Text style={{ textAlign: 'center',alignSelf:'center',fontWeight: 'bold',fontSize:15, color:'#FFFFFF'}}>UPDATE</Text>
                    </ImageBackground>
                   
                    </TouchableOpacity>
              </View>


                    </View>
                     
                  </View>
              </ScrollView>  
              { this.state.isloading ? <ActivityIndicator
                                      animating     ={true}
                                      transparent   ={true}
                                      visible       ={false}
                                      style         ={styles.indicator}
                                      size          ="large"
                                    /> : <View/>}  
              <Quotes/>       
            </View> 
                  
      </View>
    );
  }
}


export default DetailScreen;