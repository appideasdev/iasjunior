import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  AsyncStorage,
  Alert,
  TextInput,
  Image,
  StatusBar,
  ImageBackground,
  ScrollView,
  TouchableOpacity,
  ActivityIndicator
} from 'react-native';


import GlobalVariables from './../GlobalVariables/GlobalVariables.js';



import DatePicker from 'react-native-datepicker';
import { Dropdown } from 'react-native-material-dropdown';
import ImagePicker from 'react-native-image-picker';
import { StackNavigator, NavigationActions } from  'react-navigation';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import RF from "react-native-responsive-fontsize";
import Quotes from "../Quotes.js";
import Icon from 'react-native-vector-icons/FontAwesome';

import renderIf from './../../../renderIf'
import styles from './../Styles/GlobalStyle.js';

class ChangePassword extends Component {

  static navigationOptions = {
    title: 'Profile',
    headerStyle: { backgroundColor:'#36404a' },
    headerTitleStyle: { color: '#FFFFFF' },
    headerTintColor: 'white',
  }

  constructor(props) {
   
    super(props);
   
    this.state = {
      TOKEN     : null,
      Profile   : null,
      avatarSource : null,
      quotesSlug   : null,
    };

    AsyncStorage.getItem('TOKEN').then((data) => { 
        
       this.setState({TOKEN:data});

      
    });

    AsyncStorage.getItem('student_id').then((data) => { 

        this.setState({student_id : data });
     

    });

  }


 
    UPDATEPROFILE(){
       this.setState({isLoading : true });

        var GVar = GlobalVariables.getApiUrl();

        var Password                = this.state.Password;
        var ConfirmPassword         = this.state.ConfirmPassword;
       
        var token                   = this.state.TOKEN;
        var student_id              = this.state.student_id;


        var error = 0;

        var error_message = '';


        if( Password == undefined ){
          error = error + 1;
          error_message = error_message + '\nPlease enter password.';
         
        }
        
        if( ConfirmPassword == undefined ){
          error = error + 1;
          error_message = error_message + '\nPlease enter confirm password';
         
        }


        if( Password && ConfirmPassword ){
            if( Password != ConfirmPassword ){
              
                error = error + 1;
                error_message = error_message + '\nPassword and Confirm Password should be same';
            }
        }



        if( error == 0 ){

        

            let formdata = new FormData();

            formdata.append('student_id',  student_id );
            formdata.append('password',  Password );
            formdata.append('confirm_password',  ConfirmPassword );
            


            console.log(formdata,"formdata");

            fetch( GVar.BaseUrl + GVar.ChangePassword,{
                          method: 'POST',
                          headers:{
                                      'token' : token
                                  },
                          body : formdata,        
                          }).then((response) => response.text())
                         .then((responseJson) => {

                          var x = JSON.parse(responseJson.replace("-", ""));
                          this.setState({isLoading : false });
                          Alert.alert(x.message);

                          this.setState({Password:null});
                          this.setState({ConfirmPassword:null});
                         
                          
            }).catch((error)=>{ Alert.alert(error)});
        
        }else{
          alert(error_message);
        }

        this.setState({isLoading : false });
        
    }

 

  render () {

    
    return (
      <View style={styles.container}>
      <StatusBar
           backgroundColor="#232A40"
           barStyle="light-content"/>
           
            <View style={{flex:1, backgroundColor:'#FFFFFF'}} pointerEvents={this.state.isloading ? 'none' : 'auto'}>
              <ScrollView>
                  <View style={{flex: 1, flexDirection: 'column', justifyContent: 'space-between', alignItems:'center', marginTop:hp('5%')}} pointerEvents={this.state.isloading ? 'none' : 'auto'}>
            
                    <View style={{flex:1, }}>

                        <View style={{flexDirection:'column',}} >
                                                          
                                <View style={{flexDirection:'row',}}>
                                    <Text style={{fontSize: RF(2.5),color:'#000000', marginLeft:wp('1%')}}>Password</Text>
                                   
                                </View>

                                <View style={{width: wp('98%'), height: hp('7%'),  alignSelf:'center', borderRadius:wp('2%'), borderWidth:1, borderColor:'#1ab394'}} >
                                      <TextInput style={{
                                            TextColor: '#000000',
                                            fontSize: RF(2.5),
                                            marginLeft : wp('2%')
                                        }}
                                        onChangeText={(Password) => this.setState({Password})}
                                        value={this.state.Password}
                                        secureTextEntry={true}
                                        //placeholder={'Password'}
                                        underlineColorAndroid="transparent"
                                       // placeholderTextColor= '#2a29c9'
                                        />   
                                </View>  
                        </View>

                        <View style={{flexDirection:'column', marginTop:hp('2%')}} >
                                                          
                                <View style={{flexDirection:'row',}}>
                                    <Text style={{fontSize: RF(2.5),color:'#000000', marginLeft:wp('1%')}}>Confirm Password</Text>
                                    
                                </View>

                                <View style={{width: wp('98%'), height: hp('7%'),  alignSelf:'center', borderRadius:wp('2%'), borderWidth:1, borderColor:'#1ab394'}} >
                                      <TextInput style={{
                                            TextColor: '#000000',
                                            fontSize: RF(2.5),
                                            marginLeft : wp('2%')
                                        }}
                                        onChangeText={(ConfirmPassword) => this.setState({ConfirmPassword})}
                                        value={this.state.ConfirmPassword}
                                        secureTextEntry={true}
                                        //placeholder={'Confirm Password'}
                                        underlineColorAndroid="transparent"
                                        //placeholderTextColor= '#2a29c9'
                                        />   
                                </View>  
                        </View>

                    </View>

                    <View style={{flex:2,flexDirection: 'column',justifyContent: 'space-between',marginTop:'10%', marginBottom:'5%',}} >
                          
                              <View style={{flex:1,width: wp('65%'),justifyContent: 'center', height: hp('6%'), alignSelf:'stretch', marginBottom:'10%'}} >
                                    <TouchableOpacity onPress={this.UPDATEPROFILE.bind(this)}>
                                    <ImageBackground  imageStyle={{ borderRadius: 10 }} style={{width: wp('40%'),justifyContent: 'center', height: hp('7%'),alignSelf:'center',  borderRadius:wp('50%')}} source={require('./../../assets/images/btn_bg.png')} >
                                    <Text style={{ textAlign: 'center',alignSelf:'center',fontWeight: 'bold',fontSize:15, color:'#FFFFFF'}}>UPDATE</Text>
                                    </ImageBackground>
                                   
                                    </TouchableOpacity>
                              </View>


                    </View>
                     
                  </View>
              </ScrollView> 
              { this.state.isloading ? <ActivityIndicator
                                      animating     ={true}
                                      transparent   ={true}
                                      visible       ={false}
                                      style         ={styles.indicator}
                                      size          ="large"
                                    /> : <View/>}   
                    
              <Quotes/>       
            </View> 
                  
      </View>
    );
  }
}



export default ChangePassword;