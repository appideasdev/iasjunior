import React, { Component } from 'react';
 
import { 
  StyleSheet, 
  View, 
  Text, 
  YellowBox, 
  TouchableOpacity, 
  AsyncStorage,
  Image,
  StatusBar,
  ScrollView,
  WebView,
  ActivityIndicator,
  FlatList,
  Dimensions,
  RefreshControl,
  Alert
 } from 'react-native';
 

import MarqueeText from 'react-native-marquee';

import { StackNavigator } from 'react-navigation';
import IOSIcon from "react-native-vector-icons/Ionicons";
import Icon from 'react-native-vector-icons/FontAwesome';
import { PieChart,ProgressCircle } from 'react-native-svg-charts'

import GlobalVariables from './../GlobalVariables/GlobalVariables.js';
import styles from './../Styles/GlobalStyle.js';
import Pdf from 'react-native-pdf';
import RNFetchBlob from 'rn-fetch-blob';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import RF from "react-native-responsive-fontsize";
import StudyMaterialWebView from './../SideMenuAction/StudyMaterialWebView.js';


import BottomNavigation, {
  FullTab
} from 'react-native-material-bottom-navigation'

import Video from 'react-native-video';
import MediaControls, { PLAYER_STATES } from 'react-native-media-controls';
import HTML from 'react-native-render-html';
import HTMLView from 'react-native-htmlview';

 
import MainActivity from "./MainActivity";



import DetailScreen from "./DetailScreen";
import StudyMaterial from "./../SideMenuAction/StudyMaterial.js";
import OnlineMentorship from "./../SideMenuAction/OnlineMentorship.js";
import FAQ from "./../SideMenuAction/FAQ.js";
import UPSC from "./../SideMenuAction/UPSC.js";
import IASjunior from "./../SideMenuAction/IASjunior.js";
import Dashboard from "./../SideMenuAction/Dashboard.js";
import Schedule from "./../SideMenuAction/Schedule.js";
import Updates from "./../SideMenuAction/Updates.js";
import Motivation from "./../SideMenuAction/Motivation.js";
import CurrentAffairs from "./../SideMenuAction/CurrentAffairs.js";
import PracticeModules from "./../SideMenuAction/PracticeModules.js";
import PracticeTest from "./../SideMenuAction/PracticeTest.js";
import AssignedModules from "./../SideMenuAction/AssignedModules.js";
import ModuleContent from "./../SideMenuAction/ModuleContent.js";
import ModuleSubContent from "./../SideMenuAction/ModuleSubContent.js";
import UPSCSolutions from "./../SideMenuAction/UPSCSolutions.js";
import PlayVideo from "./../SideMenuAction/PlayVideo.js";
import PrelimQuiz from "./../SideMenuAction/PrelimQuiz.js";
import MockmainSyllabus from "./../SideMenuAction/MockmainSyllabus.js";
import MockprelimsSyllabus from "./../SideMenuAction/MockprelimsSyllabus.js";
import Result from "./../SideMenuAction/Result.js";
import ViewPDF from "./../SideMenuAction/ViewPDF.js";
import Payment from "./../SideMenuAction/Payment.js";




import ChangePassword from "./../app/ChangePassword.js";


 
export default ActivityProject = StackNavigator(
{
 
  First : {
    screen: MainActivity,
    
    navigationOptions: ({navigation}) => ({
      headerLeft: (<TouchableOpacity style={{marginLeft:wp('5%')}} onPress={() => {navigation.openDrawer()}}>
                    <IOSIcon name="ios-menu" size={40} color="#FFFFFF"/>
                  </TouchableOpacity>
      ),
      
      headerRight:(<TouchableOpacity style={{marginRight:wp('5%')}} onPress={() =>  navigation.navigate("Detail")}>
                        <Icon name="user" size={40} color="#FFFFFF" />
                  </TouchableOpacity>
      ),
      headerStyle: { paddingRight: 10, paddingLeft: 15, backgroundColor:'#36404a', },
      headerTitleStyle: { alignItems: 'center', marginLeft:wp('5%') },
    }),
  },
  Detail: {
    screen: DetailScreen,
    navigationOptions: ({navigation}) => ({
      title: "Profile",
    })     
  },
  IASjunior: {
    screen: IASjunior,
    navigationOptions: ({navigation}) => ({
      title: "IAS junior",
    })    
  },
  StudyMaterial: {
    screen: StudyMaterial,
    navigationOptions: ({navigation}) => ({
      title: "Study Material",
    }),
      
  },
  OnlineMentorship: {
    screen: OnlineMentorship,
    navigationOptions: ({navigation}) => ({
      title: "Online Mentorship",
    })     
  },
  FAQ: {
    screen: FAQ,
    navigationOptions: ({navigation}) => ({
      title: "Ask Query",
    })     
  },
  UPSC: {
    screen: UPSC,
    navigationOptions: ({navigation}) => ({
      title: "IAS/UPSC",
    })     
  },
  Dashboard: {
    screen: Dashboard,
    navigationOptions: ({navigation}) => ({
      title: "Dashboard",
    })     
  },
  Schedule: {
    screen: Schedule,
    navigationOptions: ({navigation}) => ({
      title: "Schedule",
    })     
  },
  Updates: {
    screen: Updates,
    navigationOptions: ({navigation}) => ({
      title: "Updates",
    })     
  },
  Motivation: {
    screen: Motivation,
    navigationOptions: ({navigation}) => ({
      title: "Motivation",
    })     
  },
  CurrentAffairs: {
    screen: CurrentAffairs,
    navigationOptions: ({navigation}) => ({
      title: "Current Affairs",
    })     
  },
  PracticeModules: {
    screen: PracticeModules,
    navigationOptions: ({navigation}) => ({
      title: "Practice Modules",
    })     
  },
  PracticeTest: {
    screen: PracticeTest,
    navigationOptions: ({navigation}) => ({
      title: navigation.getParam('moduleName'),
    })      
  },
  AssignedModules: {
    screen: AssignedModules,
    navigationOptions: ({navigation}) => ({
      title: "Assigned Modules",
    })     
  },
  ModuleContent: {
    screen: ModuleContent,
    navigationOptions: ({navigation}) => ({
      title: navigation.getParam('moduleName'),
    }) 
  },
  ModuleSubContent: {
    screen: ModuleSubContent,
    navigationOptions: ({navigation}) => ({
      title: navigation.getParam('ModuleSubContent'),
    })     
  },
  UPSCSolutions: {
    screen: UPSCSolutions,
    navigationOptions: ({navigation}) => ({
      title: 'Study Material',
    })     
  },
  StudyMaterialWebView:{
    screen:StudyMaterialWebView,
    navigationOptions: ({navigation}) => ({
      title: "UPSC Prelims",
    })    
  },
  PlayVideo: {
    screen: PlayVideo,
  },
  PrelimQuiz: {
    screen: PrelimQuiz,
  },
  MockprelimsSyllabus: {
    screen: MockprelimsSyllabus,
    navigationOptions: ({navigation}) => ({
      title: 'Mock Prelims',
    })     
  },
  MockmainSyllabus: {
    screen: MockmainSyllabus,
    navigationOptions: ({navigation}) => ({
      title: 'Mock Mains',
    })     
  },
  Result: {
    screen: Result,
    navigationOptions: ({navigation}) => ({
      title: 'Result',
    })     
  },
  ChangePassword: {
    screen: ChangePassword,
    navigationOptions: ({navigation}) => ({
      title: 'Change Password',
    })     
  },
  ViewPDF: {
    screen: ViewPDF,
  },
  Payment: {
    screen: Payment,
  },

});

