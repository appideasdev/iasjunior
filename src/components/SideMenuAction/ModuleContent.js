import * as React from 'react';
import { View, StyleSheet, Dimensions,AsyncStorage } from 'react-native';
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';

import ContentFromModule from './ContentFromModule.js';
import PrelimQuiz from './PrelimQuiz.js';
import MainQuiz from './MainQuiz.js';

import GlobalVariables from './../GlobalVariables/GlobalVariables.js';
import HTML from 'react-native-render-html';
import Pdf from 'react-native-pdf';
import RNFetchBlob from 'rn-fetch-blob';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import RF from "react-native-responsive-fontsize";
import { StackNavigator } from 'react-navigation';
import { HeaderBackButton } from 'react-navigation';



import ModuleSubContent from './ModuleSubContent.js';

export default class TabViewExample extends React.Component {

  
  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('moduleName'),
      headerStyle: { backgroundColor:'#36404a' },
      headerTitleStyle: { color: '#FFFFFF' },
      headerTintColor: 'white',
      headerLeft:(<HeaderBackButton tintColor='#FFFFFF' onPress={()=>{navigation.navigate('AssignedModules');}}/>),
    };
  };

  constructor(props) {
    super(props);

    const { navigation } = this.props;
    const module_id = navigation.getParam('id');
    const moduleName = navigation.getParam('moduleName');
    const moduleType = navigation.getParam('moduleType');

    console.log(moduleType,"moduleTypeTest");

    let content;
    let prelimscore;
    let mainscore;
    let mainsTest;
    let prelimExam;
    


    AsyncStorage.multiGet(["TOKEN", "student_id"]).then(response => {

            var GVar = GlobalVariables.getApiUrl();

            var TOKEN                          = response[0][1];
            var student_id                     = response[1][1];
           
            this.setState({
                            TOKEN          : TOKEN,    
                            student_id     : student_id,
                      });


            let formdata = new FormData();

            formdata.append('student_id',        student_id );

            console.log(formdata,"formdata");
            console.log(TOKEN,"TOKEN");

            fetch( GVar.BaseUrl + GVar.ModuleContent + module_id ,{
                      method: 'POST',
                      headers:{
                                  'token' : TOKEN
                              },
                      body: formdata,       
                      }).then((response) => response.text())
                     .then((responseJson) => {

                      var x = JSON.parse(responseJson.replace("-", ""));
                      this.setState({isLoading : false });
                      console.log("modulecontent",x);

                      if( x.status == 1 ){

                          let content       = x.content;
                          let prelimscore   = x.prelimscore;
                          let mainscore     = x.mainscore;
                          let mainsTest     = x.mainsTest;
                          let prelimExam    = x.prelimExam;
                          let moduleType    = x.module.moduleType;


                          
                      }
                      
                     }).catch((error)=>{ Alert.alert(error)});

    }); 

    console.log(moduleType,"moduleTypeTest1");
    if( moduleType == 'CSAT' ){

      this.state = {
        index: 0,
        routes: [
          { key: 'first', title: 'Content', id : module_id, moduleName : moduleName, },
          { key: 'second', title: 'Prelim' , id : module_id, moduleName : moduleName},
        ],
      };

    }else{

      this.state = {
        index: 0,
        routes: [
          { key: 'first', title: 'Content', id : module_id, moduleName : moduleName, },
          { key: 'second', title: 'Prelim' , id : module_id, moduleName : moduleName},
          { key: 'third', title: 'Mains' , id : module_id, moduleName : moduleName},
        ],
      };
    }
    
    
  }


  _handleIndexChange = index => this.setState({ index });

  _renderTabBar = props => <TabBar {...props} />;

  _renderScene = SceneMap({
    first   : ContentFromModule,
    second  : PrelimQuiz,
    third   : MainQuiz,
  });

  render() {

    console.log("navigationoptionsfortab",this.props.navigation)
     
    return (
      <TabView
        navigationState={this.state}
        renderScene={this._renderScene}
        renderTabBar={this._renderTabBar}
        onIndexChange={this._handleIndexChange}
        renderTabBar={(props) =>
                    <TabBar
                      {...props}
                      style={{backgroundColor: "black", height: 40}}
                      indicatorStyle={{backgroundColor: "#FFFFFF"}}
                    />
                  }
      />
    );
  }
}

const styles = StyleSheet.create({
  scene: {
    flex: 1,
  },
  header: {
    paddingTop: 0,
  },
});
