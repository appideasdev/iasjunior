import React, { Component } from 'react';
import { WebView } from 'react-native';
import { HeaderBackButton } from 'react-navigation';


export default class StudyMaterialWebView extends Component {


  static navigationOptions = ({ navigation }) => {
    return {
      title: 'UPSC Solutions',
      headerStyle: { backgroundColor:'#36404a' },
      headerTitleStyle: { color: '#FFFFFF' },
      headerTintColor: 'white',
      headerLeft:(<HeaderBackButton tintColor='#FFFFFF' onPress={()=>{navigation.navigate('UPSCSolutions');}}/>)
  
    };
  };

  constructor(props) {
    super(props);
    this.state =  { 
                    source : null,
                    isLoading : true,
                  };
  }


  componentDidMount = async () => {

      this.setState({isLoading : false});
  }



  render() {
    const { navigation } = this.props;
    const navsource = navigation.getParam('source');

   

    if( navsource != null || navsource == undefined ){

     
     // this.setState({ isLoading : true});
    }
    return (
      <WebView
        source={{uri:navsource}}
          javaScriptEnabled={true}
          domStorageEnabled={true}
          decelerationRate="normal"
          

      />
    );
  }
}