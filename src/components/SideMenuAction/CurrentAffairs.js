/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component,Toast } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  StatusBar,
  ImageBackground,
  ScrollView,
  WebView,
  AsyncStorage,
  Dimensions,
  Alert,
  ListView,
  FlatList,
  ActivityIndicator,
  TouchableOpacity,
  Image
} from 'react-native';

import Quotes from "../Quotes.js";

import GlobalVariables from './../GlobalVariables/GlobalVariables.js';
import HTML from 'react-native-render-html';
import Pdf from 'react-native-pdf';
import RNFetchBlob from 'rn-fetch-blob';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import RF from "react-native-responsive-fontsize";
import styles from './../Styles/GlobalStyle.js';
import { HeaderBackButton } from 'react-navigation';


type Props = {};
export default class CurrentAffairs extends Component<Props> {

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Current Affairs',
      headerStyle: { backgroundColor:'#36404a' },
      headerTitleStyle: { color: '#FFFFFF' },
      headerTintColor: 'white',
      headerLeft:(<HeaderBackButton tintColor='#FFFFFF' onPress={()=>{navigation.navigate('First');}}/>)
    };
  };


  constructor(props) {
    super(props);


    this.state = { 
                    query       : null,
                    TOKEN       : null,
                    student_id  : null,
                    quotesSlug  : null,
                    currentaffairs  : [],
                    isLoading   : true,
         };
  }


  componentDidMount = async () => {

    
    this.callApi = this.callApi();
     
  
  }

  callApi(){

    var GVar = GlobalVariables.getApiUrl();

    AsyncStorage.multiGet(["TOKEN", "student_id"]).then(response => {

            var TOKEN                          = response[0][1];
            var student_id                     = response[1][1];
           
            this.setState({
                            TOKEN          : TOKEN,    
                            student_id     : student_id,
                      });


            let formdata = new FormData();

            formdata.append('student_id',        student_id );

            console.log(formdata,"formdata");
            console.log(TOKEN,"TOKEN");

            fetch( GVar.BaseUrl + GVar.CurrentAffairs ,{
                      method: 'POST',
                      headers:{
                                  'token' : TOKEN
                              },
                      body: formdata,       
                      }).then((response) => response.text())
                     .then((responseJson) => {

                      var x = JSON.parse(responseJson.replace("-", ""));
                      this.setState({isLoading : false });
                      

                      if( x.status == 1 ){

                          console.log(x.currentaffairs, "currentaffairs");
                          this.setState({currentaffairs : x.currentaffairs });
                      }
                      
                     }).catch((error)=>{ Alert.alert(error)});

    }); 
  }


  onPDFPressed(item){   

      var GVar = GlobalVariables.getApiUrl();

      this.setState({isLoading : true });

      if( Platform.OS == 'ios' ){

        const dirs = RNFetchBlob.fs.dirs

            RNFetchBlob
            .config({
              fileCache : true,
              path: dirs.DocumentDir + "/" + item
            })
            .fetch('GET', GVar.CurrentAffairsFile +item)
            .then((res) => {
             

              try{
                
                this.setState({isLoading : false });
                RNFetchBlob.ios.openDocument(dirs.DocumentDir + "/" + item);

              }catch(error){
                this.setState({isLoading : true });
               Alert.alert('Please check your internet connection.')
              }


            }, (error) => {
                this.setState({isLoading : true });
               Alert.alert('Please check your internet connection.')
              
            })
      }else{

        RNFetchBlob
            .config({
                notification: true,
                path: 'downloads/'+item,
                indicator: true,
                overwrite: true,
                addAndroidDownloads: {
                    path: RNFetchBlob.fs.dirs.SDCardDir +'/downloads/'+item,
                    useDownloadManager: true,
                    notification: true,
                    overwrite: true,
                    description: 'downloading content...',
                    mime: 'application/pdf',
                    mediaScannable: false
                }
            })
            .fetch('GET', GVar.CurrentAffairsFile +item)
            .progress((received, total) => {
                this.setState({isLoading : false });
                console.log('progress', received / total)    
            })
            .then((response) => {
              console.log("my url",GVar.CurrentAffairsFile +item);
              this.setState({isLoading : false });
              });
      }
         
  }


  

  render() {

    return (
      <View style={{flex: 1, width:wp('100%')}}>
            <View style={{ alignItems:'center', alignself:'center'}}>
                  <View style={{lexDirection:'row',width:wp('95%'), height:hp('8%'), justifyContent:'center', backgroundColor:'#1ab394',marginTop:hp('2%') }}>
                        <View style={{flex:1, flexDirection:'row',marginLeft:wp('2%'), marginTop:hp('2%'), marginBottom:hp('2%')}}>
                          
                              <View  style={{flex:3,}}>
                                    <Text style={styles.TopTitle}>Title</Text>
                              </View>

                              <View  style={{flex:1}}>
                                  <Text style={styles.TopTitle}>Year</Text>
                              </View>

                              <View  style={{flex:1}}>
                                  <Text style={styles.TopTitle}>Month</Text>
                              </View>

                              <View  style={{flex:1.5,}}>
                                  <Text style={styles.TopTitle}>PDF</Text>
                              </View>
                          

                          </View>
                  </View>
            </View>

            <ScrollView>
              <View style={{ alignItems:'center', alignself:'center'}}>
                    <FlatList
                      extraData={this.state}
                      data={this.state.currentaffairs}
                      renderItem={({item}) => 

                      <View style={{flex:1, flexDirection:'row',width:wp('95%'), justifyContent:'center', backgroundColor:'#FFFFFF',  borderWidth:2, borderColor:'#CCCCCC'}}>
                          

                          <View style={{flex:1, flexDirection:'row',marginLeft:wp('2%'), marginTop:hp('2%'), marginBottom:hp('2%')}}>
                          
                              <View  style={{flex:3,}}>
                                    <Text style={{color:'#000000',fontSize: RF(3)}}>{item.title}</Text>
                              </View>

                              <View  style={{flex:1}}>
                                  <Text style={{color:'#000000',fontSize: RF(3)}}>{item.year}</Text>
                              </View>

                              <View  style={{flex:1}}>
                                  <Text style={{color:'#000000',fontSize: RF(3)}}>{item.month}</Text>
                              </View>

                              <View  style={{flex:1.5 , alignItems:'center'}}>
                                  <TouchableOpacity onPress={this.onPDFPressed.bind(this, item.pdfFile)}>
                                     <Text style={{color:'#FF0000',fontSize: RF(2.5), alignself:'center',}}>Download</Text>
                                  </TouchableOpacity>
                              </View>

                              
                          
                          </View>

                      </View>
                    }
                    />

                    
              </View>
            </ScrollView>
            {this.state.isLoading ? <ActivityIndicator
                    animating     ={true}
                    transparent   ={true}
                    visible       ={false}
                    style         ={styles.indicator}
                    size          ="large"
                  /> : <View/>  } 

            <Quotes/>          
      </View>
    );
  }
}


