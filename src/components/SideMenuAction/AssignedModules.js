/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  StatusBar,
  ImageBackground,
  ScrollView,
  WebView,
  AsyncStorage,
  Dimensions,
  Alert,
  ListView,
  FlatList,
  ActivityIndicator,
  PermissionsAndroid,
  TouchableOpacity
} from 'react-native';

import Quotes from "../Quotes.js";
import Icon from 'react-native-vector-icons/FontAwesome';
import GlobalVariables from './../GlobalVariables/GlobalVariables.js';
import HTML from 'react-native-render-html';
import Pdf from 'react-native-pdf';
import RNFetchBlob from 'rn-fetch-blob';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import RF from "react-native-responsive-fontsize";
import { HeaderBackButton } from 'react-navigation';
import styles from './../Styles/GlobalStyle.js';


type Props = {};
export default class AssignedModules extends Component<Props> {

  
  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Assigned Modules',
      headerStyle: { backgroundColor:'#36404a' },
      headerTitleStyle: { color: '#FFFFFF' },
      headerTintColor: 'white',
      headerLeft:(<HeaderBackButton tintColor='#FFFFFF' onPress={()=>{navigation.navigate('First');}}/>)
    };
  };


  constructor(props) {
    super(props);
   
    this.state = { 
                    query       : null,
                    TOKEN       : null,
                    student_id  : null,
                    quotesSlug  : null,
                    assignmodules     : [],
                    isLoading   : true,
         };
  }


  componentDidMount = async () => {


    var GVar = GlobalVariables.getApiUrl();

    
    AsyncStorage.multiGet(["TOKEN", "student_id"]).then(response => {

            var TOKEN                          = response[0][1];
            var student_id                     = response[1][1];
           
            this.setState({
                            TOKEN          : TOKEN,    
                            student_id     : student_id,
                      });


            let formdata = new FormData();

            formdata.append('student_id',        student_id );

            console.log(formdata,"formdata");
            console.log(TOKEN,"TOKEN");

            fetch( GVar.BaseUrl + GVar.AssignedModules ,{
                      method: 'POST',
                      headers:{
                                  'token' : TOKEN
                              },
                      body: formdata,       
                      }).then((response) => response.text())
                     .then((responseJson) => {

                      var x = JSON.parse(responseJson.replace("-", ""));
                      this.setState({isLoading : false });
                      this.setState({status : x.status });
                      

                      if( x.status == 1 ){

                          console.log(x.assignmodules, "assignmodules");
                          this.setState({assignmodules : x.assignmodules });
                      }else{

                          this.setState({message : x.message });
                      }
                      
                     }).catch((error)=>{ Alert.alert(error)});

      });  

    
     
  }



  renderItem(data) {

    let { item, index } = data;
    
    return (

      <TouchableOpacity  onPress={() => { AsyncStorage.setItem('moduleId', item.moduleId) ,item.showmodule ? this.props.navigation.navigate('ModuleContent', {id: item.moduleId, moduleName: item.moduleName, moduleType : item.moduleType}) : ''}}>
                                
      <View style={{flex:1, flexDirection:'row',width:wp('95%'), justifyContent:'center', backgroundColor:'#1ab394', marginTop:hp('1%'), borderRadius:wp('2%')}}>
                          <View style={{flex:1, flexDirection:'row',marginLeft:wp('2%'), marginTop:hp('2%'), marginBottom:hp('2%')}}>
                          
                              <View style={{flex:1, flexDirection:'row',}}>
                                  <Text style={{color:'#FFFFFF', fontSize: RF(2.5)}}>{item.moduleName}</Text>
                                
                              </View>
                          </View>
                      </View>
       </TouchableOpacity>               
    ) 
  }


 
  

  render() {

    
    return (

      <View style={{flex: 1,width:wp('100%')}}>

            <ScrollView>
              <View style={{ alignItems:'center', alignself:'center'}}>

                  {this.state.status ? 
                    <FlatList
                      extraData={this.state}
                      data={this.state.assignmodules}
                      renderItem={this.renderItem.bind(this)}
                    />
                  :
                  <View style={{ alignItems:'center', alignself:'center',}}>
                    <View style={{flex:1,height:hp('70%'), flexDirection:'column', backgroundColor:'#1ab394', width:wp('95%'), alignself:'center', marginTop:hp('5%')}}>
                        <Text style={styles.error}>{this.state.message}</Text>
                    </View>
                  </View>
                  }
                    
              </View>
            </ScrollView>
            {this.state.isLoading ? <ActivityIndicator
                    animating     ={true}
                    transparent   ={true}
                    visible       ={false}
                    style         ={styles.indicator}
                    size          ="large"
                  /> : <View/>  } 
            <Quotes/>      
      </View>
    );
  }
}

