/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  StatusBar,
  ImageBackground,
  ScrollView,
  WebView,
  AsyncStorage,
  Dimensions,
  Alert,
  ListView,
  FlatList,
  ActivityIndicator,
  PermissionsAndroid,
  TouchableOpacity,
  Image
} from 'react-native';
import Quotes from "../Quotes.js";
import { HeaderBackButton } from 'react-navigation';


import GlobalVariables from './../GlobalVariables/GlobalVariables.js';
import HTML from 'react-native-render-html';
import Pdf from 'react-native-pdf';
import RNFetchBlob from 'rn-fetch-blob';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import RF from "react-native-responsive-fontsize";
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import {StackNavigator} from 'react-navigation'; 




export default class MainContentFromModule extends Component<Props> {


  static navigationOptions = {
    header: null,
  }

  

  constructor(props) {
    super(props);

    this.state = { 
                    query       : null,
                    TOKEN       : null,
                    student_id  : null,
                    quotesSlug  : null,
                    content     : [],
                    isLoading   : true,
                    moduleName  : null,
         };
  }


  componentDidMount = async () => {

   
    var GVar = GlobalVariables.getApiUrl();

    
    AsyncStorage.multiGet(["TOKEN", "student_id", "moduleId"]).then(response => {

            var TOKEN                          = response[0][1];
            var student_id                     = response[1][1];
            var module_id                       = response[2][1];

            this.setState({
                            TOKEN          : TOKEN,    
                            student_id     : student_id,
                      });

            



            let formdata = new FormData();

            formdata.append('student_id',        student_id );

            console.log(formdata,"formdata");
            console.log(TOKEN,"TOKEN");

            fetch( GVar.BaseUrl + GVar.ModuleContent + module_id ,{
                      method: 'POST',
                      headers:{
                                  'token' : TOKEN
                              },
                      body: formdata,       
                      }).then((response) => response.text())
                     .then((responseJson) => {

                      var x = JSON.parse(responseJson.replace("-", ""));
                      this.setState({isLoading : false });
                      

                      if( x.status == 1 ){

                          console.log(x.content, "content");
                          this.setState({content : x.content });
                          this.setState({moduleName : x.moduleName });
                      }
                      
                     }).catch((error)=>{ Alert.alert(error)});

      });  

    
     
  }

 
  render() {

  
    return (

      <View style={{flex: 1,width:wp('100%')}}>

            <ScrollView>
              <View style={{ alignItems:'center', alignself:'center'}}>
                    <FlatList
                      extraData={this.state}
                      data={this.state.content}
                      renderItem={({item}) => 

                      <TouchableOpacity onPress={() => { this.setState({isLoading:false}); this.props.navigation.navigate('ModuleSubContent', {content_id: item.content_id}); }}>
                                
                      <View style={{flex:1, flexDirection:'row',width:wp('95%'), justifyContent:'center', backgroundColor:'#1ab394', marginTop:hp('1%'), borderRadius:wp('2%')}}>
                          <View style={{flex:1, flexDirection:'row',marginLeft:wp('2%'), marginTop:hp('2%'), marginBottom:hp('2%')}}>
                              <View style={{flex:1, flexDirection:'row',}}>
                                       <Text style={{color:'#FFFFFF', fontSize: RF(2.5)}}>{item.contentTitle}</Text>
                                </View>

                              
                          </View>
                      </View>
                      </TouchableOpacity>
                    }
                    />

              </View>
            </ScrollView>
            {this.state.isLoading ? <ActivityIndicator
                    animating     ={true}
                    transparent   ={true}
                    visible       ={false}
                    style         ={styles.indicator}
                    size          ="large"
                  /> : <View/>  } 
            <Quotes/>         
      </View>

      );
      }
 }

const styles = StyleSheet.create({
  indicator: {
      alignItems: 'center',
      height: '100%',
      position: 'absolute',
      backgroundColor: 'rgba(192,192,192,0.3)',
      width: '100%'
    },
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginTop: 25,
    },
    pdf: {
        flex:1,
        width:Dimensions.get('window').width,
    }
});
