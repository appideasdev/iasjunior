/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  StatusBar,
  ImageBackground,
  ScrollView,
  WebView,
  AsyncStorage,
  Dimensions,
  Alert,
  ListView,
  FlatList,
  ActivityIndicator,
  PermissionsAndroid,
  TouchableOpacity,
  Image,
  Modal,
  TouchableHighlight
} from 'react-native';

import Quotes from "../Quotes.js";
import { HeaderBackButton } from 'react-navigation';


import GlobalVariables from './../GlobalVariables/GlobalVariables.js';
import HTML from 'react-native-render-html';
import Pdf from 'react-native-pdf';
import RNFetchBlob from 'rn-fetch-blob';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import RF from "react-native-responsive-fontsize";
import YouTube from 'react-native-youtube'
import styles from './../Styles/GlobalStyle.js';

type Props = {};
export default class Updates extends Component<Props> {

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Updates',
      headerStyle: { backgroundColor:'#36404a' },
      headerTitleStyle: { color: '#FFFFFF' },
      headerTintColor: 'white',
      headerLeft:(<HeaderBackButton tintColor='#FFFFFF' onPress={()=>{navigation.navigate('First');}}/>)
  
    };
  };


  constructor(props) {
    super(props);


    this.state = { 
                    query       : null,
                    TOKEN       : null,
                    student_id  : null,
                    quotesSlug  : null,
                    updates     : [],
                    isLoading   : true,
                    modalVisible: false
         };
  }


  componentDidMount = async () => {


    var GVar = GlobalVariables.getApiUrl();

    
    AsyncStorage.multiGet(["TOKEN", "student_id"]).then(response => {

            var TOKEN                          = response[0][1];
            var student_id                     = response[1][1];
           
            this.setState({
                            TOKEN          : TOKEN,    
                            student_id     : student_id,
                      });


            let formdata = new FormData();

            formdata.append('student_id',        student_id );

            console.log(formdata,"formdata");
            console.log(TOKEN,"TOKEN");

            fetch( GVar.BaseUrl + GVar.Updates ,{
                      method: 'POST',
                      headers:{
                                  'token' : TOKEN
                              },
                      body: formdata,       
                      }).then((response) => response.text())
                     .then((responseJson) => {

                      var x = JSON.parse(responseJson.replace("-", ""));
                      this.setState({isLoading : false });
                      

                      if( x.status == 1 ){

                          console.log(x.updates, "updates");
                          this.setState({updates : x.updates });
                      }
                      
                     }).catch((error)=>{ Alert.alert(error)});

      });  

   
     
  }

  onPDFPressed(item){   
      this.setState({isLoading : true });

      var GVar = GlobalVariables.getApiUrl();

      if( Platform.OS == 'ios' ){

        const dirs = RNFetchBlob.fs.dirs

            RNFetchBlob
            .config({
              fileCache : true,
              path: dirs.DocumentDir + "/" + item
            })
            .fetch('GET', GVar.UpdatesFile +item)
            .then((res) => {
             
              try{

                this.setState({isLoading : false });
                RNFetchBlob.ios.openDocument(dirs.DocumentDir + "/" + item);

              }catch(error){
                this.setState({isLoading : false });
                Alert.alert('Please check your internet connection.')
              }


            }, (error) => {
              
              this.setState({isLoading : false });
              Alert.alert('Please check your internet connection.')
              
            })

      }else{

            RNFetchBlob
                .config({
                    notification: true,
                    path: 'downloads/'+item,
                    indicator: true,
                    overwrite: true,
                    addAndroidDownloads: {
                        path: RNFetchBlob.fs.dirs.SDCardDir +'/downloads/'+item,
                        useDownloadManager: true,
                        notification: true,
                        overwrite: true,
                        description: 'downloading content...',
                        mime: 'application/pdf',
                        mediaScannable: false
                    }
                })
                .fetch('GET', GVar.UpdatesFile +item)
                .progress((received, total) => {
                    this.setState({isLoading : false });
                    console.log('progress', received / total)    
                })
      }          
  }

  
  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }




  render() {

    //const source = {uri:'http://samples.leanpub.com/thereactnativebook-sample.pdf',cache:true};

    return (
      <View style={{flex: 1,widthwp:wp('100%')}}>

            <View style={{ alignItems:'center', alignself:'center'}}>
                  <View style={{flexDirection:'row',width:wp('95%'), height:hp('8%'), justifyContent:'center', backgroundColor:'#1ab394', marginTop:hp('1%'),}}>
                        <View style={{flex:1, flexDirection:'row',marginLeft:wp('2%'), marginTop:hp('2%'), marginBottom:hp('2%')}}>
                          
                              <View  style={{flex:4,}}>
                                    <Text style={styles.TopTitle}>Title</Text>
                              </View>


                              <View  style={{flex:2}}>
                                  <Text style={styles.TopTitle}>PDF / Video</Text>
                              </View>
                          
                          </View>
                  </View>
            </View>
           
            <ScrollView>
              <View style={{ alignItems:'center', alignself:'center'}}>
                    <FlatList
                      extraData={this.state}
                      data={this.state.updates}
                      renderItem={({item}) => 

                      <View style={{flex:1, flexDirection:'row',width:wp('95%'), justifyContent:'center', backgroundColor:'#FFFFFF', borderColor:'#CCCCCC', borderWidth:2}}>
                          

                          <View style={{flex:1, flexDirection:'row',marginLeft:wp('2%'),}}>
                          
                              <View  style={{flex:4,borderRightWidth: .2}}>
                              <TouchableOpacity onPress={() => {this.props.navigation.navigate('PlayVideo', {item: item})}}>
                                    <HTML html={item.update_name.substring(0, 200)}  imagesMaxWidth={Dimensions.get('window').width} />
                              </TouchableOpacity>
                              </View>

                              <View  style={{flex:2, alignItems:'center'}}>
                                  <Text style={{fontSize: RF(2)}}>{item.month} {item.year}</Text>
                                  { item.updateFile ? 
                                    <TouchableOpacity onPress={this.onPDFPressed.bind(this, item.updateFile)}>
                                       <Text style={{color:'#FF0000',fontSize: RF(2.5)}}>Download</Text>
                                    </TouchableOpacity>
                                    :
                                    <View/>
                                  }

                                  { item.updateVideo ? 
                                    <TouchableOpacity onPress={() => {this.props.navigation.navigate('PlayVideo', {item: item})}}>
                                       <Text style={{color:'#FF0000',fontSize: RF(2.5)}}>Play Video</Text>
                                    </TouchableOpacity>
                                    :
                                    <View/>
                                  }
                              </View>
                          
                          </View>

                      </View>
                    }
                    />

                    
              </View>

            </ScrollView>
             

            {this.state.isLoading ? <ActivityIndicator
                    animating     ={true}
                    transparent   ={true}
                    visible       ={false}
                    style         ={styles.indicator}
                    size          ="large"
                  /> : <View/>  } 
            <Quotes/>         
      </View>
    );
  }
}

