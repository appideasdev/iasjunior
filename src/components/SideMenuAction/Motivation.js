/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  StatusBar,
  ImageBackground,
  ScrollView,
  WebView,
  AsyncStorage,
  Dimensions,
  Alert,
  ListView,
  FlatList,
  ActivityIndicator,
  TouchableOpacity,
  Image
} from 'react-native';
import Quotes from "../Quotes.js";

import GlobalVariables from './../GlobalVariables/GlobalVariables.js';
import styles from './../Styles/GlobalStyle.js';
import HTML from 'react-native-render-html';
import Pdf from 'react-native-pdf';
import RNFetchBlob from 'rn-fetch-blob';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import RF from "react-native-responsive-fontsize";
import { HeaderBackButton } from 'react-navigation';



type Props = {};
export default class Motivation extends Component<Props> {

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Motivation',
      headerStyle: { backgroundColor:'#36404a' },
      headerTitleStyle: { color: '#FFFFFF' },
      headerTintColor: 'white',
      headerLeft:(<HeaderBackButton tintColor='#FFFFFF' onPress={()=>{navigation.navigate('First');}}/>)
  
  
    };
  };


  constructor(props) {
    super(props);


    this.state = { 
                    query       : null,
                    TOKEN       : null,
                    student_id  : null,
                    quotesSlug  : null,
                    motivation  : [],
                    isLoading   : true,
         };
  }


  componentDidMount = async () => {

    

    var GVar = GlobalVariables.getApiUrl();

    
    AsyncStorage.multiGet(["TOKEN", "student_id"]).then(response => {

            var TOKEN                          = response[0][1];
            var student_id                     = response[1][1];
           
            this.setState({
                            TOKEN          : TOKEN,    
                            student_id     : student_id,
                      });


            let formdata = new FormData();

            formdata.append('student_id',        student_id );

            console.log(formdata,"formdata");
            console.log(TOKEN,"TOKEN");

            fetch( GVar.BaseUrl + GVar.Motivation ,{
                      method: 'POST',
                      headers:{
                                  'token' : TOKEN
                              },
                      body: formdata,       
                      }).then((response) => response.text())
                     .then((responseJson) => {

                      var x = JSON.parse(responseJson.replace("-", ""));
                      this.setState({isLoading : false });
                      

                      if( x.status == 1 ){

                          console.log(x.motivation, "updates");
                          this.setState({motivation : x.motivation });
                      }
                      
                     }).catch((error)=>{ Alert.alert(error)});

      });  

     
  }


  onPDFPressed(item){   


      this.setState({isLoading : true });

      var GVar = GlobalVariables.getApiUrl();

      if( Platform.OS == 'ios' ){

        const dirs = RNFetchBlob.fs.dirs

            RNFetchBlob
            .config({
              fileCache : true,
              path: dirs.DocumentDir + "/" + item
            })
            .fetch('GET', GVar.MotivationFile +item)
            .then((res) => {
             
              try{

                this.setState({isLoading : false });
                RNFetchBlob.ios.openDocument(dirs.DocumentDir + "/" + item);

              }catch(error){
                this.setState({isLoading : false });
                Alert.alert('Please check your internet connection.')
              }


            }, (error) => {
              
              this.setState({isLoading : false });
              Alert.alert('Please check your internet connection.')
              
            })

      }else{

          RNFetchBlob
          .config({
              notifition: true,
              path: RNFetchBlob.fs.dirs.DownloadDir +'/downloads/'+item,
              indicator: true,
              overwrite: true,
              addAndroidDownloads: {
                  path: RNFetchBlob.fs.dirs.SDCardDir +'/downloads/'+item,
                  useDownloadManager: true,
                  notification: true,
                  overwrite: true,
                  description: 'downloading content...',
                  mime: 'application/pdf;base64',
                  mediaScannable: false
              }
          })
          .fetch('GET', GVar.MotivationFile +item)
          .progress((received, total) => {
              console.log('progress', received / total)    
          })
      }          

    
      
  }


  render() {

    return (
      <View style={{flex: 1,widthwp:wp('100%')}}>
            <View style={{ alignItems:'center', alignself:'center'}}>
                  <View style={{lexDirection:'row',width:wp('95%'), height:hp('8%'), justifyContent:'center', backgroundColor:'#1ab394', marginTop:hp('1%'), }}>
                        <View style={{flex:1, flexDirection:'row',marginLeft:wp('2%'), marginTop:hp('2%'), marginBottom:hp('2%')}}>
                          
                              <View  style={{flex:4}}>
                                    <Text style={styles.TopTitle}>Title</Text>
                              </View>

                              <View  style={{flex:2}}>
                                  <Text style={styles.TopTitle}>PDF</Text>
                              </View>

                              <View  style={{flex:2}}>
                                  <Text style={styles.TopTitle}>Video</Text>
                              </View>
                          
                          </View>
                  </View>
            </View>

            <ScrollView>
              <View style={{ alignItems:'center', alignself:'center'}}>
                    <FlatList
                      extraData={this.state}
                      data={this.state.motivation}
                      renderItem={({item}) => 

                      <View style={{flex:1, flexDirection:'row',width:wp('95%'), justifyContent:'center', backgroundColor:'#FFFFFF', borderWidth:2, borderColor:'#CCCCCC',}}>
                          

                          <View style={{flex:1, flexDirection:'row',marginLeft:wp('2%'), alignItems:'center', marginTop:hp('2%'), marginBottom:hp('2%')}}>
                          
                              <View  style={{flex:4, alignItems:'center',borderRightWidth: .2}}>
                                    <Text style={{color:'#000000',fontSize: RF(3)}}>{item.motivationName}</Text>
                              </View>


                              { item.PFD == 'true' ? 

                                <View  style={{flex:2, alignItems:'center'}}>
                                    <TouchableOpacity onPress={this.onPDFPressed.bind(this, item.motivationFile)}>
                                       <Text style={{color:'#FF0000',fontSize: RF(2.5), alignself:'center',}}>Download</Text>
                                    </TouchableOpacity>
                                </View>
                                :

                                <View  style={{flex:2, alignItems:'center'}}>
                                       <Text style={{color:'#FF0000',fontSize: RF(2.5), alignself:'center',}}>No PDF</Text>
                                </View>
                              }

                              { item.Video == 'true' ? 
                                <View  style={{flex:2, alignItems:'center'}}>
                                  <TouchableOpacity onPress={() => {this.props.navigation.navigate('PlayVideo', {item: item})}}>
                                     <Text style={{color:'#FF0000',fontSize: RF(2.5), alignself:'center',}}>Play</Text>
                                  </TouchableOpacity>
                                </View>
                                :
                                <View  style={{flex:2, alignItems:'center'}}>
                                  <TouchableOpacity>
                                     <Text style={{color:'#FF0000',fontSize: RF(2.5), alignself:'center',}}>No Video</Text>
                                  </TouchableOpacity>
                                </View>
                              }
                          
                          </View>

                      </View>
                    }
                    />
                   
              </View>
            </ScrollView>
            {this.state.isLoading ? <ActivityIndicator
                    animating     ={true}
                    transparent   ={true}
                    visible       ={false}
                    style         ={styles.indicator}
                    size          ="large"
                  /> : <View/>  } 
            <Quotes/>          
      </View>
    );
  }
}



