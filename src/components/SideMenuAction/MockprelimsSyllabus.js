/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  StatusBar,
  ImageBackground,
  ScrollView,
  WebView,
  AsyncStorage,
  Dimensions,
  Alert,
  ListView,
  FlatList,
  ActivityIndicator,
  PermissionsAndroid,
  TouchableOpacity,
  Modal
} from 'react-native';
import RNHTMLtoPDF from 'react-native-html-to-pdf';
import { CheckBox } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import CountDown from 'react-native-countdown-component';

import Quotes from "../Quotes.js";
import styles from './../Styles/GlobalStyle.js';
import GlobalVariables from './../GlobalVariables/GlobalVariables.js';
import HTML from 'react-native-render-html';
import Pdf from 'react-native-pdf';
import RNFetchBlob from 'rn-fetch-blob';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import RF from "react-native-responsive-fontsize";
import { HeaderBackButton } from 'react-navigation';



type Props = {};
export default class MockprelimsSyllabus extends Component<Props> {

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Practice Test',
      headerStyle: { backgroundColor:'#36404a' },
      headerTitleStyle: { color: '#FFFFFF' },
      headerTintColor: 'white',
      headerLeft:(<HeaderBackButton tintColor='#FFFFFF' onPress={()=>{navigation.navigate('First');}}/>)
  
    };
  };


  constructor(props) {
    super(props);



    this.state = {  noOfMockPrelimGiven:0,
                    query       : null,
                    TOKEN       : null,
                    student_id  : null,
                    quotesSlug  : null,
                    prelim_score     : [],
                    isLoading   : true,
                    moduleName  : null,
                    disableTS1:false,
                    disableTS2:false,
                    disableTS3:false,
                    disableTS4:false,
                    disableTS5:false,               
                    timeCompleted:true,
                    givenAllTest:0,
                    testCompleted:true,
                    testModalVisible:false,
                    latest_result : [],
                     questions   : [],
                     isModalLoading   : false,
                     totalDuration: '',
                     plusModalVisible: false,
                     startTime: false,
                     nextPostion : 0,
                     resultModalVisible:false,
                     totalQuestionCount:0,
                     isresultLoading:false,
                     pos         : [
                                     {"id":"1", "indexstart":"1", "indexend":"5" },
                                     {"id":"2", "indexstart":"1", "indexend":"5" },
                                     {"id":"3", "indexstart":"1", "indexend":"5" },
                                     {"id":"4", "indexstart":"1", "indexend":"5" },
                                     {"id":"5", "indexstart":"1", "indexend":"5" },
 
                                     {"id":"6", "indexstart":"6", "indexend":"10" },
                                     {"id":"7", "indexstart":"6", "indexend":"10" },
                                     {"id":"8", "indexstart":"6", "indexend":"10" },
                                     {"id":"9", "indexstart":"6", "indexend":"10" },
                                     {"id":"10", "indexstart":"6", "indexend":"10" },
 
                                     {"id":"11", "indexstart":"11", "indexend":"15" },
                                     {"id":"12", "indexstart":"11", "indexend":"15" },
                                     {"id":"13", "indexstart":"11", "indexend":"15" },
                                     {"id":"14", "indexstart":"11", "indexend":"15" },
                                     {"id":"15", "indexstart":"11", "indexend":"15" },
 
                                     {"id":"16", "indexstart":"16", "indexend":"20" },
                                     {"id":"17", "indexstart":"16", "indexend":"20" },
                                     {"id":"18", "indexstart":"16", "indexend":"20" },
                                     {"id":"19", "indexstart":"16", "indexend":"20" },
                                     {"id":"20", "indexstart":"16", "indexend":"20" },
 
                                     {"id":"21", "indexstart":"21", "indexend":"25" },
                                     {"id":"22", "indexstart":"21", "indexend":"25" },
                                     {"id":"23", "indexstart":"21", "indexend":"25" },
                                     {"id":"24", "indexstart":"21", "indexend":"25" },
                                     {"id":"25", "indexstart":"21", "indexend":"25" },
 
                                     {"id":"26", "indexstart":"26", "indexend":"30" },
                                     {"id":"27", "indexstart":"26", "indexend":"30" },
                                     {"id":"28", "indexstart":"26", "indexend":"30" },
                                     {"id":"29", "indexstart":"26", "indexend":"30" },
                                     {"id":"30", "indexstart":"26", "indexend":"30" },
 
                                     {"id":"31", "indexstart":"31", "indexend":"35" },
                                     {"id":"32", "indexstart":"31", "indexend":"35" },
                                     {"id":"33", "indexstart":"31", "indexend":"35" },
                                     {"id":"34", "indexstart":"31", "indexend":"35" },
                                     {"id":"35", "indexstart":"31", "indexend":"35" },
 
                                     {"id":"36", "indexstart":"36", "indexend":"40" },
                                     {"id":"37", "indexstart":"36", "indexend":"40" },
                                     {"id":"38", "indexstart":"36", "indexend":"40" },
                                     {"id":"39", "indexstart":"36", "indexend":"40" },
                                     {"id":"40", "indexstart":"36", "indexend":"40" },
 
                                     {"id":"41", "indexstart":"41", "indexend":"45" },
                                     {"id":"42", "indexstart":"41", "indexend":"45" },
                                     {"id":"43", "indexstart":"41", "indexend":"45" },
                                     {"id":"44", "indexstart":"41", "indexend":"45" },
                                     {"id":"45", "indexstart":"41", "indexend":"45" },
 
                                     {"id":"46", "indexstart":"46", "indexend":"50" },
                                     {"id":"47", "indexstart":"46", "indexend":"50" },
                                     {"id":"48", "indexstart":"46", "indexend":"50" },
                                     {"id":"49", "indexstart":"46", "indexend":"50" },
                                     {"id":"50", "indexstart":"46", "indexend":"50" },
 
                                     {"id":"51", "indexstart":"51", "indexend":"55" },
                                     {"id":"52", "indexstart":"51", "indexend":"55" },
                                     {"id":"53", "indexstart":"51", "indexend":"55" },
                                     {"id":"54", "indexstart":"51", "indexend":"55" },
                                     {"id":"55", "indexstart":"51", "indexend":"55" },
 
                                     {"id":"56", "indexstart":"56", "indexend":"60" },
                                     {"id":"57", "indexstart":"56", "indexend":"60" },
                                     {"id":"58", "indexstart":"56", "indexend":"60" },
                                     {"id":"59", "indexstart":"56", "indexend":"60" },
                                     {"id":"60", "indexstart":"56", "indexend":"60" },
 
                                     {"id":"61", "indexstart":"61", "indexend":"65" },
                                     {"id":"62", "indexstart":"61", "indexend":"65" },
                                     {"id":"63", "indexstart":"61", "indexend":"65" },
                                     {"id":"64", "indexstart":"61", "indexend":"65" },
                                     {"id":"65", "indexstart":"61", "indexend":"65" },
 
                                     {"id":"66", "indexstart":"66", "indexend":"70" },
                                     {"id":"67", "indexstart":"66", "indexend":"70" },
                                     {"id":"68", "indexstart":"66", "indexend":"70" },
                                     {"id":"69", "indexstart":"66", "indexend":"70" },
                                     {"id":"70", "indexstart":"66", "indexend":"70" },
 
                                     {"id":"71", "indexstart":"71", "indexend":"75" },
                                     {"id":"72", "indexstart":"71", "indexend":"75" },
                                     {"id":"73", "indexstart":"71", "indexend":"75" },
                                     {"id":"74", "indexstart":"71", "indexend":"75" },
                                     {"id":"75", "indexstart":"71", "indexend":"75" },
 
                                     {"id":"76", "indexstart":"76", "indexend":"80" },
                                     {"id":"77", "indexstart":"76", "indexend":"80" },
                                     {"id":"78", "indexstart":"76", "indexend":"80" },
                                     {"id":"79", "indexstart":"76", "indexend":"80" },
                                     {"id":"80", "indexstart":"76", "indexend":"80" },
 
                                     {"id":"81", "indexstart":"81", "indexend":"85" },
                                     {"id":"82", "indexstart":"81", "indexend":"85" },
                                     {"id":"83", "indexstart":"81", "indexend":"85" },
                                     {"id":"84", "indexstart":"81", "indexend":"85" },
                                     {"id":"85", "indexstart":"81", "indexend":"85" },
 
                                     {"id":"86", "indexstart":"86", "indexend":"90" },
                                     {"id":"87", "indexstart":"86", "indexend":"90" },
                                     {"id":"88", "indexstart":"86", "indexend":"90" },
                                     {"id":"89", "indexstart":"86", "indexend":"90" },
                                     {"id":"90", "indexstart":"86", "indexend":"90" },
 
                                     {"id":"91", "indexstart":"91", "indexend":"95" },
                                     {"id":"92", "indexstart":"91", "indexend":"95" },
                                     {"id":"93", "indexstart":"91", "indexend":"95" },
                                     {"id":"94", "indexstart":"91", "indexend":"95" },
                                     {"id":"95", "indexstart":"91", "indexend":"95" },
 
                                     {"id":"96", "indexstart":"96", "indexend":"100" },
                                     {"id":"97", "indexstart":"96", "indexend":"100" },
                                     {"id":"98", "indexstart":"96", "indexend":"100" },
                                     {"id":"99", "indexstart":"96", "indexend":"100" },
                                     {"id":"100", "indexstart":"96", "indexend":"100" },
                                   ]

         };
  }


  componentDidMount = async () => {

    const { navigation } = this.props;
    const module_id = navigation.getParam('id');


    var GVar = GlobalVariables.getApiUrl();

    
    AsyncStorage.multiGet(["TOKEN", "student_id"]).then(response => {

            var TOKEN                          = response[0][1];
            var student_id                     = response[1][1];
           
            this.setState({
                            TOKEN          : TOKEN,    
                            student_id     : student_id,
                      });


            let formdata = new FormData();

            formdata.append('student_id',        student_id );
            console.log("formadata MOCK prelim",formdata);

            console.log("mockprelins",GVar.BaseUrl + GVar.MockPrelims)
            fetch( GVar.BaseUrl + GVar.MockPrelims ,{
                      method: 'POST',
                      headers:{
                                  'token' : TOKEN
                              },
                      body: formdata,       
                      }).then((response) => response.text())
                     .then((responseJson) => {

                      var x = JSON.parse(responseJson.replace("-", ""));
                      this.setState({status : x.status });
                      this.setState({isLoading : false });

                      if( x.status == 1){

                        console.log("mock prelim ",x);
                       // for now considering x.mockMains.test is yes

                       //comment this after removing async comment
                        if(x.mockPrelim.test == 'yes'){
                          this.setState({givenAllTest:1});
                        }
                        //this.setState({givenAllTest:1});
                        //remove comment here
                        this.setState({noOfMockPrelimGiven:parseInt(x.mockPrelim.noOfMockPrelimsGiven)+1});
                        this.setState({assignedModules:x.mockPrelim.assignedModules});

                        console.log("noOfGivenTest",this.state.noOfMockPrelimGiven);
                        this.setState({mockmain_score:x.mockPrelim.mockmain_score});

                        if(this.state.noOfMockPrelimGiven == 1){
                          this.setState({disableTS1:false});
                          this.setState({disableTS2:true});
                          this.setState({disableTS3:true});
                          this.setState({disableTS4:true});
                          this.setState({disableTS5:true});
                        }else if(this.state.noOfMockPrelimGiven == 2){
                          this.setState({disableTS1:true});
                          this.setState({disableTS2:false});
                          this.setState({disableTS3:true});
                          this.setState({disableTS4:true});
                          this.setState({disableTS5:true});
                        }else if(this.state.noOfMockPrelimGiven == 3){
                          this.setState({disableTS1:true});
                          this.setState({disableTS2:true});
                          this.setState({disableTS3:false});
                          this.setState({disableTS4:true});
                          this.setState({disableTS5:true});
                        }else if(this.state.noOfMockPrelimGiven == 4){
                          this.setState({disableTS1:true});
                          this.setState({disableTS2:true});
                          this.setState({disableTS3:true});
                          this.setState({disableTS4:false});
                          this.setState({disableTS5:true});
                        }else if(this.state.noOfMockPrelimGiven == 5){
                          this.setState({disableTS1:true});
                          this.setState({disableTS2:true});
                          this.setState({disableTS3:true});
                          this.setState({disableTS4:true});
                          this.setState({disableTS5:false});
                        }else if(this.state.noOfMockPrelimGiven > 5){
                          console.log("prelim test > than 5");
                          this.setState({disableTS1:true});
                          this.setState({disableTS2:true});
                          this.setState({disableTS3:true});
                          this.setState({disableTS4:true});
                          this.setState({disableTS5:true});

                        }

                          
                          
                      }else{
                        this.setState({message : x.message });

                      }
                      
                     }).catch((error)=>{ Alert.alert(error)});

      });  

    //this.onPDFPressed();
     this.startTimer();
  }

  startTimer(){

    var that = this;
   
    if(that.state.startTime){

      that.setState({ totalDuration: 7200 });
    }else{
      that.setState({totalDuration:30})
    }
    
  }

  onFinishTimer(){

    console.log("Time Elapsed....!");
    if(this.state.timeCompleted){
      Alert.alert("Exam Submitted");
      this.setTestModalVisible(false);
      this.changingStatusOfStartTest();
    }
  
  }

  getMockPrelimQuestion(){

          var GVar = GlobalVariables.getApiUrl();

          console.log("get prelim Questions");

           //Starting Prelim test fetching here

          AsyncStorage.multiGet(["TOKEN", "student_id", "moduleId"]).then(response => {

                var TOKEN                          = response[0][1];
                var student_id                     = response[1][1];
              
              
                this.setState({
                                TOKEN          : TOKEN,    
                                student_id     : student_id,
                                isModalLoading      : true, 
                          });

                let prelimTestFormData = new FormData();

                prelimTestFormData.append('student_id',        student_id );
                prelimTestFormData.append('count',this.state.noOfMockPrelimGiven);

              
                console.log(prelimTestFormData," FULL QUEST formdata of prelimTestFormData");
                console.log(TOKEN,"TOKEN");

                console.log(GVar.BaseUrl + GVar.GetPrelimFullQuestions,"formdata GET fULL qUES");
                fetch( GVar.BaseUrl + GVar.GetPrelimFullQuestions,{
                          method: 'POST',
                          headers:{
                                      'token' : TOKEN
                                  },
                          body: prelimTestFormData,       
                          }).then((response) => response.text())
                          .then((responseJson) => {

                          console.log("get practice questions",responseJson);
                          var x = JSON.parse(responseJson.replace("-", ""));
                          this.setState({isModalLoading : false });
                        
                          if( x.status == 1 ){
                            this.setTestModalVisible(true);
                            this.setState({startTime:true})

                              console.log(x, " question for getMockPrelim questions");
                              console.log("timer state",this.state.startTime);
                              this.startTimer();                                                                             
                              this.setState({questions : x.questions });
                              
                          }else{

                          }
                          
                          }).catch((error)=>{ Alert.alert(error)}); 

          });
  }

  getMockReviews(){                                                                                                                                                                                        

      //Async Storage for Reviews
      var GVar = GlobalVariables.getApiUrl();

      console.log("get reviews ");

      AsyncStorage.multiGet(["TOKEN", "student_id", "moduleId"]).then(response => {

            var TOKEN                          = response[0][1];
            var student_id                     = response[1][1];

            this.setState({isModalLoading:true})

            let formdata = new FormData();

            formdata.append('student_id',        student_id );
            formdata.append('count',this.state.noOfMockPrelimGiven);
        
            
            console.log(formdata,"GET FULL REVIEWS formdata");
            console.log(TOKEN,"TOKEN");

            console.log("GET FULL REVIEWS ",GVar.BaseUrl + GVar.GetPrelimFullReviews);
            fetch( GVar.BaseUrl + GVar.GetPrelimFullReviews,{
                      method: 'POST',
                      headers:{
                                  'token' : TOKEN
                              },
                      body: formdata,       
                      }).then((response) => response.text())
                     .then((responseJson) => {

                      var x = JSON.parse(responseJson.replace("-", ""));
                      this.setState({isModalLoading : false });
                      

                      if( x.status == 1 ){

                          console.log("reviews",x.review.length);
                          this.setState({review : x.review });
                          this.setState({totalQuestionCount: x.review.length});

                      }else{

                      }
                      
                     }).catch((error)=>{ Alert.alert(error)});

      });

  }

  insertMockPrelimQuestions(){

      var GVar = GlobalVariables.getApiUrl();

      AsyncStorage.multiGet(["TOKEN", "student_id"]).then(response => {

              var TOKEN                          = response[0][1];
              var student_id                     = response[1][1];
             
              this.setState({isLoading:true});
              console.log("insert prelim question")
              let formdata = new FormData();
              formdata.append("student_id",student_id);
              formdata.append("count",this.state.noOfMockPrelimGiven);
              formdata.append("module_id",this.state.assignedModules);
             // need to ask
              console.log("my form data INSERT FULL QUEST",formdata);

              console.log("INSERT FULL QUEST",GVar.BaseUrl + GVar.InsertPrelimFullQuestions);
            
              fetch( GVar.BaseUrl + GVar.InsertPrelimFullQuestions,{
                  method: 'POST',
                  headers:{
                              'token' : TOKEN
                          },
                  body: formdata,       
                  }).then((response) => response.text())
                 .then((responseJson) => {

                 
                  var x = JSON.parse(responseJson.replace("-", ""));
                  this.setState({isLoading : false });
                  if(x.status == 0){
                    Alert.alert("Questions are not prepared..Please Try Later!!");
                   
                  }else{ 
                    this.getMockPrelimQuestion();
                    this.getMockReviews();
                  }
                  console.log("PRELIMquestion",x);

                  
              
             }).catch((error)=>{ Alert.alert(error)});
      });          
    
  }

 
   //modal plus Sign modal(part of StartTestModal)
   setPlusModalVisible(visible) {
    this.setState({plusModalVisible: visible});
  }

  //part of StartTesrt Moda;

  onNext( go ){

    var index_end;

    if( go == 'previous'){

      index_end = this.state.nextPostion - 5;
    }else if( go == 'next' ){

      index_end = this.state.nextPostion + 5;
    }
     
    console.log("index_end", index_end );  
  
    var GVar = GlobalVariables.getApiUrl();

    AsyncStorage.multiGet(["TOKEN", "student_id"]).then(response => {

            var TOKEN                          = response[0][1];
            var student_id                     = response[1][1];

            this.setState({nextPostion : index_end});
            this.setState({isModalLoading:true});

            let formdata = new FormData();

            formdata.append('student_id',       student_id );
            formdata.append('count',        this.state.noOfMockPrelimGiven );
            formdata.append('go',               go );
            formdata.append('index_end',        index_end );
            
            console.log(formdata,"formdata ON NEXT CALLING");
            console.log(TOKEN,"TOKEN");

            console.log(GVar.BaseUrl + GVar.GetPrelimFullQuestions,"NEXT CALLING");
            fetch( GVar.BaseUrl + GVar.GetPrelimFullQuestions,{
                      method: 'POST',
                      headers:{
                                  'token' : TOKEN
                              },
                      body: formdata,       
                      }).then((response) => response.text())
                     .then((responseJson) => {

                      var x = JSON.parse(responseJson.replace("-", ""));
                      this.setState({isModalLoading : false });
                      

                      if( x.status == 1 ){

                          console.log(x, "questions");
                          this.setState({questions : x.questions });

                      }else{

                      }
                      
                     }).catch((error)=>{ Alert.alert(error)});

    }); 
  }



  //part of StartTestModal
 reviewQuestions( item ){

    console.log("ReviewAnswer", item);
    var ischecked = false;
    var color = '';

    

    if( item.review_later == 1 ){

        this.ischecked = true;
        color = 'violet';
    }else if( item.answer_id ){

      this.ischecked = true;
      color = 'green';
    }else if( item.answer_id && item.review_later == 1 ){

      this.ischecked = true;
      color = 'violet';
    }else{

      this.ischecked = false;
      color = '';
    }


    console.log("colorReview", color);

  
    return  <View style={{flex:1, flexDirection:'row',justifyContent:'center'}}>
                  <View style={{}}>
                    <CheckBox 
                      center
                      checkedColor = {color}
                      size = {hp('2.5%')}
                      containerStyle={{backgroundColor:'transparent', borderColor:'transparent'}}
                      textStyle ={{fontSize: RF(2.5),fontWeight: 'bold',color:'#FFFFFF'}}
                      checked={item.answer_id || item.review_later == 1 ? true : false } 
                      //onPress={ }
                      onPress={() => {
                                    this.setState({plusModalVisible : false });
                                    this._onClickReview( item, ischecked);
                                  }}
                      />
                  </View>

                  <View style={{marginTop:'20%'}}>
                      <Text style={{flex:1,marginTop:'4.5%',marginLeft:-22, marginTop:'5%', color:'#000000'}}>{item.question_ordering}</Text>
                  </View>
            </View>;
  }



  //All Part of Start Test modal

  /*showSingleOptionAnswers( item ){

    var questions = this.state.questions;


    return  <FlatList
                extraData={this.state}
                data={item}
                renderItem={({item, index}) => 
                    <View style={{flex:1, flexDirection:'row',marginLeft:wp('2%'),}}>
                            <CheckBox
                              checkedIcon='dot-circle-o'
                              uncheckedIcon='circle-o'
                              checked={ item.answer_id == questions[index].answer_id ? true : false}
                            />
                          <Text style={{flex:1,marginTop:'4.5%',marginLeft:-22}}>{questions[index].answer_id}</Text>
                          <Text style={{flex:1,marginTop:'4.5%',marginLeft:-22}}>{item.answer_id}</Text>
                         <Text style={{flex:1,marginTop:'4.5%',marginLeft:-22}}>{index + 1 }.  {item.answer}</Text>
                    </View>
              }/>; 
  }*/

  _onClickReview( item, ischecked ){
    //this.setModalVisible(!this.state.modalVisible);
    console.log("onclickReviewButton",item)
    console.log("onclickReviewButton",ischecked)
    console.log("nextPostion",this.state.nextPostion)

    var index = item.question_ordering;
    var pos   = this.state.pos;
    
    var index_end = pos[item.question_ordering -1 ].indexend - 5 ;
    console.log("nextPostion",index_end)
    var go        = 'next';

    var GVar = GlobalVariables.getApiUrl();

    AsyncStorage.multiGet(["TOKEN", "student_id"]).then(response => {

            var TOKEN                          = response[0][1];
            var student_id                     = response[1][1];
           
            this.setState({nextPostion : index_end});

            this.setState({
                            TOKEN          : TOKEN,    
                            student_id     : student_id,
                         
                            isModalLoading      : true, 
                      });


            let formdata = new FormData();

            formdata.append('student_id',       student_id );
            formdata.append('count',        this.state.noOfMockPrelimGiven );
            formdata.append('go',               go );
            formdata.append('index_end',        index_end );
            
            console.log(formdata,"formdata ON CLICK REVIEW");
            console.log(TOKEN,"TOKEN");

            console.log(GVar.BaseUrl + GVar.GetPrelimFullQuestions ,"ON CLICK REVIEW");
            fetch( GVar.BaseUrl + GVar.GetPrelimFullQuestions ,{
                      method: 'POST',
                      headers:{
                                  'token' : TOKEN
                              },
                      body: formdata,       
                      }).then((response) => response.text())
                     .then((responseJson) => {

                      var x = JSON.parse(responseJson.replace("-", ""));
                      this.setState({isModalLoading : false });
                      

                      if( x.status == 1 ){

                          console.log(x, "questions");
                          this.setState({questions : x.questions });

                      }else{

                      }
                      
                     }).catch((error)=>{ Alert.alert(error)});

    }); 

  }

  _reviewLater( item ){

    console.log("xyz", item );
    console.log("questions", this.state.questions );

    
    var GVar = GlobalVariables.getApiUrl();
    

    //this.setState({ questions})

    AsyncStorage.multiGet(["TOKEN", "student_id"]).then(response => {

            var TOKEN                          = response[0][1];
            var student_id                     = response[1][1];
            
           
            this.setState({
                            isModalLoading      : true, 
                      });

            var review;
            if( item.review_later == 0 ){

              review = '1';
            }else if( item.review_later == 1 ){

              review = '0';
            }

            var questions  = this.state.questions;

            var index = this.getIndex(item.exam_id, questions, 'exam_id');

            questions[index].review_later = review;
            this.setState({ questions : questions });

            let formdata = new FormData();

            formdata.append('student_id',       student_id );
            formdata.append('exam_id',          item.exam_id );
            formdata.append('review',           review );
           
            
            console.log(formdata,"formdata REVIEW LATER");
            console.log(TOKEN,"TOKEN");

            console.log(GVar.BaseUrl + GVar.UpdateReview,"REVIEW LATER");
            fetch( GVar.BaseUrl + GVar.UpdateReview,{
                      method: 'POST',
                      headers:{
                                  'token' : TOKEN
                              },
                      body: formdata,       
                      }).then((response) => response.text())
                     .then((responseJson) => {

                      var x = JSON.parse(responseJson.replace("-", ""));
                      this.setState({isModalLoading : false });
                      

                      if( x.status == 1 ){

                          console.log(x, "reviewResponce");
                          //this.setState({questions : x.questions });

                      }else{

                      }
                      
                     }).catch((error)=>{ Alert.alert(error)});

    }); 

    AsyncStorage.multiGet(["TOKEN", "student_id"]).then(response => {

            var TOKEN                          = response[0][1];
            var student_id                     = response[1][1];

            this.setState({
                            TOKEN          : TOKEN,    
                            student_id     : student_id,
                           
                            isModalLoading      : true, 
                      });


            let formdata = new FormData();

            formdata.append('student_id',        student_id );
            formdata.append('count',         this.state.noOfMockPrelimGiven );
            
            console.log(formdata,"formdata REVIEW LATER FULL REVIEW");
            console.log(TOKEN,"TOKEN");

            console.log(GVar.BaseUrl + GVar.GetPrelimFullReviews," REVIEW LATER FULL REVIEW");
            fetch(  GVar.BaseUrl + GVar.GetPrelimFullReviews,{
                      method: 'POST',
                      headers:{
                                  'token' : TOKEN
                              },
                      body: formdata,       
                      }).then((response) => response.text())
                     .then((responseJson) => {

                      var x = JSON.parse(responseJson.replace("-", ""));
                      this.setState({isModalLoading : false });
                      

                      if( x.status == 1 ){

                          console.log("reviews",x);
                          this.setState({review : x.review });

                      }else{

                      }
                      
                     }).catch((error)=>{ Alert.alert(error)});
      
    }); 
  }

  DownloadPDFModal( index ){

      console.log("pdf index",index);
     

        var GVar = GlobalVariables.getApiUrl();
        this.setState({isresultLoading : true });
      
       
        console.log("my count",GVar.MockPrelimPdf+index);
        let formdata = new FormData();

        formdata.append('student_id',        this.state.student_id );
    
        var TOKEN = this.state.TOKEN;

        console.log("mty Token",TOKEN);
        console.log("my formadat",formdata);
        fetch( GVar.MockPrelimPdf+ index ,{
                          method: 'POST',
                          headers:{
                                      'token' : TOKEN
                                  },
                          body: formdata,       
                          }).then((response) => response.text())
                        .then((responseJson) => {

                          var x = JSON.parse(responseJson.replace("-", ""));
                          this.setState({isresultLoading : false });
                        
                            var options = {
                    
                                html: x.html, // HTML String

                              // ****************** OPTIONS BELOW WILL NOT WORK ON ANDROID **************                              
                                fileName: 'prelim-full-syllabus' + index ,          /* Optional: Custom Filename excluded extension
                                                            Default: Randomly generated
                                                          */

                                directory: 'docs',         /* Optional: 'docs' will save the file in the `Documents`
                                                            Default: Temp directory */

                                base64: true ,               


                                height: 800,                
                                width: 1056,               /* Optional: 1056 sets the width of the DOCUMENT that will produced
                                                            Default: 792
                                                          */
                                padding: 24,                /* Optional: 24 is the # of pixels between the outer paper edge and
                                                                    corresponding content edge.  Example: width of 1056 - 2*padding
                                                                    => content width of 1008
                                                            Default: 10
                                                          */
                          };

                          //const source = {uri:data.filePath};
                          this.setResultModalVisible(false);

                          console.log("options",options);
                          RNHTMLtoPDF.convert(options).then((data) => {

                        if( Platform.OS == 'ios' ){
                            this.setState({isLoading : false });
                            RNFetchBlob.ios.openDocument(data.filePath);

                          }else{
                            this.setState({isLoading : false });
                            this.props.navigation.navigate('ViewPDF',{source:data.filePath })
                            Alert.alert('Successfully downloaded in Documents' );
                          }
                        // this.props.navigation.navigate('ViewPDF',{source:data.filePath })
                        // Alert.alert('Successfully downloaded in Downloads' );
                      });
                          
                              
                          
                        }).catch((error)=>{ Alert.alert(error)});



  }

  getIndex(value, arr, prop) {
    for(var i = 0; i < arr.length; i++) {
        if(arr[i][prop] === value) {
            return i;
        }
    }
    return -1; //to handle the case where the value doesn't exist
  }


  _OnClickAnswer( item, exam_id){

    console.log("answerITEM",item);
    console.log("exam_id",exam_id);

    var questions  = this.state.questions;

    var index = this.getIndex(exam_id, questions, 'exam_id');

    this.setState({ questions : questions });
    console.log("questions on click",questions[index]);
    console.log("answer type on click Answer",questions[index].answer_id);


    var Passanswer_id;
    if(questions[index].answer_type == 'single'){

     
      console.log("in if");
      if( questions[index].answer_id == item.answer_id ){
  
        questions[index].answer_id = '';
        Passanswer_id = '';
      }else{
        
        questions[index].answer_id = item.answer_id;
        Passanswer_id = item.answer_id;
      }
    }else{

      if( questions[index].answer_id.split(',').contains(item.answer_id)){

        questions[index].answer_id = '';
        Passanswer_id = '';
      }else{
        questions[index].answer_id = item.answer_id;
        Passanswer_id = item.answer_id;
      }

    }

    var GVar = GlobalVariables.getApiUrl();

    AsyncStorage.multiGet(["TOKEN", "student_id"]).then(response => {

            var TOKEN                          = response[0][1];
            var student_id                     = response[1][1];
            
           
            this.setState({
                            isModalLoading      : true, 
                      });
           
       
            let formdata = new FormData();

            formdata.append('student_id',       student_id );
            formdata.append('exam_id',          exam_id );
            formdata.append('question_id',      item.question_id );
            formdata.append('answer_id',        Passanswer_id);
           
            
            console.log(formdata,"on click answer formdata");
            console.log(TOKEN,"TOKEN");

            console.log( GVar.BaseUrl + GVar.UpdatePrelimAnswers,"on click answer ");
            fetch( GVar.BaseUrl + GVar.UpdatePrelimAnswers,{
                      method: 'POST',
                      headers:{
                                  'token' : TOKEN
                              },
                      body: formdata,       
                      }).then((response) => response.text())
                     .then((responseJson) => {

                      var x = JSON.parse(responseJson.replace("-", ""));
                      this.setState({isModalLoading : false });
                      

                      if( x.status == 1 ){

                          console.log(x, "AnswerResponce");
                          //this.setState({questions : x.questions });

                      }else{

                      }
                      
                     }).catch((error)=>{ Alert.alert(error)});

    });

    AsyncStorage.multiGet(["TOKEN", "student_id"]).then(response => {

            var TOKEN                          = response[0][1];
            var student_id                     = response[1][1];
          
            this.setState({
                            TOKEN          : TOKEN,    
                            student_id     : student_id,
                            isModalLoading :true
                      });

            let formdata = new FormData();

            formdata.append('student_id',        student_id );
            formdata.append('count',         this.state.noOfMockPrelimGiven );
            
            console.log(formdata,"formdata ON CLICK ANSWER FULL REVIEW");
            console.log(TOKEN,"TOKEN");

            console.log(  GVar.BaseUrl + GVar.GetPrelimFullReviews," ON CLICK ANSWER FULL REVIEW");

            fetch(  GVar.BaseUrl + GVar.GetPrelimFullReviews,{
                      method: 'POST',
                      headers:{
                                  'token' : TOKEN
                              },
                      body: formdata,       
                      }).then((response) => response.text())
                     .then((responseJson) => {

                      var x = JSON.parse(responseJson.replace("-", ""));
                      this.setState({isModalLoading : false });
                      

                      if( x.status == 1 ){

                          console.log("reviews",x.review);
                          this.setState({review : x.review });

                      }else{

                      }
                      
                     }).catch((error)=>{ Alert.alert(error)});
      
    }); 
  }

  setTestModalVisible(visiblity){
    this.setState({testModalVisible:visiblity});
  }

  changingStatusOfStartTest(){

    var GVar = GlobalVariables.getApiUrl();

    AsyncStorage.multiGet(["TOKEN", "student_id"]).then(response => {

      var TOKEN                          = response[0][1];
      var student_id                     = response[1][1];
     
      //this.setState({nextPostion : index_end});
      this.setState({isLoading:true})

      let formdata = new FormData();

      formdata.append('student_id',       student_id );
      formdata.append('count',        this.state.noOfMockPrelimGiven );
      formdata.append('go',               'submit' );
      
      console.log(formdata,"formdata ON SUBMIT CHANGE STATUS");
      console.log(TOKEN,"TOKEN");

      console.log(GVar.BaseUrl + GVar.GetPrelimFullQuestions," ON SUBMIT CHANGE STATUS");
      fetch( GVar.BaseUrl + GVar.GetPrelimFullQuestions,{
                method: 'POST',
                headers:{
                            'token' : TOKEN
                        },
                body: formdata,       
                }).then((response) => response.text())
               .then((responseJson) => {

                var x = JSON.parse(responseJson.replace("-", ""));
                this.setState({isLoading : false });
                

                if( x.status == 1 ){

                    console.log(x, "questions");
                    //this.setState({questions : x.questions });

                }else{

                }
                
               }).catch((error)=>{ Alert.alert(error)});

        }); 


        AsyncStorage.multiGet(["TOKEN", "student_id"]).then(response => {

          var TOKEN                          = response[0][1];
          var student_id                     = response[1][1];
        
          this.setState({
                          TOKEN          : TOKEN,    
                          student_id     : student_id,
                    });

                    this.setState({isLoading : true });

          let formdata = new FormData();

          formdata.append('student_id',        student_id );

          console.log(formdata,"formdata ON SUBMIT CHANGE STATUS MOCK MAINS");
          console.log(TOKEN,"TOKEN");

          console.log(GVar.BaseUrl + GVar.MockPrelims ," ON SUBMIT CHANGE STATUS MOCK prelim");
          fetch( GVar.BaseUrl + GVar.MockPrelims ,{
                    method: 'POST',
                    headers:{
                                'token' : TOKEN
                            },
                    body: formdata,       
                    }).then((response) => response.text())
                  .then((responseJson) => {

                    var x = JSON.parse(responseJson.replace("-", ""));
                    this.setState({status : x.status });
                    this.setState({isLoading : false });
                    this.setState({nextPostion : 0 });

                    if( x.status == 1){
                      
                      console.log("mock prelim ",x);
                     // for now considering x.mockMains.test is yes

                    
                       //comment this after removing async comment
                       if(x.mockPrelim.test == 'yes'){
                        this.setState({givenAllTest:1});
                      }
                    

                      console.log("submit length",x.mockPrelim.mockmain_score.length);
                      console.log("score total ",x.mockPrelim.mockmain_score[x.mockPrelim.mockmain_score.length-1].total_questions)
                      if(x.mockPrelim.mockmain_score.length > 0){
                        this.setState({last_result_count:x.mockPrelim.mockmain_score.length})
                        console.log("in if total question =",x.mockPrelim.mockmain_score[x.mockPrelim.mockmain_score.length-1]);
                        this.setState({latest_result:x.mockPrelim.mockmain_score[x.mockPrelim.mockmain_score.length-1]})
                        this.setResultModalVisible(true);
                      }
                      

                      this.setState({noOfMockPrelimGiven:parseInt(x.mockPrelim.noOfMockPrelimsGiven)+1});
                      this.setState({assignedModules:x.mockPrelim.assignedModules});

                      console.log("noOfGivenTest",this.state.noOfMockPrelimGiven);
                      this.setState({mockmain_score:x.mockPrelim.mockmain_score});

                      if(this.state.noOfMockPrelimGiven == 1){
                        this.setState({disableTS1:false});
                        this.setState({disableTS2:true});
                        this.setState({disableTS3:true});
                        this.setState({disableTS4:true});
                        this.setState({disableTS5:true});
                      }else if(this.state.noOfMockPrelimGiven == 2){
                        this.setState({disableTS1:true});
                        this.setState({disableTS2:false});
                        this.setState({disableTS3:true});
                        this.setState({disableTS4:true});
                        this.setState({disableTS5:true});
                      }else if(this.state.noOfMockPrelimGiven == 3){
                        this.setState({disableTS1:true});
                        this.setState({disableTS2:true});
                        this.setState({disableTS3:false});
                        this.setState({disableTS4:true});
                        this.setState({disableTS5:true});
                      }else if(this.state.noOfMockPrelimGiven == 4){
                        this.setState({disableTS1:true});
                        this.setState({disableTS2:true});
                        this.setState({disableTS3:true});
                        this.setState({disableTS4:false});
                        this.setState({disableTS5:true});
                      }else if(this.state.noOfMockPrelimGiven == 5){
                        this.setState({disableTS1:true});
                        this.setState({disableTS2:true});
                        this.setState({disableTS3:true});
                        this.setState({disableTS4:true});
                        this.setState({disableTS5:false});
                      }else if(this.state.noOfMockPrelimGiven > 5){
                        console.log("prelim test > than 5");
                        this.setState({disableTS1:true});
                        this.setState({disableTS2:true});
                        this.setState({disableTS3:true});
                        this.setState({disableTS4:true});
                        this.setState({disableTS5:true});

                      }

                      
                    }else{
                      this.setState({message : x.message });

                    }
                    
                  }).catch((error)=>{ Alert.alert(error)});

        });  


    
  }


  DownloadPDF( index ){

        console.log("pdf index",index);

          var GVar = GlobalVariables.getApiUrl();
          this.setState({isLoading : true });
        
          var count  = index +1;
          console.log("my count",GVar.MockPrelimPdf+count);
          let formdata = new FormData();

          formdata.append('student_id',        this.state.student_id );
      
          var TOKEN = this.state.TOKEN;

          console.log("mty Token",TOKEN);
          console.log("my formadat",formdata);
          fetch( GVar.MockPrelimPdf+ count ,{
                            method: 'POST',
                            headers:{
                                        'token' : TOKEN
                                    },
                            body: formdata,       
                            }).then((response) => response.text())
                          .then((responseJson) => {

                            var x = JSON.parse(responseJson.replace("-", ""));
                            this.setState({isLoading : false });
                          
                              var options = {
                      
                                  html: x.html, // HTML String

                                // ****************** OPTIONS BELOW WILL NOT WORK ON ANDROID **************                              
                                  fileName: 'prelim-full-syllabus' + count ,          /* Optional: Custom Filename excluded extension
                                                              Default: Randomly generated
                                                            */

                                  directory: 'docs',         /* Optional: 'docs' will save the file in the `Documents`
                                                              Default: Temp directory */

                                  base64: true ,               


                                  height: 800,                
                                  width: 1056,               /* Optional: 1056 sets the width of the DOCUMENT that will produced
                                                              Default: 792
                                                            */
                                  padding: 24,                /* Optional: 24 is the # of pixels between the outer paper edge and
                                                                      corresponding content edge.  Example: width of 1056 - 2*padding
                                                                      => content width of 1008
                                                              Default: 10
                                                            */
                            };

                            //const source = {uri:data.filePath};

                            console.log("options",options);
                            RNHTMLtoPDF.convert(options).then((data) => {

                        if( Platform.OS == 'ios' ){
                         
                            RNFetchBlob.ios.openDocument(data.filePath);

                          }else{
                           
                            this.props.navigation.navigate('ViewPDF',{source:data.filePath })
                            Alert.alert('Successfully downloaded in Documents' );
                          }
                        // this.props.navigation.navigate('ViewPDF',{source:data.filePath })
                        // Alert.alert('Successfully downloaded in Downloads' );
                      });
                            
                                
                            
                          }).catch((error)=>{ Alert.alert(error)});

  }

  setResultModalVisible(visible){
    this.setState({resultModalVisible:visible});
  }


  

  render() {

    const styleStart = '<div style="color:#150F68;font-weight: 700;">';
    const styleEnd   = '</div>';  
  
    return (
      <View style={{flex: 1,width:wp('100%')}}>

            <ScrollView>

              <View style={{ alignItems:'center', alignself:'center',}}>

             { this.state.status? 
            
              <View style={{flex: 1, height:hp('400%'),width:wp('95%'),backgroundColor: '#FFFFFF',marginTop:'0%',}}>

                 {this.state.givenAllTest? 
                     <View> 
                      <View style={{ height:hp('5%'), backgroundColor: '#3f4c6b', marginTop:'2%', justifyContent:'center', alignItems:'center'}}>
                      <Text  style={{fontSize: RF(3),color:'#FFFFFF', alignself:'center', marginLeft:wp('2%') }}>Instructions</Text>
                      </View>
    
                    <View style={{  backgroundColor: '#FFFFFF',marginTop:'5%'}}>
                        <Text style={{fontSize: RF(2.5),color:'#000000', marginLeft:wp('2%') }}>{'\u2022'} All questions are compulsory.</Text>
                        <Text style={{fontSize: RF(2.5),color:'#000000', marginLeft:wp('2%') }}>{'\u2022'} Number of questions: 100 Questions</Text>
                        <Text style={{fontSize: RF(2.5),color:'#000000', marginLeft:wp('2%') }}>{'\u2022'} Time allotted: 120 minutes </Text>
                        <Text style={{fontSize: RF(2.5),color:'#000000', marginLeft:wp('2%') }}>{'\u2022'} 2 marks will be given for each correct question.</Text>
                        <Text style={{fontSize: RF(2.5),color:'#000000', marginLeft:wp('2%') }}>{'\u2022'} Wrong Answer: -0.67marks /Qusetion</Text>
                        <Text style={{fontSize: RF(2.5),color:'#000000', marginLeft:wp('2%') }}>{'\u2022'} No Answer : 0 marks / Question</Text>
                     </View>
    
                    <View style={{marginTop:'10%',flexDirection:'row',alignItems:'center',justifyContent:"center"}}>
    
                      
                        <TouchableOpacity style={{ height:hp('5%'),width:wp('27%'), backgroundColor:'#1ab395' , justifyContent:'center', alignItems:'center',marginLeft:'3%', marginBottom:'0%'}} 
                          onPress={() => {
                            //this.setTestModalVisible(true);
                            this.insertMockPrelimQuestions();
                           
                          }}
                          disabled={this.state.disableTS1}
   
                          >
                        <View style={{ alignItems:'center',backgroundColor: this.state.disableTS1 ? '#DCDCDC' : '#1ab395'}}>
                            <Text  style={{fontSize: RF(3),color:'#FFFFFF', alignself:'center',  }}>Take Test 1</Text>
                        </View>
                        </TouchableOpacity>
                      
                        <TouchableOpacity style={{ height:hp('5%'),width:wp('27%'), backgroundColor: '#1ab394', justifyContent:'center',marginLeft:'3%', alignItems:'center',  marginBottom:'0%'}} 
                          onPress={() => {//this.setTestModalVisible(true);
                            this.insertMockPrelimQuestions();
                          
                          }}
                          disabled={this.state.disableTS2}>
                        <View style={{ alignItems:'center', backgroundColor: this.state.disableTS2 ? '#DCDCDC' : '#1ab395'}}>
                            <Text  style={{fontSize: RF(3),color:'#FFFFFF', alignself:'center',  }}>Take Test 2</Text>
                        </View>
                        </TouchableOpacity>
                      
    
                        <TouchableOpacity style={{ height:hp('5%'),width:wp('27%'), backgroundColor: '#1ab394',marginLeft:'3%',marginRight:'3%', justifyContent:'center', alignItems:'center',  marginBottom:'0%'}} 
                          onPress={() => {//this.setTestModalVisible(true);
                            this.insertMockPrelimQuestions();
                            
                          }}
                          disabled={this.state.disableTS3}>
                        <View style={{ alignItems:'center',backgroundColor: this.state.disableTS3 ? '#DCDCDC' : '#1ab395'}}>
                            <Text  style={{fontSize: RF(3),color:'#FFFFFF', alignself:'center',  }}>Take Test 3</Text>
                        </View>
                        </TouchableOpacity>
                    
                    </View>
    
                    <View style={{marginTop:'10%',flexDirection:'row',alignItems:'center',justifyContent:"center"}}>
    
                        <TouchableOpacity style={{ height:hp('5%'),width:wp('27%'), backgroundColor: '#1ab394', justifyContent:'center', alignItems:'center',marginLeft:'3%', marginBottom:'0%'}} 
                          onPress={() => {//this.setTestModalVisible(true);
                             this.insertMockPrelimQuestions();
                            
                          }}
                          disabled={this.state.disableTS4}>
                        <View style={{ alignItems:'center',backgroundColor: this.state.disableTS4 ? '#DCDCDC' : '#1ab395'}}>
                            <Text  style={{fontSize: RF(3),color:'#FFFFFF', alignself:'center',  }}>Take Test4</Text>
                        </View>
                        </TouchableOpacity>
                      
    
                      
                        <TouchableOpacity style={{ height:hp('5%'),width:wp('27%'), backgroundColor: '#1ab394', justifyContent:'center',marginLeft:'3%',marginRight:'3%', alignItems:'center',  marginBottom:'0%'}} 
                          onPress={() => {//this.setTestModalVisible(true);
                            this.insertMockPrelimQuestions();
                           
                          }}
                          disabled={this.state.disableTS5}>
                        <View style={{ alignItems:'center',backgroundColor: this.state.disableTS5 ? '#DCDCDC' : '#1ab395'}}>
                            <Text  style={{fontSize: RF(3),color:'#FFFFFF', alignself:'center',  }}>Take Test 5</Text>
                        </View>
                        </TouchableOpacity>
                    
                    </View>
                    </View>
                    :
                    <View/>

                 }    

                {this.state.noOfMockPrelimGiven > 0  ? 

                        <View style={{ alignItems:'center', alignself:'center',}}>
                        <FlatList
                          extraData={this.state}
                          data={this.state.mockmain_score}
                          renderItem={({item, index}) => 
                          <View style={{flex:1, width:wp('95%'), marginTop:hp('2%')}}>
                              <View style={{flex: 1, backgroundColor: '#1ab394',  justifyContent:'center', alignItems:'center'}} >
                                  <Text  style={{fontSize: RF(4),color:'#FFFFFF', alignself:'center' }}>Mock Prelim Test {index + 1} Score</Text>
                              </View>

                              <View style={{flex: 4, backgroundColor: '#1f2f46', flexDirection:'column', marginTop:hp('1%')}} >
                                          <View style={{flex: 4,flexDirection: 'row',justifyContent: 'center',marginTop:hp('2%'),  height: '80%', marginBottom:hp('4%')}} >

                                          <View style={{width: wp('50%'),justifyContent:'space-between'}} >

                                              <View>
                                                      <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>Total Questions</Text>
                                              </View>

                                              <View>
                                                      <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>Correct Answers</Text>
                                              </View>

                                              <View>
                                                      <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>Wrong Answers</Text>
                                              </View>

                                              <View>
                                                      <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>UnAttempted Question</Text>
                                              </View>

                                              <View>
                                                      <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }} >Total Marks</Text>
                                              </View>

                                              <View>
                                                      <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>Your Score</Text>
                                              </View>

                                              <View>
                                                      <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>Grade</Text>
                                              </View>
                                          </View>

                                          <View style={{width: wp('20%'),justifyContent:'space-between' }} >

                                              <View>
                                                      <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>{item.total_questions}</Text>
                                              </View>

                                              <View>
                                                      <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>{item.correct_answer_count}</Text>
                                              </View>

                                              <View>
                                                      <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>{item.wrong_answer_count}</Text>
                                              </View>

                                              <View>
                                                      <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>{item.unattempted_question}</Text>
                                              </View>

                                              <View>
                                                      <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>{item.total_marks}</Text>
                                              </View>

                                              <View>
                                                      <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>{item.your_score}</Text>
                                              </View>

                                              <View>
                                                      <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>{item.grade}</Text>
                                              </View>
                                          </View>

                                    </View>

                                    <View style={{flex: 2,flexDirection: 'row',justifyContent: 'center',}} >
                                          <View style= {{backgroundColor:'#FFFFFF', width:wp('80%'), marginBottom: hp('1%'), justifyContent:'center'}}>
                                              <Text style={{color:'#e16800', fontSize: RF(2.5), alignself:'center',marginLeft:wp('2%') }}>Remark: {item.remarks}</Text>
                                          </View>
                                    </View>
                              </View>

                              <View style={{flex: 1, backgroundColor: '#FFFFFF',}} >
                                  <TouchableOpacity onPress={ 
                                    this.DownloadPDF.bind(this, index)}>
                                        <View style={{flexDirection: 'row', backgroundColor:'#1ab394', width:wp('80%'), marginLeft:wp('9%'), justifyContent:'center', borderRadius: wp('2%'), alignItems:'center', marginTop:hp('2%'),}}>
                                            <Text style={{color:'#FFFFFF',fontSize: RF(4), alignself:'center'}}>Download Solution</Text>
                                        </View>
                                    </TouchableOpacity>
                              </View>

                          </View>    
                        }/>
                        </View>
                  :
                  <View/>
                }
                    
 
               
 
                </View>
              :
              <View style={{ alignItems:'center', alignself:'center',}}>
                <View style={{flex:1,height:hp('70%'), flexDirection:'column', backgroundColor:'#1ab394', width:wp('95%'), alignself:'center', marginTop:hp('5%')}}>
                    <Text style={styles.error}>You need to pass all the assigned modules to take the full syllabus exam"</Text>
                </View>
              </View>
            }
            </View>

            </ScrollView>
            {this.state.isLoading ? <ActivityIndicator
                    animating     ={true}
                    transparent   ={true}
                    visible       ={false}
                    style         ={styles.indicator}
                    size          ="large"
                  /> : <View/>  } 
            <Quotes/>  

            <Modal
              animationType="slide"
              transparent={false}
              visible={this.state.testModalVisible}
              onRequestClose={() => {
                Alert.alert(
                  'Submit Exam',
                  'Do You Really want to Submit Your Exam!',
                  [
                    {text: 'Cancel', onPress: () => {}},
                    {text: 'OK', onPress: () => {  this.setTestModalVisible(false);
                      this.changingStatusOfStartTest();}},
                  ],
                  { cancelable: false }
                );
              }}>

            <View style={{flex: 1, width:wp('100%')}}>

              <View style={{flex: .5, flexDirection:'row', marginTop:'2%'}}>
                    <View style={{flex: 5,flexDirection:'row', justifyContent:'center'}} >
                          <CountDown
                            digitBgColor  = '#1bb496'
                            digitTxtColor = '#FFFFFF'
                            timeTxtColor  = '#000000'
                            until={this.state.totalDuration}
                            //duration of countdown in seconds
                            timetoShow={('H', 'M', 'S')}
                            //formate to show
                            onFinish={() => {this.onFinishTimer();}}
                            //on Finish call
                            //onPress={() => alert('hello')}
                            //on Press call
                            size={20}
                          />
                    </View>
                    
                    <View style={{flex: 1}} >
                          <TouchableOpacity
                            onPress={() => {
                              this.setPlusModalVisible(!this.state.plusModalVisible);
                            }}>

                            <View style={{
                                  borderWidth:1,
                                  borderColor:'rgba(0,0,0,0.2)',
                                  alignItems:'center',
                                  justifyContent:'center',
                                  width:50,
                                  position: 'relative',                                          
                                  bottom: 0,                                                    
                                  right: 10,
                                  height:50,
                                  backgroundColor:'#fff',
                                  borderRadius:100,
                                }}
                              >    
                              <Text textAlign={'center'} style={{fontSize: RF(4), fontWeight:'bold', alignself:'center', color:'#1ab395'}}>RL</Text>
                            
                            </View>
                          </TouchableOpacity>
                    </View>
              </View>
              <View style={{flex: 5.5,marginTop:'5%'}}>
                <ScrollView>
                <View style={{ alignItems:'center', alignself:'center'}}>
                      <FlatList
                        extraData={this.state}
                        data={this.state.questions}
                        renderItem={({item, index}) => 

                        <View style={{flex:1, flexDirection:'column',width:wp('95%'), justifyContent:'center', backgroundColor:'#FFFFFF',  borderWidth:2, borderColor:'#CCCCCC'}}>
                            

                            <View style={{flex:1, flexDirection:'column',marginLeft:wp('2%'), marginTop:hp('2%'),}}>
                                <Text style={{marginTop:'3.5%', color:'#150F68'}}>Q. {this.state.nextPostion + index + 1}. </Text>
                                <HTML html={styleStart + item.question + styleEnd}  imagesMaxWidth={Dimensions.get('window').width} />
                            </View>
                          
                                
                            { item.answer_type == 'single' ? 

                                <FlatList
                                  extraData={this.state}
                                  data={item.answers}
                                  renderItem={({item : answers, index}) => 
                              
                                  <View key={index} style={{flex:1, flexDirection:'row',marginLeft:wp('2%'),}}>
                                            <CheckBox
                                                checkedIcon='dot-circle-o'
                                                uncheckedIcon='circle-o'
                                                checked={ answers.answer_id == item.answer_id ? true : false}
                                                onPress={() => this._OnClickAnswer(answers, item.exam_id)}
                                            />
                                          <Text style={{flex:1,marginTop:'4.5%',marginLeft:-22}}>{String.fromCharCode(index+65)}.  {answers.answer}</Text>
                                  </View>
                                }/>  

                                :

                              <View/>
                            } 


                            <View style={{flex:1, flexDirection:'row',marginLeft:wp('50%')}}>
                                      <CheckBox
                                        center
                                        checkedColor = '#7f00ff'
                                        containerStyle={{backgroundColor:'transparent', borderColor:'transparent'}}
                                        textStyle ={{fontSize: RF(2.5),fontWeight: 'bold',color:'#FFFFFF'}}
                                        size = {hp('2.5%')}
                                        checked={item.review_later == '1' ? true : false } 
                                        onPress={this._reviewLater.bind(this, item) }/>

                                  <Text style={{flex:1,marginTop:'9%',marginLeft:-22, color:"#7f00ff", fontWeight:'bold'}}>Review Later</Text>
                              </View>    
                                

                        </View>
                      }
                      />
                </View>
                </ScrollView>
              </View>
              <View style={{flex: .2, flexDirection:'row', marginTop:'2%', alignItems:'center', justifyContent:'space-between'}}>
                    
                    { this.state.nextPostion > '4' ?
                      <View style={{flex: 2,}} >
                          <TouchableOpacity
                            onPress={() => {
                              this.onNext("previous");
                            }}>

                            <View style={{marginLeft:5,alignItems:'center',  backgroundColor:'#1ab395', height:hp('5%')}}>    
                              <Text textAlign={'center'} style={{fontSize: RF(2.5), fontWeight:'bold', alignself:'center', color:'#FFFFFF'}}>PREVIOUS</Text>
                            </View>
                          </TouchableOpacity>
                      </View>
                      :
                      <View/>
                    }
                    
                    { ( this.state.nextPostion + 5 < this.state.totalQuestionCount) ?
                  
                      <View style={{flex: 2, }} >
                            <TouchableOpacity
                              onPress={() => {
                                this.onNext("next");
                              }}>

                              <View style={{marginLeft:5,marginRight:5, alignItems:'center', backgroundColor:'#1ab395', height:hp('5%')}}>
                                <Text textAlign={'center'} style={{fontSize: RF(2.5), fontWeight:'bold', alignself:'center',color:'#FFFFFF'}}>NEXT</Text>
                              </View>
                            </TouchableOpacity>
                      </View>
                    :
                      <View/>
                    }

                    { this.state.nextPostion >= this.state.totalQuestionCount - 5  ?
                  
                      <View style={{flex: 2, }} >
                            <TouchableOpacity
                              onPress={() => {
                              
                             
                                this.setTestModalVisible(false);
                                this.changingStatusOfStartTest();
                              }}>

                              <View style={{marginLeft:5,marginRight:5, alignItems:'center', backgroundColor:'#1ab395', height:hp('5%')}}>
                                <Text textAlign={'center'} style={{fontSize: RF(2.5), fontWeight:'bold', alignself:'center',color:'#FFFFFF'}}>SUBMIT</Text>
                              </View>
                            </TouchableOpacity>
                      </View>
                    :
                      <View/>
                    }
              </View>
            <Modal
              animationType="slide"
              transparent={false}
              visible={this.state.plusModalVisible}
              onRequestClose={() => {
                this.setPlusModalVisible(false);
              }}>
              <View style={{ height:hp('5%'), backgroundColor: '#1ab395', marginTop:'1%',marginLeft:'5%',marginRight:'5%', justifyContent:'center', alignItems:'center'}}>
                  <View style={{flex: 1,alignSelf: 'flex-end', position: 'absolute',marginTop:'12%',backgroundColor:'#FFFFFF'}} >
                  <Text  style={{fontSize: RF(3),color:'#FFFFFF', alignself:'center', marginLeft:wp('2%') }}>Review Later</Text>
                </View>
                          <TouchableOpacity
                            onPress={() => {
                              this.setPlusModalVisible(false);
                            }}>

                            <View style={{
                                    borderWidth:1,
                                    borderColor:'rgba(0,0,0,0.2)',
                                    alignItems:'center',
                                    justifyContent:'center',
                                    width:50,
                                    position: 'relative',                                          
                                    bottom: 0,                                                    
                                    right: 10,
                                    height:50,
                                    backgroundColor:'#fff',
                                    borderRadius:100,
                                  }}
                              >    
                              <Icon name="times-circle"  size={30} color="#01a699" />
                            
                            </View>
                          </TouchableOpacity>
                    </View>
              <View style={{marginTop:hp('20%'), marginBottom:hp('.5%'), backgroundColor:'#FFFFFF'}}>
                <View style={{marginBottom:hp('10%')}}>
                  <ScrollView>
                  
                  <FlatList
                      extraData={this.state}
                      data={this.state.review}
                      renderItem={({item, index}) => 
                          <View style={{flex:1, flexDirection:'row',marginLeft:wp('1%'),}}>
                            {this.reviewQuestions(item)}
                          </View>
                  }
                  numColumns={5}
                  />
                  </ScrollView>
                </View>
              </View>
            </Modal>

            {this.state.isModalLoading ? <ActivityIndicator
                    animating     ={true}
                    transparent   ={true}
                    visible       ={false}
                    style         ={styles.indicator}
                    size          ="large"
                  /> : <View/>  } 

            </View>

          
        </Modal>

        <Modal
        animationType="slide"
        transparent={false}
        visible={this.state.resultModalVisible}
        onRequestClose={() => {
          this.setResultModalVisible(false);
        }}
        >
                  <View style={{ height:hp('5%'), backgroundColor: '#1ab395', marginTop:'1%',marginLeft:'5%',marginRight:'5%', justifyContent:'center', alignItems:'center'}}>
                    <Text  style={{fontSize: RF(3),color:'#FFFFFF', alignself:'center', marginLeft:wp('2%') }}>Prelim Full Syllabus Test {this.state.last_result_count} Score</Text>
                  </View>
                
                    <View style={{flex: 1,alignSelf: 'flex-end', position: 'absolute',marginTop:'12%',backgroundColor:'#FFFFFF'}} >
                            <TouchableOpacity
                              onPress={() => {
                                this.setResultModalVisible(false);
                              }}>

                              <View style={{
                                      borderWidth:1,
                                      borderColor:'rgba(0,0,0,0.2)',
                                      alignItems:'center',
                                      justifyContent:'center',
                                      width:50,
                                      position: 'relative',                                          
                                      bottom: 0,                                                    
                                      right: 10,
                                      height:50,
                                      backgroundColor:'#fff',
                                      borderRadius:100,
                                    }}
                                >    
                                <Icon name="times-circle"  size={30} color="red" />
                              
                              </View>
                            </TouchableOpacity>
                      </View>

                      <View style={{flex: 4, backgroundColor: '#1f2f46', flexDirection:'column', marginTop:hp('15%'),marginRight:'5%',marginLeft:'5%'}} >
                        <View style={{flex: 4,flexDirection: 'row',justifyContent: 'center',marginTop:hp('2%'),  height: '80%', marginBottom:hp('4%')}} >

                          <View style={{width: wp('50%'),justifyContent:'space-between'}} >

                              <View>
                                      <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>Total Questions</Text>
                              </View>

                              <View>
                                      <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>Correct Answers</Text>
                              </View>

                              <View>
                                      <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>Wrong Answers</Text>
                              </View>

                              <View>
                                      <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>UnAttempted Question</Text>
                              </View>

                              <View>
                                      <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }} >Total Marks</Text>
                              </View>

                              <View>
                                      <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>Your Score</Text>
                              </View>

                              <View>
                                      <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>Grade</Text>
                              </View>
                          </View>

                          <View style={{width: wp('20%'),justifyContent:'space-between' }} >

                              <View>
                                      <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>{this.state.latest_result.total_questions}</Text>
                              </View>

                              <View>
                                  <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>{this.state.latest_result.correct_answer_count}</Text>
                              </View>



                              <View>
                                      <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>{this.state.latest_result.wrong_answer_count}</Text>
                              </View>

                              <View>
                                      <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>{this.state.latest_result.unattempted_question}</Text>
                              </View>

                              <View>
                                      <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>{this.state.latest_result.total_marks}</Text>
                              </View>

                              <View>
                                      <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>{this.state.latest_result.your_score}</Text>
                              </View>

                              <View>
                                      <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>{this.state.latest_result.grade}</Text>
                              </View>
                          </View>

                        </View>

                        <View style={{flex: 2,flexDirection: 'row',justifyContent: 'center',}} >
                              <View style= {{backgroundColor:'#FFFFFF', width:wp('80%'), marginBottom: hp('1%'), justifyContent:'center'}}>
                                  <Text style={{color:'#e16800', fontSize: RF(2.5), alignself:'center',marginLeft:wp('2%') }}>Remark: {this.state.latest_result.remarks}</Text>
                              </View>
                        </View>
                      </View>

                      <View style={{flex: 1, backgroundColor: '#FFFFFF',}} >
                          <TouchableOpacity onPress={
                            this.DownloadPDFModal.bind(this,this.state.last_result_count)
                            }>
                                <View style={{flexDirection: 'row', backgroundColor:'#1ab394', width:wp('80%'), height:hp('10%'), marginLeft:wp('9%'), justifyContent:'center', borderRadius: wp('2%'), alignItems:'center', marginTop:hp('2%'),}}>
                                    <Text style={{color:'#FFFFFF',fontSize: RF(4), alignself:'center'}}>Download Solution</Text>
                                </View>
                            </TouchableOpacity>
                      </View>
                      {this.state.isresultLoading ? <ActivityIndicator
                        animating     ={true}
                        transparent   ={true}
                        visible       ={false}
                        style         ={styles.indicator}
                        size          ="large"
                      /> : <View/>  } 
                    
              </Modal>
 
      </View>
    );
  }
}

