/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  StatusBar,
  ImageBackground,
  WebView,
  AsyncStorage,
  Dimensions,
  Alert,
  ListView,
  FlatList,
  ActivityIndicator,
  PermissionsAndroid,
  TouchableOpacity,
  Image,
  ScrollView
} from 'react-native';

import GlobalVariables from './../GlobalVariables/GlobalVariables.js';
import HTML from 'react-native-render-html';
import Pdf from 'react-native-pdf';
import RNFetchBlob from 'rn-fetch-blob';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import RF from "react-native-responsive-fontsize";
import Icon from 'react-native-vector-icons/FontAwesome';
import styles from './../Styles/GlobalStyle.js';
import Quotes from "../Quotes.js";
import { HeaderBackButton } from 'react-navigation';
//import ScrollView, { ScrollViewChild } from 'react-native-directed-scrollview';

type Props = {};

export default class ModuleSubContent extends Component<Props> {


  static navigationOptions = {
    header: null,
  }

  

  constructor(props) {
    super(props);


    this.state = { 
                    TOKEN       : null,
                    student_id  : null,
                    content     : [],
                    isLoading   : true,
                    moduleName  : null,
                    isVideo     : false,
                    isImage     : false,
                    isPDF       : false,
                    isHTML      : false,
         };
  }


  componentDidMount = async () => {

    var GVar = GlobalVariables.getApiUrl();

    
    AsyncStorage.multiGet(["TOKEN", "student_id"]).then(response => {

            const { navigation } = this.props;
            const content_id = navigation.getParam('content_id');
            
            console.log(content_id,"content_id")
            

            var TOKEN                          = response[0][1];
            var student_id                     = response[1][1];
            

            this.setState({ TOKEN          : TOKEN, student_id     : student_id});

            let formdata = new FormData();
            formdata.append('student_id',        student_id );

            fetch( GVar.BaseUrl + GVar.Content + content_id ,{
                      method: 'POST',
                      headers:{
                                  'token' : TOKEN
                              },
                      body: formdata,       
                      }).then((response) => response.text())
                     .then((responseJson) => {

                      var x = JSON.parse(responseJson.replace("-", ""));
                      
                      this.setState({status : x.status });
                      

                      if( x.status == 1 ){

                          var content       = x.content;

                          var word_text     = content.word_text;
                          var pdf_file      = content.pdf_file;
                          var videos        = content.videos;
                          var jpg_file      = content.jpg_file;
                          var image_title   = content.image_title;
                          var pdf_title     = content.pdf_title;

                          this.setState({

                              word_text   : word_text,
                              pdf_file    : pdf_file,
                              videos      : videos,
                              jpg_file    : jpg_file,
                              image_title : image_title,
                              pdf_title   : pdf_title,
                              
                          });

                          this.setState({isLoading:false});

                          if( pdf_file != ''){

                            this.setState({isPDF : true});

                          }

                          if( videos != ''){

                            this.setState({isVideo : true });
                          }

                          if( jpg_file != ''){
                            this.setState({ isImage : true});
                          }

                          this.setState({isHTML : true});

                      }else{

                        this.setState({
                            message : x.message,
                        });
                      }

                      
                     }).catch((error)=>{ Alert.alert(error)});

      });  

  }

  

  onPDFPressed(item){   

      this.setState({isLoading : true });
      var GVar = GlobalVariables.getApiUrl();

      if( Platform.OS == 'ios' ){

        const dirs = RNFetchBlob.fs.dirs

            RNFetchBlob
            .config({
              fileCache : true,
              path: dirs.DocumentDir + "/" + item
            })
            .fetch('GET', GVar.CurrentAffairsFile +item)
            .then((res) => {
             
              try{
                
                this.setState({isLoading : false });
                RNFetchBlob.ios.openDocument(dirs.DocumentDir + "/" + item);

              }catch(error){
                
                this.setState({isLoading : false });
                Alert.alert('Please check your internet connection.')
              }


            }, (error) => {
              
               this.setState({isLoading : false });
               Alert.alert('Please check your internet connection.')
              
            })

      }else{

            RNFetchBlob
                .config({
                    notification: true,
                    path: 'downloads/'+item,
                    indicator: true,
                    overwrite: true,
                    addAndroidDownloads: {
                        path: RNFetchBlob.fs.dirs.SDCardDir +'/downloads/'+item,
                        useDownloadManager: true,
                        notification: true,
                        overwrite: true,
                        description: 'downloading content...',
                        //mime: 'application/pdf/png/jpg',
                        mediaScannable: false
                    }
                })
                .fetch('GET', 'http://mentorship.iasjunior.com/admin/assets/content/' +item)
                .progress((received, total) => {

                    this.setState({isLoading : false });
                    console.log('progress', received / total)    
                })
                .then((response) => {
                  console.log("my url",GVar.CurrentAffairsFile +item);
                  this.setState({isLoading : false });
                  });
      }          
  }



  render() {

    var video = '<iframe width="325%" height="300%" src="'+this.state.videos+'"></iframe>';

    var html = '<div class="row"><div class="col-md-6">'+video+'</div></div>';

   
    return (

     <View style={{flex: 1,width:wp('100%')}} >
               
             
            <View style={{flex: 1,width:wp('100%')}}>

            <View style={{flexDirection:'row', alignself:'center', marginTop:hp('2%'), justifyContent:'space-between', backgroundColor:"#FFFFFF",}}>
              

                {this.state.isPDF ?
                  
                  <TouchableOpacity onPress={ () => this.onPDFPressed(this.state.pdf_file)}>
                        <Image style={{height:RF(10), width:RF(10), marginLeft:wp('2%')}} source={{uri: 'https://mentorship.iasjunior.com/student/assets/img/pdf.png'}}/>
                  </TouchableOpacity>
                  :
                  <View/>
                }
                
                { this.state.isImage ?
                  <TouchableOpacity onPress={ () => this.onPDFPressed(this.state.jpg_file)}>
                        <Image style={{height:RF(10), width:RF(14), marginRight:wp('2%')}} source={{uri: 'https://mentorship.iasjunior.com/student/assets/img/image.png'}}/>
                  </TouchableOpacity>
                  :
                  <View/>
                }

            </View>

            <View style={{flexDirection:'row', alignself:'center',justifyContent:'space-between', backgroundColor:"#FFFFFF",}}>
                {this.state.isPDF ?
                      <Text style={styles.Show}>{this.state.pdf_title}</Text>
                  :
                  <View/>
                }
                
                { this.state.isImage ?
                    <Text style={styles.Show}>{this.state.image_title}</Text>
                  :
                  <View/>
                }

            </View>

            <View style={{flex: 1, width:wp('100%')}}  onPress={ () => alert("MAAn")}>

                  
                  { this.state.word_text ? <WebView  source={{ html: this.state.word_text }} scalesPageToFit={true} style={{ flex: 1 }}  /> : <View/> }
                        
                   
            </View>
           
            
          
           

            </View>
             {this.state.isLoading ? <ActivityIndicator
                    animating     ={true}
                    transparent   ={true}
                    visible       ={false}
                    style         ={styles.indicator}
                    size          ="large"
                  /> : <View/>  } 
            <Quotes/>          
      </View>

    );
  }
}
