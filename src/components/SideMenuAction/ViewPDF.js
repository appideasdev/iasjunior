/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  StatusBar,
  ImageBackground,
  ScrollView,
  WebView,
  AsyncStorage,
  Dimensions,
  Alert,
  ActivityIndicator,
  TouchableHighlight
} from 'react-native';

import GlobalVariables from './../GlobalVariables/GlobalVariables.js';
import HTML from 'react-native-render-html';
import HTMLView from 'react-native-htmlview';
import { HeaderBackButton } from 'react-navigation';


import Pdf from 'react-native-pdf';
import RNFetchBlob from 'rn-fetch-blob';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import RF from "react-native-responsive-fontsize";
import styles from './../Styles/GlobalStyle.js';
import RNHTMLtoPDF from 'react-native-html-to-pdf';
import Quotes from "../Quotes.js";


type Props = {};
export default class ViewPDF extends Component<Props> {

  static navigationOptions = {
    header: null,
  }


  constructor(props) {
    super(props);
    this.state =  { 
                    source : null,
                    isLoading : true,
                  };
  }


  componentDidMount = async () => {

      this.setState({isLoading : false});
  }


  render() {

    const { navigation } = this.props;
    const navsource = navigation.getParam('source');

    const source = {uri : 'file:' + navsource}

    if( source == null || source == undefined ){
      this.setState({ isLoading : true});
    }

    return (
      <View style={styles.container}>
                

                    {this.state.isLoading ? <ActivityIndicator
                    animating     ={true}
                    transparent   ={true}
                    visible       ={false}
                    style         ={styles.indicator}
                    size          ="large"
                  /> : 

                  <Pdf
                    source={source}
                    onLoadComplete={(numberOfPages,filePath)=>{
                        console.log(`number of pages: ${numberOfPages}`);
                    }}
                    onPageChanged={(page,numberOfPages)=>{
                        console.log(`current page: ${page}`);
                    }}
                    onError={(error)=>{
                        console.log(error,"errorPDF");
                    }}
                    style={styles.pdf}/>  } 
      </View>
    );
  }
}
