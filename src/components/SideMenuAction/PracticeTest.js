/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  StatusBar,
  ImageBackground,
  ScrollView,
  WebView,
  AsyncStorage,
  Dimensions,
  Alert,
  ListView,
  FlatList,
  ActivityIndicator,
  PermissionsAndroid,
  TouchableOpacity,
  Modal
} from 'react-native';

import GlobalVariables from './../GlobalVariables/GlobalVariables.js';
import HTML from 'react-native-render-html';
import Pdf from 'react-native-pdf';
import RNFetchBlob from 'rn-fetch-blob';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import RF from "react-native-responsive-fontsize";
import styles from './../Styles/GlobalStyle.js';
import RNHTMLtoPDF from 'react-native-html-to-pdf';
import { HeaderBackButton } from 'react-navigation';


import Quotes from "../Quotes.js";
//import TakePracticeTest from "./TakePracticeTest.js";


import Icon from 'react-native-vector-icons/FontAwesome';
import { CheckBox } from 'react-native-elements'

import CountDown from 'react-native-countdown-component';
import moment, { locale } from 'moment';

type Props = {};
export default class PracticeTest extends Component<Props> {

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Practice Test',
      headerStyle: { backgroundColor:'#36404a' },
      headerTitleStyle: { color: '#FFFFFF' },
      headerTintColor: 'white',
      headerLeft:(<HeaderBackButton tintColor='#FFFFFF' onPress={()=>{navigation.navigate('First');}}/>)
  
  
    };
  };


  constructor(props) {
    super(props);


    this.state = { 
                    query       : null,
                    TOKEN       : null,
                    student_id  : null,
                    quotesSlug  : null,
                    practicetest_score     : [],
                    latest_result : [],
                    isLoading   : true,
                    moduleName  : '',
                    TestmodalVisible : false,
                    questions   : [],
                    isLoading   : true,
                    totalDuration: '',
                    modalVisible: false,
                    count : 0,
                    startTime:false,
                    nextPostion : 0,
                    timeCompleted: true,
                    totalQuestionCount:0,
                    resultModalVisible:false,
                    pos         : [
                                    {"id":"1", "indexstart":"1", "indexend":"5" },
                                    {"id":"2", "indexstart":"1", "indexend":"5" },
                                    {"id":"3", "indexstart":"1", "indexend":"5" },
                                    {"id":"4", "indexstart":"1", "indexend":"5" },
                                    {"id":"5", "indexstart":"1", "indexend":"5" },

                                    {"id":"6", "indexstart":"6", "indexend":"10" },
                                    {"id":"7", "indexstart":"6", "indexend":"10" },
                                    {"id":"8", "indexstart":"6", "indexend":"10" },
                                    {"id":"9", "indexstart":"6", "indexend":"10" },
                                    {"id":"10", "indexstart":"6", "indexend":"10" },

                                    {"id":"11", "indexstart":"11", "indexend":"15" },
                                    {"id":"12", "indexstart":"11", "indexend":"15" },
                                    {"id":"13", "indexstart":"11", "indexend":"15" },
                                    {"id":"14", "indexstart":"11", "indexend":"15" },
                                    {"id":"15", "indexstart":"11", "indexend":"15" },

                                    {"id":"16", "indexstart":"16", "indexend":"20" },
                                    {"id":"17", "indexstart":"16", "indexend":"20" },
                                    {"id":"18", "indexstart":"16", "indexend":"20" },
                                    {"id":"19", "indexstart":"16", "indexend":"20" },
                                    {"id":"20", "indexstart":"16", "indexend":"20" },

                                    {"id":"21", "indexstart":"21", "indexend":"25" },
                                    {"id":"22", "indexstart":"21", "indexend":"25" },
                                    {"id":"23", "indexstart":"21", "indexend":"25" },
                                    {"id":"24", "indexstart":"21", "indexend":"25" },
                                    {"id":"25", "indexstart":"21", "indexend":"25" },
                                  ]
         };

         const { navigation } = this.props;
          const module_id = navigation.getParam('id');
          this.setState({module_id : module_id});
  }


  componentDidMount = async () => {

    const { navigation } = this.props;
    const module_id = navigation.getParam('id');
    console.log("component module id",module_id);

    this.setState({module_id : module_id})


    var GVar = GlobalVariables.getApiUrl();

    
    AsyncStorage.multiGet(["TOKEN", "student_id"]).then(response => {

            var TOKEN                          = response[0][1];
            var student_id                     = response[1][1];
           
            this.setState({
                            TOKEN          : TOKEN,    
                            student_id     : student_id,
                      });


            let formdata = new FormData();

            formdata.append('student_id',        student_id );

            console.log(formdata,"formdata");
            console.log(TOKEN,"TOKEN");

            fetch( GVar.BaseUrl + GVar.PracticeTest + module_id ,{
                      method: 'POST',
                      headers:{
                                  'token' : TOKEN
                              },
                      body: formdata,       
                      }).then((response) => response.text())
                     .then((responseJson) => {

                      var x = JSON.parse(responseJson.replace("-", ""));
                      this.setState({isLoading : false });
                      this.setState({status : x.status });
                      console.log( "component x=",x);

                      if( x.status == 1 ){

                         

                          console.log(x.practicetest_score, "component did mount practicetest_score");
                          console.log("component did mount length",x.practicetest_score.length);
                          this.setState({practicetest_score : x.practicetest_score });
                          this.setState({moduleName : x.moduleName });
                          this.setState({count:x.practicetest_score.length+1})
                      }else{
                        this.setState({message : x.message });
                        console.log(x.message, "component did mount else message");
                        this.setState({count:1})
                      }
                      
                     }).catch((error)=>{ Alert.alert(error)});

      });  

  
      this.startTimer();

     
  }

  startTimer(){

    var that = this;
   
    //converting in seconds
    if(that.state.startTime){

    that.setState({ totalDuration: 1800 });    
    }else{
      that.setState({totalDuration:400});
      //that.setState({timerCompleted:false});
    }
    //Settign up the duration of countdown in seconds to re-render
  }

  getPracticeQuestion(){

    var GVar = GlobalVariables.getApiUrl();

    console.log("get practice Questions");

   //Starting Prelim test fetching here

   AsyncStorage.multiGet(["TOKEN", "student_id", "moduleId"]).then(response => {

    var TOKEN                          = response[0][1];
    var student_id                     = response[1][1];
    //var module_id                      = response[2][1];

    this.setState({
                    TOKEN          : TOKEN,    
                    student_id     : student_id,
              });

    //AsyncStorage.removeItem('moduleId')

    console.log(this.state.module_id,"module_id get practice ques")

    

    var TOKEN                          = response[0][1];
    var student_id                     = response[1][1];
   
    this.setState({
                    TOKEN          : TOKEN,    
                    student_id     : student_id,
                   // module_id      : module_id,
                    isLoading      : true, 
              });


    let formdata = new FormData();

    formdata.append('student_id',        student_id );
    formdata.append('module_id',         this.state.module_id );
    //formdata.append('count',        '1' );
    formdata.append('count',this.state.count);
    console.log("form practice ques",formdata);
    console.log("Token get prac ques ",TOKEN);

    fetch( GVar.BaseUrl + GVar.StartPracticeTest,{
              method: 'POST',
              headers:{
                          'token' : TOKEN
                      },
              body: formdata,       
              }).then((response) => response.text())
             .then((responseJson) => {

              console.log("Practicequestion response",responseJson);
              var x = JSON.parse(responseJson.replace("-", ""));
              this.setState({isLoading : false });
              

              if( x.status == 1 ){

                this.setState({startTime:true});
                this.startTimer();
                  console.log(x, "questions");
                  this.setState({questions : x.questions });
                  this.setTestModalVisible(true);
                  
                 
              }else{

              }
              
             }).catch((error)=>{ Alert.alert(error)});

    }); 
  }

  getReviews(){

     //Async Storage for Reviews
     var GVar = GlobalVariables.getApiUrl();

     console.log("get reviews ");

     AsyncStorage.multiGet(["TOKEN", "student_id", "moduleId"]).then(response => {

      var TOKEN                          = response[0][1];
      var student_id                     = response[1][1];
      //var module_id                      = response[2][1];

      this.setState({
                      TOKEN          : TOKEN,    
                      student_id     : student_id,
                });

      //AsyncStorage.removeItem('moduleId')

      console.log(this.state.module_id,"module_id review");

      var TOKEN                          = response[0][1];
      var student_id                     = response[1][1];
     
      this.setState({
                      TOKEN          : TOKEN,    
                      student_id     : student_id,
                      //module_id      : module_id,
                      isLoading      : true, 
                });


      let formdata = new FormData();

      //formdata.append('module_id',          '25' );
      formdata.append('module_id',          this.state.module_id );
      //formdata.append('count',        '1' );
      formdata.append('count',this.state.count);
      console.log(formdata,"formdata review");
      console.log(TOKEN,"TOKEN");
      
      formdata.append('student_id',        student_id );
      fetch( GVar.BaseUrl + GVar.StartPracticeReviews,{
                method: 'POST',
                headers:{
                            'token' : TOKEN
                        },
                body: formdata,       
                }).then((response) => response.text())
               .then((responseJson) => {

               
                var x = JSON.parse(responseJson.replace("-", ""));
                this.setState({isLoading : false });
                console.log("get review response",x);

                if( x.status == 1 ){

                    console.log("reviews",x);
                    this.setState({review : x.review });
                    this.setState({totalQuestionCount: x.review.length})


                }else{

                }
                
               }).catch((error)=>{ Alert.alert(error)});

    }); 

  }

  insertPracticeQuestions(){

         //Async Storage for Reviews
         var GVar = GlobalVariables.getApiUrl();

         console.log("insert prac ques ");
    
         AsyncStorage.multiGet(["TOKEN", "student_id", "moduleId"]).then(response => {
    
          var TOKEN                          = response[0][1];
          var student_id                     = response[1][1];
          //var module_id                      = response[2][1];
    
          this.setState({
                          TOKEN          : TOKEN,    
                          student_id     : student_id,
                    });
    
          //AsyncStorage.removeItem('moduleId')
    
          console.log(this.state.module_id,"module_id insert");
    
          var TOKEN                          = response[0][1];
          var student_id                     = response[1][1];
         
          this.setState({
                          TOKEN          : TOKEN,    
                          student_id     : student_id,
                          //module_id      : module_id,
                          isLoading      : true, 
                    });
    
                    console.log("insert practice question")
                    let myFormdata = new FormData();
                    myFormdata.append("student_id",student_id);
                    myFormdata.append("module_id",this.state.module_id);
                
                    console.log("my form data",myFormdata);
                    console.log("token",TOKEN);
                
                
                       console.log("practice api",GVar.BaseUrl + GVar.InsertPracticeQuestions);
                     

                      fetch(GVar.BaseUrl+ GVar.InsertPracticeQuestions,{
                        method:'POST',
                        headers:{
                          'token':TOKEN,
                        },
                        body:myFormdata,
                      }).then((response)=>response.text())
                      .then((responseJson)=>{

                        var x = JSON.parse(responseJson.replace("-", ""));
                        if(x.status == 0){
                          Alert.alert("Questions are not prepared..Please Try Later!!");
                        
                          this.setState({totalDuration:0});
                        }else{

                          this.getPracticeQuestion();
                          this.getReviews();
                        }
                        console.log("new insert quest response",responseJson);
                      }).catch((error)=>{Alert.alert(error)});
    
        }); 
    



    
  }

  changingStatusOfStartTest(){


    const { navigation } = this.props;
    var module_idd = navigation.getParam('id');
    var GVar = GlobalVariables.getApiUrl();

    var GVar = GlobalVariables.getApiUrl();

    AsyncStorage.multiGet(["TOKEN", "student_id", "moduleId"]).then(response => {

            var TOKEN                          = response[0][1];
            var student_id                     = response[1][1];
            //var module_id                      = response[2][1];
            

            //this.setState({nextPostion : index_end});

            this.setState({
                            TOKEN          : TOKEN,    
                            student_id     : student_id,
                      });

            //AsyncStorage.removeItem('moduleId')

            console.log(this.state.module_id,"module_id on next called")

            var TOKEN                          = response[0][1];
            var student_id                     = response[1][1];
           
            this.setState({
                            TOKEN          : TOKEN,    
                            student_id     : student_id,
                            //module_id      : module_id,
                            isLoading      : true, 
                      });


            let formdata = new FormData();

            formdata.append('student_id',       student_id );
            formdata.append('module_id',        module_idd );
            formdata.append('go',               'submit');
            formdata.append('count',      this.state.count );
            
            console.log(formdata,"formdata on next called");
            console.log(TOKEN,"TOKEN on next called");

            fetch( GVar.BaseUrl + GVar.StartPracticeTest,{
                      method: 'POST',
                      headers:{
                                  'token' : TOKEN
                              },
                      body: formdata,       
                      }).then((response) => response.text())
                     .then((responseJson) => {

                      var x = JSON.parse(responseJson.replace("-", ""));
                      this.setState({isLoading : false });
                      

                      if( x.status == 1 ){

                          console.log(x, "on next called questions");
                         // this.setState({questions : x.questions });

                      }else{

                      }
                      
                     }).catch((error)=>{ Alert.alert(error)});

    }); 




   
    console.log("next position on end ",this.state.nextPostion);

      console.log("on Submit practic etest")
     
    AsyncStorage.multiGet(["TOKEN", "student_id"]).then(response => {

      var TOKEN                          = response[0][1];
      var student_id                     = response[1][1];
     
      this.setState({
                      TOKEN          : TOKEN,    
                      student_id     : student_id,
                });


      let formdata = new FormData();

      formdata.append('student_id',        student_id );

      console.log(formdata,"formdata");
      console.log(TOKEN,"TOKEN");

      fetch( GVar.BaseUrl + GVar.PracticeTest + module_idd ,{
                method: 'POST',
                headers:{
                            'token' : TOKEN
                        },
                body: formdata,       
                }).then((response) => response.text())
               .then((responseJson) => {

                var x = JSON.parse(responseJson.replace("-", ""));
                this.setState({isLoading : false });
                this.setState({status : x.status });
                this.setState({nextPostion : 0 });
                 this.setState({questions:[]});
                
                if( x.status == 1 ){

                    console.log(x.practicetest_score, "submit practicetest_score");
                    console.log("submit length",x.practicetest_score.length);
                    console.log("score total ",x.practicetest_score[x.practicetest_score.length-1].total_questions)
                    if(x.practicetest_score.length > 0){
                      this.setState({last_result_count:x.practicetest_score.length})
                      console.log("in if total question =",x.practicetest_score[x.practicetest_score.length-1]);
                      this.setState({latest_result:x.practicetest_score[x.practicetest_score.length-1]})
                      this.setResultModalVisible(true);
                    }
                    
                    this.setState({practicetest_score : x.practicetest_score });
                    this.setState({moduleName : x.moduleName });
                    this.setState({count:x.practicetest_score.length+1})
                   
                }else{
                  this.setState({message : x.message });
                  console.log(x.message, "submit else message");
                  this.setState({count:1})
                }
                
               }).catch((error)=>{ Alert.alert(error)});

});  

  }


  onPDFPressed(item){   

      var GVar = GlobalVariables.getApiUrl();

      //http://mentorship.iasjunior.com/admin/assets/update/15397839485bc73d0c40c35.pdf 

      RNFetchBlob
          .config({
              notification: true,
              path: 'downloads/'+item,
              indicator: true,
              overwrite: true,
              addAndroidDownloads: {
                  path: RNFetchBlob.fs.dirs.SDCardDir +'/downloads/'+item,
                  useDownloadManager: true,
                  notification: true,
                  overwrite: true,
                  description: 'downloading content...',
                  mime: 'application/pdf',
                  mediaScannable: false
              }
          })
          .fetch('GET', GVar.UpdatesFile +item)
          .progress((received, total) => {
              console.log('progress', received / total)    
          })
  }

  DownloadPDF( index ){


    var GVar = GlobalVariables.getApiUrl();
    this.setState({isLoading : true });
  
    var count = index +1;

    let formdata = new FormData();

    formdata.append('student_id',        this.state.student_id );
    formdata.append('count',        count );

    var TOKEN = this.state.TOKEN;

    console.log(this.state.module_id,"module_id")

    fetch( GVar.DownloadPracticePDF+ this.state.module_id ,{
                      method: 'POST',
                      headers:{
                                  'token' : TOKEN
                              },
                      body: formdata,       
                      }).then((response) => response.text())
                     .then((responseJson) => {

                      var x = JSON.parse(responseJson.replace("-", ""));
                      this.setState({isLoading : false });
                     
                        var options = {
                 
                            html: x.html, // HTML String

                          // ****************** OPTIONS BELOW WILL NOT WORK ON ANDROID **************                              
                            fileName: 'practice-test' + count ,          /* Optional: Custom Filename excluded extension
                                                        Default: Randomly generated
                                                      */

                            directory: 'docs',         /* Optional: 'docs' will save the file in the `Documents`
                                                        Default: Temp directory */

                            base64: true ,               


                            height: 800,                
                            width: 1056,               /* Optional: 1056 sets the width of the DOCUMENT that will produced
                                                        Default: 792
                                                      */
                            padding: 24,                /* Optional: 24 is the # of pixels between the outer paper edge and
                                                                corresponding content edge.  Example: width of 1056 - 2*padding
                                                                => content width of 1008
                                                        Default: 10
                                                      */
                      };

                      //const source = {uri:data.filePath};
                     
                      RNHTMLtoPDF.convert(options).then((data) => {

                        if( Platform.OS == 'ios' ){
                           
                            RNFetchBlob.ios.openDocument(data.filePath);

                          }else{
                           
                            this.props.navigation.navigate('ViewPDF',{source:data.filePath })
                            Alert.alert('Successfully downloaded in Documents' );
                          }
                        // this.props.navigation.navigate('ViewPDF',{source:data.filePath })
                        // Alert.alert('Successfully downloaded in Downloads' );
                      });
                      
                          
                      this.setState({isLoading:false})
                      
                     }).catch((error)=>{ Alert.alert(error)});

  }


  DownloadPDFModal( index ){
    console.log("my index for pdf",index);


    var GVar = GlobalVariables.getApiUrl();
    this.setState({isLoading : true });
  

    let formdata = new FormData();

    formdata.append('student_id',        this.state.student_id );
    formdata.append('count',        index );

    var TOKEN = this.state.TOKEN;

    console.log(this.state.module_id,"module_id")

    fetch( GVar.DownloadPracticePDF+ this.state.module_id ,{
                      method: 'POST',
                      headers:{
                                  'token' : TOKEN
                              },
                      body: formdata,       
                      }).then((response) => response.text())
                     .then((responseJson) => {

                      var x = JSON.parse(responseJson.replace("-", ""));
                      this.setState({isLoading : false });
                     
                        var options = {
                 
                            html: x.html, // HTML String

                          // ****************** OPTIONS BELOW WILL NOT WORK ON ANDROID **************                              
                            fileName: 'practice-test' + index ,          /* Optional: Custom Filename excluded extension
                                                        Default: Randomly generated
                                                      */

                            directory: 'docs',         /* Optional: 'docs' will save the file in the `Documents`
                                                        Default: Temp directory */

                            base64: true ,               


                            height: 800,                
                            width: 1056,               /* Optional: 1056 sets the width of the DOCUMENT that will produced
                                                        Default: 792
                                                      */
                            padding: 24,                /* Optional: 24 is the # of pixels between the outer paper edge and
                                                                corresponding content edge.  Example: width of 1056 - 2*padding
                                                                => content width of 1008
                                                        Default: 10
                                                      */
                      };

                      //const source = {uri:data.filePath};
                     
                        this.setResultModalVisible(false);
                    

                      RNHTMLtoPDF.convert(options).then((data) => {

                        if( Platform.OS == 'ios' ){
                            
                            RNFetchBlob.ios.openDocument(data.filePath);

                          }else{
                           
                            this.props.navigation.navigate('ViewPDF',{source:data.filePath })
                            Alert.alert('Successfully downloaded in Documents' );
                          }
                        // this.props.navigation.navigate('ViewPDF',{source:data.filePath })
                        // Alert.alert('Successfully downloaded in Downloads' );
                      });
                          
                      this.setState({isLoading:false})
                      
                     }).catch((error)=>{ Alert.alert(error)});

  }



  setCheckBoxState(trueOrfalse){

    this.setState({updateCheckBoxState:trueOrfalse});
  }

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  setTestModalVisible(visible) {
    this.setState({TestmodalVisible: visible});
  }


    /*setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }*/

  onNext( go ){

    var index_end;
    console.log("on next called with p/n",go);

    if( go == 'previous'){

      index_end = this.state.nextPostion - 5;
    }else if( go == 'next' ){

      index_end = this.state.nextPostion + 5;
    }
     
    console.log("on next called index_end", index_end );  
    console.log("count on next",this.state.count);
  
    var GVar = GlobalVariables.getApiUrl();

    AsyncStorage.multiGet(["TOKEN", "student_id", "moduleId"]).then(response => {

            var TOKEN                          = response[0][1];
            var student_id                     = response[1][1];
            //var module_id                      = response[2][1];
            

            this.setState({nextPostion : index_end});

            this.setState({
                            TOKEN          : TOKEN,    
                            student_id     : student_id,
                      });

            //AsyncStorage.removeItem('moduleId')

            console.log(this.state.module_id,"module_id on next called")

            var TOKEN                          = response[0][1];
            var student_id                     = response[1][1];
           
            this.setState({
                            TOKEN          : TOKEN,    
                            student_id     : student_id,
                            //module_id      : module_id,
                            isLoading      : true, 
                      });


            let formdata = new FormData();

            formdata.append('student_id',       student_id );
            formdata.append('module_id',        this.state.module_id );
            formdata.append('go',               go );
            formdata.append('count',      this.state.count );
            formdata.append('index_end',        index_end );
           
            console.log(formdata,"formdata on next called");
            console.log(TOKEN,"TOKEN on next called");

            fetch( GVar.BaseUrl + GVar.StartPracticeTest,{
                      method: 'POST',
                      headers:{
                                  'token' : TOKEN
                              },
                      body: formdata,       
                      }).then((response) => response.text())
                     .then((responseJson) => {

                      var x = JSON.parse(responseJson.replace("-", ""));
                      this.setState({isLoading : false });
                      

                      if( x.status == 1 ){

                          console.log(x, "on  questions response");
                          this.setState({questions : x.questions });

                      }else{

                      }
                      
                     }).catch((error)=>{ Alert.alert(error)});

    }); 
  }

  reviewQuestions( item ){

    console.log("ReviewAnswer", item);
    var ischecked = false;
    var color = '';

    if( item.review_later == 1 ){

      this.ischecked = true;
      color = 'violet';
  }else if( item.answer_id ){

    this.ischecked = true;
    color = 'green';
  }else if( item.answer_id && item.review_later == 1 ){

    this.ischecked = true;
    color = 'violet';
  }else{

    this.ischecked = false;
    color = '';
  }


  console.log("colorReview", color);
    

  
    return  <View style={{flex:1, flexDirection:'row',justifyContent:'center'}}>
                  <View style={{}}>
                    <CheckBox 
                      center
                      checkedColor = {color}
                      size = {hp('2.5%')}
                      containerStyle={{backgroundColor:'transparent', borderColor:'transparent'}}
                      textStyle ={{fontSize: RF(2.5),fontWeight: 'bold',color:'#FFFFFF'}}
                      checked={item.answer_id || item.review_later == 1 ? true : false } 
                      //onPress={ }
                      onPress={() => {
                                    this.setModalVisible(!this.state.modalVisible);
                                    this._onClickReview( item, ischecked)
                                  }}
                      />
                  </View>

                  <View style={{marginTop:'20%'}}>
                      <Text style={{flex:1,marginTop:'4.5%',marginLeft:-22, marginTop:'5%', color:'#000000'}}>{item.question_ordering}</Text>
                  </View>

            </View>;
  }


  /*showSingleOptionAnswers( item ){

    var questions = this.state.questions;


    return  <FlatList
                extraData={this.state}
                data={item}
                renderItem={({item, index}) => 
                    <View style={{flex:1, flexDirection:'row',marginLeft:wp('2%'),}}>
                            <CheckBox
                              checkedIcon='dot-circle-o'
                              uncheckedIcon='circle-o'
                              checked={ item.answer_id == questions[index].answer_id ? true : false}
                            />
                          <Text style={{flex:1,marginTop:'4.5%',marginLeft:-22}}>{questions[index].answer_id}</Text>
                          <Text style={{flex:1,marginTop:'4.5%',marginLeft:-22}}>{item.answer_id}</Text>
                         <Text style={{flex:1,marginTop:'4.5%',marginLeft:-22}}>{index + 1 }.  {item.answer}</Text>
                    </View>
              }/>; 
  }*/

  _onClickReview( item, ischecked ){
    //this.setModalVisible(!this.state.modalVisible);
    console.log("onclickReviewButton item ",item)
    console.log("onclickReviewButton ischked",ischecked)

    var index = item.question_ordering;
    var pos   = this.state.pos;
    
    var index_end = pos[item.question_ordering -1 ].indexend - 5 ;
    var go        = 'next';

    var GVar = GlobalVariables.getApiUrl();

    AsyncStorage.multiGet(["TOKEN", "student_id", "moduleId"]).then(response => {

            var TOKEN                          = response[0][1];
            var student_id                     = response[1][1];
            //var module_id                      = response[2][1];
            

            this.setState({nextPostion : index_end});

            this.setState({
                            TOKEN          : TOKEN,    
                            student_id     : student_id,
                      });

            //AsyncStorage.removeItem('moduleId')

            console.log(this.state.module_id,"module_id onclick review")

            var TOKEN                          = response[0][1];
            var student_id                     = response[1][1];
           
            this.setState({
                            TOKEN          : TOKEN,    
                            student_id     : student_id,
                            //module_id      : module_id,
                            isLoading      : true, 
                      });


            let formdata = new FormData();

            formdata.append('student_id',       student_id );
            formdata.append('module_id',        this.state.module_id );
            formdata.append('go',               go );
            formdata.append('count',this.state.count);
            formdata.append('index_end',        index_end );
            
            console.log(formdata,"formdata");
            console.log(TOKEN,"TOKEN");

            fetch( GVar.BaseUrl + GVar.StartPracticeTest ,{
                      method: 'POST',
                      headers:{
                                  'token' : TOKEN
                              },
                      body: formdata,       
                      }).then((response) => response.text())
                     .then((responseJson) => {

                      var x = JSON.parse(responseJson.replace("-", ""));
                      this.setState({isLoading : false });
                      

                      if( x.status == 1 ){

                          console.log(x, "questions on click review");
                          this.setState({questions : x.questions });

                      }else{

                      }
                      
                     }).catch((error)=>{ Alert.alert(error)});

    }); 

  }

  _reviewLater( item ){

    console.log(" review laterxyz", item );
    console.log("review later questions", this.state.questions );

    
    var GVar = GlobalVariables.getApiUrl();
    

    //this.setState({ questions})

    AsyncStorage.multiGet(["TOKEN", "student_id"]).then(response => {

            var TOKEN                          = response[0][1];
            var student_id                     = response[1][1];
            
           
            this.setState({
                            isLoading      : true, 
                      });

            var review;
            if( item.review_later == 0 ){

              review = '1';
            }else if( item.review_later == 1 ){

              review = '0';
            }

            var questions  = this.state.questions;

            var index = this.getIndex(item.exam_id, questions, 'exam_id');

            questions[index].review_later = review;
            this.setState({ questions : questions });

            let formdata = new FormData();

            formdata.append('student_id',       student_id );
            formdata.append('exam_id',          item.exam_id );
            formdata.append('review',           review );
           
            console.log(formdata,"formdata review later");
            
            console.log(TOKEN,"TOKEN review later");
 
            fetch( GVar.BaseUrl + GVar.UpdateReview,{
                      method: 'POST',
                      headers:{
                                  'token' : TOKEN
                              },
                      body: formdata,       
                      }).then((response) => response.text())
                     .then((responseJson) => {

                      var x = JSON.parse(responseJson.replace("-", ""));
                      this.setState({isLoading : false });
                      

                      if( x.status == 1 ){

                          console.log(x, "reviewResponce");
                          //this.setState({questions : x.questions });

                      }else{

                      }
                      
                     }).catch((error)=>{ Alert.alert(error)});

    }); 

    this.getReviews();

   
  }


  getIndex(value, arr, prop) {
    console.log("get Index caLLED ")
    for(var i = 0; i < arr.length; i++) {
        if(arr[i][prop] === value) {
            return i;
        }
    }
    return -1; //to handle the case where the value doesn't exist
  }


  _OnClickAnswer( item, exam_id,answer_type){

    console.log("click answerITEM",item);
    console.log("click exam_id",exam_id);
    console.log("click answr_type",answer_type);
   
    var questions  = this.state.questions;

    var index = this.getIndex(exam_id, questions, 'exam_id');
    this.setState({questions:questions});
    
    console.log("index dcalled", index);
    console.log("questions ",questions[index].answer_type);


    var Passanswer_id;
    if(questions[index].answer_type == 'single'){

     
      if( questions[index].answer_id == item.answer_id ){
  
        questions[index].answer_id = '';
        Passanswer_id = '';
      }else{
        
        questions[index].answer_id = item.answer_id;
        Passanswer_id = item.answer_id;
      }
    }else{

      if( questions[index].answer_id.split(',').contains(item.answer_id)){

        questions[index].answer_id = '';
        Passanswer_id = '';
      }else{
        questions[index].answer_id = item.answer_id;
        Passanswer_id = item.answer_id;
      }

    }

   

    

    this.setState({ questions : questions });

    console.log("get question id ,get answer id, get exam id",item.question_id ,item.anwer_id )

    var GVar = GlobalVariables.getApiUrl();

    AsyncStorage.multiGet(["TOKEN", "student_id"]).then(response => {

            var TOKEN                          = response[0][1];
            var student_id                     = response[1][1];
            
           
            this.setState({
                            isLoading      : true, 
                      });
           
       
            let formdata = new FormData();

            formdata.append('student_id',       student_id );
            formdata.append('exam_id',          exam_id );
            formdata.append('question_id',      item.question_id );
            formdata.append('answer_id',        Passanswer_id);
            formdata.append('answer_type',      answer_type);
            
            console.log(formdata,"formdata _OnClickAnswer");
            console.log(TOKEN,"TOKEN _OnClickAnswer");

            console.log("update practice answer  url",GVar.BaseUrl + GVar.UpdatePracticeAnswers);
            fetch( GVar.BaseUrl + GVar.UpdatePracticeAnswers,{
                      method: 'POST',
                      headers:{
                                  'token' : TOKEN
                              },
                      body: formdata,       
                      }).then((response) => response.text())
                     .then((responseJson) => {

                      var x = JSON.parse(responseJson.replace("-", ""));
                      this.setState({isLoading : false });
                      

                      if( x.status == 1 ){

                          console.log(x, "AnswerResponce");
                          //this.setState({questions : x.questions });

                      }else{

                      }
                      
                     }).catch((error)=>{ Alert.alert(error)});

    });

    this.getReviews();

   
    
  }
  setResultModalVisible(visible){
    this.setState({resultModalVisible:visible});
  }

  onFinishTimer(){

    if(this.state.timeCompleted){
      Alert.alert("Exam Submitted");
      this.setTestModalVisible(false);
      //this.setState({nextPostion:0})
      this.changingStatusOfStartTest();
    }

  }

  
  render() {

    const styleStart = '<div style="color:#150F68;font-weight: 700;">';
    const styleEnd   = '</div>';  
  
    return (
      <View style={{flex: 1,widthwp:wp('100%')}}>

            <ScrollView>

              <View>

              {this.state.moduleName == 'Module CSAT Aptitude' ?

                    <View style={{flex: 1, height:hp('60%'),width:wp('95%'), justifyContent:'space-between',backgroundColor: '#FFFFFF',marginTop:'2%',marginLeft:wp('2.5%')}}>
                                        
                    <View style={{ height:hp('5%'), backgroundColor: '#3f4c6b', marginTop:'2%', justifyContent:'center', alignItems:'center'}}>
                      <Text  style={{fontSize: RF(3),color:'#FFFFFF', alignself:'center', marginLeft:wp('2%') }}>Instructions</Text>
                    </View>

                    <View style={{  backgroundColor: '#FFFFFF', }}>
                      <Text  style={{fontSize: RF(2.5),color:'#000000', marginLeft:wp('2%') }}>{'\u2022'} All questions are compulsory.</Text>
                      <Text  style={{fontSize: RF(2.5),color:'#000000', marginLeft:wp('2%') }}>{'\u2022'} Number of questions: 20 Questions</Text>
                      <Text  style={{fontSize: RF(2.5),color:'#000000', marginLeft:wp('2%') }}>{'\u2022'} Time allotted: 30 minutes </Text>
                      <Text  style={{fontSize: RF(2.5),color:'#000000', marginLeft:wp('2%') }}>{'\u2022'} Correct Answer: 2.5 marks / Question </Text>
                      <Text  style={{fontSize: RF(2.5),color:'#000000', marginLeft:wp('2%') }}>{'\u2022'} Wrong Answer: - 0.83 marks / Question </Text>
                      <Text  style={{fontSize: RF(2.5),color:'#000000', marginLeft:wp('2%') }}>{'\u2022'} No Answer: 0 marks / Question</Text>
                      <Text  style={{fontSize: RF(2.5),color:'#000000', marginLeft:wp('2%') }}>{'\u2022'} Please do not refresh the page while you are taking Test</Text>
                    
                    </View>

                    <TouchableOpacity style={{ height:hp('5%'),width:wp('30%'), backgroundColor: '#1ab394', marginTop:'2%', justifyContent:'center', alignItems:'center', marginLeft:'31.55%', marginBottom:'10%'}} 
                    onPress={() => {
                      //this.setTestModalVisible(true);
                      this.insertPracticeQuestions();
                      //this.getPracticeQuestion();
                      //this.getReviews();

                    }}>
                    <View >
                        <Text  style={{fontSize: RF(3),color:'#FFFFFF', alignself:'center',  }}>Start Test</Text>
                    </View>
                    </TouchableOpacity>

                    </View> 


                  :

                  <View style={{flex: 1, height:hp('60%'),width:wp('95%'), justifyContent:'space-between',backgroundColor: '#FFFFFF',marginTop:'2%',marginLeft:wp('2.5%')}}>
                    
                  <View style={{ height:hp('5%'), backgroundColor: '#3f4c6b', marginTop:'2%', justifyContent:'center', alignItems:'center'}}>
                    <Text  style={{fontSize: RF(3),color:'#FFFFFF', alignself:'center', marginLeft:wp('2%') }}>Instructions</Text>
                  </View>

                  <View style={{  backgroundColor: '#FFFFFF', }}>
                    <Text  style={{fontSize: RF(2.5),color:'#000000', marginLeft:wp('2%') }}>{'\u2022'} All questions are compulsory.</Text>
                    <Text  style={{fontSize: RF(2.5),color:'#000000', marginLeft:wp('2%') }}>{'\u2022'} Number of questions: 25 Questions</Text>
                    <Text  style={{fontSize: RF(2.5),color:'#000000', marginLeft:wp('2%') }}>{'\u2022'} Time allotted: 30 minutes </Text>
                    <Text  style={{fontSize: RF(2.5),color:'#000000', marginLeft:wp('2%') }}>{'\u2022'} Correct Answer: 2 marks / Question </Text>
                    <Text  style={{fontSize: RF(2.5),color:'#000000', marginLeft:wp('2%') }}>{'\u2022'} Wrong Answer: - 0.67 marks / Question </Text>
                    <Text  style={{fontSize: RF(2.5),color:'#000000', marginLeft:wp('2%') }}>{'\u2022'} No Answer: 0 marks / Question</Text>
                    <Text  style={{fontSize: RF(2.5),color:'#000000', marginLeft:wp('2%') }}>{'\u2022'} Please do not refresh the page while you are taking Test</Text>
                   
                  </View>

                  <TouchableOpacity style={{ height:hp('5%'),width:wp('30%'), backgroundColor: '#1ab394', marginTop:'2%', justifyContent:'center', alignItems:'center', marginLeft:'31.55%', marginBottom:'10%'}} 
                  onPress={() => {
                    //this.setTestModalVisible(true);
                    this.insertPracticeQuestions();
                    //this.getPracticeQuestion();
                    //this.getReviews();

                  }}>
                  <View >
                      <Text  style={{fontSize: RF(3),color:'#FFFFFF', alignself:'center',  }}>Start Test</Text>
                  </View>
                  </TouchableOpacity>

              </View> 

              

              }  

            

              { this.state.status ? 
                <View style={{ alignItems:'center', alignself:'center',}}>
                  <FlatList
                    extraData={this.state}
                    data={this.state.practicetest_score}
                    renderItem={({item, index}) => 
                    <View style={{flex:1, width:wp('95%'), marginTop:hp('2%')}}>
                        <View style={{flex: 1, backgroundColor: '#1ab394',  justifyContent:'center', alignItems:'center'}} >
                            <Text  style={{fontSize: RF(4),color:'#FFFFFF', alignself:'center' }}>Practice Test {index + 1} Score</Text>
                        </View>

                        <View style={{flex: 4, backgroundColor: '#1f2f46', flexDirection:'column', marginTop:hp('1%')}} >
                                    <View style={{flex: 4,flexDirection: 'row',justifyContent: 'center',marginTop:hp('2%'),  height: '80%', marginBottom:hp('4%')}} >

                                    <View style={{width: wp('50%'),justifyContent:'space-between'}} >

                                        <View>
                                                <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>Total Questions</Text>
                                        </View>

                                        <View>
                                                <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>Correct Answers</Text>
                                        </View>

                                        <View>
                                                <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>Wrong Answers</Text>
                                        </View>

                                        <View>
                                                <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>UnAttempted Question</Text>
                                        </View>

                                        <View>
                                                <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }} >Total Marks</Text>
                                        </View>

                                        <View>
                                                <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>Your Score</Text>
                                        </View>

                                        <View>
                                                <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>Grade</Text>
                                        </View>
                                    </View>

                                    <View style={{width: wp('20%'),justifyContent:'space-between' }} >

                                        <View>
                                                <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>{item.total_questions}</Text>
                                        </View>

                                        <View>
                                                <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>{item.correct_answers}</Text>
                                        </View>

                                        <View>
                                                <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>{item.wrong_answers}</Text>
                                        </View>

                                        <View>
                                                <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>{item.unattempted_questions}</Text>
                                        </View>

                                        <View>
                                                <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>{item.total_marks}</Text>
                                        </View>

                                        <View>
                                                <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>{item.your_score}</Text>
                                        </View>

                                        <View>
                                                <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>{item.grade}</Text>
                                        </View>
                                    </View>

                              </View>

                              <View style={{flex: 2,flexDirection: 'row',justifyContent: 'center',}} >
                                    <View style= {{backgroundColor:'#FFFFFF', width:wp('80%'), marginBottom: hp('1%'), justifyContent:'center'}}>
                                        <Text style={{color:'#e16800', fontSize: RF(2.5), alignself:'center',marginLeft:wp('2%') }}>Remark: {item.remarks}</Text>
                                    </View>
                              </View>
                        </View>

                        <View style={{flex: 1, backgroundColor: '#FFFFFF',}} >
                            <TouchableOpacity onPress={this.DownloadPDF.bind(this, index)}>
                                  <View style={{flexDirection: 'row', backgroundColor:'#1ab394', width:wp('80%'), marginLeft:wp('9%'), justifyContent:'center', borderRadius: wp('2%'), alignItems:'center', marginTop:hp('2%'),}}>
                                      <Text style={{color:'#FFFFFF',fontSize: RF(4), alignself:'center'}}>Download Solution</Text>
                                  </View>
                              </TouchableOpacity>
                        </View>

                    </View>    
                  }/>
                </View>
                :
                <View/>
              }


            

              </View> 


                

            </ScrollView>
            {this.state.isLoading ? <ActivityIndicator
                    animating     ={true}
                    transparent   ={true}
                    visible       ={false}
                    style         ={styles.indicator}
                    size          ="large"
                  /> : <View/>  } 
            <Quotes/>   

              <Modal
                  animationType="slide"
                  transparent={false}
                  visible={this.state.TestmodalVisible}
                  onRequestClose={() => {
                    Alert.alert(
                      'Submit Exam',
                      'Do You Really want to Submit Your Exam!',
                      [
                        {text: 'Cancel', onPress: () => {}},
                        {text: 'OK', onPress: () => {this.setTestModalVisible(false)
                                                    this.changingStatusOfStartTest()}},
                      ],
                      { cancelable: false }
                    );
                  }}>
                  <View style={{flex: 1, width:wp('100%')}}>

                    <View style={{flex: .5, flexDirection:'row', marginTop:'2%'}}>
                          <View style={{flex: 5,flexDirection:'row', justifyContent:'center'}} >
                                <CountDown
                             
                                  digitBgColor  = '#1bb496'
                                  digitTxtColor = '#FFFFFF'
                                  timeTxtColor  = '#000000'
                                  until={
                                    
                                    this.state.totalDuration}
                                  //duration of countdown in seconds
                                  timetoShow={('H', 'M', 'S')}
                                  //formate to show
                                  onFinish={() => {
                                                    this.onFinishTimer();
                                                }}
                                  //on Finish call
                                  //onPress={() => alert('hello')}
                                  //on Press call
                                  size={20}
                                />
                          </View>
                          
                          <View style={{flex: 1}} >
                                <TouchableOpacity
                                  onPress={() => {
                                    this.setModalVisible(!this.state.modalVisible);
                                  }}>

                                  <View style={{
                                         borderWidth:1,
                                         borderColor:'rgba(0,0,0,0.2)',
                                         alignItems:'center',
                                         justifyContent:'center',
                                         width:50,
                                         position: 'relative',                                          
                                         bottom: 0,                                                    
                                         right: 10,
                                         height:50,
                                         backgroundColor:'#fff',
                                         borderRadius:100,
                                       }}
                                    >    
                                  <Text textAlign={'center'} style={{fontSize: RF(4), fontWeight:'bold', alignself:'center', color:'#1ab395'}}>RL</Text>

                                  
                                  </View>
                                </TouchableOpacity>
                          </View>
                    </View>
                    <View style={{flex: 5.5,marginTop:'3%'}}>
                      <ScrollView >
                      <View style={{ alignItems:'center', alignself:'center'}}>
                            <FlatList
                              extraData={this.state}
                              data={this.state.questions}
                              renderItem={({item, index}) => 

                              <View style={{flex:1, flexDirection:'column',width:wp('95%'), justifyContent:'center', backgroundColor:'#FFFFFF',  borderWidth:2, borderColor:'#CCCCCC'}}>
                                  

                                  <View style={{flex:1, flexDirection:'column',marginLeft:wp('2%'), marginTop:hp('2%'),}}>
                                       <Text style={{marginTop:'3.5%', color:'#150F68'}}>Q. {item.question_ordering}. </Text>
                                       <HTML html={styleStart + item.question + styleEnd}  imagesMaxWidth={Dimensions.get('window').width} />
                                  </View>
                                 
                                      
                                  { item.answer_type == 'single' ? 

                                      <FlatList
                                        extraData={this.state}
                                        data={item.answers}
                                        renderItem={({item : answers, index}) => 
                                    
                                        <View key={index} style={{flex:1, flexDirection:'row',marginLeft:wp('2%'),}}>
                                                  <CheckBox
                                                      checkedIcon='dot-circle-o'
                                                      uncheckedIcon='circle-o'
                                                      checked={ answers.answer_id == item.answer_id ? true : false}
                                                      onPress={() => {
                                                        this._OnClickAnswer(answers, item.exam_id,item.answer_type)}}
                                                  />
                                                 <Text style={{flex:1,marginTop:'4.5%',marginLeft:-22}}>{String.fromCharCode(65+index)}.  {answers.answer}</Text>
                                        </View>
                                      }/>  
            
                                      :

                                      <FlatList
                                        extraData={this.state}
                                        data={item.answers}
                                        renderItem={({item, index}) => 
                                            <View style={{flex:1, flexDirection:'row',marginLeft:wp('2%'),}}>
                                                    <CheckBox
                                                    
                                                      center
                                                      checkedColor = '#FFFFFF'
                                                      size = {hp('2.5%')}
                                                      containerStyle={{backgroundColor:'transparent', borderColor:'transparent'}}
                                                      textStyle ={{fontSize: RF(2.5),fontWeight: 'bold',color:'#FFFFFF'}}
                                                      checked={ answers.answer_id.split(',').contains(item.answer_id) ? true:false}
                                                      onPress={() => this._OnClickAnswer(answers, item.answer_id,item.answer_type)}
                                                    />

                                                 <Text style={{flex:1,marginTop:'4.5%',marginLeft:-22}}>{String.fromCharCode(65+index)}.  {item.answer}</Text>
                                            </View>
                                      }/>   
                                  } 


                                  <View style={{flex:1, flexDirection:'row',marginLeft:wp('50%')}}>
                                            <CheckBox
                                              center
                                              checkedColor = '#7f00ff'
                                              size = {hp('2.5%')}
                                              containerStyle={{backgroundColor:'transparent', borderColor:'transparent'}}
                                              textStyle ={{fontSize: RF(2.5),fontWeight: 'bold',color:'#FFFFFF'}}
                                              checked={item.review_later == '1' ? true : false } 
                                              onPress={this._reviewLater.bind(this, item) }/>

                                         <Text style={{flex:1,marginTop:'9%',marginLeft:-22, color:"#7f00ff", fontWeight:'bold'}}>Review Later</Text>
                                    </View>    
                                      

                              </View>
                            }
                            />
                      </View>
                      </ScrollView>
                    </View>
                    <View style={{flex: .2, flexDirection:'row', marginTop:'2%', alignItems:'center', justifyContent:'space-between'}}>
                          
                          { this.state.nextPostion > '4' ?
                            <View style={{flex: 2,}} >
                                <TouchableOpacity
                                  onPress={() => {
                                    this.onNext("previous");
                                  }}>

                                  <View style={{marginLeft:5,alignItems:'center',  backgroundColor:'#1ab395', height:hp('5%')}}>    
                                    <Text textAlign={'center'} style={{fontSize: RF(2.5), fontWeight:'bold', alignself:'center', color:'#FFFFFF'}}>PREVIOUS</Text>
                                  </View>
                                </TouchableOpacity>
                            </View>
                            :
                            <View/>
                          }
                          
                          {(this.state.nextPostion < this.state.totalQuestionCount && this.state.nextPostion + 5 != this.state.totalQuestionCount) ?
                        
                            <View style={{flex: 2, }} >
                                  <TouchableOpacity
                                    onPress={() => {
                                      this.onNext("next");
                                    }}>

                                    <View style={{marginLeft:5,marginRight:5, alignItems:'center', backgroundColor:'#1ab395', height:hp('5%')}}>
                                      <Text textAlign={'center'} style={{fontSize: RF(2.5), fontWeight:'bold', alignself:'center',color:'#FFFFFF'}}>NEXT</Text>
                                    </View>
                                  </TouchableOpacity>
                            </View>
                          :
                            <View/>
                          }

                          { this.state.nextPostion == this.state.totalQuestionCount - 5 ?
                        
                            <View style={{flex: 2, }} >
                                  <TouchableOpacity
                                    onPress={() => {
                                    
                                      this.setTestModalVisible(false);
                                      this.changingStatusOfStartTest();
                                      
                                    
                                      
                                    }}>

                                    <View style={{marginLeft:5,marginRight:5, alignItems:'center', backgroundColor:'#1ab395', height:hp('5%')}}>
                                      <Text textAlign={'center'} style={{fontSize: RF(2.5), fontWeight:'bold', alignself:'center',color:'#FFFFFF'}}>SUBMIT</Text>
                                    </View>
                                  </TouchableOpacity>
                            </View>
                          :
                            <View/>
                          }
                    </View>
                    <Modal
                      animationType="slide"
                      transparent={false}
                      visible={this.state.modalVisible}
                      onRequestClose={() => {
                       this.setModalVisible(false);
                      }}
                      >
                       <View style={{ height:hp('5%'), backgroundColor: '#1ab395', marginTop:'1%',marginLeft:'5%',marginRight:'5%', justifyContent:'center', alignItems:'center'}}>
                        <Text  style={{fontSize: RF(3),color:'#FFFFFF', alignself:'center', marginLeft:wp('2%') }}>Review Later</Text>
                      </View>
                        <View style={{flex: 1,alignSelf: 'flex-end', position: 'absolute',marginTop:'12%',backgroundColor:'#FFFFFF'}} >
                                <TouchableOpacity
                                  onPress={() => {
                                    this.setModalVisible(!this.state.modalVisible);
                                  }}>

                                  <View style={{
                                         borderWidth:1,
                                         borderColor:'rgba(0,0,0,0.2)',
                                         alignItems:'center',
                                         justifyContent:'center',
                                         width:50,
                                         position: 'relative',                                          
                                         bottom: 0,                                                    
                                         right: 10,
                                         height:50,
                                         backgroundColor:'#fff',
                                         borderRadius:100,
                                       }}
                                    >    
                                    <Icon name="times-circle"  size={30} color="#01a699" />
                                  
                                  </View>
                                </TouchableOpacity>
                          </View>

                      <View style={{marginTop:hp('20%'), marginBottom:hp('.5%'), backgroundColor:'#FFFFFF'}}>
                        <View style={{marginBottom:hp('10%')}}>
                          <ScrollView>
                          
                          <FlatList
                              extraData={this.state}
                              data={this.state.review}
                              renderItem={({item, index}) => 
                                  <View style={{flex:1, flexDirection:'row',marginLeft:wp('1%'),}}>
                                    {this.reviewQuestions(item)}
                                  </View>
                          }
                          numColumns={5}
                          />
                          </ScrollView>
                        </View>
                      </View>
                    </Modal>

                    {this.state.isLoading ? <ActivityIndicator
                            animating     ={true}
                            transparent   ={true}
                            visible       ={false}
                            style         ={styles.indicator}
                            size          ="large"
                          /> : <View/>  } 
          
                  </View>
              </Modal> 
              <Modal
              animationType="slide"
              transparent={false}
              visible={this.state.resultModalVisible}
              onRequestClose={() => {
               this.setResultModalVisible(false);
              }}
              >
                      <View style={{ height:hp('5%'), backgroundColor: '#1ab395', marginTop:'1%',marginLeft:'5%',marginRight:'5%', justifyContent:'center', alignItems:'center'}}>
                        <Text  style={{fontSize: RF(3),color:'#FFFFFF', alignself:'center', marginLeft:wp('2%') }}>Practice Test {this.state.last_result_count} Score</Text>
                      </View>
                   
                        <View style={{flex: 1,alignSelf: 'flex-end', position: 'absolute',marginTop:'12%',backgroundColor:'#FFFFFF'}} >
                                <TouchableOpacity
                                  onPress={() => {
                                    this.setResultModalVisible(false);
                                  }}>

                                  <View style={{
                                         borderWidth:1,
                                         borderColor:'rgba(0,0,0,0.2)',
                                         alignItems:'center',
                                         justifyContent:'center',
                                         width:50,
                                         position: 'relative',                                          
                                         bottom: 0,                                                    
                                         right: 10,
                                         height:50,
                                         backgroundColor:'#fff',
                                         borderRadius:100,
                                       }}
                                    >    
                                    <Icon name="times-circle"  size={30} color="red" />
                                  
                                  </View>
                                </TouchableOpacity>
                          </View>

                          <View style={{flex: 4, backgroundColor: '#1f2f46', flexDirection:'column', marginTop:hp('15%'),marginRight:'5%',marginLeft:'5%'}} >
                            <View style={{flex: 4,flexDirection: 'row',justifyContent: 'center',marginTop:hp('2%'),  height: '80%', marginBottom:hp('4%')}} >

                              <View style={{width: wp('50%'),justifyContent:'space-between'}} >

                                  <View>
                                          <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>Total Questions</Text>
                                  </View>

                                  <View>
                                          <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>Correct Answers</Text>
                                  </View>

                                  <View>
                                          <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>Wrong Answers</Text>
                                  </View>

                                  <View>
                                          <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>UnAttempted Question</Text>
                                  </View>

                                  <View>
                                          <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }} >Total Marks</Text>
                                  </View>

                                  <View>
                                          <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>Your Score</Text>
                                  </View>

                                  <View>
                                          <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>Grade</Text>
                                  </View>
                              </View>

                              <View style={{width: wp('20%'),justifyContent:'space-between' }} >

                                  <View>
                                          <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>{this.state.latest_result.total_questions}</Text>
                                  </View>

                                  <View>
                                      <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>{this.state.latest_result.correct_answers}</Text>
                                  </View>

                                  <View>
                                          <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>{this.state.latest_result.wrong_answers}</Text>
                                  </View>

                                  <View>
                                          <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>{this.state.latest_result.unattempted_questions}</Text>
                                  </View>

                                  <View>
                                          <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>{this.state.latest_result.total_marks}</Text>
                                  </View>

                                  <View>
                                          <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>{this.state.latest_result.your_score}</Text>
                                  </View>

                                  <View>
                                          <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>{this.state.latest_result.grade}</Text>
                                  </View>
                              </View>

                            </View>

                            <View style={{flex: 2,flexDirection: 'row',justifyContent: 'center',}} >
                                  <View style= {{backgroundColor:'#FFFFFF', width:wp('80%'), marginBottom: hp('1%'), justifyContent:'center'}}>
                                      <Text style={{color:'#e16800', fontSize: RF(2.5), alignself:'center',marginLeft:wp('2%') }}>Remark: {this.state.latest_result.remarks}</Text>
                                  </View>
                            </View>
                          </View>

                          <View style={{flex: 1, backgroundColor: '#FFFFFF',}} >
                              <TouchableOpacity onPress={
                                this.DownloadPDFModal.bind(this,this.state.last_result_count)
                                }>
                                    <View style={{flexDirection: 'row', backgroundColor:'#1ab394', width:wp('80%'), height:hp('10%'), marginLeft:wp('9%'), justifyContent:'center', borderRadius: wp('2%'), alignItems:'center', marginTop:hp('2%'),}}>
                                        <Text style={{color:'#FFFFFF',fontSize: RF(4), alignself:'center'}}>Download Solution</Text>
                                    </View>
                                </TouchableOpacity>
                          </View>
                        
              </Modal>
      </View>
    );
  }
}

