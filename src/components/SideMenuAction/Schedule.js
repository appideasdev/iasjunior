/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  StatusBar,
  ImageBackground,
  ScrollView,
  WebView,
  AsyncStorage,
  Dimensions,
  Alert,
  ActivityIndicator,
  TouchableHighlight
} from 'react-native';

import GlobalVariables from './../GlobalVariables/GlobalVariables.js';
import HTML from 'react-native-render-html';
import HTMLView from 'react-native-htmlview';

import Pdf from 'react-native-pdf';
import RNFetchBlob from 'rn-fetch-blob';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import RF from "react-native-responsive-fontsize";
import styles from './../Styles/GlobalStyle.js';
import RNHTMLtoPDF from 'react-native-html-to-pdf';
import Quotes from "../Quotes.js";
import { HeaderBackButton } from 'react-navigation';



type Props = {};
export default class Schedule extends Component<Props> {

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Schedule',
      headerStyle: { backgroundColor:'#36404a' },
      headerTitleStyle: { color: '#FFFFFF' },
      headerTintColor: 'white',
      headerLeft:(<HeaderBackButton tintColor='#FFFFFF' onPress={()=>{navigation.navigate('First');}}/>)
  
  
    };
  };


  constructor(props) {
    super(props);
    this.state = { 
                    query : '',
                    TOKEN : null,
                    student_id : null,
                    quotesSlug : null,
                    isLoading   : true,
                    schedule    : null,
         };
  }


  componentDidMount = async () => {

    var GVar = GlobalVariables.getApiUrl();

    
    AsyncStorage.multiGet(["TOKEN", "student_id"]).then(response => {

            var TOKEN                          = response[0][1];
            var student_id                     = response[1][1];
           
            this.setState({
                            TOKEN          : TOKEN,    
                            student_id     : student_id,
                      });


            let formdata = new FormData();

            formdata.append('student_id',        student_id );
      
            fetch( GVar.BaseUrl + GVar.Schedule ,{
                      method: 'POST',
                      headers:{
                                  'token' : TOKEN
                              },
                      body: formdata,       
                      }).then((response) => response.text())
                     .then((responseJson) => {

                      var x = JSON.parse(responseJson.replace("-", ""));

                      this.setState({isLoading : false });
                      this.setState({status : x.status });
                     
                      
                      if( x.status == 1 ){

                        str = x.schedule;

                        str = str.replace(/\\'/g, '\'');
                        str = str.replace(/\\"/g, '"');
                        str = str.replace(/\\0/g, '\0');
                        str = str.replace(/\\\\/g, '\\');

                        this.setState({schedule : str });
                       
                      }else{

                        this.setState({message : x.message });
                      }
                     
                          
                     }).catch((error)=>{ Alert.alert(error)});

      });  

     
  }


 
  

  render() {



    console.log("schedulerender",this.state.schedule);
    return (
      <View style={{flex: 1,width:wp('100%')}}>
                       

            <WebView source={{uri: 'https://mentorship.iasjunior.com/student/api/schedules/11-'+this.state.student_id+'-56'}} scalesPageToFit={true}/>
          
            {this.state.isLoading ? <ActivityIndicator
                    animating     ={true}
                    transparent   ={true}
                    visible       ={false}
                    style         ={styles.indicator}
                    size          ="large"
                  /> : <View/>  } 
            <Quotes/>          
      </View>
    );
  }
}
