import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  Button,
  WebView,
  ImageBackground,
  AsyncStorage
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import RF from "react-native-responsive-fontsize";
import { HeaderBackButton } from 'react-navigation';


class IASjunior extends Component {

   static navigationOptions = ({ navigation }) => {
    return {
      title: 'IAS junior',
      headerStyle: { backgroundColor:'#36404a' },
      headerTitleStyle: { color: '#FFFFFF' },
      headerTintColor: 'white',
      headerLeft:(<HeaderBackButton tintColor='#FFFFFF' onPress={()=>{navigation.navigate('First');}}/>)
    };
  };

 
  constructor(props) {
   
    super(props);
   
    this.state = {
      TOKEN     : null,
      Profile   : null,
      quotesSlug : null,
      
    };

    AsyncStorage.getItem('TOKEN').then((data) => { 
        
       this.setState({TOKEN:data});
    });

  }


  
  componentDidMount = async () => {

      /*AsyncStorage.getItem('TOKEN').then((data) => { 

        this.setState({TOKEN : data });
        
              fetch( 'http://mentorship.iasjunior.com/student/api/student-profile',{
                      method: 'POST',
                      headers:{
                                  'token' : data
                              },
                      }).then((response) => response.text())
                     .then((responseJson) => {

                      this.setState({isloading : false });
                     
                      
                      console.log(responseJson.replace(/-\s?/g, ""),'responseJson1');

                      var x = JSON.parse(responseJson.replace(/-\s?/g, ""));
                     
                      if( x.status == 1 ){

                          var Profile = x.profile
                          this.setState({ Profile             : Profile, 
                                          TOKEN               : data,
                                          student_id          : Profile.student_id,
                                          name                : Profile.student_name,
                                          email               : Profile.student_email,
                                          phone               : Profile.phone,
                                          father_name         : Profile.father_name,
                                          paddress            : Profile.permanent_address,
                                          father_profession   : Profile.father_profession,
                                          laddress            : Profile.local_address,
                                          mother_name         : Profile.mother_name,
                                          current_status      : Profile.current_status,
                                          optional_subject    : Profile.optional_subject,
                                          isloading           : false,
                                         })

                          AsyncStorage.setItem('Profile', x.profile);

                          
                      }else{
                        Alert.alert(x.message)
                      }
                          
                     }).catch((error)=>{ Alert.alert(error)});


                     fetch( 'http://mentorship.iasjunior.com/student/api/quotes',{
                      method: 'POST',
                      headers:{
                                  'token' : data
                              },
                      }).then((response) => response.text())
                     .then((responseJson) => {

                      this.setState({isloading : false });
                     
                      
                      console.log(responseJson.replace(/-\s?/g, ""),'responseJson123');

                      var x = JSON.parse(responseJson.replace(/-\s?/g, ""));
                     
                      if( x.status == 1 ){

                        var thoughts = x.quotes;  

                        this.setState({quotes:x.quotes});
                        this.setState({quotesSlug:thoughts.quotesName});
                        console.log(x.quotes.quotesSlug,"quotesSlugquotesSlug")

                      }else{
                        Alert.alert(x.message)
                      }
                          
                     }).catch((error)=>{ Alert.alert(error)});
      });*/
  }


  render () {
    return (
          <View style={{flex: 1,widthwp:wp('100%')}}>

              
        
     
              <WebView
                    useWebKit={true}
                    ref="webview"
                    //style={{width: '100%'}}
                    automaticallyAdjustContentInsets={true}
                    source={{uri: 'https://www.iasjunior.com/'}}
                    javaScriptEnabled={true}
                    domStorageEnabled={true}
                    decelerationRate="normal"
                    startInLoadingState={true}
                  />   
          </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center'
  }
});

export default IASjunior;