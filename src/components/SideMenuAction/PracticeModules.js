/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  StatusBar,
  ImageBackground,
  ScrollView,
  WebView,
  AsyncStorage,
  Dimensions,
  Alert,
  ListView,
  FlatList,
  ActivityIndicator,
  PermissionsAndroid,
  TouchableOpacity
} from 'react-native';


import Quotes from "../Quotes.js";

import GlobalVariables from './../GlobalVariables/GlobalVariables.js';
import HTML from 'react-native-render-html';
import Pdf from 'react-native-pdf';
import RNFetchBlob from 'rn-fetch-blob';
import styles from './../Styles/GlobalStyle.js';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import RF from "react-native-responsive-fontsize";
import { HeaderBackButton } from 'react-navigation';



type Props = {};
export default class PracticeModules extends Component<Props> {

  static navigationOptions = ({ navigation }) => {
    return {
      title:'Practice Modules',
      headerStyle: { backgroundColor:'#36404a' },
      headerTitleStyle: { color: '#FFFFFF' },
      headerTintColor: 'white',
      headerLeft:(<HeaderBackButton tintColor='#FFFFFF' onPress={()=>{navigation.navigate('First');}}/>)
  
  
    };
  };


  constructor(props) {
    super(props);


    this.state = { 
                    query       : null,
                    TOKEN       : null,
                    student_id  : null,
                    quotesSlug  : null,
                    practicemodules     : [],
                    isLoading   : true,
         };
  }


  componentDidMount = async () => {


    var GVar = GlobalVariables.getApiUrl();

    
    AsyncStorage.multiGet(["TOKEN", "student_id"]).then(response => {

            var TOKEN                          = response[0][1];
            var student_id                     = response[1][1];
           
            this.setState({
                            TOKEN          : TOKEN,    
                            student_id     : student_id,
                      });


            let formdata = new FormData();

            formdata.append('student_id',        student_id );

            console.log(formdata,"formdata");
            console.log(TOKEN,"TOKEN");

            fetch( GVar.BaseUrl + GVar.PracticeModules ,{
                      method: 'POST',
                      headers:{
                                  'token' : TOKEN
                              },
                      body: formdata,       
                      }).then((response) => response.text())
                     .then((responseJson) => {

                      var x = JSON.parse(responseJson.replace("-", ""));
                      this.setState({isLoading : false });
                      this.setState({status : x.status });

                      if( x.status == 1 ){

                          console.log(x.practicemodules, "practicemodules");
                          this.setState({practicemodules : x.practicemodules });
                      }else{
                        this.setState({message : x.message });
                      }
                      
                      
                     }).catch((error)=>{ Alert.alert(error)});

      });  

    //this.onPDFPressed();
     
  }

  renderItem(data) {

    let { item, index } = data;
    
    return (

      <TouchableOpacity  onPress={() => { this.props.navigation.navigate('PracticeTest', {id: item.moduleId, moduleName: item.moduleName})}}>
                                
      <View style={{flex:1, flexDirection:'row',width:wp('95%'), justifyContent:'center', backgroundColor:'#1ab394', marginTop:hp('1%'), borderRadius:wp('2%')}}>
                          <View style={{flex:1, flexDirection:'row',marginLeft:wp('2%'), marginTop:hp('2%'), marginBottom:hp('2%')}}>
                          
                              <View style={{flex:1, flexDirection:'row',}}>
                                  <Text style={{color:'#FFFFFF', fontSize: RF(2.5)}}>{item.moduleName}</Text>
                                
                              </View>
                          </View>
                      </View>
       </TouchableOpacity>               
    ) 
  }

  

  render() {

    //const source = {uri:'http://samples.leanpub.com/thereactnativebook-sample.pdf',cache:true};

    return (
     <View style={{flex: 1}}>

            <ScrollView>

                { this.state.status ? 
                  <View style={{ alignItems:'center', alignself:'center'}}>
                        <FlatList
                          extraData={this.state}
                          data={this.state.practicemodules}
                          renderItem={this.renderItem.bind(this)}
                        />
                  </View>
                  :
                  <View style={{ alignItems:'center', alignself:'center',}}>
                    <View style={{flex:1,height:hp('70%'), flexDirection:'column', backgroundColor:'#1ab394', width:wp('95%'), alignself:'center', marginTop:hp('5%')}}>
                        <Text style={styles.error}>{this.state.message}</Text>
                    </View>
                  </View>
                }

            </ScrollView>
            {this.state.isLoading ? <ActivityIndicator
                    animating     ={true}
                    transparent   ={true}
                    visible       ={false}
                    style         ={styles.indicator}
                    size          ="large"
                  /> : <View/>  } 
      <Quotes/>                
      </View>
    );
  }
}


