/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  StatusBar,
  ImageBackground,
  ScrollView,
  WebView,
  AsyncStorage,
  Dimensions,
  Alert,
  ListView,
  FlatList,
  ActivityIndicator,
  TouchableOpacity,
  Image
} from 'react-native';

import GlobalVariables from './../GlobalVariables/GlobalVariables.js';
import styles from './../Styles/GlobalStyle.js';
import HTML from 'react-native-render-html';
import Pdf from 'react-native-pdf';
import RNFetchBlob from 'rn-fetch-blob';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import RF from "react-native-responsive-fontsize";
import { HeaderBackButton } from 'react-navigation';

import StudyMaterialWebView from './../SideMenuAction/StudyMaterialWebView.js';


import Quotes from "../Quotes.js";

type Props = {};
export default class UPSCSolutions extends Component<Props> {

 static navigationOptions = ({ navigation }) => {
    return {
      title: 'UPSC Solutions',
      headerStyle: { backgroundColor:'#36404a' },
      headerTitleStyle: { color: '#FFFFFF' },
      headerTintColor: 'white',
      headerLeft:(<HeaderBackButton tintColor='#FFFFFF' onPress={()=>{navigation.navigate('First');}}/>)
  
  
    };
  };


  constructor(props) {
    super(props);


    this.state = { 
                    TOKEN       : null,
                    student_id  : null,
                    studymaterials  : [],
                    exam  : [],
                    isLoading   : true,
         };
  }


  componentDidMount = async () => {

    

    var GVar = GlobalVariables.getApiUrl();

    
    AsyncStorage.multiGet(["TOKEN", "student_id"]).then(response => {

            var TOKEN                          = response[0][1];
            var student_id                     = response[1][1];
           
            this.setState({
                            TOKEN          : TOKEN,    
                            student_id     : student_id,
                      });


            let formdata = new FormData();

            formdata.append('student_id',        student_id );

            console.log(formdata,"formdata");
            console.log(TOKEN,"TOKEN");

            fetch( GVar.BaseUrl + GVar.UPSCStudyMaterial ,{
                      method: 'POST',
                      headers:{
                                  'token' : TOKEN
                              },
                      body: formdata,       
                      }).then((response) => response.text())
                     .then((responseJson) => {

                      var x = JSON.parse(responseJson.replace("-", ""));
                      this.setState({isLoading : false });
                      this.setState({status : x.status });
                      

                      if( x.status == 1 ){

                          //console.log("studymaterials", x.studymaterials);
                          this.setState({studymaterials : x.studymaterials });
                          console.log("my study materials",this.state.studymaterials);
                          this.setState({exam : x.exam });
                      }else{
                        this.setState({message : x.message });

                      }
                          
                     }).catch((error)=>{ Alert.alert(error)});

      });  

     
  }


   onPDFPressed(item){   

      this.setState({isLoading : true });
      var GVar = GlobalVariables.getApiUrl();

     // Alert.alert(Platform.OS)

      if( Platform.OS == 'ios' ){



        const dirs = RNFetchBlob.fs.dirs

            RNFetchBlob
            .config({
              fileCache : true,
              path: dirs.DocumentDir + "/" + item
            })
            .fetch('GET', GVar.CurrentAffairsFile +item)
            .then((res) => {
             
              try{
                
                this.setState({isLoading : false });
                RNFetchBlob.ios.openDocument(dirs.DocumentDir + "/" + item);

              }catch(error){
                
                this.setState({isLoading : false });
                Alert.alert('Please check your internet connection.')
              }


            }, (error) => {
              
               this.setState({isLoading : false });
               Alert.alert('Please check your internet connection.')
              
            })

      }else{

            RNFetchBlob
                .config({
                    notification: true,
                    path: 'downloads/'+item,
                    indicator: true,
                    overwrite: true,
                    addAndroidDownloads: {
                        path: RNFetchBlob.fs.dirs.SDCardDir +'/downloads/'+item,
                        useDownloadManager: true,
                        notification: true,
                        overwrite: true,
                        description: 'downloading content...',
                        mime: 'application/pdf',
                        mediaScannable: false
                    }
                })
                .fetch('GET', GVar.CurrentAffairsFile +item)
                .progress((received, total) => {

                    this.setState({isLoading : false });
                    console.log('progress', received / total)    
                })
      }          
  }

  OnClickItem(item){
    
    console.log("item on click",item);
    if(item.ansvideos != null && item.ansvideos != ""){
        this.props.navigation.navigate('StudyMaterialWebView',{source:item.ansvideos});
    }
    if(item.anspdfFile != null && item.anspdfFile != ""){
      this.onPDFPressed(item.pdfFile);
    }

  }

  showstudymaterial(data) {

      var exam = this.state.exam;


      for (var i = 1, l = Object.keys(data).length-1; i <= l; i++) {
          
          return <FlatList
                      extraData={this.state}
                      data={Object.values(data)}
                      renderItem={({item, index}) => 

                      <View style={{flex:1, flexDirection:'column',width:wp('98%'), justifyContent:'center', backgroundColor:'#FFFFFF', marginTop:hp('1%'), borderWidth:2, borderColor:'#CCCCCC', alignItems:'center', }}>

                              <View style={{flex:1, flexDirection:'row',width:wp('95%'), justifyContent:'center', backgroundColor:'#3f4c6b', marginTop:hp('1%'), borderWidth:2, borderColor:'#CCCCCC', alignself:'center',}}>
                                  

                                  <View style={{flex:1, flexDirection:'column',marginLeft:wp('2%'), marginTop:hp('2%'), marginBottom:hp('2%')}}>
                                  
                                      <View  style={{flex:3,}}>
                                            <Text style={styles.TopTitle}>{exam[index + 1]}</Text>
                                      </View>

                                  </View>

                              </View>

                              <View style={{flex:1, flexDirection:'row',width:wp('95%'), justifyContent:'center', backgroundColor:'#1ab394',  borderWidth:2, borderColor:'#CCCCCC', alignself:'center',}}>
                                  

                                  <View style={{flex:1, flexDirection:'row',marginLeft:wp('2%'), marginTop:hp('2%'), marginBottom:hp('2%')}}>
                                  
                                      <View  style={{width:'20%'}}>
                                            <Text style={{color:'#000000',fontSize: RF(3),alignself:'center'}}>Year</Text>
                                      </View>

                                     <View  style={{width:'40%'}}>
                                            <Text style={{color:'#000000',fontSize: RF(3),alignself:'center'}}>Question Paper</Text>
                                      </View>

                                      <View  style={{width:'40%'}}>
                                            <Text style={{color:'#000000',fontSize: RF(3),alignself:'center'}}>Question and Solutions</Text>
                                      </View>

                                  </View>

                              </View>

                              <View style={{flex:1, flexDirection:'row',width:wp('95%'), justifyContent:'center', backgroundColor:'#FFFFFF',  borderWidth:2, borderColor:'#CCCCCC', alignself:'center',}}>
                                  
                                  { item ? this.showstudymaterialComponents(item) : <View/>}
                                  
                              </View>

                      </View>
                    }
                    />
      }
  }

  showstudymaterialComponents( components ){

    console.log("items",components);

    return <FlatList
                      extraData={this.state}
                      data={components}
                      renderItem={({item}) => 
                          <View>
                                  
                                  <View  style={{flex:1,flexDirection:'row', justifyContent:'space-between'}}>
                                  
                                      <View  style={{width:'20%',borderRightWidth: .5,alignItems:'center'}}>
                                            <Text style={{color:'#000000',fontSize: RF(3),alignself:'center' }}>{item.year}</Text>
                                      </View>
                                      <TouchableOpacity style={{width:'40%',borderRightWidth: .5,alignItems:'center', justifyContent:'center'}} onPress={this.onPDFPressed.bind(this, item.pdfFile)}>
                                      <View >
                                            <Text style={{color:'#000000',fontSize: RF(3), alignself:'center', marginLeft:wp('1%')}}>{item.title}</Text>
                                      </View>
                                      </TouchableOpacity>
                                       <TouchableOpacity style={{width:'40%',borderRightWidth: .5,alignItems:'center', justifyContent:'center'}}
                                          onPress={()=>{this.OnClickItem(item)}}>
                                          <View  >
                                                <Text style={{color:'#000000',fontSize: RF(3), alignself:'center', marginLeft:wp('1%')}}>{item.anstitle}</Text>
                                          </View>
                                        </TouchableOpacity>

                                  </View>

                                  <View style={styles.separator} />
                                
                          </View>        
                    }
                    />
  }
  

  render() {

    return (
      <View style={{flex: 1,widthwp:wp('100%')}}>

            <ScrollView>
            { this.state.status ? 
              <View style={{ alignItems:'center', alignself:'center',}}>
                <View style={{ alignItems:'center', alignself:'center'}}>
                      { this.state.studymaterials ? this.showstudymaterial(this.state.studymaterials) : <View/>}
                </View>
              </View>
              :
              <View style={{ alignItems:'center', alignself:'center',}}>
                <View style={{flex:1,height:hp('70%'), flexDirection:'column', backgroundColor:'#1ab394', width:wp('95%'), alignself:'center', marginTop:hp('5%')}}>
                    <Text style={styles.error}>{this.state.message}</Text>
                </View>
              </View>
            }
            </ScrollView>
            {this.state.isLoading ? <ActivityIndicator
                    animating     ={true}
                    transparent   ={true}
                    visible       ={false}
                    style         ={styles.indicator}
                    size          ="large"
                  /> : <View/>  } 
            <Quotes/>          
      </View>
    );
  }
}

