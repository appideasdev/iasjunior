/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  StatusBar,
  ImageBackground,
  ScrollView,
  WebView,
  AsyncStorage,
  Dimensions,
  Alert,
  ListView,
  FlatList,
  ActivityIndicator,
  PermissionsAndroid,
  TouchableOpacity,
  Image,
  Modal,
  TouchableHighlight
} from 'react-native';

import GlobalVariables from './../GlobalVariables/GlobalVariables.js';
import HTML from 'react-native-render-html';
import Pdf from 'react-native-pdf';
import RNFetchBlob from 'rn-fetch-blob';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import RF from "react-native-responsive-fontsize";
import YouTube from 'react-native-youtube'

import Video from 'react-native-video';
import MediaControls, { PLAYER_STATES } from 'react-native-media-controls';
import { HeaderBackButton } from 'react-navigation';


import styles from './../Styles/GlobalStyle.js';
import Quotes from "../Quotes.js";

type Props = {};
export default class PlayVideo extends Component<Props> {

  static navigationOptions = ({ navigation }) => {
    return {
      //title: navigation.getParam('moduleName'),
      headerStyle: { backgroundColor:'#36404a' },
      headerTitleStyle: { color: '#FFFFFF' },
      headerTintColor: 'white',
      headerLeft:(<HeaderBackButton tintColor='#FFFFFF' onPress={()=>{navigation.navigate('First');}}/>)
  
    };
  };


  constructor(props) {
    super(props);

     this.state = { 
                    isLoading   : true,
                    item        : [],
                    currentTime: 0,
                    duration: 0,
                    isFullScreen: false,
                    isLoading: true,
                    paused: true,
                    playerState: PLAYER_STATES.PLAYING,
         };

    const { navigation } = this.props;
    const item  = navigation.getParam('item');

    this.setState({item : item });
    this.setState({name : item.update_name });
    console.log(item.update_name,"update_nameupdate_name");
    
  }

  onPDFPressed(item){   
      this.setState({isLoading : true });

      var GVar = GlobalVariables.getApiUrl();

      if( Platform.OS == 'ios' ){

        const dirs = RNFetchBlob.fs.dirs

            RNFetchBlob
            .config({
              fileCache : true,
              path: dirs.DocumentDir + "/" + item
            })
            .fetch('GET', GVar.UpdatesFile +item)
            .then((res) => {
             
              try{

                this.setState({isLoading : false });
                RNFetchBlob.ios.openDocument(dirs.DocumentDir + "/" + item);

              }catch(error){
                this.setState({isLoading : false });
                Alert.alert('Please check your internet connection.')
              }


            }, (error) => {
              
              this.setState({isLoading : false });
              Alert.alert('Please check your internet connection.')
              
            })

      }else{

            RNFetchBlob
                .config({
                    notification: true,
                    path: 'downloads/'+item,
                    indicator: true,
                    overwrite: true,
                    addAndroidDownloads: {
                        path: RNFetchBlob.fs.dirs.SDCardDir +'/downloads/'+item,
                        useDownloadManager: true,
                        notification: true,
                        overwrite: true,
                        description: 'downloading content...',
                        mime: 'application/pdf',
                        mediaScannable: false
                    }
                })
                .fetch('GET', GVar.UpdatesFile +item)
                .progress((received, total) => {
                    this.setState({isLoading : false });
                    console.log('progress', received / total)    
                })
      }          
  }


  onSeek = seek => {
    this.videoPlayer.seek(seek);
  };

  onPaused = playerState => {
    this.setState({
      paused: !this.state.paused,
      playerState,
    });
  };

  onReplay = () => {
    this.setState({ playerState: PLAYER_STATES.PLAYING });
    this.videoPlayer.seek(0);
  };

  onProgress = data => {
    const { isLoading, playerState } = this.state;
    // Video Player will continue progress even if the video already ended
    if (!isLoading && playerState !== PLAYER_STATES.ENDED) {
      this.setState({ currentTime: data.currentTime });
    }
  };

  onLoad = data => this.setState({ duration: data.duration, isLoading: false });

  onLoadStart = data => this.setState({ isLoading: true });

  onEnd = () => this.setState({ playerState: PLAYER_STATES.ENDED });

  onError = () => alert('Oh! ', error);

  exitFullScreen = () => {};

  enterFullScreen = () => {};

  onFullScreen = () => {};
  
  onSeeking = currentTime => this.setState({ currentTime });

  render() {

    const { navigation } = this.props;
    const item  = navigation.getParam('item');
    var name = item.update_name;
    var frame;

    console.log(item,"itemfromNavigation")

    if( item.motivationVideo ){

       frame = '<iframe height="250"  src="'+item.motivationVideo+'"></iframe>';

    }

    if( item.updateVideo ){
       frame = '<iframe height="250" src="'+item.updateVideo+'"></iframe>';
    }

    console.log(frame,"itemfromNavigation")
    
    //var frame = '<iframe src="https://www.youtube.com/embed/CTaz9fGRnFY"></iframe>';
   
    return (
      <View style={{flex: 1, justifyContent:'center',widthwp:wp('100%'), backgroundColor:'#FFFFFF',justifyContent:"center", aliignItems:'center'}}>
          <ScrollView>
            { item.updateVideo ? 
                  <HTML html={frame} />
                  :
                  <View/>
                }

                { item.motivationVideo ? 
                  <HTML html={frame} />
                  :
                  <View/>
                }
            <View  style={{flexDirection:'row',width:wp('95%'), height:hp('8%'), justifyContent:'center', backgroundColor:'#FFFFFF', marginTop:hp('1%'), marginLeft:wp('2.5%')}}>
                

                

                <Text style={{fontSize: RF(2)}}>{item.month} {item.year}</Text>
                { item.updateFile ? 
                  <TouchableOpacity onPress={this.onPDFPressed.bind(this, item.updateFile)}>
                     <Text style={{color:'#FF0000',fontSize: RF(2.5)}}>Download PDF</Text>
                  </TouchableOpacity>
                  :
                  <View/>
                }

                

                
            </View>
            <View  style={{marginLeft:wp('2%')}} > 
                { name ? 
                  <HTML html={name} />
                  :
                  <View/>
                }
            </View>
          </ScrollView>
      <Quotes/>        
      </View>

    );
  }
}
