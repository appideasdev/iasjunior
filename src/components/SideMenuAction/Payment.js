/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  StatusBar,
  ImageBackground,
  ScrollView,
  WebView,
  AsyncStorage,
  ActivityIndicator
} from 'react-native';



type Props = {};


import GlobalVariables from './../GlobalVariables/GlobalVariables.js';
import styles from './../Styles/GlobalStyle.js';
import HTML from 'react-native-render-html';
import Pdf from 'react-native-pdf';
import RNFetchBlob from 'rn-fetch-blob';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import RF from "react-native-responsive-fontsize";
import { HeaderBackButton } from 'react-navigation';



export default class Payment extends Component<Props> {

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Pay to IAS Junior',
      headerStyle: { backgroundColor:'#36404a' },
      headerTitleStyle: { color: '#FFFFFF' },
      headerTintColor: 'white',
      headerLeft:(<HeaderBackButton tintColor='#FFFFFF' onPress={()=>{navigation.navigate('First', { isRefresh: 1 });}}/>)
  
    };
  };

  constructor(props) {
    super(props);
    this.state = { 
                    TOKEN       : null,
                    student_id  : null,
                    url         : null,
                    isLoading   : true,
         };

    AsyncStorage.getItem('TOKEN').then((data) => { 

        this.setState({TOKEN : data });

    });    

   
  }

  componentDidMount = async () => {

    

    var GVar = GlobalVariables.getApiUrl();

    
    AsyncStorage.multiGet(["TOKEN", "student_id"]).then(response => {

            var TOKEN                          = response[0][1];
            var student_id                     = response[1][1];
           
            this.setState({
                            TOKEN          : TOKEN,    
                            student_id     : student_id,
                      });
            this.setState({isLoading : false });

            /*let formdata = new FormData();

            formdata.append('student_id',        student_id );

            console.log(formdata,"formdata");
            console.log(TOKEN,"TOKEN");

            fetch( GVar.BaseUrl + GVar.Payment ,{
                      method: 'POST',
                      headers:{
                                  'token' : TOKEN
                              },
                      body: formdata,       
                      }).then((response) => response.text())
                     .then((responseJson) => {

                      var x = JSON.parse(responseJson.replace("-", ""));

                      
                      this.setState({url : x.url });
                      this.setState({isLoading : false });
 
            }).catch((error)=>{ Alert.alert(error)});*/

      });  

     
  }


  render() {

    const TOKEN = this.state.TOKEN;

    
    
    console.log(TOKEN,"TOKEN")

    return (
      <View style={{flex: 1,widthwp:wp('100%')}}>
            { TOKEN != null ? 

              <WebView
                      ref="webview"
                      style={{width: '100%'}}
                      automaticallyAdjustContentInsets={false}
                      source={
                        {
                          uri: "https://mentorship.iasjunior.com/student/api/payment",
                          headers: {"token": TOKEN},
                        }
                      }
                      javaScriptEnabled={true}
                      domStorageEnabled={true}
                      decelerationRate="normal"
                      startInLoadingState={true}
                    />
            
                  :
                  <View/>
            }


            {this.state.isLoading ? <ActivityIndicator
                    animating     ={true}
                    transparent   ={true}
                    visible       ={false}
                    style         ={styles.indicator}
                    size          ="large"
                  /> : <View/>  } 
                
      </View>
    );
  }
}

