/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  StatusBar,
  ImageBackground,
  ScrollView,
  WebView,
  AsyncStorage,
  Dimensions,
  Alert,
  ListView,
  FlatList,
  ActivityIndicator,
  PermissionsAndroid,
  TouchableOpacity,
  Image,
  Modal
} from 'react-native';
import CountDown from 'react-native-countdown-component';
import moment from 'moment';
import Icon from 'react-native-vector-icons/FontAwesome';


import Quotes from "../Quotes.js";

import GlobalVariables from './../GlobalVariables/GlobalVariables.js';
import HTML from 'react-native-render-html';
import Pdf from 'react-native-pdf';
import RNFetchBlob from 'rn-fetch-blob';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import RF from "react-native-responsive-fontsize";
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import {StackNavigator} from 'react-navigation'; 
import styles from './../Styles/GlobalStyle.js';
import RNHTMLtoPDF from 'react-native-html-to-pdf';
import { HeaderBackButton } from 'react-navigation';




import ModuleSubContent from './ModuleSubContent.js';

const ActivityProject = StackNavigator({
    ModuleSubContent: {screen: ModuleSubContent},
});


import ViewPDF from "./ViewPDF.js";
import { CheckBox } from 'react-native-elements'


class PrelimQuiz extends Component<Props> {


  static navigationOptions = {
    header: null,
  }


  static navigationOptions = {
    header: null,
  }


  
  setSecondModalVisible(visible) {
    this.setState({visiblemodal: visible});
    
  }
  

  

  constructor(props) {
    super(props);

    this.state = { 
                    query       : null,
                    TOKEN       : null,
                    student_id  : null,
                    quotesSlug  : null,
                    prelimscore : [],
                    isLoading   : true,
                    moduleName  : null,
                    timeCompleted:true,
                    totalQuestionCount:0,

                     // for opening modal taking state variables
                     testCompleted:true,
                     visiblemodal:false,
                     questions   : [],
                     isModalLoading   : true,
                     totalDuration: '',
                     plusModalVisible: false,
                     startTime: false,
                     nextPostion : 0,
                     pos         : [
                                     {"id":"1", "indexstart":"1", "indexend":"5" },
                                     {"id":"2", "indexstart":"1", "indexend":"5" },
                                     {"id":"3", "indexstart":"1", "indexend":"5" },
                                     {"id":"4", "indexstart":"1", "indexend":"5" },
                                     {"id":"5", "indexstart":"1", "indexend":"5" },
 
                                     {"id":"6", "indexstart":"6", "indexend":"10" },
                                     {"id":"7", "indexstart":"6", "indexend":"10" },
                                     {"id":"8", "indexstart":"6", "indexend":"10" },
                                     {"id":"9", "indexstart":"6", "indexend":"10" },
                                     {"id":"10", "indexstart":"6", "indexend":"10" },
 
                                     {"id":"11", "indexstart":"11", "indexend":"15" },
                                     {"id":"12", "indexstart":"11", "indexend":"15" },
                                     {"id":"13", "indexstart":"11", "indexend":"15" },
                                     {"id":"14", "indexstart":"11", "indexend":"15" },
                                     {"id":"15", "indexstart":"11", "indexend":"15" },
 
                                     {"id":"16", "indexstart":"16", "indexend":"20" },
                                     {"id":"17", "indexstart":"16", "indexend":"20" },
                                     {"id":"18", "indexstart":"16", "indexend":"20" },
                                     {"id":"19", "indexstart":"16", "indexend":"20" },
                                     {"id":"20", "indexstart":"16", "indexend":"20" },
 
                                     {"id":"21", "indexstart":"21", "indexend":"25" },
                                     {"id":"22", "indexstart":"21", "indexend":"25" },
                                     {"id":"23", "indexstart":"21", "indexend":"25" },
                                     {"id":"24", "indexstart":"21", "indexend":"25" },
                                     {"id":"25", "indexstart":"21", "indexend":"25" },
 
                                     {"id":"26", "indexstart":"26", "indexend":"30" },
                                     {"id":"27", "indexstart":"26", "indexend":"30" },
                                     {"id":"28", "indexstart":"26", "indexend":"30" },
                                     {"id":"29", "indexstart":"26", "indexend":"30" },
                                     {"id":"30", "indexstart":"26", "indexend":"30" },
 
                                     {"id":"31", "indexstart":"31", "indexend":"35" },
                                     {"id":"32", "indexstart":"31", "indexend":"35" },
                                     {"id":"33", "indexstart":"31", "indexend":"35" },
                                     {"id":"34", "indexstart":"31", "indexend":"35" },
                                     {"id":"35", "indexstart":"31", "indexend":"35" },
 
                                     {"id":"36", "indexstart":"36", "indexend":"40" },
                                     {"id":"37", "indexstart":"36", "indexend":"40" },
                                     {"id":"38", "indexstart":"36", "indexend":"40" },
                                     {"id":"39", "indexstart":"36", "indexend":"40" },
                                     {"id":"40", "indexstart":"36", "indexend":"40" },
 
                                     {"id":"41", "indexstart":"41", "indexend":"45" },
                                     {"id":"42", "indexstart":"41", "indexend":"45" },
                                     {"id":"43", "indexstart":"41", "indexend":"45" },
                                     {"id":"44", "indexstart":"41", "indexend":"45" },
                                     {"id":"45", "indexstart":"41", "indexend":"45" },
 
                                     {"id":"46", "indexstart":"46", "indexend":"50" },
                                     {"id":"47", "indexstart":"46", "indexend":"50" },
                                     {"id":"48", "indexstart":"46", "indexend":"50" },
                                     {"id":"49", "indexstart":"46", "indexend":"50" },
                                     {"id":"50", "indexstart":"46", "indexend":"50" },
 
                                     {"id":"51", "indexstart":"51", "indexend":"55" },
                                     {"id":"52", "indexstart":"51", "indexend":"55" },
                                     {"id":"53", "indexstart":"51", "indexend":"55" },
                                     {"id":"54", "indexstart":"51", "indexend":"55" },
                                     {"id":"55", "indexstart":"51", "indexend":"55" },
 
                                     {"id":"56", "indexstart":"56", "indexend":"60" },
                                     {"id":"57", "indexstart":"56", "indexend":"60" },
                                     {"id":"58", "indexstart":"56", "indexend":"60" },
                                     {"id":"59", "indexstart":"56", "indexend":"60" },
                                     {"id":"60", "indexstart":"56", "indexend":"60" },
 
                                     {"id":"61", "indexstart":"61", "indexend":"65" },
                                     {"id":"62", "indexstart":"61", "indexend":"65" },
                                     {"id":"63", "indexstart":"61", "indexend":"65" },
                                     {"id":"64", "indexstart":"61", "indexend":"65" },
                                     {"id":"65", "indexstart":"61", "indexend":"65" },
 
                                     {"id":"66", "indexstart":"66", "indexend":"70" },
                                     {"id":"67", "indexstart":"66", "indexend":"70" },
                                     {"id":"68", "indexstart":"66", "indexend":"70" },
                                     {"id":"69", "indexstart":"66", "indexend":"70" },
                                     {"id":"70", "indexstart":"66", "indexend":"70" },
 
                                     {"id":"71", "indexstart":"71", "indexend":"75" },
                                     {"id":"72", "indexstart":"71", "indexend":"75" },
                                     {"id":"73", "indexstart":"71", "indexend":"75" },
                                     {"id":"74", "indexstart":"71", "indexend":"75" },
                                     {"id":"75", "indexstart":"71", "indexend":"75" },
 
                                     {"id":"76", "indexstart":"76", "indexend":"80" },
                                     {"id":"77", "indexstart":"76", "indexend":"80" },
                                     {"id":"78", "indexstart":"76", "indexend":"80" },
                                     {"id":"79", "indexstart":"76", "indexend":"80" },
                                     {"id":"80", "indexstart":"76", "indexend":"80" },
 
                                     {"id":"81", "indexstart":"81", "indexend":"85" },
                                     {"id":"82", "indexstart":"81", "indexend":"85" },
                                     {"id":"83", "indexstart":"81", "indexend":"85" },
                                     {"id":"84", "indexstart":"81", "indexend":"85" },
                                     {"id":"85", "indexstart":"81", "indexend":"85" },
 
                                     {"id":"86", "indexstart":"86", "indexend":"90" },
                                     {"id":"87", "indexstart":"86", "indexend":"90" },
                                     {"id":"88", "indexstart":"86", "indexend":"90" },
                                     {"id":"89", "indexstart":"86", "indexend":"90" },
                                     {"id":"90", "indexstart":"86", "indexend":"90" },
 
                                     {"id":"91", "indexstart":"91", "indexend":"95" },
                                     {"id":"92", "indexstart":"91", "indexend":"95" },
                                     {"id":"93", "indexstart":"91", "indexend":"95" },
                                     {"id":"94", "indexstart":"91", "indexend":"95" },
                                     {"id":"95", "indexstart":"91", "indexend":"95" },
 
                                     {"id":"96", "indexstart":"96", "indexend":"100" },
                                     {"id":"97", "indexstart":"96", "indexend":"100" },
                                     {"id":"98", "indexstart":"96", "indexend":"100" },
                                     {"id":"99", "indexstart":"96", "indexend":"100" },
                                     {"id":"100", "indexstart":"96", "indexend":"100" },
                                   ]
         };
  }


  componentDidMount = async () => {


    var GVar = GlobalVariables.getApiUrl();

    AsyncStorage.multiGet(["TOKEN", "student_id", "moduleId"]).then(response => {

            var TOKEN                          = response[0][1];
            var student_id                     = response[1][1];
            var module_id                      = response[2][1];

            this.setState({
                            TOKEN          : TOKEN,    
                            student_id     : student_id,
                      });

            var TOKEN                          = response[0][1];
            var student_id                     = response[1][1];
 

            let formdata = new FormData();

            formdata.append('student_id',        student_id );

            console.log(formdata,"formdata");
            console.log(TOKEN,"TOKEN");

            fetch( GVar.BaseUrl + GVar.ModuleContent + module_id ,{
                      method: 'POST',
                      headers:{
                                  'token' : TOKEN
                              },
                      body: formdata,       
                      }).then((response) => response.text())
                     .then((responseJson) => {

                      var x = JSON.parse(responseJson.replace("-", ""));
                      this.setState({isLoading : false });
                      

                      if( x.status == 1 ){

                          console.log(x, "prelimscore1233");
                          this.setState({prelimscore : x.prelimscore });
                          this.setState({moduleName : x.module.moduleName });
                          this.setState({prelimExam : x.prelimExam });
                          this.setState({moduleType : x.module.moduleType });

                      }
                      
                     }).catch((error)=>{ Alert.alert(error)});


    });


    this.startTimer();
  
  }

  startTimer(){

    var that = this;
  
    if(that.state.startTime){

      that.setState({ totalDuration: 7200 });
    }else{
      that.setState({totalDuration:300})
    }
    
    //Settign up the duration of countdown in seconds to re-render

  
  }

  onFinishTimer(){

    console.log("test complte");
    if(this.state.timeCompleted){
      Alert.alert("Exam Submitted");
      this.setSecondModalVisible(false);
      this.changingStatusOfStartTest();
    }
  
  }

  getPrelimQuestion(){

          var GVar = GlobalVariables.getApiUrl();

          console.log("get prelim Questions");

           //Starting Prelim test fetching here

          AsyncStorage.multiGet(["TOKEN", "student_id", "moduleId"]).then(response => {

                var TOKEN                          = response[0][1];
                var student_id                     = response[1][1];
                var module_id                      = response[2][1];

                this.setState({
                                TOKEN          : TOKEN,    
                                student_id     : student_id,
                          });

                //AsyncStorage.removeItem('moduleId')

                console.log(module_id,"module_id get questions")

                

                var TOKEN                          = response[0][1];
                var student_id                     = response[1][1];
               
                this.setState({
                                TOKEN          : TOKEN,    
                                student_id     : student_id,
                                module_id      : module_id,
                                isLoading      : true, 
                          });

                let prelimTestFormData = new FormData();

                prelimTestFormData.append('student_id',        student_id );
                prelimTestFormData.append('module_id',         module_id );
              
                console.log(prelimTestFormData,"formdata");
                console.log(TOKEN,"TOKEN");

                fetch( GVar.BaseUrl + GVar.StartPrelimTest,{
                          method: 'POST',
                          headers:{
                                      'token' : TOKEN
                                  },
                          body: prelimTestFormData,       
                          }).then((response) => response.text())
                          .then((responseJson) => {

                          console.log("get practice questions",responseJson);
                          var x = JSON.parse(responseJson.replace("-", ""));
                          this.setState({isModalLoading : false });
                        
                          if( x.status == 1 ){
                            this.setSecondModalVisible(true);              
                            this.setState({startTime:true})
                              console.log(x, "questions");
                              console.log("timer state",this.state.startTime);
                              this.startTimer();           
                                                                            
                              this.setState({questions : x.questions });
                              

                          }else{

                          }
                          
                          }).catch((error)=>{ Alert.alert(error)}); 

          });
  }

  getReviews(){                                                                                                                                                                                        

      //Async Storage for Reviews
      var GVar = GlobalVariables.getApiUrl();

      console.log("get reviews ");

      AsyncStorage.multiGet(["TOKEN", "student_id", "moduleId"]).then(response => {

            var TOKEN                          = response[0][1];
            var student_id                     = response[1][1];
            var module_id                      = response[2][1];
         
            console.log(module_id,"module_idnew")


            var TOKEN                          = response[0][1];
            var student_id                     = response[1][1];
           

            let formdata = new FormData();

            formdata.append('student_id',        student_id );
            formdata.append('module_id',         module_id );
            
            console.log(formdata,"formdata");
            console.log(TOKEN,"TOKEN");

            fetch( GVar.BaseUrl + GVar.StartPrelimReviews,{
                      method: 'POST',
                      headers:{
                                  'token' : TOKEN
                              },
                      body: formdata,       
                      }).then((response) => response.text())
                     .then((responseJson) => {

                      var x = JSON.parse(responseJson.replace("-", ""));
                      this.setState({isModalLoading : false });
                      

                      if( x.status == 1 ){

                          console.log("reviews",x);
                          this.setState({review : x.review });
                          this.setState({totalQuestionCount:x.review.length})
                          console.log("review length123456",x.review.length);
                              

                      }else{

                      }
                      
                     }).catch((error)=>{ Alert.alert(error)});

      });

  }

  insertPrelimQuestions(){
    console.log("my prelim test");

      var GVar = GlobalVariables.getApiUrl();

      AsyncStorage.multiGet(["TOKEN", "student_id", "moduleId"]).then(response => {

              var TOKEN                          = response[0][1];
              var student_id                     = response[1][1];
              var module_id                      = response[2][1];


              console.log("insert prelim question")
              let formdata = new FormData();
              formdata.append("student_id",student_id);
              formdata.append("module_id",module_id);

              console.log("my form data",formdata);

            
              fetch( GVar.BaseUrl + GVar.InsertPrelimQuestions,{
                  method: 'POST',
                  headers:{
                              'token' : TOKEN
                          },
                  body: formdata,       
                  }).then((response) => response.text())
                 .then((responseJson) => {

                 
                  var x = JSON.parse(responseJson.replace("-", ""));
                  this.setState({isLoading : false });
                  if(x.status == 0){
                    Alert.alert("Questions are not prepared..Please Try Later!!");
                   
                  }else{ 
                    this.getPrelimQuestion();
                    this.getReviews();
                  }
                  console.log("PRELIMquestion",x);

                  
              
             }).catch((error)=>{ Alert.alert(error)});
      });          
    
  }

 

  onPDFPressed(item){   

      this.setState({isLoading : true})

      AsyncStorage.multiGet(["TOKEN", "student_id", "moduleId"]).then(response => {

        var TOKEN                          = response[0][1];
        var student_id                     = response[1][1];
        var module_id                      = response[2][1];

        var GVar = GlobalVariables.getApiUrl();

       
        let formdata = new FormData();
  
        formdata.append('student_id',        student_id );
  
        console.log("responseJsonresponseJson",formdata)
        console.log("TOKEN",TOKEN)
       
        fetch( GVar.DownloadPrelimPdf + module_id ,{
                        method: 'POST',
                        headers:{
                                    'token' : TOKEN
                                },
                        body: formdata,       
                        }).then((response) => response.text())
                       .then((responseJson) => {
  
                        var x = JSON.parse(responseJson.replace("-", ""));
                        this.setState({isLoading : false });
  
                        console.log("responseJsonresponseJson",x);
                        console.log("my mod id",this.state.module_id);
                        console.log("source module id",module_id);
  
                       
                        var options = {
                   
                              html: x.html, 
  
  
                              fileName: 'prelim-quiz-' + module_id ,          
  
                              directory: 'docs',         
                              base64: true ,               
  
  
                              height: 800,                
                              width: 1056,              
                                                      
                              padding: 24,               
                        };
  
                        const source = {uri:data.filePath};
                        console.log("source file ",source);
  
                        RNHTMLtoPDF.convert(options).then((data) => {

                        if( Platform.OS == 'ios' ){
                           
                            RNFetchBlob.ios.openDocument(data.filePath);

                          }else{
                            
                            this.props.navigation.navigate('ViewPDF',{source:data.filePath })
                            Alert.alert('Successfully downloaded in Documents' );
                          }
                        // this.props.navigation.navigate('ViewPDF',{source:data.filePath })
                        // Alert.alert('Successfully downloaded in Downloads' );
                      });
                            
                        
                        
                       }).catch((error)=>{ Alert.alert(error)});

      });

  }



 


   //modal plus Sign modal(part of StartTestModal)
   setPlusModalVisible(visible) {
    this.setState({plusModalVisible: visible});
  }

  //part of StartTesrt Moda;

  onNext( go ){

    var index_end;

    if( go == 'previous'){

      index_end = this.state.nextPostion - 5;
    }else if( go == 'next' ){

      index_end = this.state.nextPostion + 5;
    }
     
    console.log("index_end", index_end );  
  
    var GVar = GlobalVariables.getApiUrl();

    AsyncStorage.multiGet(["TOKEN", "student_id", "moduleId"]).then(response => {

            var TOKEN                          = response[0][1];
            var student_id                     = response[1][1];
            var module_id                      = response[2][1];
            

            this.setState({nextPostion : index_end});

            console.log(module_id,"module_idnew")

            var TOKEN                          = response[0][1];
            var student_id                     = response[1][1];
       
            let formdata = new FormData();

            formdata.append('student_id',       student_id );
            formdata.append('module_id',        module_id );
            formdata.append('go',               go );
            formdata.append('index_end',        index_end );
            
            console.log(formdata,"formdata");
            console.log(TOKEN,"TOKEN");

            fetch( GVar.BaseUrl + GVar.StartPrelimTest,{
                      method: 'POST',
                      headers:{
                                  'token' : TOKEN
                              },
                      body: formdata,       
                      }).then((response) => response.text())
                     .then((responseJson) => {

                      var x = JSON.parse(responseJson.replace("-", ""));
                      this.setState({isModalLoading : false });
                      

                      if( x.status == 1 ){

                          console.log(x, "questions");
                          this.setState({questions : x.questions });

                      }else{

                      }
                      
                     }).catch((error)=>{ Alert.alert(error)});

    }); 
  }



  //part of StartTestModal
 reviewQuestions( item ){

    console.log("ReviewAnswer", item);
    var ischecked = false;
    var color = '';

    

    if( item.review_later == 1 ){

        this.ischecked = true;
        color = 'violet';
    }else if( item.answer_id ){

      this.ischecked = true;
      color = 'green';
    }else if( item.answer_id && item.review_later == 1 ){

      this.ischecked = true;
      color = 'violet';
    }else{

      this.ischecked = false;
      color = '';
    }


    console.log("colorReview", color);

  
    return  <View style={{flex:1, flexDirection:'row',justifyContent:'center'}}>
                  <View style={{}}>
                    <CheckBox 
                      center
                      checkedColor = {color}
                      size = {hp('2.5%')}
                      containerStyle={{backgroundColor:'transparent', borderColor:'transparent'}}
                      textStyle ={{fontSize: RF(2.5),fontWeight: 'bold',color:'#FFFFFF'}}
                      checked={item.answer_id || item.review_later == 1 ? true : false } 
                      //onPress={ }
                      onPress={() => {
                                    this.setState({plusModalVisible : false });
                                    this._onClickReview( item, ischecked);
                                  }}
                      />
                  </View>

                  <View style={{marginTop:'20%'}}>
                      <Text style={{flex:1,marginTop:'4.5%',marginLeft:-22, marginTop:'5%', color:'#000000'}}>{item.question_ordering}</Text>
                  </View>
            </View>;
  }



  //All Part of Start Test modal

  /*showSingleOptionAnswers( item ){

    var questions = this.state.questions;


    return  <FlatList
                extraData={this.state}
                data={item}
                renderItem={({item, index}) => 
                    <View style={{flex:1, flexDirection:'row',marginLeft:wp('2%'),}}>
                            <CheckBox
                              checkedIcon='dot-circle-o'
                              uncheckedIcon='circle-o'
                              checked={ item.answer_id == questions[index].answer_id ? true : false}
                            />
                          <Text style={{flex:1,marginTop:'4.5%',marginLeft:-22}}>{questions[index].answer_id}</Text>
                          <Text style={{flex:1,marginTop:'4.5%',marginLeft:-22}}>{item.answer_id}</Text>
                         <Text style={{flex:1,marginTop:'4.5%',marginLeft:-22}}>{index + 1 }.  {item.answer}</Text>
                    </View>
              }/>; 
  }*/

  _onClickReview( item, ischecked ){
    //this.setModalVisible(!this.state.modalVisible);
    console.log("onclickReviewButton",item)
    console.log("onclickReviewButton",ischecked)
    console.log("nextPostion",this.state.nextPostion)

    var index = item.question_ordering;
    var pos   = this.state.pos;
    
    var index_end = pos[item.question_ordering -1 ].indexend - 5 ;
    console.log("nextPostion",index_end)
    var go        = 'next';

    var GVar = GlobalVariables.getApiUrl();

    AsyncStorage.multiGet(["TOKEN", "student_id", "moduleId"]).then(response => {

            var TOKEN                          = response[0][1];
            var student_id                     = response[1][1];
            var module_id                      = response[2][1];
            

            this.setState({nextPostion : index_end});

            this.setState({
                            TOKEN          : TOKEN,    
                            student_id     : student_id,
                      });

            //AsyncStorage.removeItem('moduleId')

            console.log(module_id,"module_idnew")

            var TOKEN                          = response[0][1];
            var student_id                     = response[1][1];
           
            this.setState({
                            TOKEN          : TOKEN,    
                            student_id     : student_id,
                            module_id      : module_id,
                            isModalLoading      : true, 
                      });


            let formdata = new FormData();

            formdata.append('student_id',       student_id );
            formdata.append('module_id',        module_id );
            formdata.append('go',               go );
            formdata.append('index_end',        index_end );
            
            console.log(formdata,"formdata");
            console.log(TOKEN,"TOKEN");

            fetch( GVar.BaseUrl + GVar.StartPrelimTest ,{
                      method: 'POST',
                      headers:{
                                  'token' : TOKEN
                              },
                      body: formdata,       
                      }).then((response) => response.text())
                     .then((responseJson) => {

                      var x = JSON.parse(responseJson.replace("-", ""));
                      this.setState({isModalLoading : false });
                      

                      if( x.status == 1 ){

                          console.log(x, "questions");
                          this.setState({questions : x.questions });

                      }else{

                      }
                      
                     }).catch((error)=>{ Alert.alert(error)});

    }); 

  }

  _reviewLater( item ){

    console.log("xyz", item );
    console.log("questions", this.state.questions );

    
    var GVar = GlobalVariables.getApiUrl();
    

    //this.setState({ questions})

    AsyncStorage.multiGet(["TOKEN", "student_id"]).then(response => {

            var TOKEN                          = response[0][1];
            var student_id                     = response[1][1];
            
           
            this.setState({
                            isLoading      : true, 
                      });

            var review;
            if( item.review_later == 0 ){

              review = '1';
            }else if( item.review_later == 1 ){

              review = '0';
            }

            var questions  = this.state.questions;

            var index = this.getIndex(item.exam_id, questions, 'exam_id');

            questions[index].review_later = review;
            this.setState({ questions : questions });

            let formdata = new FormData();

            formdata.append('student_id',       student_id );
            formdata.append('exam_id',          item.exam_id );
            formdata.append('review',           review );
           
            
            console.log(formdata,"formdata");
            console.log(TOKEN,"TOKEN");

            fetch( GVar.BaseUrl + GVar.UpdateReview,{
                      method: 'POST',
                      headers:{
                                  'token' : TOKEN
                              },
                      body: formdata,       
                      }).then((response) => response.text())
                     .then((responseJson) => {

                      var x = JSON.parse(responseJson.replace("-", ""));
                      this.setState({isLoading : false });
                      

                      if( x.status == 1 ){

                          console.log(x, "reviewResponce");
                          //this.setState({questions : x.questions });

                      }else{

                      }
                      
                     }).catch((error)=>{ Alert.alert(error)});

    }); 

    AsyncStorage.multiGet(["TOKEN", "student_id", "moduleId"]).then(response => {

            var TOKEN                          = response[0][1];
            var student_id                     = response[1][1];
            var module_id                      = response[2][1];

            this.setState({
                            TOKEN          : TOKEN,    
                            student_id     : student_id,
                      });

            //AsyncStorage.removeItem('moduleId')

            console.log(module_id,"module_idnew")

            

            var TOKEN                          = response[0][1];
            var student_id                     = response[1][1];
           
            this.setState({
                            TOKEN          : TOKEN,    
                            student_id     : student_id,
                            module_id      : module_id,
                            isLoading      : true, 
                      });


            let formdata = new FormData();

            formdata.append('student_id',        student_id );
            formdata.append('module_id',         module_id );
            
            console.log(formdata,"formdata");
            console.log(TOKEN,"TOKEN");

            fetch(  GVar.BaseUrl + GVar.StartPrelimReviews,{
                      method: 'POST',
                      headers:{
                                  'token' : TOKEN
                              },
                      body: formdata,       
                      }).then((response) => response.text())
                     .then((responseJson) => {

                      var x = JSON.parse(responseJson.replace("-", ""));
                      this.setState({isLoading : false });
                      

                      if( x.status == 1 ){

                          console.log("reviews",x);
                          this.setState({review : x.review });

                      }else{

                      }
                      
                     }).catch((error)=>{ Alert.alert(error)});
      
    }); 
  }


  getIndex(value, arr, prop) {
    for(var i = 0; i < arr.length; i++) {
        if(arr[i][prop] === value) {
            return i;
        }
    }
    return -1; //to handle the case where the value doesn't exist
  }


  _OnClickAnswer( item, exam_id){

    console.log("answerITEM",item);
    console.log("exam_id",exam_id);

    var questions  = this.state.questions;

    var index = this.getIndex(exam_id, questions, 'exam_id');

    this.setState({ questions : questions });
    console.log("questions on click",questions[index]);
    console.log("answer type on click Answer",questions[index].answer_id);


    var Passanswer_id;
    if(questions[index].answer_type == 'single'){

     
      console.log("in if");
      if( questions[index].answer_id == item.answer_id ){
  
        questions[index].answer_id = '';
        Passanswer_id = '';
      }else{
        
        questions[index].answer_id = item.answer_id;
        Passanswer_id = item.answer_id;
      }
    }else{

      if( questions[index].answer_id.split(',').contains(item.answer_id)){

        questions[index].answer_id = '';
        Passanswer_id = '';
      }else{
        questions[index].answer_id = item.answer_id;
        Passanswer_id = item.answer_id;
      }

    }

    var GVar = GlobalVariables.getApiUrl();

    AsyncStorage.multiGet(["TOKEN", "student_id"]).then(response => {

            var TOKEN                          = response[0][1];
            var student_id                     = response[1][1];
            
           
            this.setState({
                            isLoading      : true, 
                      });
           
       
            let formdata = new FormData();

            formdata.append('student_id',       student_id );
            formdata.append('exam_id',          exam_id );
            formdata.append('question_id',      item.question_id );
            formdata.append('answer_id',        Passanswer_id);
           
            
            console.log(formdata,"on click answer formdata");
            console.log(TOKEN,"TOKEN");

            fetch( GVar.BaseUrl + GVar.UpdatePrelimAnswers,{
                      method: 'POST',
                      headers:{
                                  'token' : TOKEN
                              },
                      body: formdata,       
                      }).then((response) => response.text())
                     .then((responseJson) => {

                      var x = JSON.parse(responseJson.replace("-", ""));
                      this.setState({isLoading : false });
                      

                      if( x.status == 1 ){

                          console.log(x, "AnswerResponce");
                          //this.setState({questions : x.questions });

                      }else{

                      }
                      
                     }).catch((error)=>{ Alert.alert(error)});

    });

    AsyncStorage.multiGet(["TOKEN", "student_id", "moduleId"]).then(response => {

            var TOKEN                          = response[0][1];
            var student_id                     = response[1][1];
            var module_id                      = response[2][1];

            this.setState({
                            TOKEN          : TOKEN,    
                            student_id     : student_id,
                      });

            //AsyncStorage.removeItem('moduleId')

            console.log(module_id,"module_idnew")

            

            var TOKEN                          = response[0][1];
            var student_id                     = response[1][1];

            let formdata = new FormData();

            formdata.append('student_id',        student_id );
            formdata.append('module_id',         module_id );
            
            console.log(formdata,"formdata");
            console.log(TOKEN,"TOKEN");

            fetch(  GVar.BaseUrl + GVar.StartPrelimReviews,{
                      method: 'POST',
                      headers:{
                                  'token' : TOKEN
                              },
                      body: formdata,       
                      }).then((response) => response.text())
                     .then((responseJson) => {

                      var x = JSON.parse(responseJson.replace("-", ""));
                      this.setState({isLoading : false });
                      

                      if( x.status == 1 ){

                          console.log("reviews",x.review);
                          this.setState({review : x.review });

                      }else{

                      }
                      
                     }).catch((error)=>{ Alert.alert(error)});
      
    }); 
  }

  changingStatusOfStartTest(){

    var GVar = GlobalVariables.getApiUrl();

    AsyncStorage.multiGet(["TOKEN", "student_id", "moduleId"]).then(response => {

      var TOKEN                          = response[0][1];
      var student_id                     = response[1][1];
      var module_id                      = response[2][1];
      

      //this.setState({nextPostion : index_end});

      console.log(module_id,"module_idnew")

      var TOKEN                          = response[0][1];
      var student_id                     = response[1][1];
 
      let formdata = new FormData();

      formdata.append('student_id',       student_id );
      formdata.append('module_id',        module_id );
      formdata.append('go',               'submit' );
      
      console.log(formdata,"formdata");
      console.log(TOKEN,"TOKEN");

      fetch( GVar.BaseUrl + GVar.StartPrelimTest,{
                method: 'POST',
                headers:{
                            'token' : TOKEN
                        },
                body: formdata,       
                }).then((response) => response.text())
               .then((responseJson) => {

                var x = JSON.parse(responseJson.replace("-", ""));
                this.setState({isModalLoading : false });
                

                if( x.status == 1 ){

                    console.log(x, "submit response");
                    //this.setState({questions : x.questions });

                }else{

                }
                
               }).catch((error)=>{ Alert.alert(error)});

}); 

 
    AsyncStorage.multiGet(["TOKEN", "student_id", "moduleId"]).then(response => {

            var TOKEN                          = response[0][1];
            var student_id                     = response[1][1];
            var module_id                      = response[2][1];



            let formdata = new FormData();

            formdata.append('student_id',        student_id );

     

            fetch( GVar.BaseUrl + GVar.ModuleContent + module_id ,{
                      method: 'POST',
                      headers:{
                                  'token' : TOKEN
                              },
                      body: formdata,       
                      }).then((response) => response.text())
                     .then((responseJson) => {

                      var x = JSON.parse(responseJson.replace("-", ""));
                      this.setState({isLoading : false });
                      

                      if( x.status == 1 ){

                          console.log(x, "prelimscore1233");
                          this.setState({prelimscore : x.prelimscore });
                          this.setState({moduleName : x.module.moduleName });
                          this.setState({prelimExam : x.prelimExam });
                          this.setState({moduleType : x.module.moduleType });

                      }
                      
                     }).catch((error)=>{ Alert.alert(error)});


    });
  }

  setAlphabeticalIndex(index){
    console.log("Ascii code index",String.fromCharCode(65+index));
    return String.fromCharCode(65+index);
  }


  render() {

    const styleStart = '<div style="color:#150F68;font-weight: 700;">';
    const styleEnd   = '</div>';   
    return (

      <View style={{flex: 1,widthwp:wp('100%')}}>

            <ScrollView>
              <View style={{ alignItems:'center', alignself:'center'}}>

                {this.state.prelimExam ?
                  <View style={{flex: 1, height:hp('100%'),width:wp('98%'), justifyContent:'space-between'}}>
                    
                    <View style={{height:hp('8%'), backgroundColor: '#1ab394', marginTop:'2%', justifyContent:'center', alignItems:'center'}}>
                      <Text  style={{fontSize: RF(4),color:'#FFFFFF', alignself:'center' }}>Your Score</Text>
                    </View>

                    <View style={{height:hp('50%'), backgroundColor: '#1f2f46'}} >

                          <View style={{flex: 4,flexDirection: 'row',justifyContent: 'center',marginTop:hp('2%'),  height: hp('80%'), marginBottom:hp('4%')}} >

                                <View style={{width: wp('50%'),justifyContent:'space-between'}} >

                                    <View>
                                            <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>Total Questions</Text>
                                    </View>

                                    <View>
                                            <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>Correct Answers</Text>
                                    </View>

                                    <View>
                                            <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>Wrong Answers</Text>
                                    </View>
                                    
                                    <View>
                                            <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>UnAttempted Questions</Text>
                                    </View>

                                    <View>
                                            <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }} >Total Marks</Text>
                                    </View>

                                    <View>
                                            <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>Your Score</Text>
                                    </View>

                                    <View>
                                            <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>Grade</Text>
                                    </View>

                                  

                                </View>

                                <View style={{width: wp('20%'),justifyContent:'space-between' }} >

                                    <View>
                                            <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>{this.state.prelimscore.total_questions}</Text>
                                    </View>

                                    <View>
                                            <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>{this.state.prelimscore.correct_answer_count}</Text>
                                    </View>

                                    <View>
                                            <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>{this.state.prelimscore.wrong_answer_count}</Text>
                                    </View>

                                    <View>
                                            <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>{this.state.prelimscore.unattempted_question}</Text>
                                    </View>
                                    <View>
                                            <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>{this.state.prelimscore.total_marks}</Text>
                                    </View>

                                    <View>
                                            <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>{this.state.prelimscore.your_score}</Text>
                                    </View>

                                    <View>
                                            <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>{this.state.prelimscore.grade}</Text>
                                    </View>

                                    
                                </View>

                          </View>

                          <View style={{flex: 2,flexDirection: 'row',justifyContent: 'center',  height: hp('20%')}} >
                                <View style= {{backgroundColor:'#FFFFFF', width:wp('80%'), marginBottom: hp('1%'), justifyContent:'center'}}>
                                    <Text style={{color:'#e16800', fontSize: RF(2.5), alignself:'center',marginLeft:wp('2%') }}>Remark: {this.state.prelimscore.remarks}</Text>
                                </View>
                          </View>
                    </View>      
        
                    <View style={{height:hp('40%'), backgroundColor: '#FFFFFF',}} >
                          <TouchableOpacity onPress={this.onPDFPressed.bind(this, this.state.prelimscore.download_solution)}>
                              <View style={{height:hp('8%'),flexDirection: 'row', backgroundColor:'#1ab394', width:wp('80%'), marginLeft:wp('9%'), justifyContent:'center', borderRadius: wp('2%'), alignItems:'center', marginTop:hp('2%')}}>
                                  <Text style={{color:'#FFFFFF',fontSize: RF(4), alignself:'center'}}>Download Solution</Text>
                              </View>
                          </TouchableOpacity>
                    </View>

                  </View> 

                  :


                  this.state.moduleType == null ? 


                    <View style={{flex: 1, height:hp('60%'),width:wp('95%'), justifyContent:'space-between',backgroundColor: '#FFFFFF',marginTop:'2%',}}>
                    
                      <View style={{ height:hp('5%'), backgroundColor: '#3f4c6b', marginTop:'2%', justifyContent:'center', alignItems:'center'}}>
                        <Text  style={{fontSize: RF(3),color:'#FFFFFF', alignself:'center', marginLeft:wp('2%') }}>Instructions</Text>
                      </View>

                      <View style={{  backgroundColor: '#FFFFFF', }}>
                        <Text  style={{fontSize: RF(2.5),color:'#000000', marginLeft:wp('2%') }}>{'\u2022'} All questions are compulsory.</Text>
                        <Text  style={{fontSize: RF(2.5),color:'#000000', marginLeft:wp('2%') }}>{'\u2022'} Number of questions: 100 Questions</Text>
                        <Text  style={{fontSize: RF(2.5),color:'#000000', marginLeft:wp('2%') }}>{'\u2022'} Time allotted: 120 minutes </Text>
                        <Text  style={{fontSize: RF(2.5),color:'#000000', marginLeft:wp('2%') }}>{'\u2022'} Correct Answer: 2 marks / Question</Text>
                        <Text  style={{fontSize: RF(2.5),color:'#000000', marginLeft:wp('2%') }}>{'\u2022'} Wrong Answer: - 0.67 marks / Question</Text>
                        <Text  style={{fontSize: RF(2.5),color:'#000000', marginLeft:wp('2%') }}>{'\u2022'} No Answer: 0 marks / Question</Text>
                        <Text  style={{fontSize: RF(2.5),color:'#000000', marginLeft:wp('2%') }}>{'\u2022'} Please do not refresh the page while you are taking Test</Text>
                        <Text  style={{fontSize: RF(2.5),color:'#ff0000', marginLeft:wp('2%') }}>{'\u2022'} Please note you can attempt the test only one time</Text>
                        
                      </View>
                      <TouchableOpacity style={{ height:hp('5%'),width:wp('30%'), backgroundColor: '#1ab394', marginTop:'2%', justifyContent:'center', alignItems:'center', marginLeft:'31.55%', marginBottom:'10%'}} 
                      onPress={() => {
                        
                        this.insertPrelimQuestions();
                        //this.setSecondModalVisible(true);
                        //this.getPrelimQuestion();
                        //this.getReviews();
                      }}>
                      <View >
                          <Text  style={{fontSize: RF(3),color:'#FFFFFF', alignself:'center',  }}>Start Test</Text>
                      </View>
                      </TouchableOpacity>
                  </View>  
                  :

                  <View style={{flex: 1, height:hp('60%'),width:wp('95%'), justifyContent:'space-between',backgroundColor: '#FFFFFF',marginTop:'2%',}}>
                    
                      <View style={{ height:hp('5%'), backgroundColor: '#3f4c6b', marginTop:'2%', justifyContent:'center', alignItems:'center'}}>
                        <Text  style={{fontSize: RF(3),color:'#FFFFFF', alignself:'center', marginLeft:wp('2%') }}>Instructions</Text>
                      </View>

                      <View style={{  backgroundColor: '#FFFFFF', }}>
                        <Text  style={{fontSize: RF(2.5),color:'#000000', marginLeft:wp('2%') }}>{'\u2022'} All questions are compulsory.</Text>
                        <Text  style={{fontSize: RF(2.5),color:'#000000', marginLeft:wp('2%') }}>{'\u2022'} Number of questions: 80 Questions</Text>
                        <Text  style={{fontSize: RF(2.5),color:'#000000', marginLeft:wp('2%') }}>{'\u2022'} Time allotted: 120 minutes </Text>
                        <Text  style={{fontSize: RF(2.5),color:'#000000', marginLeft:wp('2%') }}>{'\u2022'} Correct Answer: 2.5 marks / Question</Text>
                        <Text  style={{fontSize: RF(2.5),color:'#000000', marginLeft:wp('2%') }}>{'\u2022'} Wrong Answer: - 0.83 marks / Question</Text>
                        <Text  style={{fontSize: RF(2.5),color:'#000000', marginLeft:wp('2%') }}>{'\u2022'} No Answer: 0 marks / Question</Text>
                        <Text  style={{fontSize: RF(2.5),color:'#000000', marginLeft:wp('2%') }}>{'\u2022'} Please do not refresh the page while you are taking Test</Text>
                        <Text  style={{fontSize: RF(2.5),color:'#ff0000', marginLeft:wp('2%') }}>{'\u2022'} Please note you can attempt the test only one time</Text>
                        
                      </View>

                      <TouchableOpacity style={{ height:hp('5%'),width:wp('30%'), backgroundColor: '#1ab394', marginTop:'2%', justifyContent:'center', alignItems:'center', marginLeft:'31.55%', marginBottom:'10%'}} 
                      onPress={() => {
                        //this.setSecondModalVisible(true);
                        this.insertPrelimQuestions();
                      }}>
                      <View >
                          <Text  style={{fontSize: RF(3),color:'#FFFFFF', alignself:'center',  }}>Start Test</Text>
                      </View>
                      </TouchableOpacity>
                  </View> 

                                          
                }


              </View>    
            </ScrollView>
            {this.state.isLoading ? <ActivityIndicator
                    animating     ={true}
                    transparent   ={true}
                    visible       ={false}
                    style         ={styles.indicator}
                    size          ="large"
                  /> : <View/>  } 
            <Quotes/>      

            <Modal
              animationType="slide"
              transparent={false}
              visible={this.state.visiblemodal}
              onRequestClose={() => {
                Alert.alert(
                  'Submit Exam',
                  'Do You Really want to Submit Your Exam!',
                  [
                    {text: 'Cancel', onPress: () => {}},
                    {text: 'OK', onPress: () => { this.setSecondModalVisible(false);
                      this.changingStatusOfStartTest();}},
                  ],
                  { cancelable: false }
                );
              }}>

            <View style={{flex: 1, width:wp('100%')}}>

              <View style={{flex: .5, flexDirection:'row', marginTop:'2%'}}>
                    <View style={{flex: 5,flexDirection:'row', justifyContent:'center'}} >
                          <CountDown
                            digitBgColor  = '#1bb496'
                            digitTxtColor = '#FFFFFF'
                            timeTxtColor  = '#000000'
                            until={this.state.totalDuration}
                            //duration of countdown in seconds
                            timetoShow={('H', 'M', 'S')}
                            //formate to show
                            onFinish={() => {this.onFinishTimer();}}
                            //on Finish call
                            //onPress={() => alert('hello')}
                            //on Press call
                            size={20}
                          />
                    </View>
                    
                    <View style={{flex: 1}} >
                          <TouchableOpacity
                            onPress={() => {
                              this.setPlusModalVisible(!this.state.plusModalVisible);
                            }}>

                            <View style={{
                                  borderWidth:1,
                                  borderColor:'rgba(0,0,0,0.2)',
                                  alignItems:'center',
                                  justifyContent:'center',
                                  width:50,
                                  position: 'relative',                                          
                                  bottom: 0,                                                    
                                  right: 10,
                                  height:50,
                                  backgroundColor:'#fff',
                                  borderRadius:100,
                                }}
                              >    
                                 <Text textAlign={'center'} style={{fontSize: RF(4), fontWeight:'bold', alignself:'center', color:'#1ab395'}}>RL</Text>

                            
                            </View>
                          </TouchableOpacity>
                    </View>
              </View>
              <View style={{flex: 5.5,marginTop:'5%'}}>
                <ScrollView>
                <View style={{ alignItems:'center', alignself:'center'}}>
                      <FlatList
                        extraData={this.state}
                        data={this.state.questions}
                        renderItem={({item, index}) => 

                        <View style={{flex:1, flexDirection:'column',width:wp('95%'), justifyContent:'center', backgroundColor:'#FFFFFF',  borderWidth:2, borderColor:'#CCCCCC'}}>
                            

                            <View style={{flex:1, flexDirection:'column',marginLeft:wp('2%'), marginTop:hp('2%'),}}>
                                <Text style={{marginTop:'3.5%', color:'#150F68'}}>Q. {this.state.nextPostion + index + 1}. </Text>
                                <HTML html={styleStart + item.question + styleEnd}  imagesMaxWidth={Dimensions.get('window').width} />
                            </View>
                          
                                
                            { item.answer_type == 'single' ? 

                                <FlatList
                                  extraData={this.state}
                                  data={item.answers}
                                  renderItem={({item : answers, index}) => 
                              
                                  <View key={index} style={{flex:1, flexDirection:'row',marginLeft:wp('2%'),}}>
                                            <CheckBox
                                                checkedIcon='dot-circle-o'
                                                uncheckedIcon='circle-o'
                                                checked={ answers.answer_id == item.answer_id ? true : false}
                                                onPress={() => this._OnClickAnswer(answers, item.exam_id)}
                                            />
                                          <Text style={{flex:1,marginTop:'4.5%',marginLeft:-22}}>{this.setAlphabeticalIndex(index) }.  {answers.answer}</Text>
                                  </View>
                                }/>  

                                :

                              <View/>
                            } 


                            <View style={{flex:1, flexDirection:'row',marginLeft:wp('50%')}}>
                                      <CheckBox
                                        center
                                        checkedColor = '#7f00ff'
                                        containerStyle={{backgroundColor:'transparent', borderColor:'transparent'}}
                                        textStyle ={{fontSize: RF(2.5),fontWeight: 'bold',color:'#FFFFFF'}}
                                        size = {hp('2.5%')}
                                        checked={item.review_later == '1' ? true : false } 
                                        onPress={this._reviewLater.bind(this, item) }/>

                                  <Text style={{flex:1,marginTop:'9%',marginLeft:-22, color:"#7f00ff", fontWeight:'bold'}}>Review Later</Text>
                              </View>    
                                

                        </View>
                      }
                      />
                </View>
                </ScrollView>
              </View>
              <View style={{flex: .2, flexDirection:'row', marginTop:'2%', alignItems:'center', justifyContent:'space-between'}}>
                    
                    { this.state.nextPostion > '4' ?
                      <View style={{flex: 2,}} >
                          <TouchableOpacity
                            onPress={() => {
                              this.onNext("previous");
                            }}>

                            <View style={{marginLeft:5,alignItems:'center',  backgroundColor:'#1ab395', height:hp('5%')}}>    
                              <Text textAlign={'center'} style={{fontSize: RF(2.5), fontWeight:'bold', alignself:'center', color:'#FFFFFF'}}>PREVIOUS</Text>
                            </View>
                          </TouchableOpacity>
                      </View>
                      :
                      <View/>
                    }
                    
                    { (this.state.nextPostion < this.state.totalQuestionCount && this.state.nextPostion + 5 != this.state.totalQuestionCount) ?
                  
                      <View style={{flex: 2, }} >
                            <TouchableOpacity
                              onPress={() => {
                                this.onNext("next");
                              }}>

                              <View style={{marginLeft:5,marginRight:5, alignItems:'center', backgroundColor:'#1ab395', height:hp('5%')}}>
                                <Text textAlign={'center'} style={{fontSize: RF(2.5), fontWeight:'bold', alignself:'center',color:'#FFFFFF'}}>NEXT</Text>
                              </View>
                            </TouchableOpacity>
                      </View>
                    :
                      <View/>
                    }

                    { this.state.nextPostion ==  this.state.totalQuestionCount - 5  ?
                  
                      <View style={{flex: 2, }} >
                            <TouchableOpacity
                              onPress={() => {
                              
                                this.setSecondModalVisible(false);
                                this.changingStatusOfStartTest();
                              }}>

                              <View style={{marginLeft:5,marginRight:5, alignItems:'center', backgroundColor:'#1ab395', height:hp('5%')}}>
                                <Text textAlign={'center'} style={{fontSize: RF(2.5), fontWeight:'bold', alignself:'center',color:'#FFFFFF'}}>SUBMIT</Text>
                              </View>
                            </TouchableOpacity>
                      </View>
                    :
                      <View/>
                    }
              </View>
            <Modal
              animationType="slide"
              transparent={false}
              visible={this.state.plusModalVisible}
              onRequestClose={() => {
                this.setPlusModalVisible(false);
              }}>
              <View style={{ height:hp('5%'), backgroundColor: '#1ab395', marginTop:'1%',marginLeft:'5%',marginRight:'5%', justifyContent:'center', alignItems:'center'}}>
                        <Text  style={{fontSize: RF(3),color:'#FFFFFF', alignself:'center', marginLeft:wp('2%') }}>Review Later</Text>
                      </View>
                        <View style={{flex: 1,alignSelf: 'flex-end', position: 'absolute',marginTop:'12%',backgroundColor:'#FFFFFF'}} >
                                <TouchableOpacity
                                  onPress={() => {
                                    this.setPlusModalVisible(false);
                                  }}>

                                  <View style={{
                                         borderWidth:1,
                                         borderColor:'rgba(0,0,0,0.2)',
                                         alignItems:'center',
                                         justifyContent:'center',
                                         width:50,
                                         position: 'relative',                                          
                                         bottom: 0,                                                    
                                         right: 10,
                                         height:50,
                                         backgroundColor:'#fff',
                                         borderRadius:100,
                                       }}
                                    >    
                                    <Icon name="times-circle"  size={30} color="#01a699" />
                                  
                                  </View>
                                </TouchableOpacity>
                          </View>

              <View style={{marginTop:hp('20%'), marginBottom:hp('.5%'), backgroundColor:'#FFFFFF'}}>
                <View style={{marginBottom:hp('10%')}}>
                  <ScrollView>
                  
                  <FlatList
                      extraData={this.state}
                      data={this.state.review}
                      renderItem={({item, index}) => 
                          <View style={{flex:1, flexDirection:'row',marginLeft:wp('1%'),}}>
                            {this.reviewQuestions(item)}
                          </View>
                  }
                  numColumns={5}
                  />
                  </ScrollView>
                </View>
              </View>
            </Modal>

            {this.state.isModalLoading ? <ActivityIndicator
                    animating     ={true}
                    transparent   ={true}
                    visible       ={false}
                    style         ={styles.indicator}
                    size          ="large"
                  /> : <View/>  } 

            </View>

          
        </Modal>






      </View>

      );
      }
 }

export default PrelimQuiz = StackNavigator(
{
 
  PrelimQuiz : {
    screen: PrelimQuiz,
  },
  ViewPDF: {
    screen: ViewPDF,
  },
});



