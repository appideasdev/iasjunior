/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  StatusBar,
  ImageBackground,
  ScrollView,
  WebView,
  AsyncStorage,
  Dimensions,
  Alert,
  ListView,
  FlatList,
  ActivityIndicator,
  PermissionsAndroid,
  TouchableOpacity
} from 'react-native';

import GlobalVariables from './../GlobalVariables/GlobalVariables.js';
import HTML from 'react-native-render-html';
import Pdf from 'react-native-pdf';
import RNFetchBlob from 'rn-fetch-blob';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import RF from "react-native-responsive-fontsize";
import styles from './../Styles/GlobalStyle.js';
import { HeaderBackButton } from 'react-navigation';


import Quotes from "../Quotes.js";

type Props = {};
export default class Result extends Component<Props> {

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Result',
      headerStyle: { backgroundColor:'#36404a' },
      headerTitleStyle: { color: '#FFFFFF' },
      headerTintColor: 'white',
      headerLeft:(<HeaderBackButton tintColor='#FFFFFF' onPress={()=>{navigation.navigate('First');}}/>)
  
  
    };
  };


  constructor(props) {
    super(props);



    this.state = { 
                    query       : null,
                    TOKEN       : null,
                    student_id  : null,
                    quotesSlug  : null,
                    practicetest_score     : [],
                    isLoading   : true,
                    moduleName  : null,
                    modules     : [],
                    mockprelim  : [],
                    mockmain    : [], 
                    showMockPrelimResult:false,
                    showMockMainResult:false,
                    showModules:false,
         };
  }


  componentDidMount = async () => {

    const { navigation } = this.props;
    const module_id = navigation.getParam('id');


    var GVar = GlobalVariables.getApiUrl();

    
    AsyncStorage.multiGet(["TOKEN", "student_id"]).then(response => {

            var TOKEN                          = response[0][1];
            var student_id                     = response[1][1];
           
            this.setState({
                            TOKEN          : TOKEN,    
                            student_id     : student_id,
                      });


            let formdata = new FormData();

            formdata.append('student_id',        student_id );

            

            fetch( GVar.BaseUrl + GVar.Results ,{
                      method: 'POST',
                      headers:{
                                  'token' : TOKEN
                              },
                      body: formdata,       
                      }).then((response) => response.text())
                     .then((responseJson) => {

                      

                      var x = JSON.parse(responseJson.replace("-", ""));
                      this.setState({status : x.status });
                      this.setState({isLoading : false });
                      
                      if( x.status == 1 ){

                        console.log("results showModules ",x.result.showModules);
                        console.log("results ",x.result.modules.main);
                          this.setState({modules : x.result.modules });
                          this.setState({mockmain : x.result.mockmain });
                          this.setState({mockprelim : x.result.mockprelim });

                          var showModulesResult = x.result.showModules;
                          if(showModulesResult == 'true'){
                            console.log("in if");
                            this.setState({showModules:true});
                          }else{
                            this.setState({showModules:false});
                            console.log("in eolse");
                          }

                          var showPrelimResult1 = this.state.mockprelim;
                          if(showPrelimResult1[Object.keys(showPrelimResult1)[0]].showresult == 'true'){
                            this.setState({showMockPrelimResult:true})
                          }

                          var showMockMainResult1 = this.state.mockmain;
                          if(showMockMainResult1[Object.keys(showMockMainResult1)[0]].showresult == 'true'){
                            this.setState({showMockMainResult:true})
                          }
                         
                                                    
                          
                      }else{
                        this.setState({message : x.message });

                      }
                      
                     }).catch((error)=>{ Alert.alert(error)});

      });  

     
  }

  componentWillReceiveProps(nextProps) {
      if (nextProps.navigation.state.params.isRefresh) {
          

    const { navigation } = this.props;
    const module_id = navigation.getParam('id');


    var GVar = GlobalVariables.getApiUrl();

    
    AsyncStorage.multiGet(["TOKEN", "student_id"]).then(response => {

            var TOKEN                          = response[0][1];
            var student_id                     = response[1][1];
           
            this.setState({
                            TOKEN          : TOKEN,    
                            student_id     : student_id,
                      });


            let formdata = new FormData();

            formdata.append('student_id',        student_id );

            

            fetch( GVar.BaseUrl + GVar.Results ,{
                      method: 'POST',
                      headers:{
                                  'token' : TOKEN
                              },
                      body: formdata,       
                      }).then((response) => response.text())
                     .then((responseJson) => {

                      

                      var x = JSON.parse(responseJson.replace("-", ""));
                      this.setState({status : x.status });
                      this.setState({isLoading : false });
                      
                      if( x.status == 1 ){

                          this.setState({modules : x.modules });
                          
                      }else{
                        this.setState({message : x.message });

                      }
                      
                     }).catch((error)=>{ Alert.alert(error)});

      });  

    //this.onPDFPressed();
      }
  }    

  showMockMains(){

    var mockMain = this.state.mockmain;
    var count = Object.keys(mockMain).length;

    var view = [];
    for(var i=0;i<Object.keys(mockMain).length;i++){
       var x = Object.keys(mockMain)[i];
       if(mockMain[x].showresult == 'true'){

        view.push(

          <View style={{width:wp('95%'),alignSelf:'center'}}>
           
            <View style={{flexDirection: 'row',marginTop:hp('0%'),width:wp('95%'),alignSelf:'center'}} >

              <View style={{ alignItems:'center', alignself:'center',backgroundColor:'white',width:wp('50%'),height:wp('7%')}}>
                <Text style={{fontSize: RF(3),color:'#000000', }}>Test {i+1}</Text>
              </View>

              <View style={{ alignItems:'center', backgroundColor:'#FFFFFF',alignSelf:'center',width:wp('50%'),height:wp('7%')}}>
                <Text style={{fontSize: RF(3),color:'#000000', }}>{mockMain[x].obtained_marks}/{mockMain[x].total_marks}</Text>
              </View>

            </View>

          </View>
          
         );

       }
    }

   
    return <View style={{ alignItems:'center'}}>
              {this.state.showMockMainResult   ?
                  <View style={{marginTop:hp('2%'),width:wp('95%')}}>
                    <View style={{ height:hp('6%'),width:wp('95%'), backgroundColor: '#3f4c6b', marginTop:'4%', justifyContent:'center', alignItems:'center'}}>
                       <Text  style={{fontSize: RF(3.5),color:'#FFFFFF', alignself:'center', marginLeft:wp('4%') }}>Full Syllabus Mains Result</Text>
                    </View>
                    {view}
                  </View>
              :
              <View/>
              }
              
            </View>;
  }

  showMockprelim(){


    var mockprelim = this.state.mockprelim;
    var count = Object.keys(mockprelim).length;

    console.log(count,"mockprelimTestResult 11"); 
    var view = [];
   


    for(var i=0; i < Object.keys(mockprelim).length; i++) {
      var x = Object.keys(mockprelim)[i];

      if( mockprelim[x].showresult == 'true' ){
         
        
        view.push(

          <View style={{width:wp('95%'),alignSelf:'center',alignItems:'center',marginLeft:'2%',marginRight:'2%',justifyContent:'center'}} >
           
            <View style={{flexDirection: 'row',marginTop:hp('0%'),width:wp('95%'),alignSelf:'center',marginLeft:'2%',marginRight:'2%',justifyContent:'center'}} >

              <View style={{ alignItems:'center', alignself:'center',justifyContent:'center',marginLeft:'2%',backgroundColor:'white',width:wp('50%'),height:wp('7%')}}>
                <Text style={{fontSize: RF(3),color:'#000000', }}>Test {i+1}</Text>
              </View>

              <View style={{ alignItems:'center', alignself:'center',justifyContent:'center',marginRight:'2%',backgroundColor:'#FFFFFF',width:wp('50%'),height:wp('7%')}}>
                <Text style={{fontSize: RF(3),color:'#000000', }}>{mockprelim[x].positive_marks}/{mockprelim[x].total_marks}</Text>
              </View>

            </View>

          </View>
          




         );
      }
    }

    return <View style={{ alignItems:'center'}}>
              {this.state.showMockPrelimResult   ?
                  <View style={{marginTop:hp('2%'),width:wp('95%')}}>
                    <View style={{ height:hp('6%'),width:wp('95%'), backgroundColor: '#3f4c6b',  justifyContent:'center', alignItems:'center'}}>
                       <Text  style={{fontSize: RF(3.5),color:'#FFFFFF', alignself:'center', marginLeft:wp('4%') }}>Full Syllabus Prelim Result</Text>
                    </View>
                    {view}
                  </View>
              :
              <View/>
              }
              
            </View>;
  
  }

  render() {

  
    return (
      <View style={{flex: 1,widthwp:wp('100%')}}>

            <ScrollView>

              <View style={{ alignItems:'center', alignself:'center',}}>

             { this.state.status ? 
            
              <View>

            {this.showMockprelim()}
            {this.showMockMains()}

              {this.state.showModules ?
                <View style={{width:wp('95%'),marginLeft:wp('2%'), marginRight:wp('2%')}}>
                  <View style={{ height:hp('5%'),width:wp('95%'), backgroundColor: '#3f4c6b', marginLeft:wp('2%'), marginRight:wp('2%'),marginTop:wp('2%'), justifyContent:'center', alignItems:'center',alignSelf:'center'}}>
                    <Text  style={{fontSize: RF(3.5),color:'#FFFFFF', alignself:'center',}}>Modules Result</Text>
                  </View>

                    <FlatList
                    extraData={this.state}
                    data={this.state.modules}
                    renderItem={({item, index}) => 
                    <View >
                      {item.showresult == 'true'  ? 
                      <View>
                        <View style={{ height:hp('6%'),width:wp('95%'),marginTop:'2%', backgroundColor: '#1ab394',  justifyContent:'center', alignItems:'center'}}>
                          <Text  style={{fontSize: RF(3.5),color:'#FFFFFF', alignself:'center', marginLeft:wp('4%') }}>{item.moduleName}</Text>
                        </View>
                        <View style={{flexDirection: 'row'}} >
                          
                            <View style={{ alignItems:'center', alignself:'center',backgroundColor:'white',width:wp('50%'),height:wp('7%')}}>
                              <Text style={{fontSize: RF(3),color:'#000000', }}>Prelim</Text>
                            </View>

                            <View style={{ alignItems:'center', alignself:'center',backgroundColor:'#FFFFFF',width:wp('50%'),height:wp('7%'),marginRight:'4%'}}>
                              <Text style={{fontSize: RF(3),color:'#000000', }}>{item.prelim.positive_marks}/{item.prelim.total_marks}</Text>
                            </View>

                        </View>
                        <View style={{flexDirection: 'row'}} >
                          
                            <View style={{ alignItems:'center', alignself:'center',backgroundColor:'white',width:wp('50%'),height:wp('7%')}}>
                              <Text style={{fontSize: RF(3),color:'#000000', }}>Mains</Text>
                            </View>

                            <View style={{ alignItems:'center', alignself:'center',backgroundColor:'#FFFFFF',width:wp('50%'),height:wp('7%'),marginRight:'4%'}}>
                              <Text style={{fontSize: RF(3),color:'#000000', }}>{item.main.obtained_marks}/{item.main.total_marks}</Text>
                            </View>

                        </View>
                      </View>  

                      :
                      <View/>

                      
                      
                      }

                    </View>    

                    }
                    />

            </View>  
              
                :
                <View/>
              }

         

            
             

              </View>
              :
                <View style={{ alignItems:'center', alignself:'center',}}>
                  <View style={{flex:1,height:hp('70%'), flexDirection:'column', backgroundColor:'#1ab394', width:wp('95%'), alignself:'center', marginTop:hp('5%')}}>
                      <Text style={styles.error}>{this.state.message}</Text>
                  </View>
                </View>
            }

          
            </View>

            </ScrollView>
            {this.state.isLoading ? <ActivityIndicator
                    animating     ={true}
                    transparent   ={true}
                    visible       ={false}
                    style         ={styles.indicator}
                    size          ="large"
                  /> : <View/>  } 
            <Quotes/>          
      </View>
    );
  }
}
