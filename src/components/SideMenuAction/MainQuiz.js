/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  StatusBar,
  ImageBackground,
  ScrollView,
  WebView,
  AsyncStorage,
  Dimensions,
  Alert,
  ListView,
  FlatList,
  ActivityIndicator,
  PermissionsAndroid,
  TouchableOpacity,
  Image,
  Modal,
  TextInput
} from 'react-native';
import Quotes from "../Quotes.js";
import { HeaderBackButton } from 'react-navigation';
import {
  DocumentPicker,
  DocumentPickerUtil,
} from 'react-native-document-picker';

import ImagePicker from 'react-native-image-picker';
import GlobalVariables from './../GlobalVariables/GlobalVariables.js';
import HTML from 'react-native-render-html';
import Pdf from 'react-native-pdf';
import RNFetchBlob from 'rn-fetch-blob';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import RF from "react-native-responsive-fontsize";
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import {StackNavigator} from 'react-navigation'; 
import RNHTMLtoPDF from 'react-native-html-to-pdf';
import styles from './../Styles/GlobalStyle.js';

import ModuleSubContent from './ModuleSubContent.js';



import Icon from 'react-native-vector-icons/FontAwesome';
import { CheckBox } from 'react-native-elements'

import CountDown from 'react-native-countdown-component';
import moment from 'moment';

const ActivityProject = StackNavigator({
    ModuleSubContent: {screen: ModuleSubContent},
});



import ViewPDF from "./ViewPDF.js";
import GlobalStyle from './../Styles/GlobalStyle.js';

class MainQuiz extends Component<Props> {



  static navigationOptions = {
    header: null,
  }

  

  constructor(props) {
    super(props);

    this.state = { 
                    query       : null,
                    TOKEN       : null,
                    student_id  : null,
                    quotesSlug  : null,
                    mainscore : [],
                    isLoading   : true,
                    moduleName  : null,
                    totalDuration: '',
                    modalVisible: false,
                    TestmodalVisible: false,
                    nextPostion : 0,
                    startTimer:false,
                    totalQuestionCount:0,
                    timeCompleted:true,
                    hideReviewFunc:false,
                    fileUploadedMessage:'',
                    textInputs  : [],
                    onFocusMargin:'0%',
                    pos         : [
                                    {"id":"1", "indexstart":"1", "indexend":"5" },
                                    {"id":"2", "indexstart":"1", "indexend":"5" },
                                    {"id":"3", "indexstart":"1", "indexend":"5" },
                                    {"id":"4", "indexstart":"1", "indexend":"5" },
                                    {"id":"5", "indexstart":"1", "indexend":"5" },

                                    {"id":"6", "indexstart":"6", "indexend":"10" },
                                    {"id":"7", "indexstart":"6", "indexend":"10" },
                                    {"id":"8", "indexstart":"6", "indexend":"10" },
                                    {"id":"9", "indexstart":"6", "indexend":"10" },
                                    {"id":"10", "indexstart":"6", "indexend":"10" },

                                    {"id":"11", "indexstart":"11", "indexend":"15" },
                                    {"id":"12", "indexstart":"11", "indexend":"15" },
                                    {"id":"13", "indexstart":"11", "indexend":"15" },
                                    {"id":"14", "indexstart":"11", "indexend":"15" },
                                    {"id":"15", "indexstart":"11", "indexend":"15" },

                                    {"id":"16", "indexstart":"16", "indexend":"20" },
                                    {"id":"17", "indexstart":"16", "indexend":"20" },
                                    {"id":"18", "indexstart":"16", "indexend":"20" },
                                    {"id":"19", "indexstart":"16", "indexend":"20" },
                                    {"id":"20", "indexstart":"16", "indexend":"20" },

                                    {"id":"21", "indexstart":"21", "indexend":"25" },
                                    {"id":"22", "indexstart":"21", "indexend":"25" },
                                    {"id":"23", "indexstart":"21", "indexend":"25" },
                                    {"id":"24", "indexstart":"21", "indexend":"25" },
                                    {"id":"25", "indexstart":"21", "indexend":"25" },
                                  ]
         };
  }


  componentDidMount = async () => {

    var GVar = GlobalVariables.getApiUrl();

    AsyncStorage.multiGet(["TOKEN", "student_id", "moduleId"]).then(response => {

            var TOKEN                          = response[0][1];
            var student_id                     = response[1][1];
            var module_id                       = response[2][1];

            this.setState({
                            TOKEN          : TOKEN,    
                            student_id     : student_id,
                      });

            //AsyncStorage.removeItem('moduleId')

            console.log(module_id,"module_idnew")

            if( student_id != null ){

                  var TOKEN                          = response[0][1];
                  var student_id                     = response[1][1];
                 
                  this.setState({
                                  TOKEN          : TOKEN,    
                                  student_id     : student_id,
                                  module_id      : module_id,
                                  isLoading      : true, 
                            });


                  let formdata = new FormData();

                  formdata.append('student_id',        student_id );

                  console.log(formdata,"formdata");
                  console.log(TOKEN,"TOKEN");

                  fetch( GVar.BaseUrl + GVar.ModuleContent + module_id ,{
                            method: 'POST',
                            headers:{
                                        'token' : TOKEN
                                    },
                            body: formdata,       
                            }).then((response) => response.text())
                           .then((responseJson) => {

                            var x = JSON.parse(responseJson.replace("-", ""));
                            this.setState({isLoading : false });
                            

                            if( x.status == 1 ){

                                console.log(x, "mainscore");
                                this.setState({mainscore  : x.mainscore });
                                this.setState({moduleName : x.moduleName });
                                this.setState({mainsTest  : x.mainsTest });
                                this.setState({message    : x.message });
                                
                            }
                            
                           }).catch((error)=>{ Alert.alert(error)});
            }



      });  

      this.setExamTimer();

  }

  textOnFocus(){
    if(Platform.OS == 'ios'){
      this.setState({onFocusMargin:'40%'})
    }
   
  }
  
  textOnBlur(){
    if(Platform.OS == 'ios'){
      this.setState({onFocusMargin:'0%'})
    }
    
  }


  onSubmitChange(){

    var GVar = GlobalVariables.getApiUrl();
    
    AsyncStorage.multiGet(["TOKEN", "student_id", "moduleId"]).then(response => {

      var TOKEN                          = response[0][1];
      var student_id                     = response[1][1];
      var module_id                      = response[2][1];
      

//this.setState({nextPostion : index_end});

      this.setState({
                      TOKEN          : TOKEN,    
                      student_id     : student_id,
                });

      //AsyncStorage.removeItem('moduleId')

      console.log(module_id,"module_idnew")

      var TOKEN                          = response[0][1];
      var student_id                     = response[1][1];
     
      this.setState({
                      TOKEN          : TOKEN,    
                      student_id     : student_id,
                      module_id      : module_id,
                      isLoading      : true, 
                });


      let formdata = new FormData();

      formdata.append('student_id',       student_id );
      formdata.append('module_id',        module_id );
      formdata.append('go',               'submit' );
     
      
      console.log(formdata,"formdata");
      console.log(TOKEN,"TOKEN");

      fetch( GVar.BaseUrl + GVar.StartMainsTest,{
                method: 'POST',
                headers:{
                            'token' : TOKEN
                        },
                body: formdata,       
                }).then((response) => response.text())
               .then((responseJson) => {

                var x = JSON.parse(responseJson.replace("-", ""));
                this.setState({isLoading : false });
                

                if( x.status == 1 ){

                    console.log(x, "questions");
                    //this.setState({questions : x.questions });

                }else{

                }
                
               }).catch((error)=>{ Alert.alert(error)});

}); 



    AsyncStorage.multiGet(["TOKEN", "student_id", "moduleId"]).then(response => {

            var TOKEN                          = response[0][1];
            var student_id                     = response[1][1];
            var module_id                       = response[2][1];

            this.setState({
                            TOKEN          : TOKEN,    
                            student_id     : student_id,
                      });

            //AsyncStorage.removeItem('moduleId')

            console.log(module_id,"module_idnew")

            if( student_id != null ){

                  var TOKEN                          = response[0][1];
                  var student_id                     = response[1][1];
                 
                  this.setState({
                                  TOKEN          : TOKEN,    
                                  student_id     : student_id,
                                  module_id      : module_id,
                                  isLoading      : true, 
                            });


                  let formdata = new FormData();

                  formdata.append('student_id',        student_id );

                  console.log(formdata,"formdata");
                  console.log(TOKEN,"TOKEN");

                  fetch( GVar.BaseUrl + GVar.ModuleContent + module_id ,{
                            method: 'POST',
                            headers:{
                                        'token' : TOKEN
                                    },
                            body: formdata,       
                            }).then((response) => response.text())
                           .then((responseJson) => {

                            var x = JSON.parse(responseJson.replace("-", ""));
                            this.setState({isLoading : false });
                            

                            if( x.status == 1 ){

                                console.log(x.mainscore, "submit mainscore");
                                this.setState({mainscore  : x.mainscore });
                                this.setState({moduleName : x.moduleName });
                                this.setState({mainsTest  : x.mainsTest });
                                this.setState({message    : x.message });
                            }
                            
                           }).catch((error)=>{ Alert.alert(error)});
            }



      });  

  }

  onPDFPressed(item){   

      this.setState({isLoading : true});

      var GVar = GlobalVariables.getApiUrl();

     
      var module_id = this.state.module_id;

      //alert(module_id)

      var TOKEN = this.state.TOKEN;

      let formdata = new FormData();

      formdata.append('student_id',        this.state.student_id );

      console.log("responseJsonresponseJson",formdata)
      console.log("TOKEN",TOKEN)
     
      fetch( GVar.DownloadMainsPdf + module_id ,{
                      method: 'POST',
                      headers:{
                                  'token' : TOKEN
                              },
                      body: formdata,       
                      }).then((response) => response.text())
                     .then((responseJson) => {

                      var x = JSON.parse(responseJson.replace("-", ""));
                      this.setState({isLoading : false });

                      console.log("responseJsonresponseJson",x.html);

                      html = x.html;
                     
                      var options = {
                            html: html, 
                            fileName: 'main-quiz-'+ module_id,          
                            directory: 'docs',         
                            base64: true , 
                            height: 800,                
                            width: 1056,              
                      };

                      
                          RNHTMLtoPDF.convert(options).then((data) => {

                        if( Platform.OS == 'ios' ){
                           
                            RNFetchBlob.ios.openDocument(data.filePath);

                          }else{
                           
                            this.props.navigation.navigate('ViewPDF',{source:data.filePath })
                            Alert.alert('Successfully downloaded in Documents' );
                          }
                        // this.props.navigation.navigate('ViewPDF',{source:data.filePath })
                        // Alert.alert('Successfully downloaded in Downloads' );
                      });
                          
                      
                      
                     }).catch((error)=>{ Alert.alert(error)});

             
  }

  insertMainQuestions(){

    var GVar = GlobalVariables.getApiUrl();

    AsyncStorage.multiGet(["TOKEN", "student_id", "moduleId"]).then(response => {

            var TOKEN                          = response[0][1];
            var student_id                     = response[1][1];
            var module_id                      = response[2][1];


            console.log("insert mains question")
            let formdata = new FormData();
            formdata.append("student_id",student_id);
            formdata.append("module_id",module_id);

            console.log("my form data",formdata);

          
            fetch( GVar.BaseUrl + GVar.InsertMainsQuestions,{
                method: 'POST',
                headers:{
                            'token' : TOKEN
                        },
                body: formdata,       
                }).then((response) => response.text())
               .then((responseJson) => {

               
                var x = JSON.parse(responseJson.replace("-", ""));
                if(x.status == 0){
                  Alert.alert("Questions are not prepared.. Please try later!");
              
                }else{ 
                  this.onStartMainsTest();
                  this.getMainReviews();
                }
                //this.setState({isLoading : false });
                console.log("maibs questions",x);

                
            
           }).catch((error)=>{ Alert.alert(error)});
    });          
  
}


  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  setTestModalVisible(visible) {
    this.setState({TestmodalVisible: visible});
  }

  onStartMainsTest(){

      
        var GVar = GlobalVariables.getApiUrl();

        AsyncStorage.multiGet(["TOKEN", "student_id", "moduleId"]).then(response => {

                var TOKEN                          = response[0][1];
                var student_id                     = response[1][1];
                var module_id                      = response[2][1];

                this.setState({
                                TOKEN          : TOKEN,    
                                student_id     : student_id,
                          });

                //AsyncStorage.removeItem('moduleId')

                console.log(module_id,"module_idnew")

                

                var TOKEN                          = response[0][1];
                var student_id                     = response[1][1];
               
                this.setState({
                                TOKEN          : TOKEN,    
                                student_id     : student_id,
                                module_id      : module_id,
                                isLoading      : true, 
                          });


                let formdata = new FormData();

                formdata.append('student_id',        student_id );
                formdata.append('module_id',        module_id );
                
                console.log(formdata,"formdata");
                console.log(TOKEN,"TOKEN");

                fetch( GVar.BaseUrl + GVar.StartMainsTest,{
                          method: 'POST',
                          headers:{
                                      'token' : TOKEN
                                  },
                          body: formdata,       
                          }).then((response) => response.text())
                         .then((responseJson) => {

                          var x = JSON.parse(responseJson.replace("-", ""));
                          this.setState({isLoading : false });
                          

                          if( x.status == 1 ){
                            this.setTestModalVisible(true);
                              console.log(x, "questions");
                              this.setState({startTimer:true})
                              this.setExamTimer();
                              this.setState({questions : x.questions });

                          }else{

                          }
                          
                         }).catch((error)=>{ Alert.alert(error)});
          
        }); 


  }

  setExamTimer(){

    var that = this;
       
   
    //converting in seconds
     if(that.state.startTimer){

      that.setState({ totalDuration: 5400 });
    }else{
      that.setState({totalDuration:30})
    }
    
    //Settign up the duration of countdown in seconds to re-render
  }

  getMainReviews(){

    var GVar = GlobalVariables.getApiUrl();


        AsyncStorage.multiGet(["TOKEN", "student_id", "moduleId"]).then(response => {

          var TOKEN                          = response[0][1];
          var student_id                     = response[1][1];
          var module_id                      = response[2][1];

          this.setState({
                          TOKEN          : TOKEN,    
                          student_id     : student_id,
                    });

          //AsyncStorage.removeItem('moduleId')

          console.log(module_id,"module_idnew")

          

          var TOKEN                          = response[0][1];
          var student_id                     = response[1][1];
        
          this.setState({
                          TOKEN          : TOKEN,    
                          student_id     : student_id,
                          module_id      : module_id,
                          isLoading      : true, 
                    });


          let formdata = new FormData();

          formdata.append('student_id',        student_id );
          formdata.append('module_id',        module_id );
          
          console.log(formdata,"formdata");
          console.log(TOKEN,"TOKEN");

          fetch( GVar.BaseUrl + GVar.StartMainsReviews,{
                    method: 'POST',
                    headers:{
                                'token' : TOKEN
                            },
                    body: formdata,       
                    }).then((response) => response.text())
                  .then((responseJson) => {

                    var x = JSON.parse(responseJson.replace("-", ""));
                    this.setState({isLoading : false });
                    

                    if( x.status == 1 ){

                        console.log("reviews",x);
                        this.setState({review : x.review });
                        console.log("total review length",x.review.length);
                        this.setState({totalQuestionCount:x.review.length});

                    }else{

                    }
                    
                  }).catch((error)=>{ Alert.alert(error)});

    }); 
  }


  onNext( go ){
    //update answers text
    var GVar = GlobalVariables.getApiUrl();

    AsyncStorage.multiGet(["TOKEN", "student_id", "moduleId"]).then(response => {

            var TOKEN                          = response[0][1];
            var student_id                     = response[1][1];
            var module_id                      = response[2][1];

            var answers = this.state.textInputs;
            var questions = this.state.questions;

            for (var i = 4 ; i >= 0; i--) {

              var answer_text = answers[i];

              if( answer_text === undefined ){
                annswer_text = ' ';
              }

              var question = questions[i];

              let formdata = new FormData();

              formdata.append('student_id',       student_id );
              formdata.append('exam_id',          question.exam_id );
              formdata.append('question_id',      question.question_id );
              formdata.append('answer_text',      answer_text );
              
              console.log(formdata,"formdata");
              console.log(TOKEN,"TOKEN");
              console.log(questions,"questions");

              fetch( GVar.BaseUrl + GVar.UpdateMainsAnswers,{
                        method: 'POST',
                        headers:{
                                    'token' : TOKEN
                                },
                        body: formdata,       
                        }).then((response) => response.text())
                       .then((responseJson) => {

                        var x = JSON.parse(responseJson.replace("-", ""));
                        this.setState({isLoading : false });
                        

                        if( x.status == 1 ){

                            console.log(x, "AnswerTEXTResponce");
                           
                        }else{

                        }
                        
                       }).catch((error)=>{ Alert.alert(error)});


              this.setState({ textInputs : [] });         
              
            }

    });  


    var index_end;

    if( go == 'previous'){

      index_end = this.state.nextPostion - 5;
    }else if( go == 'next' ){

      index_end = this.state.nextPostion + 5;
    }
     
    console.log("index_end", index_end );  
  

    AsyncStorage.multiGet(["TOKEN", "student_id", "moduleId"]).then(response => {

            var TOKEN                          = response[0][1];
            var student_id                     = response[1][1];
            var module_id                      = response[2][1];
            

            this.setState({nextPostion : index_end});

            this.setState({
                            TOKEN          : TOKEN,    
                            student_id     : student_id,
                      });

            //AsyncStorage.removeItem('moduleId')

            console.log(module_id,"module_idnew")

            var TOKEN                          = response[0][1];
            var student_id                     = response[1][1];
           
            this.setState({
                            TOKEN          : TOKEN,    
                            student_id     : student_id,
                            module_id      : module_id,
                            isLoading      : true, 
                      });


            let formdata = new FormData();

            formdata.append('student_id',       student_id );
            formdata.append('module_id',        module_id );
            formdata.append('go',               go );
            //formdata.append('index_start',      index_start );
            formdata.append('index_end',        index_end );
            
            console.log(formdata,"formdata");
            console.log(TOKEN,"TOKEN");

            fetch( GVar.BaseUrl + GVar.StartMainsTest,{
                      method: 'POST',
                      headers:{
                                  'token' : TOKEN
                              },
                      body: formdata,       
                      }).then((response) => response.text())
                     .then((responseJson) => {

                      var x = JSON.parse(responseJson.replace("-", ""));
                      this.setState({isLoading : false });
                      

                      if( x.status == 1 ){

                          console.log(x, "questions");
                          this.setState({questions : x.questions });

                      }else{

                      }
                      
                     }).catch((error)=>{ Alert.alert(error)});

    }); 


          
  }

  reviewQuestions( item ){

    console.log("ReviewAnswer", item);
    var ischecked = false;
    var color = '';

    if( item.answer_id ){

      this.ischecked = true;
      color = '#008000';
    }

    if( item.review_later == 1 ){

        this.ischecked = true;
        color = '#800080';
    }else if( item.review_later == 0 ){

        this.ischecked = false;
    }

    

  
    return  <View style={{flex:1, flexDirection:'row',justifyContent:'center'}}>
                  <View style={{}}>
                    <CheckBox 
                      center
                      checkedColor = {color}
                      size = {hp('2.5%')}
                      containerStyle={{backgroundColor:'transparent', borderColor:'transparent'}}
                      textStyle ={{fontSize: RF(2.5),fontWeight: 'bold',color:'#FFFFFF'}}
                      checked={this.ischecked} 
                      //onPress={ }
                      onPress={() => {
                                    this.setModalVisible(!this.state.modalVisible);
                                    this._onClickReview( item, ischecked)
                                  }}
                      />
                  </View>

                  <View style={{marginTop:'20%'}}>
                      <Text style={{flex:1,marginTop:'4.5%',marginLeft:-22, marginTop:'5%', color:'#000000'}}>{item.question_ordering}</Text>
                  </View>
            </View>;
  }


  showSingleOptionAnswers( item ){

    var questions = this.state.questions;


    return  <FlatList
                extraData={this.state}
                data={item}
                renderItem={({item, index}) => 
                    <View style={{flex:1, flexDirection:'row',marginLeft:wp('2%'),}}>
                            <CheckBox
                              checkedIcon='dot-circle-o'
                              uncheckedIcon='circle-o'
                              checked={ item.answer_id == questions[index].answer_id ? true : false}
                            />
                          <Text style={{flex:1,marginTop:'4.5%',marginLeft:-22}}>{questions[index].answer_id}</Text>
                          <Text style={{flex:1,marginTop:'4.5%',marginLeft:-22}}>{item.answer_id}</Text>
                         <Text style={{flex:1,marginTop:'4.5%',marginLeft:-22}}>{index + 1 }.  {item.answer}</Text>
                    </View>
              }/>; 
  }

  _onClickReview( item, ischecked ){
    //this.setModalVisible(!this.state.modalVisible);
    console.log("onclickReviewButton",item)
    console.log("onclickReviewButton",ischecked)

    var index = item.question_ordering;
    var pos   = this.state.pos;
    
    var index_end = pos[item.question_ordering -1 ].indexend - 5 ;
    var go        = 'next';

    var GVar = GlobalVariables.getApiUrl();

    AsyncStorage.multiGet(["TOKEN", "student_id", "moduleId"]).then(response => {

            var TOKEN                          = response[0][1];
            var student_id                     = response[1][1];
            var module_id                      = response[2][1];
            

            this.setState({nextPostion : index_end});

            this.setState({
                            TOKEN          : TOKEN,    
                            student_id     : student_id,
                            module_id      : module_id,
                            isLoading      : true, 
                      });


            let formdata = new FormData();

            formdata.append('student_id',       student_id );
            formdata.append('module_id',        module_id );
            formdata.append('go',               go );
            formdata.append('index_end',        index_end );
            
            console.log(formdata,"formdata");
            console.log(TOKEN,"TOKEN");

            fetch( GVar.BaseUrl + GVar.StartMainsTest ,{
                      method: 'POST',
                      headers:{
                                  'token' : TOKEN
                              },
                      body: formdata,       
                      }).then((response) => response.text())
                     .then((responseJson) => {

                      var x = JSON.parse(responseJson.replace("-", ""));
                      this.setState({isLoading : false });
                      

                      if( x.status == 1 ){

                          console.log(x, "questions");
                          this.setState({questions : x.questions });

                      }else{

                      }
                      
                     }).catch((error)=>{ Alert.alert(error)});

    }); 

  }

  _reviewLater( item ){

    console.log("xyz", item );
    console.log("questions", this.state.questions );

    
    var GVar = GlobalVariables.getApiUrl();
    

    //this.setState({ questions})
    AsyncStorage.multiGet(["TOKEN", "student_id"]).then(response => {

            var TOKEN                          = response[0][1];
            var student_id                     = response[1][1];
            
           
            this.setState({
                            isLoading      : true, 
                      });

            var review;
            if( item.review_later == 0 ){

              review = '1';
            }else if( item.review_later == 1 ){

              review = '0';
            }

            var questions  = this.state.questions;

            var index = this.getIndex(item.exam_id, questions, 'exam_id');

            questions[index].review_later = review;
            this.setState({ questions : questions });

            let formdata = new FormData();

            formdata.append('student_id',       student_id );
            formdata.append('exam_id',          item.exam_id );
            formdata.append('review',           review );
           
            
            console.log(formdata,"formdata");
            console.log(TOKEN,"TOKEN");

            fetch( GVar.BaseUrl + GVar.UpdateReview,{
                      method: 'POST',
                      headers:{
                                  'token' : TOKEN
                              },
                      body: formdata,       
                      }).then((response) => response.text())
                     .then((responseJson) => {

                      var x = JSON.parse(responseJson.replace("-", ""));
                      this.setState({isLoading : false });
                      

                      if( x.status == 1 ){

                          console.log(x, "reviewResponce");
                          //this.setState({questions : x.questions });

                      }else{

                      }
                      
                     }).catch((error)=>{ Alert.alert(error)});

    });
    
    this.getMainReviews()

   
  }




  _OnClickAnswer( item, exam_id){

    console.log("answerITEM",item);
    console.log("exam_id",exam_id);

    var questions  = this.state.questions;

    var index = this.getIndex(exam_id, questions, 'exam_id');

    questions[index].answer_id = item.answer_id;

    this.setState({ questions : questions });

    var GVar = GlobalVariables.getApiUrl();

    AsyncStorage.multiGet(["TOKEN", "student_id"]).then(response => {

            var TOKEN                          = response[0][1];
            var student_id                     = response[1][1];
            
           
            this.setState({
                            isLoading      : true, 
                      });
           
       
            let formdata = new FormData();

            formdata.append('student_id',       student_id );
            formdata.append('exam_id',          exam_id );
            formdata.append('question_id',      item.question_id );
            formdata.append('answer_id',        item.answer_id);
           
            
            console.log(formdata,"formdata");
            console.log(TOKEN,"TOKEN");

            fetch( GVar.BaseUrl + GVar.UpdateMainsAnswers,{
                      method: 'POST',
                      headers:{
                                  'token' : TOKEN
                              },
                      body: formdata,       
                      }).then((response) => response.text())
                     .then((responseJson) => {

                      var x = JSON.parse(responseJson.replace("-", ""));
                      this.setState({isLoading : false });
                      

                      if( x.status == 1 ){

                          console.log(x, "AnswerResponce");
                          //this.setState({questions : x.questions });

                      }else{

                      }
                      
                     }).catch((error)=>{ Alert.alert(error)});

    });

    this.getMainReviews();

   
  }

  getIndex(value, arr, prop) {
    console.log("get Index caLLED ")
    for(var i = 0; i < arr.length; i++) {
        if(arr[i][prop] === value) {
            return i;
        }
    }
    return -1; //to handle the case where the value doesn't exist
  }
 
handleChange(item) {
  var GVar = GlobalVariables.getApiUrl();
  //Opening Document Picker
  console.log("show document picker");
  DocumentPicker.show(
    {
      filetype: [DocumentPickerUtil.allFiles()],
      //All type of Files DocumentPickerUtil.allFiles()
      //Only PDF DocumentPickerUtil.pdf()
      //Audio DocumentPickerUtil.audio()
      //Plain Text DocumentPickerUtil.plainText()
    },
    (error, res) => {
      console.log("onn handlechange",res);
      if(res !=null){

      if(res.type == 'application/pdf'|| res.type == 'image/jpeg'|| res.type == 'image/jpg' || res.type == 'image/png'){

        this.setState({ fileUri: res.uri });
        this.setState({ fileType: res.type });
        this.setState({ fileName: res.fileName });
        this.setState({ fileSize: res.fileSize });

        console.log('res : ' + JSON.stringify(res));
        console.log('URI : ' + res.uri);
        console.log('Type : ' + res.type);
        console.log('File Name : ' + res.fileName);
        console.log('File Size : ' + res.fileSize);

        let formdata = new FormData();

        formdata.append('student_id',  this.state.student_id );
        formdata.append('exam_id',        item.exam_id );
        formdata.append('question_id',    item.question_id );
        formdata.append('answerfile', {
          uri: res.uri,
          type: res.type, // or photo.type
          name: res.fileName
        });

        

        this.setState({isLoading:true});
        console.log(formdata,"formdata");
        console.log(this.state.TOKEN,"token");

        fetch( GVar.BaseUrl +GVar.UpdateMainsFileSpecificAnswers,{
                      method: 'POST',
                      headers:{
                                  'token' : this.state.TOKEN
                              },
                      body : formdata,        
                      }).then((response) => response.text())
                     .then((responseJson) => {

                      
                      console.log(responseJson,'on handle responseJson2');
                      var x = JSON.parse(responseJson.replace(/-\s?/g, ""));
                      console.log(x,'on handle responseJson2');
                      this.setState({isLoading:false});
                      if(x.status == 1){

                        this.setState({fileUploadedMessage:x.message+ " Uploaded Successfully!"})
                        Alert.alert("Your"+x.message+"Uploaded Successfully!");
                      }
                     
                     
                      
        }).catch((error)=>{ Alert.alert(error)});

        var questions  = this.state.questions;
        var index = this.getIndex(item.exam_id, questions, 'exam_id');
        console.log("get Index i=",index);
        console.log("ques index upload",questions[index].answer_upload);

        if(questions[index].answer_upload == ''){

          questions[index].answer_upload = "Answer Uploaded Successfully";
        }




      }else{
        Alert.alert("UnSupported File Format.Only Images(.jpeg,.jpg,.png) and PDF are allowed.");
      }
      
      }

    
    }
  );
}

_uploadAllAnswerFile(){


  console.log("show All Answerdocument picker exam_id");
  DocumentPicker.show(
    {
      filetype: [DocumentPickerUtil.allFiles()],
      //All type of Files DocumentPickerUtil.allFiles()
      //Only PDF DocumentPickerUtil.pdf()
      //Audio DocumentPickerUtil.audio()
      //Plain Text DocumentPickerUtil.plainText()
    },
    (error, res) => {
      console.log("onn upload file answer",res);
      if(res !=null){

      if(res.type == 'application/pdf'|| res.type == 'image/jpeg'|| res.type == 'image/jpg' || res.type == 'image/png'){

        this.setState({ fileUri: res.uri });
        this.setState({ fileType: res.type });
        this.setState({ fileName: res.fileName });
        this.setState({ fileSize: res.fileSize });

        console.log('res : ' + JSON.stringify(res));
        console.log('URI : ' + res.uri);
        console.log('Type : ' + res.type);
        console.log('File Name : ' + res.fileName);
        console.log('File Size : ' + res.fileSize);

        Alert.alert(
          'Submit Exam',
          'Are You Sure that you want to Submit Exam ?',
          [
            {text: 'Cancel', onPress: () => {this.onCancelSubmission();console.log('Cancel Pressed')}, style: 'cancel'},
            {text: 'OK', onPress: () => {this.onFileSubmission(res.uri,res.type,res.fileName)}},
          ],
          { cancelable: false }
        );


      
      }else{
        Alert.alert("UnSupported File Format.Only Images(.jpeg,.jpg,.png) and PDF are allowed.");
      }
      
      }

    
    });

}

onFileSubmission(uri,type,filename){

  console.log("on File Submission uri",uri);
  console.log("on File Submission type",type);
  console.log("on File Submission filenam,e",filename);

  AsyncStorage.multiGet(["TOKEN", "student_id", "moduleId"]).then(response =>{

    var TOKEN                          = response[0][1];
    var student_id                     = response[1][1];
    var module_id                       = response[2][1];

    this.setState({
                    TOKEN          : TOKEN,    
                    student_id     : student_id,
                    module_id        :module_id,
                    isLoading      : true
              });

    //AsyncStorage.removeItem('moduleId')

    console.log(module_id,"module_idnew");

    let formdata = new FormData();

        formdata.append('student_id', student_id );
        formdata.append('module_id',  module_id );
        formdata.append('answer_type','mains-test' );
        formdata.append('answerfile', {
          uri: uri,
          type: type, // or photo.type
          name: filename
        });

        var GV = GlobalVariables.getApiUrl();

        console.log(formdata,"formdata");
        console.log(this.state.TOKEN,"token");

        fetch( GV.BaseUrl + GV.UpdateMainsSingleFileAnswers,{
                      method: 'POST',
                      headers:{
                                  'token' : this.state.TOKEN
                              },
                      body : formdata,        
                      }).then((response) => response.text())
                     .then((responseJson) => {

                      
                      console.log(responseJson,'on handle responseJson2');
                      var x = JSON.parse(responseJson.replace(/-\s?/g, ""));
                      console.log(x,'on handle responseJson2');
                      this.setState({isLoading:false});
                      Alert.alert(
                        'Submit Exam',
                        'Your Exam Submitted Successfully!',
                        [
                          {text: 'OK', onPress: () => {this.setTestModalVisible(false)
                                                      this.onSubmitChange()}},
                        ],
                        { cancelable: false }
                      );
                     
                      
        }).catch((error)=>{ Alert.alert(error)});

  });
}

onFinishTimer(){

  console.log("onFinish timer");
  if(this.state.timeCompleted){
    Alert.alert("Exam Submitted");
    this.setTestModalVisible(false);
    this.onSubmitChange();
  }

}

onCancelSubmission(){

  this.setState({ fileUri:''});
        this.setState({ fileType: ''});
        this.setState({ fileName:'' });
        this.setState({ fileSize: ''});
}


  render() {

    const styleStart = '<div style="color:#150F68;font-weight: 700;">';
    const styleEnd   = '</div>';  
    return (

      <View style={{flex: 1,width:wp('100%')}}>

            <ScrollView>
              <View style={{ alignItems:'center', alignself:'center'}}>

                { this.state.mainsTest == 2 ? 

                    <View style={{flex: 1, height:hp('100%'),width:wp('98%'), justifyContent:'space-between'}}>
                    
                    <View style={{height:hp('8%'), backgroundColor: '#1ab394', marginTop:'2%', justifyContent:'center', alignItems:'center'}}>
                      <Text  style={{fontSize: RF(4),color:'#FFFFFF', alignself:'center' }}>Your Score</Text>
                    </View>

                    <View style={{height:hp('70%'), backgroundColor: '#1f2f46'}} >

                          <View style={{flex: 4,flexDirection: 'row',justifyContent: 'center',marginTop:hp('2%'),  height: hp('80%'),marginBottom:hp('4%')}} >

                                <View style={{width: wp('50%'),justifyContent:'space-between'}} >

                                    <View>
                                            <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>Total Questions</Text>
                                    </View>

                                    <View>
                                            <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>UnAttempted</Text>
                                    </View>
                                    <View>
                                            <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>Attempted</Text>
                                    </View>

                                    <View>
                                            <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }} >Total Marks</Text>
                                    </View>

                                    <View>
                                            <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>Your Score</Text>
                                    </View>

                                    <View>
                                            <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>Grade</Text>
                                    </View>
                        
                                </View>

                                <View style={{width: wp('20%'),justifyContent:'space-between' }} >

                                    <View>
                                            <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>{this.state.mainscore.total_questions}</Text>
                                    </View>

                                    <View>
                                            <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>{this.state.mainscore.unattempted_question}</Text>
                                    </View>
                                    <View>
                                            <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>{this.state.mainscore.attempted_question}</Text>
                                    </View>

                                    <View>
                                            <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>{this.state.mainscore.total_marks}</Text>
                                    </View>

                                    <View>
                                            <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>{this.state.mainscore.your_score}</Text>
                                    </View>

                                    <View>
                                            <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>{this.state.mainscore.grade}</Text>
                                    </View>
                                   

                                    
                                </View>

                          </View>

                          

                        
                         

                          <View style={{flex: 2,flexDirection: 'row',justifyContent: 'center',  height: hp('20%')}} >
                                <View style= {{backgroundColor:'#FFFFFF', width:wp('80%'), marginBottom: hp('1%'), justifyContent:'center'}}>
                                    <Text style={{color:'#e16800', fontSize: RF(2.5), alignself:'center',marginLeft:wp('2%') }}>Remark: {this.state.mainscore.remarks}</Text>
                                </View>
                          </View>

                          <View style={{flex: 2,flexDirection: 'row',justifyContent: 'center', height: hp('20%')}} >
                                <View style= {{backgroundColor:'#FFFFFF', width:wp('80%'), marginBottom: hp('1%'), justifyContent:'center'}}>
                                    <Text style={{color:'#e16800', fontSize: RF(2.5), alignself:'center',marginLeft:wp('2%') }}>Total Remark: {this.state.mainscore.total_remark}</Text>
                                </View>
                          </View>
                    </View>      
        
                    <View style={{height:hp('40%'), backgroundColor: '#FFFFFF',}} >
                          <TouchableOpacity onPress={this.onPDFPressed.bind(this, this.state.mainscore.your_score)}>
                              <View style={{height:hp('8%'),flexDirection: 'row', backgroundColor:'#1ab394', width:wp('80%'), marginLeft:wp('9%'), justifyContent:'center', borderRadius: wp('2%'), alignItems:'center', marginTop:hp('2%')}}>
                                  <Text style={{color:'#FFFFFF',fontSize: RF(4), alignself:'center'}}>Download Solution</Text>
                              </View>
                          </TouchableOpacity>
                    </View>

                  </View>

                  :
                  <View/>
                }
                  {this.state.mainsTest == 0 ?

                  <View style={{flex: 1, height:hp('100%'),width:wp('95%'), justifyContent:'space-between',backgroundColor: '#FFFFFF',marginTop:'2%',}}>
                    
                      <View style={{ height:hp('5%'), backgroundColor: '#3f4c6b', marginTop:'2%', justifyContent:'center', alignItems:'center'}}>
                        <Text  style={{fontSize: RF(3),color:'#FFFFFF', alignself:'center', marginLeft:wp('2%') }}>Instructions</Text>
                      </View>

                      <View style={{  backgroundColor: '#FFFFFF', }}>
                        <Text  style={{fontSize: RF(2.5),color:'#000000', marginLeft:wp('2%') }}>{'\u2022'} All questions are compulsory.</Text>
                        <Text  style={{fontSize: RF(2.5),color:'#000000', marginLeft:wp('2%') }}>{'\u2022'} Number of questions: 10 Questions</Text>
                        <Text  style={{fontSize: RF(2.5),color:'#000000', marginLeft:wp('2%') }}>{'\u2022'} Time allotted: 90 minutes </Text>
                        <Text  style={{fontSize: RF(2.5),color:'#000000', marginLeft:wp('2%') }}>{'\u2022'} Questions 1 to 5 carry 10 marks each, 6 to 10 carry 15 marks each.</Text>
                        <Text  style={{fontSize: RF(2.5),color:'#000000', marginLeft:wp('2%') }}>{'\u2022'} The answers must not exceed 150 words for 10 markers and 250 for 15 markers.</Text>
                        <Text  style={{fontSize: RF(2.5),color:'#000000', marginLeft:wp('2%') }}>{'\u2022'} Please read the question carefully and type in the answer in the text box provided. </Text>
                        <Text  style={{fontSize: RF(2.5),color:'#000000', marginLeft:wp('2%') }}>{'\u2022'} We recommend you to write the answer for all the question in a model answer booklet. Scan it and upload the answer pdf in the upload answer option provided.</Text>
                        <Text  style={{fontSize: RF(2.5),color:'#000000', marginLeft:wp('2%') }}>{'\u2022'} Make sure the scanned documents are legible to read with correct test number and question number written</Text>
                        <Text  style={{fontSize: RF(2.5),color:'#000000', marginLeft:wp('2%') }}>{'\u2022'} Upload file size limit: 2MB</Text>
                        <Text  style={{fontSize: RF(2.5),color:'#000000', marginLeft:wp('2%') }}>{'\u2022'} Please do not refresh the page while you are taking Test</Text>
                        <Text  style={{fontSize: RF(2.5),color:'#ff0000', marginLeft:wp('2%') }}>{'\u2022'} Please note you can attempt the test only one time</Text>
                        
                      </View>

                      <TouchableOpacity style={{ height:hp('7%'),width:wp('30%'), backgroundColor: '#1ab394', marginTop:'2%', justifyContent:'center', alignItems:'center', marginLeft:'31.55%', }} 
                        onPress={() => {
                          //this.setTestModalVisible(true);
                          this.insertMainQuestions();
                          //this.onStartMainsTest();
                          //this.getMainReviews();
                        }}>
                      <View>
                          <Text  style={{fontSize: RF(3),color:'#FFFFFF', alignself:'center',  }}>Start Test</Text>
                      </View>
                      </TouchableOpacity>
                  </View>    
                  :
                  <View/>
                }

                {this.state.mainsTest == 1 ?
              
              <Text  style={{fontSize: RF(2.5),color:'#000000', marginLeft:wp('2%') }}>{'\u2022'}Congrats! Attempting answer writing in itself is an edge out process. You will be receiving the mail once the scores are ready (within 3 days max). All the best. Enjoy writing.</Text>
              :
              <View/>
                  
              
              }

                  
              </View>    
            </ScrollView>
            {this.state.isLoading ? <ActivityIndicator
                    animating     ={true}
                    transparent   ={true}
                    visible       ={false}
                    style         ={styles.indicator}
                    size          ="large"
                  /> : <View/>  } 
            <Quotes/> 

            <Modal
                  animationType="slide"
                  transparent={false}
                  visible={this.state.TestmodalVisible}
                  onRequestClose={() => {
                    Alert.alert(
                      'Submit Exam',
                      'Do You Really want to Submit Your Exam!',
                      [
                        {text: 'Cancel', onPress: () => {}},
                        {text: 'OK', onPress: () => {this.setTestModalVisible(false)
                                                    this.onSubmitChange()}},
                      ],
                      { cancelable: false }
                    );
                  }}>
                  <View style={{flex: 1, width:wp('100%')}}>

                    <View style={{flex: .5, flexDirection:'row', marginTop:'2%'}}>
                          <View style={{flex: 5,flexDirection:'row', justifyContent:'center'}} >
                                <CountDown
                                  digitBgColor  = '#1bb496'
                                  digitTxtColor = '#FFFFFF'
                                  timeTxtColor  = '#000000'
                                  until={this.state.totalDuration}
                                  //duration of countdown in seconds
                                  timetoShow={('H', 'M', 'S')}
                                  //formate to show
                                  onFinish={() => {this.onFinishTimer()}}
                                  //on Finish call
                                  //onPress={() => alert('hello')}
                                  //on Press call
                                  size={20}
                                />
                          </View>
                          {false ?
                          <View  style={{flex: 1}} >
                          <TouchableOpacity
                            visible={false}
                            onPress={() => {
                              this.setModalVisible(!this.state.modalVisible);
                            }}>

                                    <View style={{
                                          borderWidth:1,
                                          borderColor:'rgba(0,0,0,0.2)',
                                          alignItems:'center',
                                          justifyContent:'center',
                                          width:50,
                                          position: 'relative',                                          
                                          bottom: 0,                                                    
                                          right: 10,
                                          height:50,
                                          backgroundColor:'#fff',
                                          borderRadius:100,
                                        }}
                                      >    
                                    <Icon name="plus"  size={30} color="#01a699" />
                                  
                                  </View>
                                </TouchableOpacity>
                          </View>
                          :
                          <View/>
                        }
                          
                    </View>

                    <View style={{flex:1, flexDirection:'row',marginLeft:wp('5%'),marginRight:wp('5%'), marginTop:'10%'}}>
                      <TouchableOpacity
                       onPress={() => {
                        this._uploadAllAnswerFile();
                      }}
                      >
                          <View style={{
                                         borderWidth:1,
                                         borderColor:'rgba(0,0,0,0.2)',
                                         alignItems:'center',
                                         justifyContent:'center',
                                         width:30,
                                         position: 'relative',                                          
                                         bottom: 5,                                                    
                                         right: 10,
                                         height:50,
                                         backgroundColor:'#fff',
                                         borderRadius:100,
                                       }}
                                    >   
                          <Icon name="upload"  size={20} color="#01a699" />
                          </View>
                      </TouchableOpacity>
                      <Text numberOfLines={4}> Upload Your All Answers File(Support Available for jpeg,jpg,png and pdf).File Size must be 20Mb only.</Text>
                    </View>
                    <View style={{flex: 5.5,marginTop:'5%'}}>
                      <ScrollView >
                      <View style={{ alignItems:'center', alignself:'center',marginBottom:hp(this.state.onFocusMargin)}}>
                            <FlatList
                              extraData={this.state}
                              data={this.state.questions}
                              renderItem={({item, index}) => 

                              <View style={{flex:1, flexDirection:'column',width:wp('95%'), justifyContent:'center', backgroundColor:'#FFFFFF',  borderWidth:2, borderColor:'#CCCCCC'}}>
                                  

                                  <View style={{flex:1, flexDirection:'column',marginLeft:wp('2%'), marginTop:hp('2%'), }}>
                                       <Text style={{marginTop:'3.5%', color:'#150F68'}}>Q. {this.state.nextPostion + index + 1 }. </Text>
                                       <HTML html={styleStart + item.question + styleEnd}  imagesMaxWidth={Dimensions.get('window').width} />
                                  </View>
                                 
                                      
                                  <View style={{flex:1, height:wp('30%'), flexDirection:'column',position: 'relative',marginLeft:wp('2%'),borderWidth:1, borderColor:'#000000', borderRadius:wp('2%'), width:wp('90%'), justifyContent:'flex-start', alignself:'center'}}>

                                    <TextInput
                                        multiline={true}
                                        onFocus={this.textOnFocus.bind(this)}
                                        onBlur={this.textOnBlur.bind(this)}
                                        underlineColorAndroid='transparent'
                                        textAlignVertical= "top"
                                        onChangeText={text => {
                                          let { textInputs } = this.state;
                                          textInputs[index] = text;
                                          this.setState({
                                            textInputs,
                                          });
                                        }}
                                        value={this.state.textInputs[index]}
                                    />

                                  </View>


                                  <View style={{flex:1, height:hp('5%'), flexDirection:'row',marginLeft:wp('2%'), marginTop:'5%',marginBottom:'5%'}}>
                                    <TouchableOpacity
                                      onPress={this.handleChange.bind(this,item)}>
                                        <Icon name="upload"  size={20} color="#01a699" />
                                    </TouchableOpacity>
                                    <Text > Only jpeg,jpg,png and pdf allowed. Max size: 2MB</Text>
                                  </View>
                                  {item.answer_upload !='' ?
                                   <View>
                                    <Text style={{marginTop:'3.5%', color:'#150F68'}}>File Uploaded Successfully!</Text>
                                  </View>  
                                 :
                                  <View/>
                                 }
                                 
                                   { false ?
                                    <View style={{flex:1, flexDirection:'row',marginLeft:wp('50%')}}>
                                    <CheckBox
                                      center
                                      checkedColor = '#7f00ff'
                                      size = {hp('2.5%')}
                                      containerStyle={{backgroundColor:'transparent', borderColor:'transparent'}}
                                      textStyle ={{fontSize: RF(2.5),fontWeight: 'bold',color:'#FFFFFF'}}
                                      checked={item.review_later == '1' ? true : false } 
                                      onPress={this._reviewLater.bind(this, item) }/>

                                    <Text style={{flex:1,marginTop:'9%',marginLeft:-22, color:"#7f00ff", fontWeight:'bold'}}>Review Later</Text>
                                    </View>
                                    :
                                    <View/>
                                   }     
                                      
                                      

                              </View>
                            }
                            />
                      </View>
                      </ScrollView>
                    </View>
                    <View style={{flex: .2, flexDirection:'row', marginTop:'0%', alignItems:'center', justifyContent:'space-between', }}>
                          
                          { this.state.nextPostion > '4' ?
                            <View style={{flex: 2,}} >
                                <TouchableOpacity
                                  onPress={() => {
                                    this.onNext("previous");
                                  }}>

                                  <View style={{marginLeft:5,alignItems:'center',  backgroundColor:'#1ab395', height: hp('5%')}}>    
                                    <Text textAlign={'center'} style={{fontSize: RF(2.5), fontWeight:'bold', alignself:'center', color:'#FFFFFF'}}>PREVIOUS</Text>
                                  </View>
                                </TouchableOpacity>
                            </View>
                            :
                            <View/>
                          }
                          
                          { ( this.state.nextPostion + 5 < this.state.totalQuestionCount) ?
                        
                            <View style={{flex: 2, }} >
                                  <TouchableOpacity
                                    onPress={() => {
                                      this.onNext("next");
                                    }}>

                                    <View style={{marginLeft:5,marginRight:5, alignItems:'center', backgroundColor:'#1ab395', height: hp('5%')}}>
                                      <Text textAlign={'center'} style={{fontSize: RF(2.5), fontWeight:'bold', alignself:'center',color:'#FFFFFF'}}>NEXT</Text>
                                    </View>
                                  </TouchableOpacity>
                            </View>
                          :
                            <View/>
                          }

                          { this.state.nextPostion >= this.state.totalQuestionCount - 5 ?
                        
                            <View style={{flex: 2, }} >
                                  <TouchableOpacity
                                    onPress={() => {
                                     
                                      this.setTestModalVisible(false);
                                        this.onSubmitChange();
                                    }}>

                                    <View style={{marginLeft:5,marginRight:5, alignItems:'center', backgroundColor:'#1ab395', height: hp('5%')}}>
                                      <Text textAlign={'center'} style={{fontSize: RF(2.5), fontWeight:'bold', alignself:'center',color:'#FFFFFF'}}>SUBMIT</Text>
                                    </View>
                                  </TouchableOpacity>
                            </View>
                          :
                            <View/>
                          }
                    </View>
                    <Modal
                      animationType="slide"
                      transparent={true}
                      visible={this.state.modalVisible}
                      onRequestClose={() => {
                        Alert.alert('Are you sure to submit test');
                      }}>
                      <View style={{marginTop:hp('20%'), marginBottom:hp('.5%'), backgroundColor:'#FFFFFF'}}>
                        <View style={{marginBottom:hp('10%')}}>
                          <ScrollView>
                          
                          <FlatList
                              extraData={this.state}
                              data={this.state.review}
                              renderItem={({item, index}) => 
                                  <View style={{flex:1, flexDirection:'row',marginLeft:wp('1%'),}}>
                                    {this.reviewQuestions(item)}
                                  </View>
                          }
                          numColumns={5}
                          />
                          </ScrollView>
                        </View>
                      </View>
                    </Modal>

                    {this.state.isLoading ? <ActivityIndicator
                            animating     ={true}
                            transparent   ={true}
                            visible       ={false}
                            style         ={styles.indicator}
                            size          ="large"
                          /> : <View/>  } 
          
                  </View>
              </Modal> 

      </View>

      );
      }
 }



export default MainQuiz = StackNavigator(
{
 
  MainQuiz : {
    screen: MainQuiz,
  },
  ViewPDF: {
    screen: ViewPDF,
  },
});