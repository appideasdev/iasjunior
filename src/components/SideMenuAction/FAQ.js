/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  StatusBar,
  ImageBackground,
  ScrollView,
  TextInput,
  TouchableOpacity,
  Alert,
  AsyncStorage,
  Image,
  FlatList,
  ActivityIndicator,
  RefreshControl
} from 'react-native';
import Quotes from "../Quotes.js";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import RF from "react-native-responsive-fontsize";
import { Dropdown } from 'react-native-material-dropdown';
import Icon from 'react-native-vector-icons/FontAwesome';
import styles from './../Styles/GlobalStyle.js';
import { HeaderBackButton } from 'react-navigation';



import GetApi from './../GlobalVariables/GetApi.js';
import GlobalVariables from '../GlobalVariables/GlobalVariables';


type Props = {};
export default class FAQ extends Component<Props> {

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Ask Query',
      headerStyle: { backgroundColor:'#36404a' },
      headerTitleStyle: { color: '#FFFFFF' },
      headerTintColor: 'white',
      headerLeft:(<HeaderBackButton tintColor='#FFFFFF' onPress={()=>{navigation.navigate('First');}}/>)
  
    };
  };

  constructor(props) {
    super(props);
    this.state = { 
                    query : '',
                    TOKEN : null,
                    student_id : null,
                    quotesSlug : null,
                    isLoading  : true,
                    limit:10,
                    pageOffset:1,
                    onFocusMargin:'0%'
         };

    AsyncStorage.getItem('TOKEN').then((data) => { 

        this.setState({TOKEN : data });

        //Alert.alert( GetApi.getApiUrl(data))
        console.log("my token data",data);


    });  
   
     AsyncStorage.getItem('student_image').then((student_image) => { 

          if(data){
            this.setState({student_image : student_image });
          }else{
            this.setState({student_image : '15385388885bb43d888ade6_thumb.png' });

          }

      });


    
  }

  componentDidMount = async () => {

      this.timer= setInterval(()=> this.HitAllApi(),10000);
     
     
  }

  
textOnFocus(){
  if(Platform.OS == 'ios'){
    this.setState({onFocusMargin:'40%'})
  }
 
}

textOnBlur(){
  if(Platform.OS == 'ios'){
  this.setState({onFocusMargin:'0%'})
  }
}


  HitAllApi(){

    var  GVar  = GlobalVariables.getApiUrl();
      AsyncStorage.multiGet(["TOKEN", "student_id","student_photo"]).then(response => {

            var TOKEN                          = response[0][1];
            var student_id                     = response[1][1];
            var student_photo                  = response[1][1];

           
           
            this.setState({
                            TOKEN          : TOKEN,    
                            student_id     : student_id,
                            student_photo  : student_photo,
                      });


            let formdata = new FormData();

            formdata.append('student_id',        student_id );
            formdata.append('page',this.state.pageOffset);
            formdata.append('limit',this.state.limit);

            console.log("chatting form data",formdata);
           

            fetch(  GVar.BaseUrl + GVar.Chatting,{
                      method: 'POST',
                      headers:{
                                  'token' : TOKEN
                              },
                      body: formdata,       
                      }).then((response) => response.text())
                     .then((responseJson) => {

                      

                      var x = JSON.parse(responseJson.replace("-", " "));
                      this.setState({status : x.status });
                      this.setState({isLoading : false });
                      console.log("my queries ",x);
                      
                      if( x.status == 1 ){
                          
                          this.setState({chatting :x.query});
                         
                          
                      }
                      
                     }).catch((error)=>{ Alert.alert(error)});

      });  
     
      /*AsyncStorage.multiGet(["TOKEN", "student_id","student_photo"]).then(response => {

            var TOKEN                          = response[0][1];
            var student_id                     = response[1][1];
            var student_photo                  = response[1][1];

            if( !student_photo || student_photo == student_id){
               student_photo = '15385388885bb43d888ade6_thumb.png';
            }

           

            this.setState({
                            TOKEN          : TOKEN,    
                            student_id     : student_id,
                            student_photo  : student_photo,
                      });

             console.log(this.state.student_photo,"student_photostudent_photo")

            let formdata = new FormData();

            formdata.append('student_id',        student_id );
            formdata.append('page',this.state.pageOffset);
            formdata.append('limit',this.state.limit);

            

            fetch( GVar.BaseUrl + GVar.Chatting,{
                      method: 'POST',
                      headers:{
                                  'token' : TOKEN
                              },
                      body: formdata,       
                      }).then((response) => response.text())
                     .then((responseJson) => {

                      

                      var x = JSON.parse(responseJson.replace("-", ""));
                      this.setState({status : x.status });
                      this.setState({isLoading : false });
                      
                      if( x.status == 1 ){

                          this.setState({chatting : x.query });
                          
                          this.setState({pageOffset:pageOffset+1});
                          this.setState({limit:limit+10});
                          
                      }else{
                        this.setState({message : x.message });

                      }
                      
                     }).catch((error)=>{ Alert.alert(error)});

      });*/  


  }

  SendFAQ(){

    var GVar = GlobalVariables.getApiUrl();
    this.setState({isLoading : true });

    var options = this.state.options;
    var query   = this.state.query;
    var TOKEN   = this.state.TOKEN;
    var student_id   = this.state.student_id;

    var error = 0;

    var error_message = '';

    if( options == undefined){
          error = error + 1;
          error_message = error_message + '\nPlease Select Query Type';
    }

    if( query == '' ){
          error = error + 1;
          error_message = error_message + '\nPlease Enter Your Query';
    }

    if( TOKEN == '' && student_id == '' ){
          
          error = error + 1;
    }

   
    if( error == 0 ){

      let formdata = new FormData();

        formdata.append('queryType' ,         options );
        formdata.append('faq'       ,         query );
        formdata.append('student_id',         student_id );

        console.log(formdata,'formdata');
        console.log("on sendfaq formdata",formdata);
      

        fetch( GVar.BaseUrl + GVar.SendFaqApi,{
                      method: 'POST',
                      headers:{
                                  'token' : TOKEN
                              },
                      body : formdata,        
                      }).then((response) => response.text())
                     .then((responseJson) => {

                      this.setState({isLoading : false });
                     
                      
                      console.log(responseJson.replace("-", ""),'responseJson123 send faq');

                      var x = JSON.parse(responseJson.replace("-", ""));

                       Alert.alert(x.message)
                       this.setState({query : "" });
                       this.setState({options : undefined });
                     
                     }).catch((error)=>{ Alert.alert(error)});
    }else{
      this.setState({isLoading : false });
      Alert.alert("Warning",error_message);
    }

    
    AsyncStorage.multiGet(["TOKEN", "student_id","student_photo"]).then(response => {

      var TOKEN                          = response[0][1];
      var student_id                     = response[1][1];
      var student_photo                  = response[1][1];

      if( !student_photo || student_photo == student_id){
         student_photo = '15385388885bb43d888ade6_thumb.png';
      }

     

      this.setState({
                      TOKEN          : TOKEN,    
                      student_id     : student_id,
                      student_photo  : student_photo,
                });

       console.log(this.state.student_photo,"student_photostudent_photo")

      let formdata = new FormData();

      formdata.append('student_id',        student_id );
      formdata.append('page',this.state.pageOffset);
      formdata.append('limit',this.state.limit);

      

      fetch( GVar.BaseUrl + GVar.Chatting ,{
                method: 'POST',
                headers:{
                            'token' : TOKEN
                        },
                body: formdata,       
                }).then((response) => response.text())
               .then((responseJson) => {

                

                var x = JSON.parse(responseJson.replace("-", ""));
                this.setState({status : x.status });
                this.setState({isLoading : false });
                console.log("on chatting send faq",x);
                
                if( x.status == 1 ){

                    this.setState({chatting : x.query });
                  
                    
                }
                
               }).catch((error)=>{ Alert.alert(error)});

});
     

  }

  _onRefresh = () => {
    this.setState({refreshing: true});
    
    this.setState({limit : this.state.limit+10 });
    
    var GVar = GlobalVariables.getApiUrl();
     
      AsyncStorage.multiGet(["TOKEN", "student_id","student_photo"]).then(response => {

            var TOKEN                          = response[0][1];
            var student_id                     = response[1][1];
            var student_photo                  = response[1][1];

            if( !student_photo || student_photo == student_id){
               student_photo = '15385388885bb43d888ade6_thumb.png';
            }

           

            this.setState({
                            TOKEN          : TOKEN,    
                            student_id     : student_id,
                            student_photo  : student_photo,
                      });

             console.log(this.state.student_photo,"student_photostudent_photo")

            let formdata = new FormData();

            formdata.append('student_id',        student_id );
            formdata.append('page',this.state.pageOffset);
            formdata.append('limit',this.state.limit);

            

            fetch( GVar.BaseUrl + GVar.Chatting ,{
                      method: 'POST',
                      headers:{
                                  'token' : TOKEN
                              },
                      body: formdata,       
                      }).then((response) => response.text())
                     .then((responseJson) => {

                      

                      var x = JSON.parse(responseJson.replace("-", ""));
                      this.setState({status : x.status });
                      this.setState({isLoading : false });
                      
                      if( x.status == 1 ){

                        console.log("om refresh ",x);
                        this.setState({chatting : x.query});
               
                      }
                      
                     }).catch((error)=>{ Alert.alert(error)});

      });  
    this.setState({refreshing: false});
  }

 
  

  render() {

    

    let options = [{
      value: 'Technical',
    }, {
      value: 'General',
    },{
      value: 'Subject',
    }];

    var quotes = this.state.quotesSlug;
    console.log(quotes,"quotes123456");

    if( quotes != null ){

      quotes   = quotes.replace(/\\/g, '');
    }

    console.log(quotes,"quotes123456");

    return (
      <View style={styles.container}>
          <StatusBar
           backgroundColor="#232A40"
           barStyle="light-content"/>
           
              
              <View style={{width: "100%",height: "100%"}} pointerEvents={this.state.isLoading ? 'none' : 'auto'}>
               
              

              

              <View style={{flex: 1, width:'90%',marginLeft:'5%',marginRight:'5%', marginTop:'2%'}}>

                <View style={{flex: 4,}} >

                <ScrollView 
                  ref="scrollView"
                  onContentSizeChange={(width,height) => this.refs.scrollView.scrollTo({y:height})} 
                  refreshControl={
                      <RefreshControl
                        refreshing={this.state.refreshing}
                        onRefresh={this._onRefresh}
                      />
                    }>
                    <View style={{backgroundColor:'#FFFFFF',}}>
                        <FlatList
                          extraData={this.state}
                          data={this.state.chatting}
                          renderItem={({item}) => 

                          <View style={{flex:1, flexDirection:'row',width:wp('100%'), justifyContent:'center', backgroundColor:'#FFFFFF',}}>
                              
                              { item.addedBY == '1' ? 

                                <View style={{flex: 1, flexDirection:'row',}}>
                                
                                <View style={{width:'72%',borderWidth:1,marginTop:3, borderRadius:wp('2%')}} >
                                    <Text style={{marginLeft:wp('2%',)}}>{item.queryMessage}</Text>
                                </View>

                                <View style={{width:'15%',marginRight:wp('8%'),marginTop:3,marginLeft:'1%'}} >
                                    <Image style={{height:hp('10%'), width:wp('15%'), alignSelf:'center', marginTop: 0,  borderRadius:37,}} source={{uri: 'https://mentorship.iasjunior.com/student/assets/student/15385388885bb43d888ade6_thumb.png'}}/>  
                                    
                                </View>

                              </View>

                                :
                                <View style={{flex: 1, flexDirection:'row',}}>
                                
                                <View style={{width:'15%', marginTop:3,}} >
                                    <Image style={{height:hp('10%'), width:wp('15%'), alignSelf:'center', marginTop: 0,  borderRadius:37,}} source={{uri: 'https://mentorship.iasjunior.com/student/assets/student/'+this.state.student_image}}/>  
                                </View>

                                <View style={{borderWidth:1,width:'72%', marginLeft:'1%',marginTop:3, borderRadius:wp('2%') }} >
                                <Text style={{marginLeft:wp('2%')}}>{item.queryMessage}</Text>
                                </View>

                              </View>
                              }
                            
                          </View>
                        }/>
                    </View>
                </ScrollView>

                </View>
                

                <View style={{height:hp('28%'), marginBottom:hp(this.state.onFocusMargin)}}>
                  
                  <View style={{flex: 1,}} >
                      <Dropdown
                        label='Select Query Type'
                        data={options}
                        valueExtractor={({value})=> value}
                        onChangeText={(value)=>{ this.setState({options : value })}}
                      /> 
                  </View>

                  <View style={{flex:1,flexDirection:'row', marginBottom:5}}>

                    <View style={{flex: 5,borderColor: '#000000', borderWidth: 1, borderRadius:wp('2%')}} >
                      <TextInput
                        onFocus = {this.textOnFocus.bind(this)}
                        onBlur = {this.textOnBlur.bind(this) }
                        multiline
                        style={{textAlignVertical: "top", }}
                        onChangeText={(query) => this.setState({query})}
                        value={this.state.query}
                        palceholder = {'Enter Your Query.....'}
                      />
                    </View>

                    <View style={{flex: 1,}}>
                        <TouchableOpacity style={{marginTop:hp('8%')}} onPress={this.SendFAQ.bind(this)}>
                           <Image style={{width: wp('10%'), height: hp('5%'),alignSelf:'flex-end',  borderRadius:wp('2%')}} source={require('./../../assets/images/B2.png')} />
                        </TouchableOpacity>
                    </View>

                  </View>
                 
                </View>

              </View>
               {this.state.isLoading ? <ActivityIndicator
                    animating     ={true}
                    transparent   ={true}
                    visible       ={false}
                    style         ={styles.indicator}
                    size          ="large"
                  /> : <View/>  } 
                    
              <Quotes/>   
              </View>
                  
      </View>
    );
  }
}

