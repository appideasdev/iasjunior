/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  StatusBar,
  ImageBackground,
  ScrollView,
  WebView,
  AsyncStorage,
  Dimensions,
  Alert,
  ListView,
  FlatList,
  ActivityIndicator,
  PermissionsAndroid,
  TouchableOpacity,
  TextInput,
  Modal,

} from 'react-native';
import {
  DocumentPicker,
  DocumentPickerUtil,
} from 'react-native-document-picker';
import RNHTMLtoPDF from 'react-native-html-to-pdf';
import { CheckBox } from 'react-native-elements'
import Icon from 'react-native-vector-icons/FontAwesome';
import CountDown from 'react-native-countdown-component';
import GlobalVariables from './../GlobalVariables/GlobalVariables.js';
import HTML from 'react-native-render-html';
import Pdf from 'react-native-pdf';
import RNFetchBlob from 'rn-fetch-blob';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import RF from "react-native-responsive-fontsize";
import styles from './../Styles/GlobalStyle.js';
import { HeaderBackButton } from 'react-navigation';


import Quotes from "../Quotes.js";
import { locale } from 'moment';

type Props = {};
export default class MockmainSyllabus extends Component<Props> {

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Practice Test',
      headerStyle: { backgroundColor:'#36404a' },
      headerTitleStyle: { color: '#FFFFFF' },
      headerTintColor: 'white',
      headerLeft:(<HeaderBackButton tintColor='#FFFFFF' onPress={()=>{navigation.navigate('First');}}/>)
  
    };
  };


  constructor(props) {
    super(props);



    this.state = {  nextPostion : 0,
                    onFocusMargin:'0%',
                    textInputs  : [],
                    query       : null,
                    TOKEN       : null,
                    student_id  : null,
                    quotesSlug  : null,
                    noOfMockMainsGiven:0,
                    givenAllTest:0,
                    isLoading   : true,
                    moduleName  : null,
                    mockmain_score: [],     
                    disableGS1:false,
                    disableGS2:false,
                    disableGS3:false,
                    disableGS4:false,
                    disableEssay:false,               
                    buttonColor:'#1ab394',
                    testModalVisible:false,
                    plusModalVisible:false,
                    startTime:false,
                    totalDuration: '',
                    timeCompleted:true,
                    totalQuestionCount:0,
                    resultModalVisible:false,
                    isresultLoading:false,
                    latest_result: []

         };
  }


  componentDidMount = async () => {

    const { navigation } = this.props;
   // const module_id = navigation.getParam('id');
    //console.log("before state",module_id);

    //this.setState({module_id:27});
   // console.log("component module_id",module_id);

    var GVar = GlobalVariables.getApiUrl();

    
    AsyncStorage.multiGet(["TOKEN", "student_id"]).then(response => {

            var TOKEN                          = response[0][1];
            var student_id                     = response[1][1];
           
            this.setState({
                            TOKEN          : TOKEN,    
                            student_id     : student_id,
                      });


            let formdata = new FormData();

            formdata.append('student_id',        student_id );

            

            fetch( GVar.BaseUrl + GVar.MockMain ,{
                      method: 'POST',
                      headers:{
                                  'token' : TOKEN
                              },
                      body: formdata,       
                      }).then((response) => response.text())
                     .then((responseJson) => {

                      

                      var x = JSON.parse(responseJson.replace("-", ""));
                      this.setState({status : x.status });
                      this.setState({isLoading : false });
                      
                      if( x.status == 1){

                        console.log("Status",x);
                       // for now considering x.mockMains.test is yes

                       //comment this after removing async comment
                        if(x.mockMains.test == 'yes'){
                          this.setState({givenAllTest:1});
                        }
                       
                        //remove comment here
                        
                        this.setState({assignedModules:x.mockMains.assignedModules});
                        //this.setState({noOfMockMainsGiven:parseInt(x.mockMains.noOfMockMainsGiven)+1})
                        this.setState({noOfMockMainsGiven:parseInt(x.mockMains.noOfMockMainsGiven)+1})
                        
                        console.log("noOfGivenTest",this.state.noOfMockMainsGiven);
                        this.setState({mockmain_score:x.mockMains.mockmain_score});

                        if(this.state.noOfMockMainsGiven== 1){
                          this.setState({disableGS1:false});
                          this.setState({disableGS2:true});
                          this.setState({disableGS3:true});
                          this.setState({disableGS4:true});
                          this.setState({disableEssay:true});
                        }else if(this.state.noOfMockMainsGiven== 2){
                          this.setState({disableGS1:true});
                          this.setState({disableGS2:false});
                          this.setState({disableGS3:true});
                          this.setState({disableGS4:true});
                          this.setState({disableEssay:true});
                        }else if(this.state.noOfMockMainsGiven == 3){
                          this.setState({disableGS1:true});
                          this.setState({disableGS2:true});
                          this.setState({disableGS3:false});
                          this.setState({disableGS4:true});
                          this.setState({disableEssay:true});
                        }else if(this.state.noOfMockMainsGiven == 4){
                          this.setState({disableGS1:true});
                          this.setState({disableGS2:true});
                          this.setState({disableGS3:true});
                          this.setState({disableGS4:false});
                          this.setState({disableEssay:true});
                        }else if(this.state.noOfMockMainsGiven == 5){
                          this.setState({disableGS1:true});
                          this.setState({disableGS2:true});
                          this.setState({disableGS3:true});
                          this.setState({disableGS4:true});
                          this.setState({disableEssay:false});
                        }else if(this.state.noOfMockMainsGiven > 5){
                          console.log("mock test > than 5");
                          this.setState({disableGS1:true});
                          this.setState({disableGS2:true});
                          this.setState({disableGS3:true});
                          this.setState({disableGS4:true});
                          this.setState({disableEssay:true});

                        }

                        
                          
                          
                      }else{
                       // Alert.alert("Plaese Try Later");
                        this.setState({message : x.message });

                      }
                      
                     }).catch((error)=>{ Alert.alert(error)});

      });

      //remove this 
      //this.setState({isLoading:false});

      this.startTimer();

    //this.onPDFPressed();
     
  }

  InsertMainsFullQuestions(){

    var GVar = GlobalVariables.getApiUrl();

    AsyncStorage.multiGet(["TOKEN", "student_id", "moduleId"]).then(response => {

            var TOKEN                          = response[0][1];
            var student_id                     = response[1][1];
         

            this.setState({isLoading:true});
            console.log("insert mains question")
            let formdata = new FormData();
            formdata.append("student_id",student_id);
            formdata.append("module_id",this.state.assignedModules);

            console.log("my form data",formdata);

          
            fetch( GVar.BaseUrl + GVar.InsertMockMainsFullQuestions,{
                method: 'POST',
                headers:{
                            'token' : TOKEN
                        },
                body: formdata,       
                }).then((response) => response.text())
               .then((responseJson) => {

               
                var x = JSON.parse(responseJson.replace("-", ""));
                console.log("maibs questions",x);
                if(x.status == 0){
                  Alert.alert("Questions are not prepared..Please Try Later!!");
                  this.setTestModalVisible(false);
                 
                }else{ 
                  this.getMainsFullQuestions();
                  this.getMainFullReviews();
                }
            this.setState({isLoading:false});
            //this.setState({isLoading : false });
               

                
            
           }).catch((error)=>{ Alert.alert(error)});
    });          
  
}

startTimer(){

  var that = this;
  //converting in seconds
  if(that.state.startTime){

  that.setState({ totalDuration: 12600 });     
  
  }else{
    that.setState({totalDuration:300});
    //that.setState({timerCompleted:false});
  }
  //Settign up the duration of countdown in seconds to re-render
}

getMainsFullQuestions(){

      
  var GVar = GlobalVariables.getApiUrl();

  AsyncStorage.multiGet(["TOKEN", "student_id"]).then(response => {

          var TOKEN                          = response[0][1];
          var student_id                     = response[1][1];
         
          this.setState({
                          TOKEN          : TOKEN,    
                          student_id     : student_id,
                          isModalLoading:true,
                    });

          let formdata = new FormData();

          formdata.append('student_id',        student_id );
          //formdata.append('module_id',        this.state.module_id );
          formdata.append('count',this.state.noOfMockMainsGiven);
          
          console.log(formdata,"formdata get all Questions");
          console.log(TOKEN,"TOKEN");

          fetch( GVar.BaseUrl + GVar.GetMockMainsFullQuestions,{

                    method: 'POST',
                    headers:{
                                'token' : TOKEN
                            },
                    body: formdata,       
                    }).then((response) => response.text())
                   .then((responseJson) => {

                    var x = JSON.parse(responseJson.replace("-", ""));
                    this.setState({isModalLoading:false});
                    

                    if( x.status == 1 ){
                      console.log(x, "questions get Full Questions");
                        this.setTestModalVisible(true);
                      
                        this.setState({startTime:true})
                        this.startTimer();
                        this.setState({questions : x.questions });

                    }else{

                    }
                    
                   }).catch((error)=>{ Alert.alert(error)});
    
  }); 


}

getMainFullReviews(){

  var GVar = GlobalVariables.getApiUrl();


      AsyncStorage.multiGet(["TOKEN", "student_id"]).then(response => {

        var TOKEN                          = response[0][1];
        var student_id                     = response[1][1];

        this.setState({
                        TOKEN          : TOKEN,    
                        student_id     : student_id,
                        isModalLoading:true
                  });


        let formdata = new FormData();

        formdata.append('student_id',        student_id );
        //formdata.append('module_id',        this.state.module_id );
        formdata.append('count',this.state.noOfMockMainsGiven);
        
        console.log(formdata,"formdata get all Reviews ");
        console.log(TOKEN,"TOKEN");

        fetch( GVar.BaseUrl + GVar.GetMockMainsFullReviews,{
                  method: 'POST',
                  headers:{
                              'token' : TOKEN
                          },
                  body: formdata,       
                  }).then((response) => response.text())
                .then((responseJson) => {

                  var x = JSON.parse(responseJson.replace("-", ""));
                  this.setState({isModalLoading : false });
                  

                  if( x.status == 1 ){

                      console.log("reviews get Full reviews",x);
                      this.setState({review : x.review });
                      console.log("total review length",x.review.length);
                      this.setState({totalQuestionCount:x.review.length});

                  }else{

                  }
                  
                }).catch((error)=>{ Alert.alert(error)});

  }); 
}


  onNext( go ){
    //update answers text
    var GVar = GlobalVariables.getApiUrl();

    AsyncStorage.multiGet(["TOKEN", "student_id", "moduleId"]).then(response => {

            var TOKEN                          = response[0][1];
            var student_id                     = response[1][1];
            var module_id                      = response[2][1];

            var answers = this.state.textInputs;
            var questions = this.state.questions;

            this.setState({isModalLoading : true });
            for (var i = 4 ; i >= 0; i--) {

              var answer_text = answers[i];

              if( answer_text === undefined ){
                annswer_text = ' ';
              }

              var question = questions[i];
             
                        

              let formdata = new FormData();

              formdata.append('student_id',       student_id );
              formdata.append('exam_id',          question.exam_id );
              formdata.append('question_id',      question.question_id );
              formdata.append('answer_text',      answer_text );
              
              console.log(formdata,"formdata");
              console.log(TOKEN,"TOKEN");
              console.log(questions,"questions");

              fetch( GVar.BaseUrl + GVar.UpdateMainsAnswers,{
                        method: 'POST',
                        headers:{
                                    'token' : TOKEN
                                },
                        body: formdata,       
                        }).then((response) => response.text())
                      .then((responseJson) => {

                        var x = JSON.parse(responseJson.replace("-", ""));
                        //this.setState({isModalLoading : false });
                        

                        if( x.status == 1 ){

                            console.log(x, "AnswerTEXTResponce update main answers");
                          
                        }else{

                        }
                        
                      }).catch((error)=>{ Alert.alert(error)});


              this.setState({ textInputs : [] });         
              
            }

    });  


    var index_end;

    if( go == 'previous'){

      index_end = this.state.nextPostion - 5;
    }else if( go == 'next' ){

      index_end = this.state.nextPostion + 5;
    }
    
    console.log("index_end", index_end );  


    AsyncStorage.multiGet(["TOKEN", "student_id", "moduleId"]).then(response => {

            var TOKEN                          = response[0][1];
            var student_id                     = response[1][1];
            var module_id                      = response[2][1];
            

            this.setState({nextPostion : index_end});


            var TOKEN                          = response[0][1];
            var student_id                     = response[1][1];
          
            this.setState({
                            TOKEN          : TOKEN,    
                            student_id     : student_id,
                            isModalLoading      : true, 
                      });


            let formdata = new FormData();

            formdata.append('student_id',       student_id );
            formdata.append('count',this.state.noOfMockMainsGiven);
            formdata.append('go',               go );
            formdata.append('index_end',        index_end );
            
            console.log(formdata,"formdata");
            console.log(TOKEN,"TOKEN");

            fetch( GVar.BaseUrl + GVar.GetMockMainsFullQuestions,{
                      method: 'POST',
                      headers:{
                                  'token' : TOKEN
                              },
                      body: formdata,       
                      }).then((response) => response.text())
                    .then((responseJson) => {

                      var x = JSON.parse(responseJson.replace("-", ""));
                     

                      if( x.status == 1 ){
                        this.setState({isModalLoading : false });
                      

                          console.log(x, "questions mock full question");
                          this.setState({questions : x.questions });

                      }else{

                      }
                      
                    }).catch((error)=>{ Alert.alert(error)});

    }); 


          
  }

  setPlusModalVisible(visiblity){
    this.setState({plusModalVisible:visiblity})
  }
  setTestModalVisible(visiblity){
    this.setState({testModalVisible:visiblity})
  }
  
 
  _uploadAllAnswerFile(){


    console.log("show All Answerdocument picker exam_id");
    DocumentPicker.show(
      {
        filetype: [DocumentPickerUtil.allFiles()],
        //All type of Files DocumentPickerUtil.allFiles()
        //Only PDF DocumentPickerUtil.pdf()
        //Audio DocumentPickerUtil.audio()
        //Plain Text DocumentPickerUtil.plainText()
      },
      (error, res) => {
        console.log("onn handlechange",res);
        if(res !=null){
  
        if(res.type == 'application/pdf'|| res.type == 'image/jpeg'|| res.type == 'image/jpg' || res.type == 'image/png'){
  
          this.setState({ fileUri: res.uri });
          this.setState({ fileType: res.type });
          this.setState({ fileName: res.fileName });
          this.setState({ fileSize: res.fileSize });
  
          console.log('res : ' + JSON.stringify(res));
          console.log('URI : ' + res.uri);
          console.log('Type : ' + res.type);
          console.log('File Name : ' + res.fileName);
          console.log('File Size : ' + res.fileSize);
  
          Alert.alert(
            'Submit Exam',
            'Are You Sure that you want to Submit Exam ?',
            [
              {text: 'Cancel', onPress: () => {this.onCancelSubmission();console.log('Cancel Pressed')}, style: 'cancel'},
              {text: 'OK', onPress: () => {this.onFileSubmission(res.uri,res.type,res.fileName)}},
            ],
            { cancelable: false }
          );
  
  
        
        }else{
          Alert.alert("UnSupported File Format.Only Images(.jpeg,.jpg,.png) and PDF are allowed.");
        }
        
        }
  
      
      });
  
  }
  
  onFileSubmission(uri,type,filename){
  
    console.log("on File Submission uri",uri);
    console.log("on File Submission type",type);
    console.log("on File Submission filenam,e",filename);
  
    AsyncStorage.multiGet(["TOKEN", "student_id", "moduleId"]).then(response =>{
  
      var TOKEN                          = response[0][1];
      var student_id                     = response[1][1];
      //var module_id                       = response[2][1];
  
      this.setState({
                      TOKEN          : TOKEN,    
                      student_id     : student_id,
                      isModalLoading      : true
                });
  
      //AsyncStorage.removeItem('moduleId')
  
     // console.log(module_id,"module_idnew");
  
      let formdata = new FormData();
  
          formdata.append('student_id', student_id );
          formdata.append('module_id',  this.state.assignedModules );
          formdata.append('answer_type',this.state.noOfMockMainsGiven );
          formdata.append('answerfile', {
            uri: uri,
            type: type, // or photo.type
            name: filename
          });
  
          var GV = GlobalVariables.getApiUrl();
  
          console.log(formdata,"formdata");
          console.log(this.state.TOKEN,"token");
  
          fetch( GV.BaseUrl + GV.UpdateMainsSingleFileAnswers,{
                        method: 'POST',
                        headers:{
                                    'token' : this.state.TOKEN
                                },
                        body : formdata,        
                        }).then((response) => response.text())
                       .then((responseJson) => {
  
                        
                        console.log(responseJson,'on handle responseJson2');
                        var x = JSON.parse(responseJson.replace(/-\s?/g, ""));
                        console.log(x,'on handle responseJson2');
                        this.setState({isModalLoading:false});
                        Alert.alert(
                          'Submit Exam',
                          'Your Exam Submitted Successfully!',
                          [
                            {text: 'OK', onPress: () => {   this.setTestModalVisible(false);
                              this.onSubmitChange();}},
                          ],
                          { cancelable: false }
                        );
                       
                        
          }).catch((error)=>{ Alert.alert(error)});
  
    });
  }


onCancelSubmission(){

  this.setState({ fileUri:''});
        this.setState({ fileType: ''});
        this.setState({ fileName:'' });
        this.setState({ fileSize: ''});
}

handleChange(item) {
  //Opening Document Picker
  console.log("show document picker");
  var GVar = GlobalVariables.getApiUrl();
  DocumentPicker.show(
    {
      filetype: [DocumentPickerUtil.allFiles()],
      //All type of Files DocumentPickerUtil.allFiles()
      //Only PDF DocumentPickerUtil.pdf()
      //Audio DocumentPickerUtil.audio()
      //Plain Text DocumentPickerUtil.plainText()
    },
    (error, res) => {
      console.log("onn handlechange",res);
      if(res !=null){

      if(res.type == 'application/pdf'|| res.type == 'image/jpeg'|| res.type == 'image/jpg' || res.type == 'image/png'){

        this.setState({ fileUri: res.uri });
        this.setState({ fileType: res.type });
        this.setState({ fileName: res.fileName });
        this.setState({ fileSize: res.fileSize });

        console.log('res : ' + JSON.stringify(res));
        console.log('URI : ' + res.uri);
        console.log('Type : ' + res.type);
        console.log('File Name : ' + res.fileName);
        console.log('File Size : ' + res.fileSize);

        let formdata = new FormData();

        formdata.append('student_id',  this.state.student_id );
        formdata.append('exam_id',        item.exam_id );
        formdata.append('question_id',    item.question_id );
        formdata.append('answerfile', {
          uri: res.uri,
          type: res.type, // or photo.type
          name: res.fileName
        });

        

        this.setState({isModalLoading:true});
        console.log(formdata,"formdata");
        console.log(this.state.TOKEN,"token");

        fetch( GVar.BaseUrl + GVar.UpdateMainsFileSpecificAnswers,{
                      method: 'POST',
                      headers:{
                                  'token' : this.state.TOKEN
                              },
                      body : formdata,        
                      }).then((response) => response.text())
                     .then((responseJson) => {

                      
                      console.log(responseJson,'on handle responseJson2');
                      var x = JSON.parse(responseJson.replace(/-\s?/g, ""));
                      console.log(x,'on handle responseJson2');
                      this.setState({isModalLoading:false});
                      if(x.status == 1){

                        this.setState({fileUploadedMessage:x.message+ " Uploaded Successfully!"})
                        Alert.alert("Your"+x.message+"Uploaded Successfully!");
                      }
                     
                     
                      
        }).catch((error)=>{ Alert.alert(error)});

        var questions  = this.state.questions;
        var index = this.getIndex(item.exam_id, questions, 'exam_id');
        console.log("get Index i=",index);
        console.log("ques index upload",questions[index].answer_upload);

        if(questions[index].answer_upload == ''){

          questions[index].answer_upload = "Answer Uploaded Successfully";
        }

      }else{
        Alert.alert("UnSupported File Format.Only Images(.jpeg,.jpg,.png) and PDF are allowed.");
      }
      
      }

    
    }
  );
}

onSubmitChange(){

  //this.setTestModalVisible(false);
  console.log("on Submit change ");
  var GVar = GlobalVariables.getApiUrl();
  
  AsyncStorage.multiGet(["TOKEN", "student_id"]).then(response => {

    var TOKEN                          = response[0][1];
    var student_id                     = response[1][1];
   
    this.setState({
                    TOKEN          : TOKEN,    
                    student_id     : student_id,
                    isLoading      : true, 
              });


    let formdata = new FormData();

    formdata.append('student_id',       student_id );
    formdata.append('go',               'submit' );
   
    
    console.log(formdata,"formdata");
    console.log(TOKEN,"TOKEN");

    fetch( GVar.BaseUrl + GVar.GetMockMainsFullQuestions,{
              method: 'POST',
              headers:{
                          'token' : TOKEN
                      },
              body: formdata,       
              }).then((response) => response.text())
             .then((responseJson) => {

              var x = JSON.parse(responseJson.replace("-", ""));
              this.setState({isLoading : false });
              

              if( x.status == 1 ){

                  console.log(x, "questions");
                  this.setState({questions : x.questions });

              }else{

              }
              
             }).catch((error)=>{ Alert.alert(error)});

}); 


      AsyncStorage.multiGet(["TOKEN", "student_id"]).then(response => {

        var TOKEN                          = response[0][1];
        var student_id                     = response[1][1];
      
        this.setState({
                        TOKEN          : TOKEN,    
                        student_id     : student_id,
                        isLoading  : true,
                  });


        let formdata = new FormData();

        formdata.append('student_id',        student_id );

        

        fetch( GVar.BaseUrl + GVar.MockMain ,{
                  method: 'POST',
                  headers:{
                              'token' : TOKEN
                          },
                  body: formdata,       
                  }).then((response) => response.text())
                .then((responseJson) => {

                  

                  var x = JSON.parse(responseJson.replace("-", ""));
                  this.setState({status : x.status });
                  this.setState({isLoading : false });
                  
                  if( x.status == 1 ){

                    console.log("givenallTest",parseInt(x.mockMains.noOfMockMainsGiven)+1);
                  // for now considering x.mockMains.test is yes

                  //comment this after removing async comment
                    if(x.mockMains.test == 'yes'){
                      this.setState({givenAllTest:1});
                    }
                   
                    //remove comment here
                    this.setState({assignedModules:x.mockMains.assignedModules});
                    //this.setState({noOfMockMainsGiven:parseInt(x.mockMains.noOfMockMainsGiven)+1})
                    this.setState({noOfMockMainsGiven:parseInt(x.mockMains.noOfMockMainsGiven)+1})
                    console.log("noOfGivenTest",this.state.noOfMockMainsGiven);
                    this.setState({mockmain_score:x.mockMains.mockmain_score});

                    console.log("submit length",x.mockMains.mockmain_score.length);
                    console.log("score total ",x.mockMains.mockmain_score[x.mockMains.mockmain_score.length-1].total_questions)
                    if(x.mockMains.mockmain_score.length > 0){
                      this.setState({last_result_count:x.mockMains.mockmain_score.length})
                      console.log("in if total question =",x.mockMains.mockmain_score[x.mockMains.mockmain_score.length-1]);
                      this.setState({latest_result:x.mockMains.mockmain_score[x.mockMains.mockmain_score.length-1]})
                      this.setResultModalVisible(true);
                    }
                    

                    if(this.state.noOfMockMainsGiven== 1){
                      this.setState({disableGS1:false});
                      this.setState({disableGS2:true});
                      this.setState({disableGS3:true});
                      this.setState({disableGS4:true});
                      this.setState({disableEssay:true});
                    }else if(this.state.noOfMockMainsGiven== 2){
                      this.setState({disableGS1:true});
                      this.setState({disableGS2:false});
                      this.setState({disableGS3:true});
                      this.setState({disableGS4:true});
                      this.setState({disableEssay:true});
                    }else if(this.state.noOfMockMainsGiven == 3){
                      this.setState({disableGS1:true});
                      this.setState({disableGS2:true});
                      this.setState({disableGS3:false});
                      this.setState({disableGS4:true});
                      this.setState({disableEssay:true});
                    }else if(this.state.noOfMockMainsGiven == 4){
                      this.setState({disableGS1:true});
                      this.setState({disableGS2:true});
                      this.setState({disableGS3:true});
                      this.setState({disableGS4:false});
                      this.setState({disableEssay:true});
                    }else if(this.state.noOfMockMainsGiven == 5){
                      this.setState({disableGS1:true});
                      this.setState({disableGS2:true});
                      this.setState({disableGS3:true});
                      this.setState({disableGS4:true});
                      this.setState({disableEssay:false});
                    }else if(this.state.noOfMockMainsGiven > 5){
                      console.log("mock test > than 5");
                      this.setState({disableGS1:true});
                      this.setState({disableGS2:true});
                      this.setState({disableGS3:true});
                      this.setState({disableGS4:true});
                      this.setState({disableEssay:true});

                    }

                    
                      
                      
                  }else{
                  // Alert.alert("Plaese Try Later");
                    this.setState({message : x.message });

                  }
                  
                }).catch((error)=>{ Alert.alert(error)});

      });


}


DownloadPDF( index ){


  console.log("pdf index",index);
    var GVar = GlobalVariables.getApiUrl();
    this.setState({isLoading : true });
  
    var count  = index +1;
    console.log("my count",GVar.MockMainPDF+count);
    let formdata = new FormData();

    formdata.append('student_id',        this.state.student_id );

    var TOKEN = this.state.TOKEN;

    console.log("mty Token",TOKEN);
    console.log("my formadat",formdata);
    fetch( GVar.MockMainPDF+ count ,{
                      method: 'POST',
                      headers:{
                                  'token' : TOKEN
                              },
                      body: formdata,       
                      }).then((response) => response.text())
                    .then((responseJson) => {

                      var x = JSON.parse(responseJson.replace("-", ""));
                      this.setState({isLoading : false });
                    
                        var options = {
                
                            html: x.html, // HTML String

                          // ****************** OPTIONS BELOW WILL NOT WORK ON ANDROID **************                              
                            fileName: 'mains-full-syllabus' + count ,          /* Optional: Custom Filename excluded extension
                                                        Default: Randomly generated
                                                      */

                            directory: 'docs',         /* Optional: 'docs' will save the file in the `Documents`
                                                        Default: Temp directory */

                            base64: true ,               


                            height: 800,                
                            width: 1056,               /* Optional: 1056 sets the width of the DOCUMENT that will produced
                                                        Default: 792
                                                      */
                            padding: 24,                /* Optional: 24 is the # of pixels between the outer paper edge and
                                                                corresponding content edge.  Example: width of 1056 - 2*padding
                                                                => content width of 1008
                                                        Default: 10
                                                      */
                      };

                      //const source = {uri:data.filePath};

                      console.log("options",options);
                      RNHTMLtoPDF.convert(options).then((data) => {

                        if( Platform.OS == 'ios' ){
                           
                            RNFetchBlob.ios.openDocument(data.filePath);

                          }else{
                            
                            this.props.navigation.navigate('ViewPDF',{source:data.filePath })
                            Alert.alert('Successfully downloaded in Documents' );
                          }
                        // this.props.navigation.navigate('ViewPDF',{source:data.filePath })
                        // Alert.alert('Successfully downloaded in Downloads' );
                      });
                          
                      
                          
                      
                    }).catch((error)=>{ Alert.alert(error)});

}

setResultModalVisible(visible){
  this.setState({resultModalVisible:visible})
}



DownloadPDFModal( index ){


  console.log("pdf index",index);
    var GVar = GlobalVariables.getApiUrl();
    this.setState({isresultLoading : true });
  
   
   
    let formdata = new FormData();

    formdata.append('student_id',        this.state.student_id );

    var TOKEN = this.state.TOKEN;

    console.log("mty Token",TOKEN);
    console.log("my formadat",formdata);
    fetch( GVar.MockMainPDF+ index ,{
                      method: 'POST',
                      headers:{
                                  'token' : TOKEN
                              },
                      body: formdata,       
                      }).then((response) => response.text())
                    .then((responseJson) => {

                      var x = JSON.parse(responseJson.replace("-", ""));
                      this.setState({isresultLoading : false });
                    
                        var options = {
                
                            html: x.html, // HTML String

                          // ****************** OPTIONS BELOW WILL NOT WORK ON ANDROID **************                              
                            fileName: 'mains-full-syllabus' + index ,          /* Optional: Custom Filename excluded extension
                                                        Default: Randomly generated
                                                      */

                            directory: 'docs',         /* Optional: 'docs' will save the file in the `Documents`
                                                        Default: Temp directory */

                            base64: true ,               


                            height: 800,                
                            width: 1056,               /* Optional: 1056 sets the width of the DOCUMENT that will produced
                                                        Default: 792
                                                      */
                            padding: 24,                /* Optional: 24 is the # of pixels between the outer paper edge and
                                                                corresponding content edge.  Example: width of 1056 - 2*padding
                                                                => content width of 1008
                                                        Default: 10
                                                      */
                      };

                      //const source = {uri:data.filePath};
                      this.setResultModalVisible(false);
                      console.log("options",options);
                      RNHTMLtoPDF.convert(options).then((data) => {

                        if( Platform.OS == 'ios' ){
                           
                            RNFetchBlob.ios.openDocument(data.filePath);

                          }else{
                           
                            this.props.navigation.navigate('ViewPDF',{source:data.filePath })
                            Alert.alert('Successfully downloaded in Documents' );
                          }
                        // this.props.navigation.navigate('ViewPDF',{source:data.filePath })
                        // Alert.alert('Successfully downloaded in Downloads' );
                      });
                          
                      
                    }).catch((error)=>{ Alert.alert(error)});

}





  getIndex(value, arr, prop) {
    console.log("get Index caLLED ")
    for(var i = 0; i < arr.length; i++) {
        if(arr[i][prop] === value) {
            return i;
        }
    }
    return -1; //to handle the case where the value doesn't exist
  }

  onFinishTimer(){

    if(this.state.timeCompleted){
      Alert.alert("Exam Submitted");
      this.setTestModalVisible(false);
      this.onSubmitChange();
    }
  
  }

  textOnFocus(){
    if(Platform.OS == 'ios'){
    this.setState({onFocusMargin:'40%'})
    }
  }
  
  textOnBlur(){
    if(Platform.OS == 'ios'){
      this.setState({onFocusMargin:'0%'})
    }
   
  }
  

  
  render() {
    const styleStart = '<div style="color:#150F68;font-weight: 700;">';
    const styleEnd   = '</div>';  

    return (
      <View style={{flex: 1,width:wp('100%')}}>

            <ScrollView>

            <View style={{ alignItems:'center', alignself:'center',}}>

                { this.state.status ? 

                <View style={{flex: 1, height:hp('400%'),width:wp('95%'),backgroundColor: '#FFFFFF',marginTop:'0%',}}>

                    { this.state.givenAllTest ? 
                        <View> 
                        <View style={{ height:hp('5%'), backgroundColor: '#3f4c6b', marginTop:'2%', justifyContent:'center', alignItems:'center'}}>
                        <Text  style={{fontSize: RF(3),color:'#FFFFFF', alignself:'center', marginLeft:wp('2%') }}>Instructions</Text>
                        </View>

                      <View style={{  backgroundColor: '#FFFFFF',marginTop:'5%'}}>
                          <Text  style={{fontSize: RF(2.5),color:'#000000', marginLeft:wp('2%') }}>{'\u2022'} All questions are compulsory.</Text>
                          <Text  style={{fontSize: RF(2.5),color:'#000000', marginLeft:wp('2%') }}>{'\u2022'} Number of questions: 20 Questions</Text>
                          <Text style={{fontSize: RF(2.5),color:'#000000', marginLeft:wp('2%') }}>{'\u2022'} Time allotted: 180 minutes + (30 minutes to upload) Make sure the scanned documents are legible to read with correct Test number and Question number written </Text> 
                          <Text  style={{fontSize: RF(2.5),color:'#000000', marginLeft:wp('2%') }}>{'\u2022'} Please do not refresh the page while you are taking Test.</Text>
                          <Text  style={{fontSize: RF(2.5),color:'#000000', marginLeft:wp('2%') }}>{'\u2022'} The answers must not exceed 150 words for 10 markers and 250 for 15 markers.</Text>
                          <Text  style={{fontSize: RF(2.5),color:'#000000', marginLeft:wp('2%') }}>{'\u2022'} We recommend you to write the answer for all the question in a model answer booklet. Scan it and upload the answer pdf in the upload answer option provided.</Text>
                          <Text style={{fontSize: RF(2.5),color:'#000000', marginLeft:wp('2%') }}>{'\u2022'} Upload file size limit: 2MB (Individual - 2MB) </Text>
                          <Text  style={{fontSize: RF(2.5),color:'#ff0000', marginLeft:wp('2%') }}>{'\u2022'} Please note you can attempt the test only one time</Text>
                          
                        </View>

                      <View style={{marginTop:'10%',flexDirection:'row',alignItems:'center',justifyContent:"center"}}>

                        
                          <TouchableOpacity style={{ height:hp('5%'),width:wp('27%'), backgroundColor:'#1ab395' , justifyContent:'center', alignItems:'center',marginLeft:'3%', marginBottom:'0%'}} 
                            onPress={() => {
                              //this.setTestModalVisible(true);
                              this.InsertMainsFullQuestions();
                              //this.getMainsFullQuestions();
                              //this.getMainFullReviews();
                              
                            }}
                            disabled={this.state.disableGS1}

                            >
                          <View style={{ alignItems:'center',backgroundColor: this.state.disableGS1 ? '#DCDCDC' : '#1ab395'}}>
                              <Text  style={{fontSize: RF(3),color:'#FFFFFF', alignself:'center',  }}>Take Test 1</Text>
                          </View>
                          </TouchableOpacity>
                        
                          <TouchableOpacity style={{ height:hp('5%'),width:wp('27%'), backgroundColor: '#1ab394', justifyContent:'center',marginLeft:'3%', alignItems:'center',  marginBottom:'0%'}} 
                            onPress={() => {//this.setTestModalVisible(true);
                              this.InsertMainsFullQuestions();
                              //this.getMainsFullQuestions();
                              //this.getMainFullReviews();

                            }}
                            disabled={this.state.disableGS2}>
                          <View style={{ alignItems:'center', backgroundColor: this.state.disableGS2 ? '#DCDCDC' : '#1ab395'}}>
                              <Text  style={{fontSize: RF(3),color:'#FFFFFF', alignself:'center',  }}>Take Test 2</Text>
                          </View>
                          </TouchableOpacity>
                        

                          <TouchableOpacity style={{ height:hp('5%'),width:wp('27%'), backgroundColor: '#1ab394',marginLeft:'3%',marginRight:'3%', justifyContent:'center', alignItems:'center',  marginBottom:'0%'}} 
                            onPress={() => {//this.setTestModalVisible(true);
                              this.InsertMainsFullQuestions();
                              //this.getMainsFullQuestions();
                              //this.getMainFullReviews();
                              
                            }}
                            disabled={this.state.disableGS3}>
                          <View style={{ alignItems:'center',backgroundColor: this.state.disableGS3 ? '#DCDCDC' : '#1ab395'}}>
                              <Text  style={{fontSize: RF(3),color:'#FFFFFF', alignself:'center',  }}>Take Test 3</Text>
                          </View>
                          </TouchableOpacity>
                      
                      </View>

                      <View style={{marginTop:'10%',flexDirection:'row',alignItems:'center',justifyContent:"center"}}>

                          <TouchableOpacity style={{ height:hp('5%'),width:wp('27%'), backgroundColor: '#1ab394', justifyContent:'center', alignItems:'center',marginLeft:'3%', marginBottom:'0%'}} 
                            onPress={() => {//this.setTestModalVisible(true);
                                this.InsertMainsFullQuestions();
                                //.getMainsFullQuestions();
                                //this.getMainFullReviews();
                              
                            }}
                            disabled={this.state.disableGS4}>
                          <View style={{ alignItems:'center',backgroundColor: this.state.disableGS4 ? '#DCDCDC' : '#1ab395'}}>
                              <Text  style={{fontSize: RF(3),color:'#FFFFFF', alignself:'center',  }}>Take Test4</Text>
                          </View>
                          </TouchableOpacity>
                        

                        
                          <TouchableOpacity style={{ height:hp('5%'),width:wp('27%'), backgroundColor: '#1ab394', justifyContent:'center',marginLeft:'3%',marginRight:'3%', alignItems:'center',  marginBottom:'0%'}} 
                            onPress={() => {//this.setTestModalVisible(true);
                              this.InsertMainsFullQuestions();
                              //this.getMainsFullQuestions();
                              //this.getMainFullReviews();
                              
                            }}
                            disabled={this.state.disableEssay}>
                          <View style={{ alignItems:'center',backgroundColor: this.state.disableEssay ? '#DCDCDC' : '#1ab395'}}>
                              <Text  style={{fontSize: RF(3),color:'#FFFFFF', alignself:'center',  }}>Take Test 5</Text>
                          </View>
                          </TouchableOpacity>
                      
                      </View>
                      </View>
                      :
                      <View/>

                    }    

                  {this.state.noOfMockMainsGiven > 0  ? 

                          <View style={{ alignItems:'center', alignself:'center',}}>
                          <FlatList
                            extraData={this.state}
                            data={this.state.mockmain_score}
                            renderItem={({item, index}) => 
                            <View style={{flex:1, width:wp('95%'), marginTop:hp('2%')}}>
                                <View style={{flex: 1, backgroundColor: '#1ab394',  justifyContent:'center', alignItems:'center'}} >
                                    <Text  style={{fontSize: RF(4),color:'#FFFFFF', alignself:'center' }}>Mains Full Syllabus {index + 1} Score</Text>
                                </View>

                                <View style={{flex: 4, backgroundColor: '#1f2f46', flexDirection:'column', marginTop:hp('1%')}} >
                                            <View style={{flex: 4,flexDirection: 'row',justifyContent: 'center',marginTop:hp('2%'),  height: '80%', marginBottom:hp('4%')}} >

                                            <View style={{width: wp('50%'),justifyContent:'space-between'}} >

                                                <View>
                                                        <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>Total Questions</Text>
                                                </View>

                                                <View>
                                                        <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>Attempted Question</Text>
                                                </View>

                                                <View>
                                                        <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>UnAttempted Question</Text>
                                                </View>

                                                <View>
                                                        <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }} >Total Marks</Text>
                                                </View>

                                                <View>
                                                        <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>Your Score</Text>
                                                </View>

                                                <View>
                                                        <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>Grade</Text>
                                                </View>
                                            </View>

                                            <View style={{width: wp('20%'),justifyContent:'space-between' }} >

                                                <View>
                                                        <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>{item.total_questions}</Text>
                                                </View>
                                                <View>
                                                        <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>{item.attempted_question}</Text>
                                                </View>
                                               
                                                <View>
                                                        <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>{item.unattempted_question}</Text>
                                                </View>

                                                <View>
                                                        <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>{item.total_marks}</Text>
                                                </View>

                                                <View>
                                                        <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>{item.obtained_marks}</Text>
                                                </View>

                                                <View>
                                                        <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>{item.grade}</Text>
                                                </View>
                                            </View>

                                      </View>

                                      <View style={{flex: 2,flexDirection: 'row',justifyContent: 'center',}} >
                                            <View style= {{backgroundColor:'#FFFFFF', width:wp('80%'), marginBottom: hp('1%'), justifyContent:'center'}}>
                                                <Text style={{color:'#e16800', fontSize: RF(2.5), alignself:'center',marginLeft:wp('2%') }}>Remark: {item.remarks}</Text>
                                            </View>
                                      </View>
                                      <View style={{flex: 2,flexDirection: 'row',justifyContent: 'center',}} >
                                            <View style= {{backgroundColor:'#FFFFFF', width:wp('80%'), marginBottom: hp('1%'), justifyContent:'center'}}>
                                                <Text style={{color:'#e16800', fontSize: RF(2.5), alignself:'center',marginLeft:wp('2%') }}>OverAllRemarks: {item.overallRemark}</Text>
                                            </View>
                                      </View>
                                </View>

                                <View style={{flex: 1, backgroundColor: '#FFFFFF',}} >
                                    <TouchableOpacity onPress={ 
                                      this.DownloadPDF.bind(this, index)}>
                                          <View style={{flexDirection: 'row', backgroundColor:'#1ab394', width:wp('80%'), marginLeft:wp('9%'), justifyContent:'center', borderRadius: wp('2%'), alignItems:'center', marginTop:hp('2%'),}}>
                                              <Text style={{color:'#FFFFFF',fontSize: RF(4), alignself:'center'}}>Download Solution</Text>
                                          </View>
                                      </TouchableOpacity>
                                </View>

                            </View>    
                          }/>
                          </View>
                    :
                    <View/>
                  }
                      
                  </View>
                :
                <View style={{ alignItems:'center', alignself:'center',}}>
                  <View style={{flex:1,height:hp('70%'), flexDirection:'column', backgroundColor:'#1ab394', width:wp('95%'), alignself:'center', marginTop:hp('5%')}}>
                      <Text style={styles.error}>{this.state.message}</Text>
                  </View>
                </View>
                }
                </View>


            </ScrollView>
            {this.state.isLoading ? <ActivityIndicator
                    animating     ={true}
                    transparent   ={true}
                    visible       ={false}
                    style         ={styles.indicator}
                    size          ="large"
                  /> : <View/>  } 
            <Quotes/> 
            <Modal
               animationType="slide"
               transparent={false}
               visible={this.state.testModalVisible}
               onRequestClose={() => {
                Alert.alert(
                  'Submit Exam',
                  'Do You Really want to Submit Your Exam!',
                  [
                    {text: 'Cancel', onPress: () => {}},
                    {text: 'OK', onPress: () => {   this.setTestModalVisible(false);
                      this.onSubmitChange();}},
                  ],
                  { cancelable: false }
                );}}
                >
                <View style={{flex: 1, width:wp('100%')}}>

                        <View style={{flex: .5, flexDirection:'row', marginTop:'2%'}}>
                              <View style={{flex: 5,flexDirection:'row', justifyContent:'center'}} >
                                    <CountDown
                                
                                      digitBgColor  = '#1bb496'
                                      digitTxtColor = '#FFFFFF'
                                      timeTxtColor  = '#000000'
                                      until={this.state.totalDuration}
                                      //duration of countdown in seconds
                                      timetoShow={('H', 'M', 'S')}
                                      //formate to show
                                      onFinish={() => {
                                                        this.onFinishTimer();
                                                    }}
                                      //on Finish call
                                      //onPress={() => alert('hello')}
                                      //on Press call
                                      size={20}
                                    />
                              </View>
                              
                        </View>

                        <View style={{flex:1, flexDirection:'row',marginLeft:wp('5%'),marginRight:wp('5%'), marginTop:'10%'}}>
                          <TouchableOpacity
                          onPress={() => {
                            this._uploadAllAnswerFile();
                          }}
                          >
                              <View style={{
                                            borderWidth:1,
                                            borderColor:'rgba(0,0,0,0.2)',
                                            alignItems:'center',
                                            justifyContent:'center',
                                            width:30,
                                            position: 'relative',                                          
                                            bottom: 5,                                                    
                                            right: 10,
                                            height:50,
                                            backgroundColor:'#fff',
                                            borderRadius:100,
                                          }}
                                        >   
                              <Icon name="upload"  size={20} color="#01a699" />
                              </View>
                          </TouchableOpacity>
                          <Text numberOfLines={4}> Upload Your All Answers File(Support Available for jpeg,jpg,png and pdf).File Size must be 20Mb only.</Text>
                        </View>
                        <View style={{flex: 5.5,marginTop:'5%'}}>
                          <ScrollView >
                          <View style={{ alignItems:'center', alignself:'center' ,marginBottom:hp(this.state.onFocusMargin)}}>
                                <FlatList
                                  extraData={this.state}
                                  data={this.state.questions}
                                  renderItem={({item, index}) => 

                                  <View style={{flex:1, flexDirection:'column',width:wp('95%'), justifyContent:'center', backgroundColor:'#FFFFFF',  borderWidth:2, borderColor:'#CCCCCC'}}>
                                      

                                      <View style={{flex:1, flexDirection:'column',marginLeft:wp('2%'), marginTop:hp('2%'), }}>
                                          <Text style={{marginTop:'3.5%', color:'#150F68'}}>Q. {this.state.nextPostion + index + 1 }. </Text>
                                          <HTML html={styleStart + item.question + styleEnd}  imagesMaxWidth={Dimensions.get('window').width} />
                                      </View>
                                    
                                          
                                      <View style={{flex:1, height:wp('30%'), flexDirection:'column',marginLeft:wp('2%'),borderWidth:1, borderColor:'#000000', borderRadius:wp('2%'), width:wp('90%'), justifyContent:'flex-start', alignself:'center'}}>

                                        <TextInput
                                            multiline={true}
                                            onFocus={this.textOnFocus.bind(this)}
                                            onBlur = {this.textOnBlur.bind(this)}
                                            underlineColorAndroid='transparent'
                                            textAlignVertical= "top"
                                            onChangeText={text => {
                                              let { textInputs } = this.state;
                                              textInputs[index] = text;
                                              this.setState({
                                                textInputs,
                                              });
                                            }}
                                            value={this.state.textInputs[index]}
                                        />

                                      </View>


                                      <View style={{flex:1, height:hp('5%'), flexDirection:'row',marginLeft:wp('2%'), marginTop:'5%',marginBottom:'5%'}}>
                                        <TouchableOpacity
                                          onPress={this.handleChange.bind(this,item)}>
                                            <Icon name="upload"  size={20} color="#01a699" />
                                        </TouchableOpacity>
                                        <Text > Only jpeg,jpg,png and pdf allowed. Max size: 2MB</Text>
                                      </View>
                                      {item.answer_upload !='' ?
                                      <View style={{ height:hp('5%'),  marginLeft:wp('5%'), marginTop:'5%',marginBottom:'5%'}}>
                                        <Text style={{marginTop:'3.5%', color:'red'}}>Answer Uploaded Successfully!</Text>
                                      </View>  
                                    :
                                      <View/>
                                    }
                                  
                                  </View>
                                }
                                />
                          </View>
                          </ScrollView>
                        </View>
                        <View style={{flex: .2, flexDirection:'row', marginTop:'0%', alignItems:'center', justifyContent:'space-between', }}>
                              
                              { this.state.nextPostion > '4' ?
                                <View style={{flex: 2,}} >
                                    <TouchableOpacity
                                      onPress={() => {
                                        this.onNext("previous");
                                      }}>

                                      <View style={{marginLeft:5,alignItems:'center',  backgroundColor:'#1ab395', height: hp('5%')}}>    
                                        <Text textAlign={'center'} style={{fontSize: RF(2.5), fontWeight:'bold', alignself:'center', color:'#FFFFFF'}}>PREVIOUS</Text>
                                      </View>
                                    </TouchableOpacity>
                                </View>
                                :
                                <View/>
                              }
                              
                              { (this.state.nextPostion < this.state.totalQuestionCount && this.state.nextPostion + 5 != this.state.totalQuestionCount) ?
                            
                                <View style={{flex: 2, }} >
                                      <TouchableOpacity
                                        onPress={() => {
                                          this.onNext("next");
                                        }}>

                                        <View style={{marginLeft:5,marginRight:5, alignItems:'center', backgroundColor:'#1ab395', height: hp('5%')}}>
                                          <Text textAlign={'center'} style={{fontSize: RF(2.5), fontWeight:'bold', alignself:'center',color:'#FFFFFF'}}>NEXT</Text>
                                        </View>
                                      </TouchableOpacity>
                                </View>
                              :
                                <View/>
                              }

                              { this.state.nextPostion == this.state.totalQuestionCount - 5 ?
                            
                                <View style={{flex: 2, }} >
                                      <TouchableOpacity
                                        onPress={() => {
                                        
                                          this.setTestModalVisible(false);
                                            this.onSubmitChange();
                                        }}>

                                        <View style={{marginLeft:5,marginRight:5, alignItems:'center', backgroundColor:'#1ab395', height: hp('5%')}}>
                                          <Text textAlign={'center'} style={{fontSize: RF(2.5), fontWeight:'bold', alignself:'center',color:'#FFFFFF'}}>SUBMIT</Text>
                                        </View>
                                      </TouchableOpacity>
                                </View>
                              :
                                <View/>
                              }
                        </View>
                       
                        {this.state.isModalLoading ? <ActivityIndicator
                            animating     ={true}
                            transparent   ={true}
                            visible       ={false}
                            style         ={styles.indicator}
                            size          ="large"
                          /> : <View/>  } 
                </View>  
            </Modal>


        <Modal
        animationType="slide"
        transparent={false}
        visible={this.state.resultModalVisible}
        onRequestClose={() => {
          this.setResultModalVisible(false);
        }}
        >
                  <View style={{ height:hp('5%'), backgroundColor: '#1ab395', marginTop:'1%',marginLeft:'5%',marginRight:'5%', justifyContent:'center', alignItems:'center'}}>
                    <Text  style={{fontSize: RF(3),color:'#FFFFFF', alignself:'center', marginLeft:wp('2%') }}>Main Full Syllabus Test {this.state.last_result_count} Score</Text>
                  </View>
                
                    <View style={{flex: 1,alignSelf: 'flex-end', position: 'absolute',marginTop:'12%',backgroundColor:'#FFFFFF'}} >
                            <TouchableOpacity
                              onPress={() => {
                                this.setResultModalVisible(false);
                              }}>

                              <View style={{
                                      borderWidth:1,
                                      borderColor:'rgba(0,0,0,0.2)',
                                      alignItems:'center',
                                      justifyContent:'center',
                                      width:50,
                                      position: 'relative',                                          
                                      bottom: 0,                                                    
                                      right: 10,
                                      height:50,
                                      backgroundColor:'#fff',
                                      borderRadius:100,
                                    }}
                                >    
                                <Icon name="times-circle"  size={30} color="red" />
                              
                              </View>
                            </TouchableOpacity>
                      </View>

                      <View style={{flex: 4, backgroundColor: '#1f2f46', flexDirection:'column', marginTop:hp('15%'),marginRight:'5%',marginLeft:'5%'}} >
                        <View style={{flex: 4,flexDirection: 'row',justifyContent: 'center',marginTop:hp('2%'),  height: '80%', marginBottom:hp('4%')}} >

                          <View style={{width: wp('50%'),justifyContent:'space-between'}} >

                              <View>
                                      <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>Total Questions</Text>
                              </View>

                              <View>
                                      <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>Attempted </Text>
                              </View>

                              <View>
                                      <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>UnAttempted </Text>
                              </View>

                              <View>
                                      <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }} >Total Marks</Text>
                              </View>

                              <View>
                                      <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>Your Score</Text>
                              </View>

                              <View>
                                      <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>Grade</Text>
                              </View>
                          </View>

                          <View style={{width: wp('20%'),justifyContent:'space-between' }} >

                              <View>
                                      <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>{this.state.latest_result.total_questions}</Text>
                              </View>

                              <View>
                                      <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>{this.state.latest_result.attempted_question}</Text>
                              </View>

                              <View>
                                      <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>{this.state.latest_result.unattempted_question}</Text>
                              </View>

                              <View>
                                      <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>{this.state.latest_result.total_marks}</Text>
                              </View>

                              <View>
                                      <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>{this.state.latest_result.obtained_marks}</Text>
                              </View>

                              <View>
                                      <Text style={{fontSize: RF(2.5),color:'#FFFFFF', }}>{this.state.latest_result.grade}</Text>
                              </View>
                          </View>

                        </View>

                        <View style={{flex: 2,flexDirection: 'row',justifyContent: 'center',}} >
                              <View style= {{backgroundColor:'#FFFFFF', width:wp('80%'), marginBottom: hp('1%'), justifyContent:'center'}}>
                                  <Text style={{color:'#e16800', fontSize: RF(2.5), alignself:'center',marginLeft:wp('2%') }}>Remark: {this.state.latest_result.remarks}</Text>
                              </View>
                        </View>
                        <View style={{flex: 2,flexDirection: 'row',justifyContent: 'center',}} >
                              <View style= {{backgroundColor:'#FFFFFF', width:wp('80%'), marginBottom: hp('1%'), justifyContent:'center'}}>
                                  <Text style={{color:'#e16800', fontSize: RF(2.5), alignself:'center',marginLeft:wp('2%') }}>Overall Remark: {this.state.latest_result.overallRemark}</Text>
                              </View>
                        </View>
                      </View>

                      <View style={{flex: 1, backgroundColor: '#FFFFFF',}} >
                          <TouchableOpacity onPress={
                            this.DownloadPDFModal.bind(this,this.state.last_result_count)
                            }>
                                <View style={{flexDirection: 'row', backgroundColor:'#1ab394', width:wp('80%'), height:hp('10%'), marginLeft:wp('9%'), justifyContent:'center', borderRadius: wp('2%'), alignItems:'center', marginTop:hp('2%'),}}>
                                    <Text style={{color:'#FFFFFF',fontSize: RF(4), alignself:'center'}}>Download Solution</Text>
                                </View>
                            </TouchableOpacity>
                      </View>
                      {this.state.isresultLoading ? <ActivityIndicator
                      animating     ={true}
                      transparent   ={true}
                      visible       ={false}
                      style         ={styles.indicator}
                      size          ="large"
                    /> : <View/>  } 
                    
              </Modal>
 
      </View>
    );
  }
}

