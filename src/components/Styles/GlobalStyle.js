
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  PixelRatio,
  TouchableOpacity,
  Image,
  Alert,
  Dimensions
} from 'react-native';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import RF from "react-native-responsive-fontsize";



export default StyleSheet.create({
  container: {
    flex: 1,
  },
  flex_column: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  flex_column_student: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    marginTop : '20%'
  },
  flex_column_login: {
    //flex: 1,
    //flexDirection: 'column',
    //justifyContent: 'space-between',
    flex:1,width: wp('100%'),
  },
  logo_view: {
    flex:1,width: '100%', height: 150,
  },
  flex_column_space: {
    flex: 1,flexDirection: 'row',justifyContent: 'space-between', width: '100%',
  },
  flex_row : {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  TopTitle :{color:'#FFFFFF',fontSize: RF(3), alignSelf:'center'},
  separator: {
    height: 0.5,
    width: "100%",
    alignSelf: 'center',
    backgroundColor: "#555"
  },
    pdf: {
        flex:1,
        width:Dimensions.get('window').width,
    },
    indicator: {
      alignItems: 'center',
      height: '100%',
      position: 'absolute',
      backgroundColor: 'rgba(192,192,192,0.3)',
      width: '100%'
    },
    error: {fontSize:RF(2.5), color:'#FFFFFF', alignItems:'center',alignSelf:'center', marginTop:hp('2%')},
    Show: {fontSize:RF(3), color:'#000000', alignItems:'center',alignSelf:'center', marginLeft:10},
    Showheader: {fontSize:RF(3.5), color:'#1ab394', alignItems:'center',alignSelf:'center',marginTop:hp('2%'), fontWeight:'bold'},
});
