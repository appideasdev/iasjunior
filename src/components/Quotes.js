import React, { Component } from 'react'
import { StyleSheet, ScrollView, View, Text,
AsyncStorage
 } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'

import GlobalVariables from './GlobalVariables/GlobalVariables.js';


import BottomNavigation, { FullTab } from 'react-native-material-bottom-navigation'
import MarqueeText from 'react-native-marquee';

import RF from "react-native-responsive-fontsize";
export default class Quotes extends Component{
    
    constructor(props){
      super(props);
      this.state = {}
    }


   componentDidMount = async () => {

    var GVar = GlobalVariables.getApiUrl();

    
    AsyncStorage.multiGet(["TOKEN", "student_id"]).then(response => {

            var TOKEN                          = response[0][1];
            var student_id                     = response[1][1];
           
            this.setState({
                            TOKEN          : TOKEN,    
                            student_id     : student_id,
                      });


            let formdata = new FormData();

            formdata.append('student_id',        student_id );

            console.log(formdata,"formdata");
            console.log(TOKEN,"TOKEN");

            fetch( GVar.BaseUrl + GVar.Quotes ,{
                      method: 'POST',
                      headers:{
                                  'token' : TOKEN
                              },
                      body: formdata,       
                      }).then((response) => response.text())
                     .then((responseJson) => {

                      var x = JSON.parse(responseJson.replace("-", ""));
                      this.setState({isLoading : false });
                      

                      console.log(x,"Quotes")
                      if( x.status == 1 ){

                          var str = x.quotes.quotesName
                          this.setState({quotes :  str.replace(/\\/g, '') });
                      }else{

                        this.setState({quotes : null });
                      }
                      
                     }).catch((error)=>{ Alert.alert(error)});

      });  

     
  }

   tabs = [
    {
      key: 'games',
      icon: 'gamepad-variant',
      label: 'Games',
      barColor: '#1f2f46',
      //pressColor: 'rgba(255, 255, 255, 0.16)',
    },
  ]

  renderIcon = icon => ({ isActive  }) => (

    <View style={{flex: 1,justifyContent: 'center',}}>
        <Text style={{fontSize:RF(2.5), fontWeight:'bold', color:'#FFFFFF',alignSelf: 'center'}}>Today's Quote</Text>
        <MarqueeText
          style={{ fontSize:RF(2.5),color:'#FFFFFF'}}
          duration={3000}
          marqueeOnStart
          loop
          marqueeDelay={1000}
          marqueeResetDelay={1000}
        >
           {this.state.quotes}
        </MarqueeText>
    </View>          
  )

  

  renderTab = ({ tab, isActive }) => (
    <FullTab
      isActive={isActive}
      key={tab.key}
      renderIcon={this.renderIcon(tab.icon)}
    />
  )
   

  render(){
    let { name } = this.props;

    return (
      <BottomNavigation
        onTabPress={newTab => this.setState({ activeTab: newTab.key })}
        renderTab={this.renderTab}
        tabs={this.tabs}
      /> 
    );
  }
}
