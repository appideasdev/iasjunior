import React, { Component } from 'react';


import { Router, Scene } from 'react-native-router-flux'

import Index from './src/components/Index.js'


const Routes = () => (
   <Router>
      <Scene key = "root">
        
         <Scene key = "Index" component = {Index} title = "Index"  initial = {true}/>
         
         </Scene>
   </Router>
)
export default Routes

